#!/bin/bash

# test slurm submission for simulation
# 1/23/2020

# RESOURCES #
#SBATCH --nodes=1
#SBATCH --ntasks=32
#SBATCH --mem=8G

# WALLTIME #
#SBATCH -t 40:00:00

# QUEUE #
#SBATCH -p normal_q

# ALLOCATION #
#SBATCH -A CPMot

# MODULES #
module purge

# modules need to load ffmpeg
module load gcc/5.2.0
module load yasm/1.3
module load x264/1.0
module load fdk-aac/1.0
module load lame/3.99.5
module load ffmpeg/2.5.4

module load Anaconda

cd $SLURM_SUBMIT_DIR

MAXSTEPS=(1e-4 1e-5)
for maxstep in "${MAXSTEPS[@]}"
do
    date
    echo "MAX_STEP =" ${maxstep}
    python exponentialGrowth.py ${maxstep}
done

exit;

