import csv
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# formats figures with larger fonts
plt.rcParams.update({'font.size': 16})
plt.rcParams['text.usetex'] = True

break_checks = []
with open("output.dir/crit_stress_check_buckling.csv") as f :
    csv_reader = csv.reader(f, delimiter=',')
    for row in csv_reader :
        vals = np.array( [float(x) for x in row] )
        break_checks.append( vals )
break_checks = np.array( break_checks )

param_vals = [1,2,5,10,20,50,100]
ax = sns.heatmap(break_checks, vmin=0, vmax=1, xticklabels=param_vals, yticklabels=param_vals, cmap='coolwarm', cbar=False)

#plt.text(10, 2, "Chain breaks")
#plt.text(1, 100 , "Chain survives")

plt.title("Does the stress exceed the crtical values?", fontsize=20)
ax.set_xlabel(r"$a$",fontsize=20)
ax.set_ylabel(r"$\Psi$", rotation=0, fontsize=20)
plt.xticks(fontsize=16)
plt.yticks(rotation=0, fontsize=16)

ax.invert_yaxis()
ax.set_aspect('equal')

plt.savefig("output.dir/breakage_heatmap.pdf")
