Rigid Rod Model for Clostridium Perfrigens
Run 10: Lateral Interaction Implementation


__________________________________________________________________________________

SIMULATION
__________________________________________________________________________________

V1: a rough version where nothing to important was implemented

V2: a complete overhaul of the simluation -- introduced a
    class for Filaments, Binding Sites, and Bonds
    all the major dynamics are handled in these classes;
    the main function basically organizes the calls
    This was the first attempt at lateral interaction
    implementation by converting torques and forces to
    qdots but it did not work

V3: the same framework used in V2 except now lateral interactions
    are implemented using Lagrangian mechanics

V4: introduced randomness in the binding sites and introduce cell division
    FilamentV4.py is the best working version of the Filament related classes

V4_paramTest: parameter testing with the working V4 model

V5: uses Lennard-Jones potential instead of Hookean spring force;
    V_LJ = epsilon [ (rm/r)^12  - 2 (rm/r)^6 ]

V6: use Euler integration
V6B: uses Euler integration with data output for stiffness check
Note: both V6 and V6B still use FilamentV5

V5H: a hybrid of adaptive time stepping and Euler Integration
     Uses a adaptive time stepping to stabilize the system initially
     Switches to Euler integration after a predefined period of adaptive stepping



__________________________________________________________________________________

ANALYSIS
__________________________________________________________________________________
