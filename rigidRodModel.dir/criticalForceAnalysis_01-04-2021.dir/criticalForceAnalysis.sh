 #!/bin/bash

echo "RUNNING"

#KBS=(1e-7)
#KBS=(1e-8 2e-8 5e-8 1e-7 2e-7 5e-7 1e-6)
#KBS=(1e-8 2e-8 3e-8 4e-8 5e-8 6e-8 7e-8 8e-8 9e-8 1e-7)
#KBS=(1e-9 2.5e-9 5e-9 7.5e-9 1e-8 2.5e-8 5e-8 7.5e-8 1e-7)
KBS=(0.1)
#KBS=(1e-25)

#MUS=(1e-9)
#MUS=(1e-12 1e-11 1e-10 1e-9 1e-8 1e-7 1e-6)
#MUS=(7e-10 8e-10 9e-10 1e-9 2e-9 3e-9 4e-9)
#MUS=(5e-6 6e-6 7e-6 8e-6 9e-6 10e-6 11e-6 12e-6 13e-6 14e-6 15e-6)
MUS=(1e-5)

RATES=(0.1)
#RATES=(0.0333 0.05 0.1 0.2 0.3)

ANGLES=(2)
#ANGLES=(1 1.5 2 2.5 3 3.5 4 4.5 5)

QS=(0.5)
#QS=(0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5)

AS=(100)
#AS=(25 50 75 100 125 150 175 200)

#BS=(0.1)
BS=(0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1)

for q in "${QS[@]}"
do
    echo "NEW Q (LOCATION)!"
    for angle in "${ANGLES[@]}"
    do
        echo "NEW ANGLE!"
        for kb in "${KBS[@]}"
        do
            echo "NEW KB!"
            for mu in "${MUS[@]}"
            do
                echo "NEW MU!"
                for rate in "${RATES[@]}"
                do
                    echo "NEW RATE!"
                    for a in "${AS[@]}"
                    do
                        echo "NEW A!"
                        for b in "${BS[@]}"
                        do
                            echo "NEW B!"
                            echo "q =" $q
                            echo "angle =" $angle
                            echo "rate =" $rate
                            echo "mu =" $mu
                            echo "kb =" $kb
                            echo "a =" $a
                            echo "b =" $b
                            flag="true"
                            numrods=20
                            maxnumrods=40
                            #python -c "import pickle; pickle.dump([], open('data.dir/tempData.pkl', 'wb'))"
                            #mv data.dir/tempData.pkl data.dir/data_kb-mu-ratio${kb_mu_ratio}_growthRate${rate}_angle${angle}.pkl
                            while [ $numrods -le $maxnumrods ]
                            do
                                echo $numrods
                                python criticalForceCalculations.py $kb $mu $a $b $rate $numrods $angle $q
                                numrods=$(( numrods + 2 ))
                            done
                            duration=$SECONDS
                            echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
                        done
                    done
                done
            done
        done
    done
done

#python criticalForcePlotting.py data*.pkl
mv data*.pkl data.dir

echo "SIMULATIONS COMPLETE!"
