#import pdb
import numpy as np
import scipy as sp
from scipy import signal
from scipy import integrate
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import optimize
from scipy.interpolate import interp1d
from scipy import interpolate
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys
import pandas as pd
import seaborn as sns

plt.style.use('./mystyle.mplstyle')

# import the predicted critical kinking length
data = []
with open(str(sys.argv[1])) as f :
    csv_reader = csv.reader(f, delimiter=',')
    for row in csv_reader :
        row = [ float(x) for x in row ]     # convert the values form strings to floats
        row = np.array(row)                 # convert the list to a numpy array
        data.append( row )
data = np.array(data)

# upack the parameters
psi, a, l0, r, kb, mu, nu, epsilon, numRods0, angle, rel_tol, abs_tol, seed_val = tuple(data[0])
states = data[1:]

# determine the number of cells in the system based on the length of the state array
numRods = lambda state : int( (len(state) - 3) // 4 )

# sort the state array values
t = np.array([state[0] for state in states])
ls = np.array([state[1] for state in states])
x0 = np.array([state[2] for state in states])
y0 = np.array([state[3] for state in states])
thetas = np.array([np.array(state[4:numRods(state)+4]) for state in states])
x0dots = np.array([state[numRods(state)+4] for state in states])
y0dots = np.array([state[numRods(state)+5] for state in states])
thetadots = np.array([np.array(state[numRods(state)+6:2*numRods(state)+6]) for state in states])
Fxs = np.array([np.array(state[2*numRods(state)+6:3*numRods(state)+5]) for state in states])
Fys = np.array([np.array(state[3*numRods(state)+5:]) for state in states])

#for time, theta in zip(t, thetas) :
#    print(time, [ (theta[i+1]-theta[i])*180/np.pi for i in range(len(theta)-1) ])

# insert zeros into the force arrays
Fxs = np.array([np.insert(Fx, [0, len(Fx)], [0,0]) for Fx in Fxs])
Fys = np.array([np.insert(Fy, [0, len(Fy)], [0,0]) for Fy in Fys])

Fmags = [ np.sqrt(Fxs[i]**2 + Fys[i]**2) for i in range(len(Fxs)) ]

r = 0.02
stress_straight = lambda t : numRods0**2 * np.exp( 2*r*t ) / 8
center_stress = np.array( [Fmag[len(Fmag)//2] for Fmag in Fmags] ) / (mu * l0**2 * r)

# import the predicted critical kinking length
kp = []
with open("fitParams.csv") as f :
    csv_reader = csv.reader(f, delimiter=',')
    for row in csv_reader : kp.append(row)
kp = kp[1:]     # remove the header row
temp = list(filter(lambda x : ( round(float(x[0])) == round(a) ), kp))
m, z = float(temp[0][2]), float(temp[0][1])
crit_stress_nd = m * psi + z

with open('stress_check.dir/out.txt', 'w') as f:
    for i in range(len(t)) :
        print(f't = {t[i]} \t F = {center_stress[i]}', file=f)

"""for i in range(len(t)) :

    Fx = Fxs[i]
    zeta = np.linspace(-1, 1, endpoint=True, num=len(Fx))
    plt.plot(zeta, Fx)
    plt.title(f"t = {t[i]}")
    plt.savefig(f"stress_check.dir/stressX.dir/stressX_{round(t[i])}_{i}.pdf")
    plt.clf()
    plt.close()"""

#for time, cs, ss in zip(t, center_stress, stress_straight(t) ) : print(time,cs, ss)
print(center_stress)

# plot the magnitude of the stress in the center linkage
plt.title(rf"Center linkage stress ($\Psi$ = 100, $a$ = 1)")
plt.xlabel("Time (min)")
plt.ylabel("Stress (non-dimensional)")
plt.plot(t, center_stress, '-', label="Simulation", zorder=1)
#plt.plot(t, max_stress_nd, '-', label="Simulation max")
t2 = np.linspace(t[0], t[-1], num=1000, endpoint=True)
plt.plot(t2, stress_straight(t2), '-', label="Straight chain", zorder=0)
plt.hlines(crit_stress_nd, xmin=t[0], xmax=t[-1], colors='k', linestyles='dashed', label="Critical breaking stress", zorder=2)
plt.legend(loc='best')

plt.savefig("stress_check.dir/center_stress.pdf")
plt.gcf().clear()
plt.close()


"""plt.plot(t, center_stress)
#plt.plot(t, stress_straight(t) )
plt.savefig("stress_check.dir/center_stress.pdf")
plt.gcf().clear(), plt.close()"""

# CONFIGURATION

minX, maxX, minY, maxY = 1E100, 0, 1E100, 0
nodeX, nodeY = [], []

for i in range( len(t) ) :
    # calculate node positions
    theta = thetas[i]

    xNodes = [ x0[i] - ls[i]/2 * np.cos( theta[0] ) ]
    yNodes = [ y0[i] - ls[i]/2 * np.sin( theta[0] ) ]
    for j in range( len(theta) ) :
        xNodes.append( xNodes[-1] + ls[i] * np.cos(theta[j]) )
        yNodes.append( yNodes[-1] + ls[i] * np.sin(theta[j]) )
        if min(xNodes) < minX : minX = min(xNodes)
        if max(xNodes) > maxX : maxX = max(xNodes)
        if min(yNodes) < minY : minY = min(yNodes)
        if max(yNodes) > maxY : maxY = max(yNodes)
    nodeX.append(xNodes)
    nodeY.append(yNodes)


minAx = round( min( [minX, minY] ) - 5 )
maxAx = round( max( [maxX, maxY] ) + 5 )

plt.rcParams['savefig.bbox'] = 'standard'
plt.rcParams['lines.linewidth'] = 3
plt.rcParams['lines.markersize'] = 8

fig = plt.figure(1)
l1, = plt.plot([], [], '-bo')       # filament 1 nodes / cells

plt.axis('scaled')

plt.xlabel(r"$x$ coordinate ($\mathrm{\mu}$m)", fontsize=16)
plt.ylabel(r"$y$ coordinate ($\mathrm{\mu}$m)", fontsize=16)

plt.xlim(minAx, maxAx)
plt.ylim(minAx, maxAx)

plt.xticks( plt.yticks()[0][1:-1] )

framesPerSec = 10
#framesPerSec = round(len(data)/data[-1][0]*10)
#print("fps =", framesPerSec)

# sets up video output stuff
FFMpegWriter = manimation.writers['ffmpeg']
metadata = dict(title='Movie Test', artist='Matplotlib',
                comment='Movie support!')
writer = FFMpegWriter(fps=framesPerSec, metadata=metadata)

outfile = f"stress_check.dir/configuration.mp4"
#print(outfile)
count = 0

with writer.saving(fig, outfile, 100):
# third arugment is dpi --> controls resolution and size of the plot

    for i in range(0, len(t), 1) :
        print(i)
        plt.title(rf"Chain Configuration at t = {str(round(t[i]))} min ($\Psi$ = {round(psi)}, $a$ = {round(a)})", fontsize=16)
        l1.set_data(nodeX[i], nodeY[i])

        writer.grab_frame()

        # use the next line to save indivdual frames if desired
        #if count == 0 or int(round(t[i],0)) == 90 :
        #    plt.savefig('plots.dir/configuration' + str(round(t[i],0))+'.pdf', format='pdf')

        count+=1

plt.gcf().clear(), plt.close()
