#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys

param_key = sys.argv[-1]    # string representing the parameter that is varied
plot_titles = {
    "kb" : "Varied Angular Spring Constant",
    "mu" : "Varied Parallel Drag Per Unit Length",
    "growthRate" : "Varied Growth Rate",
    "angle" : "Varied Pertubation Angle",
    "q" : "Varied Location"
}

plt.title( plot_titles[param_key] )

plt.axhline(y=0, color='black', linestyle='-')

for i in range(1, len(sys.argv)-1) :

    outfile = os.path.basename(sys.argv[i])
    filename = sys.argv[i]

    data = pickle.load(open(filename, "rb"))

    kb, mu, growthRate, numRods, angle, pert_coefficient = data[0]
    data = np.array(data[1:])

    paramstrs = {
        "kb" : r"$k_b$ = " + str(kb),
        "mu" : r"$\mu$ = " + str(mu),
        "growthRate" : r"$G_r$ = " + str(growthRate),
        "angle" : r"$\theta$ = " + str(angle),
        "q" : r"$q$ = " + str(pert_coefficient)
    }
    paramstr = paramstrs[param_key]

    l0 = 5
    lengths, thetadotDiffs = data[:,0], data[:,1]
    stress = 0.5 * mu * lengths**2 * (growthRate/l0) * pert_coefficient * (1-pert_coefficient)

    plt.xlabel(r"Stress ($kg\mu$m/min$^2$)")
    plt.ylabel("Rate of Change of Pertubation Angle (radians / min)")
    plt.plot(stress, thetadotDiffs, '-o', label=paramstr)

plt.legend(loc="upper right")
plt.savefig('plots.dir/' + outfile[:-4] + '.pdf', format='pdf')
