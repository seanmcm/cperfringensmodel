#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import optimize
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys
import pandas as pd


output = sys.argv[1]

f = open('stressVsBending.pkl', 'rb')
stress_df, bending_dfs, breakAngles = pickle.load(f)
f.close()

for bending_df, breakAngle in zip(bending_dfs, breakAngles) :

    # Compare first passage times to breaking conditions

    fig, ax = plt.subplots()

    for psi in [1, 2, 5, 10, 20, 50, 100] :

        stress_df_sub = stress_df[ stress_df['psi'] == psi ]
        bending_df_sub = bending_df[ bending_df['psi'] == psi ]

        aVals = list(stress_df_sub["a"])

        annotations = [f"{aVals[i]}" for i in range(len(aVals))]

        stressFPT = list(stress_df_sub["breakFPT"])
        bendingFPT = list(bending_df_sub["breakFPT"])

        plt.title(rf"FPT to breaks ($\Delta\theta_c$ = {breakAngle}$^\circ$)")
        #ax.scatter(bendingFPT, stressFPT, label=rf'$\Psi$ = {psi}')
        ax.scatter(stressFPT, bendingFPT, label=rf'$\Psi$ = {psi}')
        plt.xlabel("FPT to stress determined breaks (mins)")
        plt.ylabel("FPT to bending determined breaks (mins)")

        for i, label in enumerate(annotations):
            #plt.annotate(label, (bendingFPT[i], stressFPT[i]), fontsize=6)
            plt.annotate(label, (stressFPT[i], bendingFPT[i]), fontsize=6)

    lims = [
        np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
        np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
    ]
    # now plot both limits against eachother
    ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
    ax.set_aspect('equal')
    ax.set_xlim(lims)
    ax.set_ylim(lims)

    plt.legend(loc="upper left")

    plt.savefig(f"{output}/stressVsBending/fptComp_deltaTheta{breakAngle}.pdf")
    plt.close()


    # compare stresses when breaking due to bending with analytic critical stress

    fig, ax = plt.subplots()

    for psi in [1, 2, 5, 10, 20, 50, 100] :

        stress_df_sub = stress_df[ stress_df['psi'] == psi ]
        bending_df_sub = bending_df[ bending_df['psi'] == psi ]

        aVals = list(stress_df_sub["a"])

        annotations = [f"{aVals[i]}" for i in range(len(aVals))]

        critStress = list(stress_df_sub["critStress"])
        bendingStress = list(bending_df_sub["breakStress"])

        plt.title(rf"Breaking stresses ($\Delta\theta_c$ = {breakAngle}$^\circ$)")
        ax.scatter(critStress, bendingStress, label=rf'$\Psi$ = {psi}')
        plt.xlabel("Critical stress")
        plt.ylabel(rf"Breaking stress for $\Delta\theta$ > $\Delta\theta_c$")

        for i, label in enumerate(annotations):
            plt.annotate(label, (critStress[i], bendingStress[i]), fontsize=6)

    lims = [
        np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
        np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
    ]
    # now plot both limits against eachother
    ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
    ax.set_aspect('equal')
    ax.set_xlim(lims)
    ax.set_ylim(lims)

    plt.legend(loc="upper left")

    plt.savefig(f"{output}/stressVsBending/stressComp_deltaTheta{breakAngle}.pdf")
    plt.close()
