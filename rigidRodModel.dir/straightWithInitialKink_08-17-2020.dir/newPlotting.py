#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys


####################################################################################################
# Filament Dynamics Functions
####################################################################################################

def length(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return l0 * np.exp( (growthRate/l0) * (t - lastAddTime) )


def growthRateFunc(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return growthRate * np.exp( (growthRate/l0) * (t - lastAddTime) )

#---------------------------------------------------------------------------------------------------

def growthDynamics(t, q, lt, kb, mu, nu, epsilon, growthRate) :
    """ updates the qdots for the filament based only on growth dynamics
            l (array) : lengths of the cells in the Filaments
            lp (array) : growth rates for each cell
            kb (float) : angular spring constant
            mu (float) : parallel drag coefficient
            nu (float) : perpendiular drag coefficient
            epsilion (float) : rotational drag coefficient """

    l = [lt] * ( len(q) - 2 )
    lp = [ growthRate ] * ( len(q) - 2 )

    x0 = q[0]
    y0 = q[1]
    thetas = q[2:]

    numRods = len(thetas)
    numNodes = numRods + 1

    #totalLength = sum(l)

    #--------------------------------------------------------

    xEqns, yEqns, thetaEqns = [], [], []

    # x_i Euler-Lagrange equations --------------------------------------------------------

    # initialize x_0 E-L equation and x E_L equation array
    xEqn0 = np.zeros(3*numRods)
    xEqn0[0] = ( mu * np.cos(thetas[0])**2 + nu * np.sin(thetas[0])**2 ) * l[0]
    xEqn0[1] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    if numRods != 1 : xEqn0[numRods + 2] = -1
    xEqns.append(xEqn0)

    # initialize y_0 E-L equation and y E_L equation array
    yEqn0 = np.zeros(3*numRods)
    yEqn0[0] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    yEqn0[1] = ( mu * np.sin(thetas[0])**2 + nu * np.cos(thetas[0])**2 ) * l[0]
    if numRods != 1 : yEqn0[2*numRods + 1] = -1
    yEqns.append(yEqn0)

    # initialize theta_0 E-L equation and theta E_L equation array
    thetaEqn0 = np.zeros(3*numRods)
    thetaEqn0[2] = epsilon * l[0]**3
    if numRods != 1 :
        thetaEqn0[numRods + 2] = 0.5 * l[0] * np.sin(thetas[0])
        thetaEqn0[2*numRods + 1] = -0.5 * l[0] * np.cos(thetas[0])
    thetaEqns.append(thetaEqn0)

    #generate the 2 through N-1 x, y, and theta E-L equations
    for i in range(1, numRods) :

        # x_i E_L equations -----------------------------------------------------------

        xEqni = np.zeros(3*numRods)

        xEqni[0] = ( mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[i]

        xEqni[1] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * np.cos(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[0] * l[i] * np.sin(thetas[0]) )

        for j in range(1, i) :
            xEqni[j+2] = (mu - nu) * l[j] * l[i] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[j] * l[i] * np.sin(thetas[j])

        xEqni[i+2] = -0.5 * nu * l[i]**2 * ( np.sin(thetas[i]) * np.cos(thetas[i])**2 + np.sin(thetas[i])**3 )

        xEqni[i + numRods + 1] = 1

        if i != (numRods-1) :
            xEqni[i + numRods + 2] = -1

        xEqns.append(xEqni)


        # y_i E_L equations -----------------------------------------------------------

        yEqni = np.zeros(3*numRods)

        yEqni[0] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        yEqni[1] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[i]

        yEqni[2] = 0.5 * ( (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[0] * l[i] * np.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * np.sin(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) )

        for j in range(1, i) :
            yEqni[j+2] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[j] * l[i] * np.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])

        yEqni[i+2] = 0.5 * nu * l[i]**2 * (np.cos(thetas[i]) * np.sin(thetas[i])**2 + np.cos(thetas[i])**3)

        yEqni[i + (2*numRods)] = 1

        if i != (numRods-1) :
            yEqni[i + (2*numRods) + 1] = -1

        yEqns.append(yEqni)


        # theta_i E_L equations -----------------------------------------------------------

        thetaEqni = np.zeros(3*numRods)

        thetaEqni[i+2] = epsilon * l[i]**3

        thetaEqni[i + numRods + 1] = 0.5 * l[i] * np.sin(thetas[i])

        thetaEqni[i + (2*numRods)] = -0.5 * l[i] * np.cos(thetas[i])

        if i != (numRods-1) :
            thetaEqni[i + numRods + 2] = 0.5 * l[i] * np.sin(thetas[i])
            thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * np.cos(thetas[i])

        thetaEqns.append(thetaEqni)


        # stress calculations --------------------------------------------------------------
        #self.stressX.append( 0.5 * ( l[i-1]*np.cos(thetas[i-1]) - l[i]*np.sin(thetas[i]) ) )
        #self.stressY.append( 0.5 * ( -l[i-1]*np.sin(thetas[i-1]) + l[i]*np.cos(thetas[i]) ) )

    # combine all E-L equation coeffcients into a single array
    eqns = np.array( xEqns + yEqns + thetaEqns )

    # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
    #   are standard arrays
    #print(eqns)
    #input("stopza")

    if numRods == 1 : xVals, yVals, thetaVals = [0.0], [0.0], [0.0]
    else : xVals, yVals, thetaVals = [0.0], [0.0], [-kb * (thetas[0]-thetas[1])]

    for i in range(1, numRods) :

        xVal = -(mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i]))
        for j in range(1,i) :
            xVal -= (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] * lp[j] * np.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        xVals.append(xVal)

        yVal = -(mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i]))
        for j in range(1,i) :
            yVal -= (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] * lp[j] * np.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        yVals.append(yVal)

        if i == (numRods-1) :
            thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
        else :
            thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
        thetaVals.append(thetaVal)

    vals = np.array( xVals + yVals + thetaVals )


    numRods = len(q)-2

    sols = np.linalg.solve(eqns, vals)

    return sols


####################################################################################################
# Plotting Functions
####################################################################################################


def configurationPlot(t, nodeX, nodeY, minX, maxX, minY, maxY, t_final, kb_mu_ratio, init_length) :#, G0, rp, xp, yp) :

    print("\tCONFIGURATION PLOT...")

    #plt.figure(1)
    minAx = round( min( [minX, minY] ) - 5 )
    maxAx = round( max( [maxX, maxY] ) + 5 )
    #axesRange = [round(minAx-5), round(maxAx+5), round(minAx-5), round(maxAx+5)]

    fig = plt.figure(1)
    l1, = plt.plot([], [], '-bo')       # filament 1 nodes / cells
    #l2, = plt.plot([xp], [yp], '-ro')       # filament 2 nodes / cells
    #l3, = plt.plot([], [], 'go')        # filament 1 binding sites
    #l4, = plt.plot([], [], 'go')        # filament 2 binding sites

    #plt.xlim(minAx, maxAx)
    #plt.ylim(minAx, maxAx)

    #plt.xlim(25, 75)
    plt.xlim(75, 125)
    plt.ylim(-5, 5)

    framesPerSec = 15
    #framesPerSec = round(len(data)/data[-1][0]*10)
    #print("fps =", framesPerSec)

    # sets up video output stuff
    FFMpegWriter = manimation.writers['ffmpeg']
    metadata = dict(title='Movie Test', artist='Matplotlib',
                    comment='Movie support!')
    writer = FFMpegWriter(fps=framesPerSec, metadata=metadata)

    # remember to only use on .csv files in the data.dir directory
    # Splicing [8:-4] removes the "data.dir" at the front and ".csv" at the end
    #outfile = "plots.dir" + str(sys.argv[1])[8:-4] + ".mp4"
    #outfile = "plots.dir" + filename[8:-4] + ".mp4"
    outfile = "plots.dir/configuration_kb-mu-ratio" + str(round(kb_mu_ratio,1)) + "_length" + str(init_length) + "_tFinal" + str(round(t_final,2)) + ".mp4"
    #print(outfile)
    count = 0

    with writer.saving(fig, outfile, 100):
    # third arugment is dpi --> controls resolution and size of the plot
        """for d in data :

            #if count % 20 == 0 :  # indent the 3 lines below and uncomment this line
            plt.title("Filament Configuration at t = " + str(round(d[0],2)) )
            l1.set_data(d[1], d[2])
            #l2.set_data(d[3], d[4])

            writer.grab_frame()

            count+=1
            #plt.savefig('plots.dir/snapshots.dir/rigid-rod'+ str(round(d[0],0))+'.svg', format='svg')"""

        for i in range(0, len(t), 1) :
            print(i)
            plt.title("Filament Configuration at t = " + str(round(t[i],2)) )
            l1.set_data(nodeX[i], nodeY[i])
            #l2.set_data(11.5, 1.5)

            writer.grab_frame()

            # use the next line to save indivdual frames if desired
            #if count == 0 or int(round(t[i],0)) == 90 :
            #    plt.savefig('plots.dir/configuration' + str(round(t[i],0))+'.pdf', format='pdf')


            count+=1


    plt.gcf().clear()

    print("\t\t Done!")


#---------------------------------------------------------------------------------------------------


def qPlot(t, qVec, ltVec) :

    print("\tQ PLOTS...")

    x0, y0 = qVec[0], qVec[1]
    theta = qVec[2:]

    finalNumRods = len(theta)

    x = [x0]
    for i in range( finalNumRods-1 ) : x.append( [None] * len(t) )
    y = [y0]
    for i in range( finalNumRods-1 ) : y.append( [None] * len(t) )

    for k in range( len(t) ) :

        temp = list(np.array(theta)[:,k])
        numRods = len(temp) - temp.count(None)

        for i in range( 1, numRods ) :

            x[i][k] = x[i-1][k] + 0.5 * ( ltVec[k][i-1] * np.cos(theta[i-1][k]) + ltVec[k][i-1] * np.cos(theta[i][k]) )

            y[i][k] = y[i-1][k] + 0.5 * ( ltVec[k][i-1] * np.sin(theta[i-1][k]) + ltVec[k][i-1] * np.sin(theta[i][k]) )


    plt.figure(2)

    plt.title(r"Cell $x$ Positions" )
    plt.xlabel("Time (min)")
    plt.ylabel("Position (micrometers)")
    for xVec in x : plt.plot(t, xVec)
    plt.legend([r"$x_{"+str(i)+"}$" for i in range(len(x)) ] , loc='upper left')
    plt.savefig('plots.dir/x.pdf', format='pdf')

    plt.figure(3)
    plt.title(r"Cell $y$ Positions" )
    plt.xlabel("Time (min)")
    plt.ylabel("Position (micrometers)")
    for yVec in y : plt.plot(t, yVec)
    plt.legend([r"$y_{"+str(i)+"}$" for i in range(len(y)) ] , loc='upper left')
    plt.savefig('plots.dir/y.pdf', format='pdf')

    plt.figure(4)
    plt.title(r"Cell $\theta$ Positions" )
    plt.xlabel("Time (min)")
    plt.ylabel("Angle (radians)")
    for thetaVec in theta : plt.plot(t, thetaVec)
    plt.legend([r"$\theta_{"+str(i)+"}$" for i in range(len(theta)) ] , loc='upper left')
    plt.savefig('plots.dir/theta.pdf', format='pdf')

    plt.figure(17)
    plt.title(r"Cell $\theta$ Differences" )
    plt.xlabel("Time (min)")
    plt.ylabel("Angle (radians)")
    theta_firstHalf = np.array(theta[:len(theta)//2])
    theta_secondHalf = np.array(theta[len(theta)//2:])
    thetaDiff = theta_firstHalf - np.flip(theta_secondHalf)
    for thetaDiffVec in thetaDiff : plt.plot(t, thetaDiffVec)
    #plt.legend([r"$\theta_{"+str(i)+"}$" for i in range(len(theta)) ] , loc='upper left')
    plt.savefig('plots.dir/thetaDiffs.pdf', format='pdf')

    print("\t\t Done!")


#---------------------------------------------------------------------------------------------------

def qdotsPlot(t, qVec, qdot, ltVec, lpVec) :

    print("\tQDOT PLOTS...")

    x0dot = [ qdot[i][0] for i in range( len(qdot) ) ]
    y0dot = [ qdot[i][1] for i in range( len(qdot) ) ]
    thetadot = [ qdot[i][2:] for i in range( len(qdot) ) ]

    finalNumRods = len(thetadot[-1])
    thetadot = [ list(thetadot[i]) + [None]*(finalNumRods - len(thetadot[i])) for i in range(len(thetadot)) ]
    thetadot = [ np.array(thetadot)[:,i] for i in range(len(thetadot[-1])) ]

    """divTimes = []
    for i in range( 1, len(thetadot) ) :
        if len(thetadot[i-1]) != len(thetadot[i]) :
            divTimes.append(i)
    print(divTimes)

    finalThetadot = []
    for i in range( finalNumRods ) :
        finalThetadot.append( [None]*len(t) )

    for i in range( len(thetadot[-1]) ) :
        j = len(divTimes)-1
        divTime = divTimes[j]
        current = i
        for k in range( len(t)-1, -1, -1 ) :
            print(j, i, current, k)
            if (k < divTime) :
                if current != None :
                    if current % 2 == 0 : current //= 2
                    else : current = None
                if j != 0 :
                    j -= 1
                    divTime = divTimes[j]
            if current == None : finalThetadot[i][k] = None
            else : finalThetadot[i][k] = thetadot[k][current]
    thetadot = finalThetadot
    """

    #qdot = [x0dot] + [y0dot] + thetadot

    plt.figure(5)

    plt.title(r"$\theta$ Derivatives" )
    plt.xlabel("Time (min)")
    plt.ylabel("Angular Velocity (radians / min)")

    for thetadotVec in thetadot : plt.plot(t, thetadotVec)

    plt.legend([r"$\dot{\theta}_{"+str(i)+"}$" for i in range(len(thetadot)) ] , loc='upper left')

    plt.savefig('plots.dir/thetadots.pdf', format='pdf')


    theta = qVec[2:]

    #l, lp = [lt[0] for lt in ltVec], [lpt[0] for lpt in lpVec]
    xdots = [x0dot]
    for i in range( finalNumRods ) : xdots.append( [None] * len(t) )
    ydots = [y0dot]
    for i in range( finalNumRods ) : ydots.append( [None] * len(t) )

    #for i in range( 1, len(thetadot[-1]) ) :

    for k in range( len(t) ) :

        for i in range( 1, len(qdot[k])-2 ) :

            xdots[i][k] = xdots[i-1][k] + 0.5 * ( lpVec[k][i-1] * np.cos(theta[i-1][k]) - ltVec[k][i-1] * thetadot[i-1][k] * np.sin(theta[i-1][k]) + lpVec[k][i] * np.cos(theta[i][k]) - ltVec[k][i] * thetadot[i][k] * np.sin(theta[i][k]) )

            ydots[i][k] = ydots[i-1][k] + 0.5 * ( lpVec[k][i-1] * np.sin(theta[i-1][k]) + ltVec[k][i-1] * thetadot[i-1][k] * np.cos(theta[i-1][k]) + lpVec[k][i] * np.sin(theta[i][k]) + ltVec[k][i] * thetadot[i][k] * np.cos(theta[i][k]) )


    plt.figure(6)

    plt.title(r"$x$ Derivatives" )
    plt.xlabel("Time (min)")
    plt.ylabel("Velocity (micrometers / min)")

    for xdot in xdots : plt.plot(t, xdot)

    plt.legend([r"$\dot{x}_{"+str(i)+"}$" for i in range(len(xdots)) ] , loc='upper left')

    plt.savefig('plots.dir/xdots.pdf', format='pdf')


    plt.figure(7)

    plt.title(r"$y$ Derivatives" )
    plt.xlabel("Time (min)")
    plt.ylabel("Velocity (micrometers / min)")

    for ydot in ydots : plt.plot(t, ydot)

    plt.legend([r"$\dot{y}_{"+str(i)+"}$" for i in range(len(ydots)) ] , loc='upper left')

    plt.savefig('plots.dir/ydots.pdf', format='pdf')

    print("\t\t Done!")

#---------------------------------------------------------------------------------------------------

def qdotsDiffPlot(t, qVec, qdot, ltVec, lpVec) :

    print("\tQDOT DIFF PLOT...")

    theta = qVec[2:]


    thetadot = [ qdot[i][2:] for i in range( len(qdot) ) ]

    finalNumRods = len(thetadot[-1])
    thetadot = [ list(thetadot[i]) + [None]*(finalNumRods - len(thetadot[i])) for i in range(len(thetadot)) ]
    thetadot = [ np.array(thetadot)[:,i] for i in range(len(thetadot[-1])) ]

    #qdot = [x0dot] + [y0dot] + thetadot

    n = len(thetadot)//2

    plt.figure(20)
    plt.title(r"Bending Angle Between Middle Cells" )
    plt.xlabel("Time (min)")
    plt.ylabel("Angle (radians)")
    thetaDiffVec = np.array(theta[n]) - np.array(theta[n-1])
    plt.plot(t, thetaDiffVec)
    plt.savefig('plots.dir/thetaDiff_' + str(len(theta)) + '.pdf', format='pdf')

    plt.figure(21)
    plt.title(r"Rate of Changing Bending Angle Between Middle Cells" )
    plt.xlabel("Time (min)")
    plt.ylabel("Angular Velocity (radians / min)")
    thetadotDiffVec = np.array(thetadot[n]) - np.array(thetadot[n-1])
    plt.plot(t, thetadotDiffVec)
    plt.savefig('plots.dir/thetadotDiff_' + str(len(theta)) + '.pdf', format='pdf')

#---------------------------------------------------------------------------------------------------


def stressPlot(t, stressX, stressY) :

    print("\tSTRESS PLOTS...")

    divTimes = []
    for i in range( 1, len(stressX) ) :
        if len(stressX[i-1]) != len(stressX[i]) :
            divTimes.append(i)
    #print(divTimes)

    fx, fy = [], []
    for i in range( len(stressX[-1]) ) :
        fx.append( [None] * len(t) )
        fy.append( [None] * len(t) )

    #for i in range( len(divTimes)-1, -1, -1 ) :
    for i in range( len(stressX[-1]) ) :
        #print("Node:", i)
        j = len(divTimes)-1
        divTime = divTimes[j]
        #for j in range( len(divTimes)-1, -1, -1 ) :
        current = i
        for k in range( len(t)-1, -1, -1 ) :
            if (k < divTime) and (j != 0) :
                current = (current-1) / 2
                if current % 1 == 0 : current = int(current)
                j -= 1
                divTime = divTimes[j]
            #print("node:", i, "\tcurrent", current, "\ttime index:", k, "\ttime:", t[k], "\tdivTime:", divTime)
            try :
                fx[i][k] = stressX[k][current]
                fy[i][k] = stressY[k][current]
            except :
                fx[i][k] = None
                fy[i][k] = None


    plt.figure(8)
    plt.title("Stress in Cell-to-Cell Connections")
    plt.xlabel("Time (min)")
    plt.ylabel("Stress Magnitude")

    for i in range( len(fx) ) :
        fmag = []
        for j in range( len(fx[i]) ) :
            try : fmag.append( np.sqrt( fx[i][j]**2 + fy[i][j]**2 ) )
            except : fmag.append(None)
        plt.plot(t, fmag)

    plt.legend([ "Node" + str(i) for i in range(1, len(fx)+1) ] , loc='upper left')

    #plt.show()
    plt.savefig('plots.dir/stresss_simulation.pdf', format='pdf')

    print("\t\tDone!")

#---------------------------------------------------------------------------------------------------

def lengthPlot(t, l0, growthRate, lastAddTime) :

    print("\tLENGTH PLOTS...")

    l = []
    i = 0
    last = lastAddTime[i]
    for time in t :
        if i < len(lastAddTime) :
            if (time > lastAddTime[i+1]) :
                i+=1
                last = lastAddTime[i]
        l.append( length(time, l0, growthRate, last) )
    plt.figure(9)
    plt.title("Cell Length")
    plt.xlabel("Time (min)")
    plt.ylabel("Length (micrometers)")

    plt.plot(t, l)
    plt.savefig('plots.dir/lengths.pdf', format='pdf')


    lp = []
    i = 0
    last = lastAddTime[i]
    for time in t :
        if i < len(lastAddTime) :
            if (time > lastAddTime[i+1]) :
                i+=1
                last = lastAddTime[i]
        lp.append( growthRateFunc(time, l0, growthRate, last) )
    plt.figure(10)
    plt.title("Cell Growth Rate")
    plt.xlabel("Time (min)")
    plt.ylabel("Growth Rate (micrometers/min)")

    plt.plot(t, lp)
    plt.savefig('plots.dir/growthRates.pdf', format='pdf')

    print("\t\t Done!")


####################################################################################################
# Data Organization
####################################################################################################

print("LOADING DATA...")

try :
    filename = str(sys.argv[1])
except IndexError :
    print("If loading package, continue, otherwise\nERROR: No file given -> try again")
    exit()

#sol = pickle.load(open("data.dir/exponentialGrowth_epsilonLJ0.0_rm1.5_Lc5.0_endTime180.0.pkl", "rb"))
sol = pickle.load(open(filename, "rb"))
l0, growthRate, kb, mu, nu, epsilon, G0, rp, xp, yp, init_length = sol[0]
sol = sol[1:]
kb_mu_ratio = kb / mu

print( sum( [ sum( [ len(sol[i].y[j]) for j in range( len(sol[i].y) ) ] ) for i in range( len(sol) ) ] ) )
print( len(sol[0].y[0]) )
print( len(sol[0].t) )
# use these three lines to coarsen data if desired
n = 1     # take every nth output
for s in sol :
    s.t = s.t[0::n]
    s.y = np.array( [ s.y[i][0::n] for i in range(len(s.y)) ] )
print( sum( [ sum( [ len(sol[i].y[j]) for j in range( len(sol[i].y) ) ] ) for i in range( len(sol) ) ] ) )
print( len(sol[0].y[0]) )
print( len(sol[0].t) )

#exit()

print("ORGANIZING DATA...")

t = np.concatenate( [s.t for s in sol], axis=None )
t_final = t[-1]

minX, maxX, minY, maxY = 1E100, 0, 1E100, 0
qdot, lambdaX, lambdaY, nodeX, nodeY = [], [], [], [], []
lastAddTime = [sol[0].t[0]]
ltVec, lpVec = [], []

qVec = []
for i in range( len(sol[-1].y) ) : qVec.append( [None] * len(t) )

k = 0   # time index

for s in sol :

    print("starting a new sol")

    q = [ s.y[:,i] for i in range(len(s.y[0])) ]
    numRods = len(q[0]) - 2

    lt = [ [ length(time, l0, growthRate, lastAddTime[-1]) ] * numRods for time in s.t ]
    ltVec += lt
    lp = [ [ growthRateFunc(time, l0, growthRate, lastAddTime[-1]) ] * numRods for time in s.t ]
    lpVec += lp

    # interating over time
    for i in range(len(q)) :

        numRods = len(q[i])-2
        lt = length(s.t[i], l0, growthRate, lastAddTime[-1])
        lp = growthRateFunc(s.t[i], l0, growthRate, lastAddTime[-1])

        # organize q
        for j in range( len(s.y) ) :
            qVec[j][k] = s.y[j][i]
        k += 1

        # compute qdots and Lagrange multipliers
        sols = growthDynamics(s.t[i], q[i], lt, kb, mu, nu, epsilon, lp)

        # organize qdots
        qdots = sols[: numRods+2]
        qdot.append(qdots)

        # organize Lagrange multipliers
        lambdaXs = sols[numRods+2 : 2*numRods+1]
        lambdaX.append(lambdaXs)
        lambdaYs = sols[2*numRods+1 :]
        lambdaY.append(lambdaYs)

        # calculate node positions
        thetas = q[i][2:]
        xNodes = [ q[i][0] - lt/2 * np.cos( thetas[0] ) ]
        yNodes = [ q[i][1] - lt/2 * np.sin( thetas[0] ) ]
        for i in range( len(q[i]) - 2 ) :
            xNodes.append( xNodes[-1] + lt * np.cos(thetas[i]) )
            yNodes.append( yNodes[-1] + lt * np.sin(thetas[i]) )
            if min(xNodes) < minX : minX = min(xNodes)
            if max(xNodes) > maxX : maxX = max(xNodes)
            if min(yNodes) < minY : minY = min(yNodes)
            if max(yNodes) > maxY : maxY = max(yNodes)
        nodeX.append(xNodes)
        nodeY.append(yNodes)

    lastAddTime.append(s.t[-1])

"""q = []
for i in range( len(sol[-1].y) ) :
    try :
        q1 = []
        for s in sol :
            q1 += list(s.y[i])
        q.append(q1)
    except :
        try :
            q1 = []
            for s in sol :
                q1 += list(s.y[i])
            q.append(q1)"""


print("PLOTTING...")

configurationPlot(t, nodeX, nodeY, minX, maxX, minY, maxY, t_final, kb_mu_ratio, init_length)#, G0, rp, xp, yp)
#qPlot(t, qVec, ltVec)
#qdotsPlot(t, qVec, qdot, ltVec, lpVec)
#qdotsDiffPlot(t, qVec, qdot, ltVec, lpVec)
#stressPlot(t, lambdaX, lambdaY)
#lengthPlot(t, l0, growthRate, lastAddTime)

print("DONE PLOTTING!")



"""
# t array
t = np.concatenate( [s.t for s in sol], axis=None )

# solution arrays
y = np.array( [ np.concatenate([s.y[i] for s in sol], axis=None) for i in range(len(sol[0].y)) ] )

# q arrays
q = [ y[:,i] for i in range(len(y)) ]
"""
