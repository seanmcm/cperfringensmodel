#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation

import pdb
import sys
import numpy as np
from numpy import squeeze
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
#import time
import csv
import os
import pickle
#import plotsV4
#import plotsV4_withSites_Single
from FilamentV5 import *

################################################################################
# SIMULATION NOTES
#   TIME-SCALE: minutes
#   LENGTH-SCALE: micrometers
################################################################################

################################################################################
# ODE FUNCTION
################################################################################

def length(t, l0, r) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    tdiv = np.log(2) / r
    return l0 * np.exp( r * (t % tdiv) )


def growthRateFunc(t, l0, r) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    tdiv = np.log(2) / r
    return l0 * r * np.exp( r * (t % tdiv) )


def chain_odes(t, q, l0, r, kb, mu, nu, epsilon) :
    """ solves for the unknown qdots and constraint force components (Fx, Fy)
        for a growing cell chain
            t (float) : time
            q (array) : current chain state [x_0, y_0, theta_0, ... , theta_N-1]
            l0 (float) : daughter cell length
            r (float) : cell growth rate
            kb (float) : angular spring constant
            mu (float) : parallel drag coefficient per unit length
            nu (float) : perpendiular drag coefficient per unit length
            epsilion (float) : rotational drag coefficient per unit length
        returns a numpy array of the form:
            [xdot_0, ydot_0, thetadot_0, ... , thetadot_N-1, Fx_0, ... Fx_N-2, Fy_0, ... Fv_N-2]
            where N is the number of cells in the chain """

    numRods = len(q)-2

    l =  [length(t, l0, r)] * numRods
    lp = [growthRateFunc(t, l0, r)] * numRods

    x0 = q[0]
    y0 = q[1]
    thetas = q[2:]

    numRods = len(thetas)
    numNodes = numRods + 1

    totalLength = sum(l)

    #--------------------------------------------------------

    xEqns, yEqns, thetaEqns = [], [], []

    # x_i Euler-Lagrange equations --------------------------------------------------------

    # initialize x_0 E-L equation and x E_L equation array
    xEqn0 = np.zeros(3*numRods)
    xEqn0[0] = ( mu * np.cos(thetas[0])**2 + nu * np.sin(thetas[0])**2 ) * l[0]
    xEqn0[1] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    if numRods != 1 : xEqn0[numRods + 2] = -1
    xEqns.append(xEqn0)

    # initialize y_0 E-L equation and y E_L equation array
    yEqn0 = np.zeros(3*numRods)
    yEqn0[0] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    yEqn0[1] = ( mu * np.sin(thetas[0])**2 + nu * np.cos(thetas[0])**2 ) * l[0]
    if numRods != 1 : yEqn0[2*numRods + 1] = -1
    yEqns.append(yEqn0)

    # initialize theta_0 E-L equation and theta E_L equation array
    thetaEqn0 = np.zeros(3*numRods)
    thetaEqn0[2] = epsilon * l[0]**3
    if numRods != 1 :
        thetaEqn0[numRods + 2] = 0.5 * l[0] * np.sin(thetas[0])
        thetaEqn0[2*numRods + 1] = -0.5 * l[0] * np.cos(thetas[0])
    thetaEqns.append(thetaEqn0)

    #generate the 2 through N-1 x, y, and theta E-L equations
    for i in range(1, numRods) :

        # x_i E_L equations -----------------------------------------------------------

        xEqni = np.zeros(3*numRods)

        xEqni[0] = ( mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[i]

        xEqni[1] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * np.cos(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[0] * l[i] * np.sin(thetas[0]) )

        for j in range(1, i) :
            xEqni[j+2] = (mu - nu) * l[j] * l[i] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[j] * l[i] * np.sin(thetas[j])

        xEqni[i+2] = -0.5 * nu * l[i]**2 * ( np.sin(thetas[i]) * np.cos(thetas[i])**2 + np.sin(thetas[i])**3 )

        xEqni[i + numRods + 1] = 1

        if i != (numRods-1) :
            xEqni[i + numRods + 2] = -1

        xEqns.append(xEqni)


        # y_i E_L equations -----------------------------------------------------------

        yEqni = np.zeros(3*numRods)

        yEqni[0] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        yEqni[1] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[i]

        yEqni[2] = 0.5 * ( (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[0] * l[i] * np.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * np.sin(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) )

        for j in range(1, i) :
            yEqni[j+2] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[j] * l[i] * np.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])

        yEqni[i+2] = 0.5 * nu * l[i]**2 * (np.cos(thetas[i]) * np.sin(thetas[i])**2 + np.cos(thetas[i])**3)

        yEqni[i + (2*numRods)] = 1

        if i != (numRods-1) :
            yEqni[i + (2*numRods) + 1] = -1

        yEqns.append(yEqni)


        # theta_i E_L equations -----------------------------------------------------------

        thetaEqni = np.zeros(3*numRods)

        thetaEqni[i+2] = epsilon * l[i]**3

        thetaEqni[i + numRods + 1] = 0.5 * l[i] * np.sin(thetas[i])

        thetaEqni[i + (2*numRods)] = -0.5 * l[i] * np.cos(thetas[i])

        if i != (numRods-1) :
            thetaEqni[i + numRods + 2] = 0.5 * l[i] * np.sin(thetas[i])
            thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * np.cos(thetas[i])

        thetaEqns.append(thetaEqni)


    # combine all E-L equation coeffcients into a single array
    # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
    eqns = np.array( xEqns + yEqns + thetaEqns )

    if numRods == 1 : xVals, yVals, thetaVals = [0.0], [0.0], [0.0]
    else : xVals, yVals, thetaVals = [0.0], [0.0], [-kb * (thetas[0]-thetas[1])]

    for i in range(1, numRods) :

        xVal = -(mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i]))
        for j in range(1,i) :
            xVal -= (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] * lp[j] * np.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        xVals.append(xVal)

        yVal = -(mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i]))
        for j in range(1,i) :
            yVal -= (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] * lp[j] * np.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        yVals.append(yVal)

        if i == (numRods-1) :
            thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
        else :
            thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
        thetaVals.append(thetaVal)

    vals = np.array( xVals + yVals + thetaVals )

    sols = np.linalg.solve(eqns, vals)

    return sols


def dqdt(t, q, l0, r, kb, mu, nu, epsilon) :
    """ isolates the qdots from the array returned by chain_odes
        returns an array of the form:
            [xdot_0, ydot_0, thetadot_0, ... , thetadot_N-1]
            where N is the number of cells in the chain """

    sol = chain_odes(t, q, l0, r, kb, mu, nu, epsilon)
    # sol has length 3N where N is the number of cells and has the form :
    # [xdot_0, ydot_0, thetadot_0, ... , thetadot_N-1, Fx_0, ... Fx_N-2, Fy_0, ... Fv_N-2]

    numRods = int(len(sol) // 3 )
    qdots = sol[:(numRods+2)]

    return qdots


################################################################################
# ODE SOLVER
################################################################################

def main(endTime, numRods, l0, growthRate, angle, mu0, nu0, epsilon0, kb0, seed_val) :
    """ simulation C. Perfringen bacteria """

    global lambdaVals
    global realCall
    global lastAddTime
    global filaments
    global dt

    t = 0.0

    #maxstep = float(sys.argv[1])
    rel_tol = 1e-3 #float(sys.argv[1]) # 1e-3 is the default value
    abs_tol = rel_tol * 1e-3

    # integration --------------------------------------------------------------

    q0 = np.array(q1)

    try : addTimeInterval = l0/growthRate * np.log(2) #l0 / growthRate
    except ZeroDivisionError : addTimeInterval = np.inf

    round(kb0 / (mu0 * l0**2 * growthRate))

    params = psi, a, l0, growthRate/l0, kb0, mu0, nu0, epsilon0, numRods, angle, rel_tol, abs_tol, seed_val
    sol = [np.array(params)]

    args = (l0, r, kb0, mu0, nu0, epsilon0)

    while t < endTime :

        print(t < endTime)

        if endTime <= (t + addTimeInterval) :
            nextStopTime = endTime
            addNodesFlag = False
        else:
            nextStopTime = t + addTimeInterval
            addNodesFlag = True

        print("Adding Nodes?", addNodesFlag)
        if addNodesFlag : print("ADDING NODES AT t =", nextStopTime)
        else : print("STOPPING AT t =", nextStopTime)

        t_span = [t, nextStopTime]                      # start time to endtime
        t_pts = np.arange( t, nextStopTime, 0.01)       # times with guaranteed
        t_pts[-1] = nextStopTime                        # ensure last time is the next stop time (numerical inaccracies messed this up)

        func = lambda time, qs : dqdt(time, qs, *args)
        s = solve_ivp(func, t_span, q0, t_eval=t_pts)#, rtol=rel_tol, atol=abs_tol)

        # organize data into [t, x0, y0, theta0, ... , theta_N-1]
        data1 =  [ np.insert(s.y[:,i], 0, s.t[i]) for i in range( len(s.y[0]) ) ]

        # compute and organize data into :
        # [xdot_0, ydot_0, thetadot_0, ... , thetadot_N-1, Fx_0, ... , Fx_N-2, Fy_0, ... , Fy_N-2]
        data2 =  [ chain_odes(s.t[i], s.y[:,i], *args) for i in range( len(s.t) ) ]

        # combine both data arrays to make :
        # [t, x0, y0, theta0, ... , theta_N-1, xdot_0, ydot_0, thetadot_0, ... , thetadot_N-1, Fx_0, ... , Fx_N-2, Fy_0, ... , Fy_N-2]
        data =  [ np.concatenate( (data1[i], data2[i] ), axis=None ) for i in range( len(data1) ) ]

        sol += data

        q_final = s.y[:,-1]
        t = s.t[-1]     # set current time to last time point of the solver

        # implement cell division
        #addNodesFlag = False      # uncomment to turn off cell divsion feature
        if addNodesFlag :

            oldThetas = q_final[2:]
            thetas = []
            for i in range( len(oldThetas) ) :
                thetas.append(oldThetas[i])
                thetas.append(oldThetas[i])

            l = length(t, l0, r)

            xOld = q_final[0]
            x = xOld - ( (l/4) * np.cos( thetas[0] ) )

            yOld = q_final[1]
            y = yOld - ( (l/4) * np.sin( thetas[0] ) )

            q0 = [x] + [y] + thetas


    with open(f'data_test.csv', 'w', newline='') as file :
        mywriter = csv.writer(file, delimiter=',')
        mywriter.writerows(sol)

    print("Integration Complete")

    return


################################################################################

# input parameters
psi = int(sys.argv[1])
a = float(sys.argv[2])
endTime = float(sys.argv[3])
seed_val = 7 #int(sys.argv[4])

random.seed(seed_val)

# fixed parameters
l0 = 5 # microns
r = 0.02 # 1/min
mu0 = 5/278

# Compute kb based on psi and the other parameters
kb0 = psi * mu0 * l0**3 * r

#startTime = time.clock()
growthRate = l0 * r



b = a/12
nu0 = a * mu0 #100*mu0#[100*mu0, 1000*mu0] # [2*mu0, 5*mu0, 10*mu0,
epsilon0 = b * mu0     # initial angular drag coefficient

dt = 0.0


lambdaVals = [None]     # none since the inital lambda values are unknown
realCall = False        # flag used to determine if a call to dqdt is real
lastAddTime = 0         # time of the last cell division

#q1 = [0, 0, 0, np.pi/8, np.pi/4]
#q2 = [0, -2.5, 0, np.pi/8, np.pi/4]
#q2 = [0, -3, 0, np.pi/6, np.pi/4, np.pi/3]
#q2 = [3, -3, -np.pi/8, -np.pi/16, np.pi/10, np.pi/6]

# similar curvature
#q1 = [0, 0, 0, np.pi/6, np.pi/4, np.pi/3]
#q2 = [0.5, -1, 0, np.pi/6, np.pi/4, np.pi/3, np.pi/2.5]

#straight configuration
#q1 = [0, 0, 0, 0, 0, 0, 0]
#q2 = [-1, -1, 0, 0, 0, 0, 0, 0]

# two cell test
# q1 = [0, 0] + [ i * np.pi/18 for i in range(-6, 7) ]


#angle = 90
#angle *= np.pi/180      # convert angle from degrees to radians

"""q1 = [0,0,-angle/2]

step = angle / (numRods-1)
for i in range(3, numRods+2) :
    q1.append(q1[-1] + step)
"""
#q2 = [1, -1, 0]


#q1 = [0.0,0.0,-35*np.pi/180, 0.0, 35*np.pi/180]
#q1 = [0.0,0.0,-45*np.pi/180, -15*np.pi/180, 0.0, 15*np.pi/180, 45*np.pi/180]

# build a configuration with numRods
# if pert == True, angle is deltaTheta of center linkages
# else angle is the deltaTheta between all adjacent cells (uniform curvature)

pert = True
angle = 2       # always provide the angle in degrees; conversion to radians is handled later in the code

bp = []
with open("bucklingParameters.csv") as f :
    csv_reader = csv.reader(f, delimiter=',')
    for row in csv_reader : bp.append(row)
temp = list(filter(lambda x : ( x[0] == str(int(psi)) and x[1] == str(int(a)) ), bp))
numRods = int(temp[0][4])


#numRods = int(sys.argv[4]) #8     # I recomend always using an even number of cells

if pert :
    if numRods % 2 == 0 :
        q1 = [0.0, 0.0] + (numRods-2)//2 * [0.0] + [-0.5 * angle * np.pi / 180, 0.5 * angle * np.pi / 180] + (numRods-2)//2 * [0.0]
    else :
        print("For perturbation simulations, numRods must be an even integer. Try again.")
        exit()
elif numRods % 2 != 0 :  # odd number of cells
    print("Use an even number of cells. It will make your life easier")
    exit()
    q1 = [0.0, 0.0] + [ i * angle * np.pi / 180 for i in range( -(numRods-1)//2, (numRods-1)//2 + 1 ) ]
else :  # even number of cells
    q1 = [0.0, 0.0] + [ 0.5 * i * angle * np.pi / 180 for i in range( -(numRods-1), numRods, 2 ) ]

# curved config : 3 cells;
#q1 = [0.0, 0.0, -1*np.pi/180,  0.0, 10*np.pi/180]

# curved config : 5 cells;
#q1 = [0.0, 0.0, -20*np.pi/180, -10*np.pi/180,  0.0, 10*np.pi/180, 20*np.pi/180]

# curved config : 7 cells;
#q1 = [0.0, 0.0, -30*np.pi/180, -20*np.pi/180, -10*np.pi/180,  0.0, 10*np.pi/180, 20*np.pi/180, 30*np.pi/180]

# curved config : 9 cells;
#q1 = [0.0, 0.0, -40*np.pi/180, -30*np.pi/180, -20*np.pi/180, -10*np.pi/180,  0.0, 10*np.pi/180, 20*np.pi/180, 30*np.pi/180, 40*np.pi/180]

# curved config : 13 cells
#q1 = [0.0, 0.0, -60*np.pi/180, -50*np.pi/180, -40*np.pi/180, -30*np.pi/180, -20*np.pi/180, -10np.pi/180,  0.0, 10*np.pi/180, 20*np.pi/180, 30*np.pi/180, 40*np.pi/180, 50*np.pi/180, 60*np.pi/180]

"""
# initialize a chain with random orientations
mean_angle = 0  # average orientation in degrees
stdev_angle = 5 # standard deviation in degrees
q1 = [0.0, 0.0] + [random.gauss(mean_angle*(np.pi / 180), stdev_angle*(np.pi / 180) ) for i in range(numRods)]
"""

# straight horizontal chain
#q1 = [0.0, 0.0, 0.0, 0.0]

# arbitrary offset distances and rotation angles
xs, ys = random.uniform(-100, 100), random.uniform(-100, 100)
rot_angle = random.uniform(0, 360)  # this value should be in degrees

#print(f"xs = {xs}, ys = {ys}, angle = {rot_angle}")

#q1[0] += xs
#q1[1] += ys
#for i in range(2, len(q1)) : q1[i] += (rot_angle * np.pi / 180)

#filaments = [Filament(q1, 5, 50, growthRate), Filament(q2, 7, 70, growthRate)]
#filaments = [Filament(q1, l0, 0, growthRate)]

print("NEW CALL:", nu0, kb0)
#main(endTime, numRods, growthRate, angle, mu0, nu0, epsilon0, kb0, width, kon, kOff0, ks, Lmax, Lc, Fc)
main(endTime, numRods, l0, growthRate, angle, mu0, nu0, epsilon0, kb0, seed_val)

# Calulating runtime, output in form "xx hrs, xx mins, xx secs"
"""totalRuntime = time.clock()-startTime
mins = round(totalRuntime//60)
secs = round(totalRuntime % 60)
if mins >= 60 :
    hrs = round(mins // 60)
    mins = round(mins % 60)
    print( "Runtime =", hrs, "hrs", mins, "minutes", secs, "seconds" )
else : print( "Runtime =", mins, "minutes", secs, "seconds" )
"""
print("Sweet Baby Jesus, IT WORKS!")


################################################################################
