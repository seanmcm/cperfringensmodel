import pdb
import numpy as np
from scipy.integrate import ode
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
#import matplotlib.animation as manimation
import sys


stiffData = pickle.load(open("testData.dir/run10_example_test_epsilonLJ0_rm1.5_Lc5.0_endTime120.0.pkl", "rb"))

t = stiffData[0]

stressX = stiffData[1][9]
stressY = stiffData[1][10]

print(stressX[0], stressY[0])

divTimes = []
for i in range( 1, len(stressX) ) :
    if len(stressX[i-1]) != len(stressX[i]) :
        divTimes.append(i)
print(divTimes)

t2, t3 = t[divTimes[0]:], t[divTimes[1]:]
#print(t2, t3)

fx1, fy1 = stressX[:divTimes[0]], stressY[:divTimes[0]]
fx2, fy2 = stressX[divTimes[0]:divTimes[1]], stressY[divTimes[0]:divTimes[1]]
fx3, fy3 = stressX[divTimes[1]:], stressY[divTimes[1]:]

fx1, fy1, fx2, fy2, fx3, fy3 = map(np.array, (fx1, fy1, fx2, fy2, fx3, fy3))

#testNode = np.concatenate( (fx1[:,0], fx2[:,1], fx3[:,3]) )
#print(len(testNode))

node0 = np.sqrt( fx3[:,0]**2 + fy3[:,0]**2 )
node1 = np.sqrt( np.concatenate( (fx2[:,0], fx3[:,1]) )**2 + np.concatenate( (fy2[:,0], fy3[:,1]) )**2 )
node2 = np.sqrt( fx3[:,2]**2 + fy3[:,2]**2 )
node3 = np.sqrt( np.concatenate( (fx1[:,0], fx2[:,1], fx3[:,3]) )**2 + np.concatenate( (fy1[:,0], fy2[:,1], fy3[:,3]) )**2 )
node4 = np.sqrt( fx3[:,4]**2 + fy3[:,4]**2 )
node5 = np.sqrt( np.concatenate( (fx2[:,2], fx3[:,5]) )**2 + np.concatenate( (fy2[:,2], fy3[:,5]) )**2 )
node6 = np.sqrt( fx3[:,6]**2 + fy3[:,6]**2 )
node7 = np.sqrt( np.concatenate( (fx1[:,1], fx2[:,3], fx3[:,7]) )**2 + np.concatenate( (fy1[:,1], fy2[:,3], fy3[:,7]) )**2 )
node8 = np.sqrt( fx3[:,8]**2 + fy3[:,8]**2 )
node9 = np.sqrt( np.concatenate( (fx2[:,4], fx3[:,9]) )**2 + np.concatenate( (fy2[:,4], fy3[:,9]) )**2 )
node10 = np.sqrt( fx3[:,10]**2 + fy3[:,10]**2)

plt.title("Stress")
plt.xlabel("Time (min)")
plt.ylabel("Stress Magnitude")

plt.plot(t3, node0)
plt.plot(t2, node1)
plt.plot(t3, node2)
plt.plot(t, node3)
plt.plot(t3, node4)
plt.plot(t2, node5)
plt.plot(t3, node6)
plt.plot(t, node7)
plt.plot(t3, node8)
plt.plot(t2, node9)
plt.plot(t3, node10)

plt.legend([ "F" + str(i) for i in range(11) ] )

plt.show()
