#!/usr/local/env python

import os
import pickle
import numpy as np

print("RUNNING")

kb_mu_ratios = [1]#, 15, 30, 45, 60, 75, 90, 100, 125, 150, 175, 200]  # argument 1
rates = [0.05]#, 0.1, 0.2, 0.3]                                       # argument 2
angles = [5]#, 10, 15, 20, 30]                                        # argument 3

kb_mu_ratios = [ str(x) for x in kb_mu_ratios ]
rates = [ str(x) for x in rates ]
angles = [ str(x) for x in angles ]

for angle in angles :
    print("angle =", angle)
    for kb_mu_ratio in kb_mu_ratios :
        print("kb_mu_ratio =", kb_mu_ratio)
        for rate in rates :
            print("rate =", rate)

            params = " " + kb_mu_ratio + " " + rate + " " + angle
            filename = "data" + "_kb_mu_ratio" + kb_mu_ratio + "_rate" + rate + "_angle" + angle + ".pkl"

            pickle.dump([], open("data.pkl", "wb"))

            endTime = 1e100
            while endTime > 0.5 :

                os.system("exponentialGrowth.py" + params)

                newData = pickle.load(open("out.pkl", "rb"))
                endTime = newData[1]

                data = pickle.load(open(filename, "rb"))
                os.remove("data.pkl")
                data.append(newData)
                pickle.dump(data, open(filename, "wb"))
