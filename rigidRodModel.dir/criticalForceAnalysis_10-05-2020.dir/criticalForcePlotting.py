#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys


def testEL(data) :

    l0 = 5
    kb, mu, growthRate, numRods, angle, pert_coefficient = data[0]

    lengths = np.array(data[1])
    lambdaXVals = np.array(data[2])
    lambdaYVals = np.array(data[3])
    qdots = np.array(data[4])
    q = np.array(data[5])

    for i in range(len(lengths)) :

        numRods = int(lengths[i] / l0)
        k = int( (pert_coefficient * numRods) - 1 )

        x0, y0 = q[i][0]/l0, q[i][1]/l0
        thetas = q[i][2:]

        x0dot, y0dot = qdots[i][0] / growthRate, qdots[i][1] / growthRate
        thetadots = l0/growthRate * qdots[i][2:]

        lambdaXs = lambdaXVals[i] / (mu * l0 * growthRate)
        lambdaYs = lambdaYVals[i] / (mu * l0 * growthRate)

        print("numRods =", numRods, ":",
            x0dot + numRods + np.cos(thetas[k]) - thetadots[k] * np.sin(thetas[k]) + np.cos(thetas[k+1]) - thetadots[k+1] * np.sin(thetas[k+1]) + lambdaXs[numRods-2] - 3, "\t:",
            (2 * np.cos(0.5 * (thetas[k+1] - thetas[k])) + numRods - 4 ) / (4 * np.sin(0.5 * (thetas[k+1] - thetas[k])) ) , "\t:",
            -(x0dot + numRods - 3 + 2 * np.cos(thetas[k]) + lambdaXs[numRods-2]) / np.sin(thetas[k])  , "\t:",
            thetadots[k+1] - thetadots[k], "\t:",
            -(2 * np.cos(thetas[k]) - 2) / np.sin(thetas[k]) )

#outfile = os.path.basename(sys.argv[1])
filename = sys.argv[1]

data = pickle.load(open(filename, "rb"))
testEL(data)

l0 = 5
kb, mu, growthRate, numRods, angle, pert_coefficient = data[0]

lengths = np.array(data[1])
lambdaXVals = np.array(data[2])
lambdaYVals = np.array(data[3])
qdots = np.array(data[4])
q = np.array(data[5])

stressFunc = lambda q,l : -0.5 * mu * l**2 * (growthRate/l0) * q * (1-q)

plotCounter = 1

#coeffs = [0.1, 0.2, 0.3, 0.4, 0.5]
#lambdaXTheory = [ np.array( [-0.5 * mu * l**2 * (growthRate/l0) * q * (1-q) for l in lengths] ) for q in coeffs ]

#for i in [7,8] :
for i in range(len(lengths)) :

    plt.figure(plotCounter)
    plt.title(r"Length = " + str(lengths[i]))
    plt.xlabel(r"Location $q$")
    plt.ylabel(r"$\lambda^x_i$ (kg $\mu$m/min$^2$)")

    qVals = np.array([(n+1)/(len(lambdaXVals[i])+1) for n in range(len(lambdaXVals[i]))])
    stressTheory = np.array([stressFunc(q,lengths[i]) for q in qVals])
    plt.plot(qVals, lambdaXVals[i], '-o', label="Numerical")
    plt.plot(qVals, stressTheory , '-o', label="Theory")
    plt.plot(qVals, lambdaXVals[i] - stressTheory, '-o', label="Difference")
    plt.legend(loc="upper right")

    plt.savefig("plots.dir/xStress_length" + str(lengths[i]) + ".pdf" )
    plotCounter += 1

    plt.figure(plotCounter)
    plt.title(r"Length = " + str(lengths[i]))
    plt.xlabel(r"Location $q$")
    plt.ylabel(r"$\lambda^y_i$ (kg $\mu$m/min$^2$)")

    qVals = np.array([(n+1)/(len(lambdaYVals[i])+1) for n in range(len(lambdaYVals[i]))])
    plt.plot(qVals, lambdaYVals[i], '-o', label="Numerical")
    plt.plot(qVals, np.array([0] * len(lambdaYVals[i])) , '-o', label="Theory")
    plt.plot([0.5 - 0.6 * lengths[0]/(2*lengths[i]), 0.5 + 0.6 * lengths[0]/(2*lengths[i])],[0, 0], label="20 Cells")
    plt.legend(loc="upper right")

    plt.savefig("plots.dir/yStress_length" + str(lengths[i]) + ".pdf" )
    plotCounter += 1

    plt.figure(plotCounter)
    plt.title(r"Length = " + str(lengths[i]))
    plt.xlabel(r"Cell Number")
    plt.ylabel(r"$\dot{\theta}$")

    plt.plot(np.array(range(len(qdots[i][2:]))), qdots[i][2:], '-o', label="Numerical")
    plt.legend(loc="upper right")

    plt.savefig("plots.dir/thetadots_length" + str(lengths[i]) + ".pdf" )
    plotCounter += 1

#plt.show()



"""

#param_key = sys.argv[-1]    # string representing the parameter that is varied
plot_titles = {
    "kb" : "Varied Angular Spring Constant",
    "mu" : "Varied Parallel Drag Per Unit Length",
    "growthRate" : "Varied Growth Rate",
    "angle" : "Varied Pertubation Angle",
    "q" : "Varied Location"
}

#plt.title( plot_titles[param_key] )
plt.title("")

plt.axhline(y=0, color='black', linestyle='-')

for i in range(1, len(sys.argv)-1) :

    outfile = os.path.basename(sys.argv[i])
    filename = sys.argv[i]

    data = pickle.load(open(filename, "rb"))

    kb, mu, growthRate, numRods, angle, pert_coefficient = data[0]
    data = np.array(data[1:])

    paramstrs = {
        "kb" : r"$k_b$ = " + str(kb),
        "mu" : r"$\mu$ = " + str(mu),
        "growthRate" : r"$G_r$ = " + str(growthRate),
        "angle" : r"$\theta$ = " + str(angle),
        "q" : r"$q$ = " + str(pert_coefficient)
    }
    paramstr = paramstrs[param_key]

    l0 = 5
    lengths, thetadotDiffs = data[:,0], data[:,1]
    stress = 0.5 * mu * lengths**2 * (growthRate/l0) * pert_coefficient * (1-pert_coefficient)

    lengths, stressDiffs = data[:,0], data[:,1]

    plt.xlabel(r"Length ($\mu$m)")
    plt.ylabel("Stress Difference")
    plt.plot(stress, thetadotDiffs, '-o', label=paramstr)

plt.legend(loc="upper right")
plt.savefig('plots.dir/' + outfile[:-4] + '.pdf', format='pdf')

"""
