#!/bin/bash

declare -a angles=(20 40 60 80)
declare -a kbs=(0.5e-7 1e-8 0.5e-8 1e-9 0.5e-9 1e-10)
mkdir plots.dir/variedKB.dir
for angle in "${angles[@]}"
do
    echo $angle
    for kb in "${kbs[@]}"
    do
        echo $kb
        python exponentialGrowth.py ${angle} ${kb}
        python newPlotting.py
        mkdir plots.dir/variedKB.dir/angle${angle}_kb${kb}.dir
        mv plots.dir/*.pdf plots.dir/variedKB.dir/angle${angle}_kb${kb}.dir
        mv plots.dir/*.mp4 plots.dir/variedKB.dir/angle${angle}_kb${kb}.dir
    done
done
