from multiprocessing import Process
from multiprocessing import Queue

def f(x, n) :
    global test
    print("before", test)
    test.put(x ** n)
    print("after", test)
    return x ** n

test = Queue()

f2 = lambda x : f(x, 2)
f3 = lambda x : f(x, 3)

p1 = Process( target=f2, args=((2),) )
p1.start()
p2 = Process( target=f3, args=((2),) )
p2.start()
p1.join()
p2.join()

results = []
while not test.empty() :
    results.append( test.get() )
print(results)
