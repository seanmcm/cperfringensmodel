#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 09/07/2018

import sys
import numpy as np
import scipy as sp
import matplotlib
import random as rand
import matplotlib as mpl
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pickle

from main import *

def makePlot(file) :

    data = pickle.load(open(file, "rb"))
    margins, probs = data[0], data[1]

    plt.figure(1)
    plt.xlim(-12,12)
    plt.xlabel(r"Initial $L_1$ - Initial $L_2$")
    plt.ylabel(r"Probability of $L_1$ Final $>$ $L_2$ Final")
    plt.title(r"Neighbor Increases Growth")
    plt.scatter(margins, probs)
    #plt.show()
    plt.savefig(str(sys.argv[1])[:-3] + "png")

#makePlot("run2_outcomes_100.pkl")
try : makePlot( str(sys.argv[1]) )
except : print("File not provided as argument. Try again")
