(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     36676,        848]
NotebookOptionsPosition[     35047,        787]
NotebookOutlinePosition[     35560,        809]
CellTagsIndexPosition[     35473,        804]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"gmax", " ", "=", " ", "0.2"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Ftrans", " ", "=", " ", "5*^-9"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Fstall", " ", "=", " ", 
   RowBox[{"10", "^", 
    RowBox[{"-", "7"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"f", "[", "x_", "]"}], " ", ":=", " ", 
  RowBox[{
   RowBox[{
    RowBox[{"-", 
     RowBox[{"(", 
      FractionBox["gmax", 
       RowBox[{"Fstall", " ", "-", " ", "Ftrans"}]], ")"}]}], 
    RowBox[{"(", 
     RowBox[{"x", "-", "Ftrans"}], ")"}]}], "+", " ", "gmax", 
   " "}]}]}], "Input",
 CellChangeTimes->{{3.74568896555818*^9, 3.745689037574123*^9}, {
  3.745689085681671*^9, 3.745689113940447*^9}, {3.745856065166404*^9, 
  3.7458560887804213`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"f", "[", "x", "]"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"x", ",", "0", ",", "10"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.74568900379352*^9, 3.7456890205680923`*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwVxXsw1AkcAHDPHLbGI/K26yeuTZKuWl38vptOyjLJc9ZEu7EUFvvbnRMq
UckUqQzFOHmUU7MenbrcFb6lpLV7eZwr9FqpFB2xxyLu7o/PfBj8pL0xWhoa
Gv7/+f+yo/xlG6LPeAWPMDmWtK7W9l46J4x+kCy+QO7Q8mhuW3rUkk2nHybj
fZxY71idbWVtefa69Fwy9Iql+zPW87YET0nijP1lkuNvur6bNdrWOx9XOGFf
S3YQBVI5S92mN6h0HrdvJnXOKcZkLH2seFKe8sm+k6xqT6TJWJZYV587mOA6
QPJka+6E+zIxluy+PCYaJm/6urm6ntqIwpE1w011o2SYXlSmw2MPNGCccH7F
miBnv7InPvsC5hvJmBX7p8jspJG7Tylv5A8Gt2p3/UPqL2QbFSl9MOXhCX6S
8xzJ2+xSHczejY3JtCCLXV9JVWxM8CqBP1akx390C9MA74Guh5GxezCrfCFw
ekwTmBn1f/+yGIgava/hbpo2zE0XNevmB6GTWJAazNSFnfPzPrvIEHyrKg41
vLcM3I/UKvN7Q3H3yKEjF1K+gdKzfexHueEY8LZgrZ2dAYyLdNhzW7j48QE/
87HUEF72ZQS4dUagq3G/rXfUcuC0i+zDM/ahAaniDU2tgPD1jYvHTaJw5zh3
xiHECCYv3n4aELMfn92P1uS6GMP1efMvXE8e3jrdHklXGcPGilKrD8v4KNCV
vFYoTCBt+StH4Rs+HhCl36KVmML5X302S6QHcH3sWgWHtxJs5A3FaZJo/MPE
xVDiYQaC2ucDqQEx+HtdVqmLtjkce+C0etZCgOmazD1dL8whtNo9MHlagAZD
ab7CplVQNQHyd7JYHLnGKCzPsICWNGrPy8I4LPzWUqc5xBIuy6JvKYQHsaBE
0MGjW4E3N40BWw8ht3CfgvbeCnjBF17Ua8bj7I6r6vIWa/AyO5huMRCPwyPb
U+/n2MAn02i6VnUCVnpusJbzbCHsuW3N26OJKD0pMY5dZwfnh+a/8/UTYk7I
AT9tlR2w/YZMrpsm4aahitocuT3UZ6b36Iwn4aU1pVK5Hx1mDpdzJiKS8Vhb
3iW/Zjqoil5fGehMRn/rkKtPHBkgHXWwbd+UgmxhR4RxLgM4ETM2wpIULLfh
O3apGODndiOvUE+Ejg9qigIiHMDdcfJ4eqIIC9bdZ/a0OECfP+snXrcIBVM6
3Z1MAvoGIte2e1LIb/xCzz1HwN1Tq7a9r6QwIStoYfd5Ak57Dvddr6ZQEtT0
F+0iAXun6xKF1yg8rZLkFxQR8IHnU66qpbDOQz1fXEbASk+Jlk4jhWpc6r92
g4D4qZ7H9FYKC3poee0dBFjtzw/hDlJYUpUYd6qTgHfm3M+2LyisEiu8fWUE
NMhX57x5SeFt84I5mYIAn+/v3YlTUjjENY3r/ZMAkfmY9Y+jFDoPW3orhwno
7NqlvDhDoVtTml31CAGF2WbpYWoKt54cVMe8JyBq6xtT63kKOc5lDaMfCZiu
Sf2hcpFCUTzdbnKSAHp2zc8NumLM2HZcfXOKgDEPii3WE+PJ5co+sYqA2xNe
A1v0xXipvurM7CwBnMh+w1aaGCszdWJ/myPAwqyyOmuFGG8ExmzPWCBAKUv0
9DESY5PDI1uvRQKkWR79+iZibJlyUi8tEZDqoZskNxXjvxGFVdI=
      "]]}}, {}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{0, 10}, {-2.105263093877551*^7, 0.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.745689020986923*^9, 3.745689039882159*^9}, {
   3.7456890909409513`*^9, 3.7456891160711412`*^9}, 3.745856091782455*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"?", "Piecewise"}]], "Input",
 CellChangeTimes->{{3.745690298627573*^9, 3.745690300761746*^9}}],

Cell[BoxData[
 RowBox[{
  StyleBox["\<\"\\!\\(\\*RowBox[{\\\"Piecewise\\\", \\\"[\\\", \
RowBox[{\\\"{\\\", RowBox[{RowBox[{\\\"{\\\", \
RowBox[{SubscriptBox[StyleBox[\\\"val\\\", \\\"TI\\\"], StyleBox[\\\"1\\\", \
\\\"TR\\\"]], \\\",\\\", SubscriptBox[StyleBox[\\\"cond\\\", \\\"TI\\\"], \
StyleBox[\\\"1\\\", \\\"TR\\\"]]}], \\\"}\\\"}], \\\",\\\", \
RowBox[{\\\"{\\\", RowBox[{SubscriptBox[StyleBox[\\\"val\\\", \\\"TI\\\"], \
StyleBox[\\\"2\\\", \\\"TR\\\"]], \\\",\\\", SubscriptBox[StyleBox[\\\"cond\\\
\", \\\"TI\\\"], StyleBox[\\\"2\\\", \\\"TR\\\"]]}], \\\"}\\\"}], \\\",\\\", \
StyleBox[\\\"\[Ellipsis]\\\", \\\"TR\\\"]}], \\\"}\\\"}], \\\"]\\\"}]\\) \
represents a piecewise function with values \\!\\(\\*SubscriptBox[StyleBox[\\\
\"val\\\", \\\"TI\\\"], StyleBox[\\\"i\\\", \\\"TI\\\"]]\\) in the regions \
defined by the conditions \\!\\(\\*SubscriptBox[StyleBox[\\\"cond\\\", \\\"TI\
\\\"], StyleBox[\\\"i\\\", \\\"TI\\\"]]\\). \\n\\!\\(\\*RowBox[{\\\"Piecewise\
\\\", \\\"[\\\", RowBox[{RowBox[{\\\"{\\\", RowBox[{RowBox[{\\\"{\\\", \
RowBox[{SubscriptBox[StyleBox[\\\"val\\\", \\\"TI\\\"], StyleBox[\\\"1\\\", \
\\\"TR\\\"]], \\\",\\\", SubscriptBox[StyleBox[\\\"cond\\\", \\\"TI\\\"], \
StyleBox[\\\"1\\\", \\\"TR\\\"]]}], \\\"}\\\"}], \\\",\\\", StyleBox[\\\"\
\[Ellipsis]\\\", \\\"TR\\\"]}], \\\"}\\\"}], \\\",\\\", StyleBox[\\\"val\\\", \
\\\"TI\\\"]}], \\\"]\\\"}]\\) uses default value \
\\!\\(\\*StyleBox[\\\"val\\\", \\\"TI\\\"]\\) if none of the \
\\!\\(\\*SubscriptBox[StyleBox[\\\"cond\\\", \\\"TI\\\"], StyleBox[\\\"i\\\", \
\\\"TI\\\"]]\\) apply. The default for \\!\\(\\*StyleBox[\\\"val\\\", \
\\\"TI\\\"]\\) is 0. \"\>", "MSG"], "\[NonBreakingSpace]", 
  ButtonBox[
   StyleBox["\[RightSkeleton]", "SR"],
   Active->True,
   BaseStyle->"Link",
   ButtonData->"paclet:ref/Piecewise"]}]], "Print", "PrintUsage",
 CellChangeTimes->{3.745690301455694*^9},
 CellTags->"Info243745675901-9571462"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"g", "[", "x_", "]"}], ":=", " ", 
  RowBox[{"Piecewise", "[", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"gmax", ",", " ", 
       RowBox[{"x", " ", "\[LessEqual]", " ", "Ftrans"}]}], "}"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"f", "[", "x", "]"}], ",", " ", 
       RowBox[{"Ftrans", "<", "x", " ", "\[LessEqual]", " ", "Fstall"}]}], 
      "}"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"0", ",", " ", 
       RowBox[{"x", " ", ">", " ", "Fstall"}]}], "}"}]}], "}"}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.7456903055525503`*^9, 3.745690407466275*^9}, {
  3.745690437791748*^9, 3.745690444838595*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"p1", " ", "=", " ", 
  RowBox[{"Plot", "[", 
   RowBox[{
    RowBox[{"g", "[", "x", "]"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"x", ",", "0", ",", " ", "1.2*^-7"}], "}"}]}], "]"}]}]], "Input",\

 CellChangeTimes->{{3.745690415963071*^9, 3.745690418310473*^9}, {
  3.745690449100725*^9, 3.745690465443932*^9}, {3.74569065785233*^9, 
  3.745690658388069*^9}, {3.745856125885367*^9, 3.7458561699183817`*^9}}],

Cell[BoxData[
 GraphicsBox[{{{{}, {}, 
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[{{2.4489795918367343`*^-15, 0.2}, {1.1777208965816197`*^-9,
        0.2}, {2.3554393441836474`*^-9, 0.2}, {4.909002850255037*^-9, 0.2}, {
       4.961734693877551*^-9, 0.2}}], LineBox[CompressedData["
1:eJwVznlM03cYBvBCApXDcSwgIuOogwkCYgGFzX1fKp3SOg6L2IwWWq4ChQkt
lKNlDiM0cQERxrEK2BaVFgpyOUFAKIdybXOuHC5DKwK6eRBuhzDYzz/evPnk
ffPkcYhOocXp4nA4hM2HLfvOokp+3RudCknXrJWPwDsXqYfiFhlpqmovEcKH
YdDRSE9lT0XX6yfE3qohaJRf81J4B6NSpefWKn4I/v7qj+ohBg3NTxaKDOgP
wHi/LOaZSRjqN9TfoNfdhx61Dl1QRkdq98YFGv4+XC6zOZJ7OhzZXgxZqKEP
wmuXseN4LRNVDxE40rYBYCoHzdpYLKQw+3h6wnwA0m99weEaRSHzgh1GVGw/
6PwTGX43OBpdW7c+I+nrA9veSLJ/Uwy6VEw862TWB/4GeUWsXXEIR0w4+zZG
Dc2eaUu+VA4i5kuq+/J6QbWBKMGKeLQfz6ihOfXAgnS8nfA2AYUlCiuOabuh
Z+aEzeEgLqK4lsh+z+mCnyqezjxSJqHQ9/M3H9t3wl03vx6PN8lIpqyo9J3u
AC7r/JVR6jmkaifdJOe0QxRPnE0oSUHvaylSpecdaNn8M0qcmIpY8sKy3KXb
kGWs1ZHs4SHx1ohUt7QNYnZTGg07eWjxaOzVJ4GtYMGUa/Zy+chqfF0Z/aYZ
foktcFm2TEMN6z/EtxOaoDZiusC5Iw3d1puLYFQ2QMANa+42Jx15TbrlhD6p
g91/3aMRDQXoWLyn6L9ABUyNrSQdqheg9I9a+EVjNyDB0Ao3Ss9ABzLtSgTm
NfCJr1w6sJGBhv1+/HSfqRQkP1t1htdlIh/LkfSVQgn4ENa0IYFZSJ2cIT6U
WArTAbFHTBeykIf2M/PM7stw8l4f/tuqbLT4yrFdO3YRJh3y5g6AEOUuOfbY
uaaBbO455RVJiHD8mMFKDR8SFaQmFVmIZMuy0T0iPmy57mS7U4Xo2cq+KZNR
Hjj4CE28woSIvW62uBOfCslBqZ9DkhCp1zf91SeTwNvsoRR3Dsv7NyOko5wL
2xo3vf5UIfLbWGI2zydC8TevH5IzsP/NlwLZxQS4E8uJo17AjBtX5PbGgY6I
eSWsAutn2GBEOsqG0S+71iyvYnlWQR58YxaU4qwZj6swW5Mp0fUR4CSecmTU
YLbzEB1/EQ5fF9O62I2YnXdpdSPDwOJMiz2hGcs7uP1ueTMUnlqaimdbsbv7
qumshAa8ql9DOB2YOep8sSoYyhUBL5IGMAtTA/GVZGBzFafcHmA+z/z+5SQJ
nN30WxaGMesXRxc5IuhuHRDxfvvQh+RiauEJ+QLCDPER5gCijXzvQQjyuXBi
VYOZ/XzWwcAeiGzblNMTmLOoLodtLeB/Kbm9yA==
       "]], LineBox[CompressedData["
1:eJxTTMoPSmViYGDQAGIQHfcv6tjkW1V2DFCwqmFumcUdBL/E+c/tn/cQfLaT
O5fVPEHwda6Y2DS9R/DvPQw9ZvYJwe9/Xxbw+guC/4lrZ0rILwR/h6Ntrxpr
NZzvvN713ikpBD+qK6xB2A3Bb3eoOLJkDoLPOlchQs2uBs73dt+e5voWwd/y
cFPD4Qm1cL6SmeNpLsM6OD/8xt4J7+8h+Buy5icaN9bD+UmfLN+ytyD4IlVX
Ku60IfjlnZwTWnsQfLsVxfuuT0fwzz51l65Zh+C/Svhw5chtBH/Oi66EmfcQ
fL8C1Te5DxH8TfVRzOLPEfzKeUcMMz8h+Cx5PsfbWBrg/LsbJXk/siL4274+
C4pmR/Azahvv6XMh+Kd7tn29wY/gL7nQbO0siODXiAQ2rhVC8PXmvOZpEkXw
2R/sCHojhuA/UG6bESaB4O9MD753QBLBn7RaQUVbGsEHALmmmVQ=
       
       "]]}, {}}, {{}, {}, {}}}, {}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{0, 1.2*^-7}, {0., 0.2}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.745690418733027*^9, 3.7456904659982862`*^9}, 
   3.745690658689962*^9, {3.7458561309714813`*^9, 3.745856170235354*^9}, {
   3.7462015187945957`*^9, 3.746201523481205*^9}}]
}, Open  ]],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{{3.745688841248012*^9, 3.745688876275153*^9}, {
  3.7456889147948*^9, 3.745688961601474*^9}, {3.745688996432816*^9, 
  3.74568900050305*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"h", "[", "x_", "]"}], ":=", " ", 
  FractionBox["gmax", 
   RowBox[{"1", "+", 
    SuperscriptBox["\[ExponentialE]", 
     RowBox[{
      FractionBox["7", 
       RowBox[{"Fstall", "-", "Ftrans"}]], 
      RowBox[{"(", 
       RowBox[{"x", "-", 
        FractionBox[
         RowBox[{"Fstall", "+", "Ftrans"}], "2"]}], ")"}]}]]}]]}]], "Input",
 CellChangeTimes->{{3.7456905038472137`*^9, 3.745690641213685*^9}, {
  3.7456906939569597`*^9, 3.745690755840889*^9}, {3.746201529297805*^9, 
  3.746201530134995*^9}, {3.7462015949606247`*^9, 3.746201625479268*^9}, {
  3.746201696725977*^9, 3.7462017085818253`*^9}, {3.746201761867816*^9, 
  3.74620179249664*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"h", "[", "x", "]"}], "//", "N"}]], "Input",
 CellChangeTimes->{{3.745856565153926*^9, 3.7458565847402678`*^9}}],

Cell[BoxData[
 FractionBox["gmax", 
  RowBox[{"1.`", "\[VeryThinSpace]", "+", 
   SuperscriptBox["2.718281828459045`", 
    FractionBox[
     RowBox[{"7.`", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"0.5`", " ", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{
            RowBox[{"-", "1.`"}], " ", "Fstall"}], "-", 
           RowBox[{"1.`", " ", "Ftrans"}]}], ")"}]}], "+", "x"}], ")"}]}], 
     RowBox[{"Fstall", "-", 
      RowBox[{"1.`", " ", "Ftrans"}]}]]]}]]], "Output",
 CellChangeTimes->{{3.74585656565143*^9, 3.7458565850442657`*^9}, 
   3.7462015348012753`*^9, 3.747058244590167*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"h", "'"}], "[", "mid", "]"}]], "Input",
 CellChangeTimes->{{3.745856677587557*^9, 3.745856681084506*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"7", " ", 
    SuperscriptBox["\[ExponentialE]", 
     FractionBox[
      RowBox[{"7", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          FractionBox["1", "2"], " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "Fstall"}], "-", "Ftrans"}], ")"}]}], "+", 
         FractionBox[
          RowBox[{"Fstall", "+", "Ftrans"}], "2"]}], ")"}]}], 
      RowBox[{"Fstall", "-", "Ftrans"}]]], " ", "gmax"}], 
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"1", "+", 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         RowBox[{"7", " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             FractionBox["1", "2"], " ", 
             RowBox[{"(", 
              RowBox[{
               RowBox[{"-", "Fstall"}], "-", "Ftrans"}], ")"}]}], "+", 
            FractionBox[
             RowBox[{"Fstall", "+", "Ftrans"}], "2"]}], ")"}]}], 
         RowBox[{"Fstall", "-", "Ftrans"}]]]}], ")"}], "2"], " ", 
    RowBox[{"(", 
     RowBox[{"Fstall", "-", "Ftrans"}], ")"}]}]]}]], "Output",
 CellChangeTimes->{
  3.745856681954754*^9, 3.746201537531807*^9, {3.747058246489669*^9, 
   3.747058250449066*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"mid", " ", "=", " ", 
  RowBox[{
   FractionBox[
    RowBox[{"Fstall", " ", "+", " ", "Ftrans"}], "2"], "//", "N"}]}]], "Input",\

 CellChangeTimes->{{3.745856427292392*^9, 3.7458564395099163`*^9}, 
   3.745856490697256*^9, {3.747058438182727*^9, 3.7470584387175703`*^9}}],

Cell[BoxData["5.25`*^-8"], "Output",
 CellChangeTimes->{
  3.74585649137239*^9, 3.746201560118635*^9, {3.747058248836412*^9, 
   3.747058258589592*^9}, 3.7470584391829453`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"N", "[", "mid", "]"}]], "Input",
 NumberMarks->False],

Cell[BoxData["5.25`*^-8"], "Output",
 CellChangeTimes->{3.745856493039834*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"p2", " ", "=", " ", 
  RowBox[{"Plot", "[", 
   RowBox[{
    RowBox[{"h", "[", "x", "]"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"x", ",", 
      RowBox[{"mid", "-", 
       RowBox[{"10", "^", 
        RowBox[{"-", "5"}]}]}], ",", 
      RowBox[{"mid", "+", 
       RowBox[{"10", "^", 
        RowBox[{"-", "5"}]}]}]}], "}"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"Show", "[", 
  RowBox[{"{", 
   RowBox[{"p1", ",", " ", "p2"}], "}"}], "]"}]}], "Input",
 CellChangeTimes->{{3.7456905355247717`*^9, 3.745690541556374*^9}, {
  3.7456905726871147`*^9, 3.745690621663191*^9}, {3.745690660993307*^9, 
  3.745690670795577*^9}, {3.745856204792419*^9, 3.745856231522188*^9}, {
  3.745856274117469*^9, 3.7458565377234077`*^9}, {3.745856626459455*^9, 
  3.745856629789055*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJxF03k41HsXAHBLdlq5ypJ9La5LUsycX5bse0S7tUmjEtmprGPLvkvGWCpL
JkmW/NIMY2mhS5HdkCskubRo6B33fd+553nO830+f53lOV8Ztyv2nhxsbGzN
zNx8e6Q3FmNGptH8vM3oQszG1+Lk/f41tfCbLJXvX9NCtFeOKkyx7JO5zPnJ
hs5yytf2q9Trkyy3NIyGjRdMsCx/5WMSkjrO8sJ+4kmL9lGWC0Ojph9zjPxr
Q8/5C2eHWG5+0bP/bvYgy0qvDs2rfXrHcti51567LN+yvPRwQSya0MdybTif
FXbyDcvpfDWNEaK9LNdbcrSbVb1k+UNQyXaZ810s2yocf7P1fTvLzm1cSbfi
KCzrdtwKPEpvYfmCGaVjrr+e5R7ykkDtETLL5/6IvmaJJbE8UHNMoOZNIZr7
//4Zd9TalXNQ08z/OrykDWz7M9DCF7n/mN98zmb4Riqq5Bfyj4m6a2T9ylso
pXMgd9Nqk1KUl8cT0PcPpjI33WvuFVsTEY/2bVFJ3/S1ulrz9Ko4dEvG+1ub
Ft3L2Oo/QEAL3vITNt1EONrnxEFAdS+ah236zJfkHF21WPSIsw1+02ynBk9J
Oseg8y3htpsuaZORZouKRjEBsoqbNlbHT9Oro9AiBmYql+mzE93cqyORqONF
5bBN+6erqvAIRqJ5FT1DOUwnGSVY7NGLQLXOGY5lM1369eOlfRdvom1nvtpn
MR1klyEdqnkD7aDrzKYzffexvWdRajg6r9UslcL0uz07K6iLoejpw71a8Uxz
X3+z+JdlCCrkw//5BtPa9FQtwcog1Gdd7NJVpj2MbYM0+AJRTwbjyUmmMyu2
tTjg/FHqhrWazub9b+1hD273Q02xJ5N+5XYhy77JxoVyvqjmUHLoPaZlB6wS
n0f4oPckCg9JM00KECO+eHYJTUFdfprnMPeD5KkIh11EbXHWdutZzPl5dj86
fQiHFq1V2/6dwfwfPdmYshV3lJH/foaS2oVI5IrQPpFdUA6r8nxaYhey0qpc
5D10EjVVD6IxorqQh3aKtev2DqjmmIXv28AuZKTNJEEyxwLd0fJnwYxHF1L1
MsblxnYsaid53ynMhFk/fMJdMpsHzRov8P6xpwvxrr7F811VB+Txa9oc/Z0I
5vGL0ROlptBQLJ5606sTmTSbHS54bw/fH1xLp3V1IPKNc538ricAJ95sFDtO
Q3DKnx4Hz54Dujr7OiO9HXn7ItBuRccdwrbWO0n1UpEAzh8dlaHnYdkpXs43
7TmyWy8E3J95QdGeH9Y/B1qQJt+fdWKclwC/RHA6rtiAqDuiIl5yPvAyBikl
5dYiG0sbvUbtV6Ez5LCG1olKpCcJSZLG+cGETQr78eMlCFH5pgmD1x8u8gZ2
a0fnIz5trRyDFQFQ+mgKiW9PQ/Rd2NFHlkGwRN5oO3yVgOxk6AenLAZD+pZr
1VNZ1xF6TuQBfGooaFH7XGte+yGPtKifjTXDYbFYPF42zguJ7uGslO2/DgSa
txIO54JUaMtq/dK9CcMtjkf1+RyR9CjK9k8CEeBACr+3sWSOhL5xWxwaiYDz
gzjyKEYf8ZDifNlZHQndsidvfxA4iFheKrlffz0KslZ51cZ+348caDYklNpE
g0VEoUMfpywiyTftkS4dA+rpChKHuEURbqdog5tfYmCB0WpO8BZCFkvlpS9T
YsFCcFCj/yUH8m65bf1UBgGovVIJDK7vgB7xHDbziINfmWHKgbWLUJ7M1aij
HQ9Rry/P192egpSRsmwF7gTQMqv/ZrPtPQSpGl/bNZAA1pOaA0UcveASNGPH
fi8RqqjaA4L+bWBKi/39c1AS6NZYJ1TINYGGsJLQqNktUMEvdCk8IsNut465
brFkGKSjcRnHy4CdjOtsmE+G8Pr1iB7DAphb5ykvf5oCs3FECV18GnxU6Nfv
Nk6Fsjsu7TT9WHgnweG2tyEVGhIne1CXUKDs0oj0VUkDK+Xs01XvfKCG/yyJ
lp8GV6S8UsPrPeA2exJFTDAdsMPOupEhJyDueyP9cng6PM0ra9FzsgH/z39x
UBfToaPIYgfR2BDcZkTkRF0yIENmo4+tXgesRw0N8W8yICc3lrtQVQ10+6+6
PzPIhGj99h6TJRlQelEUtasuE4gv9Jemtv8GwpRXJTiFLOjMlPy7QF4A2Bp/
Upuzs+CKqPD9modssFCjMr2NNxuUE56JbcStYN+XO23xCM4Gu9m01gSDj1ha
YYx8w1w2kCwjy913jmNrMx8ZCZ7OgYIDB0JMJfqwdxInPVxe5YAWaZ/mfEQH
NjFyW0wd5IKu6nh04txTbFAwtoyXnAtmEKYx0fkQ6+GDbz8tkwfCPbdt8BNl
WDtc3gdyeh6URv1pzpjNx8LZDi6uLflgzX9zjPNWKlbVcVXhhH8+nPvK70g/
FY0VtZQzrp7JhwSxFq6tAsHYLYZ259mdC6CNZ7JE9vVl7NLhG7GOXQVwuah5
wGrZDTuqUV1+X/c2GFWvLJpYO2G7lYZp65W3Iet06BPuSkts/V6+v+wkC6Ha
VkKyp1gfWyKiw1OeXAiCK9sUz5C1samCnkprvwphpIOcN16tig3jzDCxvnoH
SM1/E+9dkMZeWGvFkeh3YH/KkTKV34Wxjl8WCV+PFYHD0Ak1gX5erMGsxD3z
9iIQXhBKCUzYwORcUfCwESdCk2krvqZzGbMVj7n24xgRRK483/Ob+F+Y6PPH
okuSiEBxPXZVyHsEs+Z6MdOqnQiEtYyhXaFvMD5nIkq/rRNBeYY/qeFmO2bG
Obeu+GAxDK7Rnhv5NWHOONS0WVwphq91ikcKjciYfhta/+rdYhBc0tHs4yjF
mFuMThdNFEP2uMD+iMJcTKvxyorZHhLwlqztdTVJxhw0EOBasSNBBvE13rso
EiN3+LCCKZUEB1y/37ij5o3JP2CrvfyTBE0ncVXVDq6Y7Rq4o7cPlEDvAjkv
zM4Rw1DM8lwqKwEryyrBSekjmHfCX8py40qB4CK8LTpgJyZu6Z7iR6tyqHit
mdj3R4Nev4S0UndkOdwV5gtwRR/oyZjlKFU+KQd7XMX9c3Elek3FMcqXZO7C
+lNKPyU+RW/B3k31y8pd0Dep/CGf7KFn+1hc/UfBffjI9we+IGCH3u6QZG2+
uSqIdO5dJWp76faz65KEqQ+hFXki52Akdng+gUHj+dQA5ResPvpxGR082yv4
88bqMygLwX+IEwvVsspNk8QbUGGpNjH3vD1dQ900sfNXLw0uji0HaHvsUPfl
igvmlOsGWzXFa6Qwn33TxH21Zh6vgO5U5SBJGVYWGhtrwgT0grtB2gz7he2K
J33vSwlK/AkfxpQW7c76ycUZz5vZfuiD1ZhDAw1CROkLxerfLFLfQoKHYzk9
Y03yHP7o7KzmAEhldL30jncXP+Tfd2ft+yC0DLDbX1LN3E3hch+kEocgUVHR
uq5rRUT9ndcAu/cIKHB/3qcpunfXQxEjsUn1MVgN7FedPn5s+8yw1ERg3zgI
2HB11PQ1CG34ND5+/moCXJ0NJvEuM/xuT6YwJaRJEHeTp52VN+K1f/zN09GP
Dj1y5ivkPWQuzWMdX547TsEZyVgew510DnydKMKvMg0xr3APxvSN2MSytCoa
90+DdKuWTNDlQ2zd/jYiXhpMBw3mGQaqsSnrEOY7dKZhgnPiiPxlEbaZxq85
McbTwPa/+A9miMtq
      "]]}}, {}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& )}},
  PlotRange->NCache[{{
      Rational[-3979, 400000000], 
      Rational[4021, 400000000]}, {0., 0.2}}, {{-9.9475*^-6, 0.0000100525}, {
    0., 0.2}}],
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.745856340314691*^9, 3.7458563514308033`*^9}, {
   3.745856481948757*^9, 3.745856537945879*^9}, 3.745856630282629*^9, {
   3.746201549443297*^9, 3.746201564212467*^9}, {3.746201599735187*^9, 
   3.7462016317722893`*^9}, {3.746201701435017*^9, 3.746201712233*^9}, {
   3.746201765756857*^9, 3.746201798914721*^9}}],

Cell[BoxData[
 GraphicsBox[{{{{{}, {}, 
      {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], 
       Opacity[1.], 
       LineBox[{{2.4489795918367343`*^-15, 0.2}, {1.1777208965816197`*^-9, 
        0.2}, {2.3554393441836474`*^-9, 0.2}, {4.909002850255037*^-9, 0.2}, {
        4.961734693877551*^-9, 0.2}}], LineBox[CompressedData["
1:eJwVznlM03cYBvBCApXDcSwgIuOogwkCYgGFzX1fKp3SOg6L2IwWWq4ChQkt
lKNlDiM0cQERxrEK2BaVFgpyOUFAKIdybXOuHC5DKwK6eRBuhzDYzz/evPnk
ffPkcYhOocXp4nA4hM2HLfvOokp+3RudCknXrJWPwDsXqYfiFhlpqmovEcKH
YdDRSE9lT0XX6yfE3qohaJRf81J4B6NSpefWKn4I/v7qj+ohBg3NTxaKDOgP
wHi/LOaZSRjqN9TfoNfdhx61Dl1QRkdq98YFGv4+XC6zOZJ7OhzZXgxZqKEP
wmuXseN4LRNVDxE40rYBYCoHzdpYLKQw+3h6wnwA0m99weEaRSHzgh1GVGw/
6PwTGX43OBpdW7c+I+nrA9veSLJ/Uwy6VEw862TWB/4GeUWsXXEIR0w4+zZG
Dc2eaUu+VA4i5kuq+/J6QbWBKMGKeLQfz6ihOfXAgnS8nfA2AYUlCiuOabuh
Z+aEzeEgLqK4lsh+z+mCnyqezjxSJqHQ9/M3H9t3wl03vx6PN8lIpqyo9J3u
AC7r/JVR6jmkaifdJOe0QxRPnE0oSUHvaylSpecdaNn8M0qcmIpY8sKy3KXb
kGWs1ZHs4SHx1ohUt7QNYnZTGg07eWjxaOzVJ4GtYMGUa/Zy+chqfF0Z/aYZ
foktcFm2TEMN6z/EtxOaoDZiusC5Iw3d1puLYFQ2QMANa+42Jx15TbrlhD6p
g91/3aMRDQXoWLyn6L9ABUyNrSQdqheg9I9a+EVjNyDB0Ao3Ss9ABzLtSgTm
NfCJr1w6sJGBhv1+/HSfqRQkP1t1htdlIh/LkfSVQgn4ENa0IYFZSJ2cIT6U
WArTAbFHTBeykIf2M/PM7stw8l4f/tuqbLT4yrFdO3YRJh3y5g6AEOUuOfbY
uaaBbO455RVJiHD8mMFKDR8SFaQmFVmIZMuy0T0iPmy57mS7U4Xo2cq+KZNR
Hjj4CE28woSIvW62uBOfCslBqZ9DkhCp1zf91SeTwNvsoRR3Dsv7NyOko5wL
2xo3vf5UIfLbWGI2zydC8TevH5IzsP/NlwLZxQS4E8uJo17AjBtX5PbGgY6I
eSWsAutn2GBEOsqG0S+71iyvYnlWQR58YxaU4qwZj6swW5Mp0fUR4CSecmTU
YLbzEB1/EQ5fF9O62I2YnXdpdSPDwOJMiz2hGcs7uP1ueTMUnlqaimdbsbv7
qumshAa8ql9DOB2YOep8sSoYyhUBL5IGMAtTA/GVZGBzFafcHmA+z/z+5SQJ
nN30WxaGMesXRxc5IuhuHRDxfvvQh+RiauEJ+QLCDPER5gCijXzvQQjyuXBi
VYOZ/XzWwcAeiGzblNMTmLOoLodtLeB/Kbm9yA==
        "]], LineBox[CompressedData["

1:eJxTTMoPSmViYGDQAGIQHfcv6tjkW1V2DFCwqmFumcUdBL/E+c/tn/cQfLaT
O5fVPEHwda6Y2DS9R/DvPQw9ZvYJwe9/Xxbw+guC/4lrZ0rILwR/h6Ntrxpr
NZzvvN713ikpBD+qK6xB2A3Bb3eoOLJkDoLPOlchQs2uBs73dt+e5voWwd/y
cFPD4Qm1cL6SmeNpLsM6OD/8xt4J7+8h+Buy5icaN9bD+UmfLN+ytyD4IlVX
Ku60IfjlnZwTWnsQfLsVxfuuT0fwzz51l65Zh+C/Svhw5chtBH/Oi66EmfcQ
fL8C1Te5DxH8TfVRzOLPEfzKeUcMMz8h+Cx5PsfbWBrg/LsbJXk/siL4274+
C4pmR/Azahvv6XMh+Kd7tn29wY/gL7nQbO0siODXiAQ2rhVC8PXmvOZpEkXw
2R/sCHojhuA/UG6bESaB4O9MD753QBLBn7RaQUVbGsEHALmmmVQ=
        
        "]]}, {}}, {{}, {}, {}}}, {}, {}}, {{{}, {}, 
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJxF03k41HsXAHBLdlq5ypJ9La5LUsycX5bse0S7tUmjEtmprGPLvkvGWCpL
JkmW/NIMY2mhS5HdkCskubRo6B33fd+553nO830+f53lOV8Ztyv2nhxsbGzN
zNx8e6Q3FmNGptH8vM3oQszG1+Lk/f41tfCbLJXvX9NCtFeOKkyx7JO5zPnJ
hs5yytf2q9Trkyy3NIyGjRdMsCx/5WMSkjrO8sJ+4kmL9lGWC0Ojph9zjPxr
Q8/5C2eHWG5+0bP/bvYgy0qvDs2rfXrHcti51567LN+yvPRwQSya0MdybTif
FXbyDcvpfDWNEaK9LNdbcrSbVb1k+UNQyXaZ810s2yocf7P1fTvLzm1cSbfi
KCzrdtwKPEpvYfmCGaVjrr+e5R7ykkDtETLL5/6IvmaJJbE8UHNMoOZNIZr7
//4Zd9TalXNQ08z/OrykDWz7M9DCF7n/mN98zmb4Riqq5Bfyj4m6a2T9ylso
pXMgd9Nqk1KUl8cT0PcPpjI33WvuFVsTEY/2bVFJ3/S1ulrz9Ko4dEvG+1ub
Ft3L2Oo/QEAL3vITNt1EONrnxEFAdS+ah236zJfkHF21WPSIsw1+02ynBk9J
Oseg8y3htpsuaZORZouKRjEBsoqbNlbHT9Oro9AiBmYql+mzE93cqyORqONF
5bBN+6erqvAIRqJ5FT1DOUwnGSVY7NGLQLXOGY5lM1369eOlfRdvom1nvtpn
MR1klyEdqnkD7aDrzKYzffexvWdRajg6r9UslcL0uz07K6iLoejpw71a8Uxz
X3+z+JdlCCrkw//5BtPa9FQtwcog1Gdd7NJVpj2MbYM0+AJRTwbjyUmmMyu2
tTjg/FHqhrWazub9b+1hD273Q02xJ5N+5XYhy77JxoVyvqjmUHLoPaZlB6wS
n0f4oPckCg9JM00KECO+eHYJTUFdfprnMPeD5KkIh11EbXHWdutZzPl5dj86
fQiHFq1V2/6dwfwfPdmYshV3lJH/foaS2oVI5IrQPpFdUA6r8nxaYhey0qpc
5D10EjVVD6IxorqQh3aKtev2DqjmmIXv28AuZKTNJEEyxwLd0fJnwYxHF1L1
MsblxnYsaid53ynMhFk/fMJdMpsHzRov8P6xpwvxrr7F811VB+Txa9oc/Z0I
5vGL0ROlptBQLJ5606sTmTSbHS54bw/fH1xLp3V1IPKNc538ricAJ95sFDtO
Q3DKnx4Hz54Dujr7OiO9HXn7ItBuRccdwrbWO0n1UpEAzh8dlaHnYdkpXs43
7TmyWy8E3J95QdGeH9Y/B1qQJt+fdWKclwC/RHA6rtiAqDuiIl5yPvAyBikl
5dYiG0sbvUbtV6Ez5LCG1olKpCcJSZLG+cGETQr78eMlCFH5pgmD1x8u8gZ2
a0fnIz5trRyDFQFQ+mgKiW9PQ/Rd2NFHlkGwRN5oO3yVgOxk6AenLAZD+pZr
1VNZ1xF6TuQBfGooaFH7XGte+yGPtKifjTXDYbFYPF42zguJ7uGslO2/DgSa
txIO54JUaMtq/dK9CcMtjkf1+RyR9CjK9k8CEeBACr+3sWSOhL5xWxwaiYDz
gzjyKEYf8ZDifNlZHQndsidvfxA4iFheKrlffz0KslZ51cZ+348caDYklNpE
g0VEoUMfpywiyTftkS4dA+rpChKHuEURbqdog5tfYmCB0WpO8BZCFkvlpS9T
YsFCcFCj/yUH8m65bf1UBgGovVIJDK7vgB7xHDbziINfmWHKgbWLUJ7M1aij
HQ9Rry/P192egpSRsmwF7gTQMqv/ZrPtPQSpGl/bNZAA1pOaA0UcveASNGPH
fi8RqqjaA4L+bWBKi/39c1AS6NZYJ1TINYGGsJLQqNktUMEvdCk8IsNut465
brFkGKSjcRnHy4CdjOtsmE+G8Pr1iB7DAphb5ykvf5oCs3FECV18GnxU6Nfv
Nk6Fsjsu7TT9WHgnweG2tyEVGhIne1CXUKDs0oj0VUkDK+Xs01XvfKCG/yyJ
lp8GV6S8UsPrPeA2exJFTDAdsMPOupEhJyDueyP9cng6PM0ra9FzsgH/z39x
UBfToaPIYgfR2BDcZkTkRF0yIENmo4+tXgesRw0N8W8yICc3lrtQVQ10+6+6
PzPIhGj99h6TJRlQelEUtasuE4gv9Jemtv8GwpRXJTiFLOjMlPy7QF4A2Bp/
Upuzs+CKqPD9modssFCjMr2NNxuUE56JbcStYN+XO23xCM4Gu9m01gSDj1ha
YYx8w1w2kCwjy913jmNrMx8ZCZ7OgYIDB0JMJfqwdxInPVxe5YAWaZ/mfEQH
NjFyW0wd5IKu6nh04txTbFAwtoyXnAtmEKYx0fkQ6+GDbz8tkwfCPbdt8BNl
WDtc3gdyeh6URv1pzpjNx8LZDi6uLflgzX9zjPNWKlbVcVXhhH8+nPvK70g/
FY0VtZQzrp7JhwSxFq6tAsHYLYZ259mdC6CNZ7JE9vVl7NLhG7GOXQVwuah5
wGrZDTuqUV1+X/c2GFWvLJpYO2G7lYZp65W3Iet06BPuSkts/V6+v+wkC6Ha
VkKyp1gfWyKiw1OeXAiCK9sUz5C1samCnkprvwphpIOcN16tig3jzDCxvnoH
SM1/E+9dkMZeWGvFkeh3YH/KkTKV34Wxjl8WCV+PFYHD0Ak1gX5erMGsxD3z
9iIQXhBKCUzYwORcUfCwESdCk2krvqZzGbMVj7n24xgRRK483/Ob+F+Y6PPH
okuSiEBxPXZVyHsEs+Z6MdOqnQiEtYyhXaFvMD5nIkq/rRNBeYY/qeFmO2bG
Obeu+GAxDK7Rnhv5NWHOONS0WVwphq91ikcKjciYfhta/+rdYhBc0tHs4yjF
mFuMThdNFEP2uMD+iMJcTKvxyorZHhLwlqztdTVJxhw0EOBasSNBBvE13rso
EiN3+LCCKZUEB1y/37ij5o3JP2CrvfyTBE0ncVXVDq6Y7Rq4o7cPlEDvAjkv
zM4Rw1DM8lwqKwEryyrBSekjmHfCX8py40qB4CK8LTpgJyZu6Z7iR6tyqHit
mdj3R4Nev4S0UndkOdwV5gtwRR/oyZjlKFU+KQd7XMX9c3Elek3FMcqXZO7C
+lNKPyU+RW/B3k31y8pd0Dep/CGf7KFn+1hc/UfBffjI9we+IGCH3u6QZG2+
uSqIdO5dJWp76faz65KEqQ+hFXki52Akdng+gUHj+dQA5ResPvpxGR082yv4
88bqMygLwX+IEwvVsspNk8QbUGGpNjH3vD1dQ900sfNXLw0uji0HaHvsUPfl
igvmlOsGWzXFa6Qwn33TxH21Zh6vgO5U5SBJGVYWGhtrwgT0grtB2gz7he2K
J33vSwlK/AkfxpQW7c76ycUZz5vZfuiD1ZhDAw1CROkLxerfLFLfQoKHYzk9
Y03yHP7o7KzmAEhldL30jncXP+Tfd2ft+yC0DLDbX1LN3E3hch+kEocgUVHR
uq5rRUT9ndcAu/cIKHB/3qcpunfXQxEjsUn1MVgN7FedPn5s+8yw1ERg3zgI
2HB11PQ1CG34ND5+/moCXJ0NJvEuM/xuT6YwJaRJEHeTp52VN+K1f/zN09GP
Dj1y5ivkPWQuzWMdX547TsEZyVgew510DnydKMKvMg0xr3APxvSN2MSytCoa
90+DdKuWTNDlQ2zd/jYiXhpMBw3mGQaqsSnrEOY7dKZhgnPiiPxlEbaZxq85
McbTwPa/+A9miMtq
       "]]}}, {}, {}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{0, 1.2*^-7}, {0., 0.2}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.745856340314691*^9, 3.7458563514308033`*^9}, {
   3.745856481948757*^9, 3.745856537945879*^9}, 3.745856630282629*^9, {
   3.746201549443297*^9, 3.746201564212467*^9}, {3.746201599735187*^9, 
   3.7462016317722893`*^9}, {3.746201701435017*^9, 3.746201712233*^9}, {
   3.746201765756857*^9, 3.746201798930196*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"q", 
    RowBox[{"(", 
     RowBox[{"1", "-", "q"}], ")"}]}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"q", ",", 
     RowBox[{"1", "/", "2"}], ",", "1"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.745692587133135*^9, 3.7456926039105062`*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJwV1Hk41VkYB3BLEq0UJiSRtU2LGtX0RaUmUUSo5pJCZbJEy7ipZKkU2kaT
paSbUFJJCsmW9fKjElE5N+vl8kNS9jmd5znPeT7/nOc57/m+7zxnT2sXCTEx
sVi6f50rvPykxMQIRsZ/LQY88R+xJuIEmttOVfzyrGs+KwMlCLgpI4a/HDS/
p7JQkkBKhWs0Rv0t/W+3iVIEuTcGa0eonTcJxzdNJHikeUJumPptnct/F6QJ
KnIGWn5Smx76alA+iUDt4FHbAeqnw5zSKbIEdzT6Hb9Ra4Q17LWcTLCz84hU
D/VVNfuhiCkEawt6N4qoJR6/v1o9lcAm2WteO7W3idWCmdMJbsWxN5qoyduK
ApsZBMoJHklfqLfv37InUo6gOLPL9iO1wbn1YbNnEbyQE52uoI77LVdrtwLB
0A730SLqGclrc2IUCTzvdSjkUrN8Q3bubILuo0KlJ9QcztNze5UJfvQfEE+i
rmQXq99VIVge2B4cR/1IXtdKW43AuKLNMZxajccTus0lkLvglh5EHWY472yS
OsEsm7Z7ftR/2yunLdQkSFVqs3OhXnBrisJyXYLVtm3jhtRRSy6k+OoR3Ax1
W6RHLZsnZfZcn0CfafuuQt3RNHbMaBHB1+B2OzHqRL3eOiwjSEjqcCwcY/Bb
1mHvgOUE1+zdXZ5Rn9/aIVOwgiBNUSTFo3bzbFpttorgXG6X0llq7fSamK1r
CYLKek8bUUeaWRuG/0Ew3nfkL23qiXWVFcw6ghT9/hp56pah4jFrE4LY4oEb
HaM0P8aZTg5mBO8yRhz/pdbg35rvup3guvdksboRBqobNcryrQhudAzvz6ZW
yrnnOXcHgX/pqh1x1FMeP8yssyXoe3Gl3JX6x7XM7ea7CQI4lQu6hxnwd384
aeBK6114KLl1iEHReweNMDcCt2g5XgF1nsXnYuEBglN1kjpx1M/RLH/XneYp
qKzEjvqOZl+igjdBcjzh5g0yONY5tWaIS9A+uCTu1E8GXvuv+Nn5E1gsCVts
Q+3+eZb6s1MEkWWGZnrUToyK++EAgqplB+Xe/mBgnqYnTkIIOkrtldSo1bkb
F725Qusrv1tw+zuDcln/4Ij79P3TRIe39NF8JljPqEmk93fqfJ9F3W+qG62c
TBBj0zKrsZfmhfvu8b2HNI/85jlHqL069T5lPSHIshmwvdbDQJH/YVl7FoFl
7j5OfjfttzADYlxFkC59oOptB4PjehPdQ6pp/x6vcbtMfelNw/fytwS6PoU3
LKjTR0Mm29UQ9M7QySsSMpjk8Xnl4XqCC01jRs/aaX4tQ8NuNhOUNv8s4LYy
GJzevLrvJ0GonaLRKwEDy6w+56VDBE6ftL08qeNdxS96DdP/0c9ZP496yyu1
hu5RgnsaAbmBhEHMIQdup4QA3mftZ2xoZGD8pjKreYoAc8QsZJ5+YhDi93Jt
rboA/G9bndbXMqjXKtmvqCGAVPxKXeEHBkuqP1yy1RQgdPb9S+HUdTr9n95p
CeC7X3VBXQ0DvZrF/lX6AlQctM9xfU/zsYT3qnSFAJHLFNd5VzOY2RK+Lnuz
ANzsxPg15Qz6jvN8FLcIUHLy5Hp+GZ1PspmJXuYC5O1a77mH+rJBi7yWpQDP
PJPduKUMpp1c0xa2Q4D40c8NacW0XjPbIxw5AkQl1VdPK2QwZmwikPAVYNRE
v//vbAbtMd8C/7wtwEVfDfmyBwy+Wu++dSlOgJvLJT+YUTdMKnjB3BEgTKZ3
eUEynR++V7tseQKoD317lZnEIG3rUjvnJHpfV6TsvfsMTo146HPTBDivbhXi
epeBwp6OqofFArQ/jpSOjaLzVEUwZ0aPAHWnXJfWnWNgd/L84M1NX6E/IVr/
qQMDhxQfzYuxX+GZHc+qaNH88drupHd9RVLwqu9hXZXQOK3gfHdDE248aYr4
PbkSwrn8Y2X/NmFDzmz/tP2ViLlc9NG+pQncWNVdlsqVkG32O5W8phmqYqFn
/suvwOhoxnHDS82YEBC0aumhCjwYevQ0sKkZZakjfYaSFTArFq4xMmrB1Esi
zcZIPlx2pczWvdCCq58e7eUs4MO4St+ovrEFcVk7uRmp5bg9ZWnKG4NWaMVJ
aj0yLEfmgl5n3/OtCBh5zxollyFP+btr6JdW/MU01Ysrl8GiKuD1x8Vt8MmI
vtJzqRTXD5Q4iILbsHlrry8RlcCp+Mbm32vboBq6MPz19hL8NpAc3KndjkEX
tRj3J8W4KH1igrN/O07v2dEQPlqEwMaKWrnKdkSHWhupbyvCqqjPoiAtIda+
/tzfHPoGtfOXbnfkUvehfyi/ECNHp07yKReiXvKakbREIT4l3RXfotGBr38u
nPBMrwAmvPxtu3w6kF6g26PmkI+rHlqi6vwOmLaSD4dC8nBLxbRsyuxOzOO9
2VcZlYslEW0jZw52wuy4kN/g9RrcFjPvF7mdKH9ptrHw+CtslViyWk9eBDnH
pYufX82CLLvLStxZhMZw89rI5JcIzDFOjnkhgsdmm9jJFzMQYJNq5y3TBZn5
x4bPn03H6opaM5m9XehvCnwiZ5yGRoUNQZMyutA4KLK/MvgYMlpGiibS3ZDa
MHZiZ1QKuhXbhWs53dj258Iq6+JE2DNz5Y+kdoNX6+vBW8BDql30+Wnj3djy
XEbZXPU28vKrjD1sWfxRIiF+lVyHfX+4hXQCiyEzIeleF4KiSlMDz/vUnu8W
iVqDsSJpQL42kcXv86WvCcKDMZ3DqUt4wCJpsXnCsy9BKCpetM/sCYtFZRt7
hv0DYRjNPxGczeKoqpT89KwzkDeV5Um+Y3GaYz87gnMMZ1RzQtzfs3h7OuJN
xMOj6B7wPviuhsWdDIf4M0O+KH3wcfHdOhYL3e93LYv0wRmFpJemX1gENeqU
f+R7gW3fxAQIWbiN/rN+r4cr9hQMPxF2sNg7w/zRgUkuKItNvW4lYmG1zdRv
e/w+JFgr7ZrHsphpc9cwr8YJnOzW5tx+Fjp9HF/LYzvBj4wq1h1gcaRl3559
iTuw2tsy+fIPFveXpU80qt8OBe3nnk5DLG6//GeDhZ8Zzoodsi4ZZnHOp1Xb
/5MxeurnGBqMsriQWavy18mV4KRXK/03xsKxU6c12F8b/IjgofFxFg13wM8V
blz3P41XNqs=
      "]]}}, {}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.5, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Part[{{Identity, Identity}, {Identity, Identity}}, 1, 2][#]& )[
         Part[#, 1]], 
        (Part[{{Identity, Identity}, {Identity, Identity}}, 2, 2][#]& )[
         Part[#, 2]]}& )}},
  PlotRange->NCache[{{
      Rational[1, 2], 1}, {0., 0.2499999999999999}}, {{0.5, 1}, {0., 
     0.2499999999999999}}],
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.745692600218446*^9, 3.745692604395801*^9}}]
}, Open  ]]
},
WindowSize->{808, 656},
WindowMargins->{{Automatic, 0}, {Automatic, 20}},
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "Info243745675901-9571462"->{
  Cell[5045, 129, 1922, 31, 82, "Print",
   CellTags->"Info243745675901-9571462"]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"Info243745675901-9571462", 35366, 798}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 791, 22, 111, "Input"],
Cell[CellGroupData[{
Cell[1374, 46, 241, 6, 32, "Input"],
Cell[1618, 54, 3269, 66, 213, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4924, 125, 118, 2, 32, "Input"],
Cell[5045, 129, 1922, 31, 82, "Print",
 CellTags->"Info243745675901-9571462"]
}, Open  ]],
Cell[6982, 163, 693, 19, 32, "Input"],
Cell[CellGroupData[{
Cell[7700, 186, 435, 10, 32, "Input"],
Cell[8138, 198, 3725, 74, 228, "Output"]
}, Open  ]],
Cell[11878, 275, 206, 3, 54, "Input"],
Cell[12087, 280, 693, 17, 59, "Input"],
Cell[CellGroupData[{
Cell[12805, 301, 146, 3, 32, "Input"],
Cell[12954, 306, 625, 17, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13616, 328, 140, 3, 32, "Input"],
Cell[13759, 333, 1251, 38, 113, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15047, 376, 296, 7, 50, "Input"],
Cell[15346, 385, 176, 3, 66, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15559, 393, 77, 2, 32, "Input"],
Cell[15639, 397, 78, 1, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15754, 403, 803, 20, 54, "Input"],
Cell[16560, 425, 5599, 106, 231, "Output"],
Cell[22162, 533, 7773, 143, 228, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[29972, 681, 308, 9, 32, "Input"],
Cell[30283, 692, 4748, 92, 234, "Output"]
}, Open  ]]
}
]
*)

