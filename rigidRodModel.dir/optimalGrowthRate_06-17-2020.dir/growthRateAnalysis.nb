(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     18431,        503]
NotebookOptionsPosition[     16237,        460]
NotebookOutlinePosition[     16575,        475]
CellTagsIndexPosition[     16532,        472]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{"one", " ", "=", " ", 
  RowBox[{"Integrate", "[", 
   RowBox[{
    SuperscriptBox["\[ExponentialE]", 
     RowBox[{"\[Lambda]", " ", "t"}]], ",", " ", 
    RowBox[{"{", 
     RowBox[{"t", ",", " ", "0", ",", " ", "T"}], "}"}]}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"two", " ", "=", " ", 
  RowBox[{"Integrate", "[", 
   RowBox[{
    RowBox[{"2", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"\[Lambda]", " ", "t"}]]}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"t", ",", " ", "T", ",", " ", 
      RowBox[{"2", "T"}]}], "}"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"three", " ", "=", " ", 
  RowBox[{"Integrate", "[", 
   RowBox[{
    RowBox[{"4", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"\[Lambda]", " ", "t"}]]}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"t", ",", " ", 
      RowBox[{"2", "T"}], ",", " ", 
      RowBox[{"3", "T"}]}], "}"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"{", 
   RowBox[{
    FractionBox["l0prime", 
     RowBox[{"6", " ", "T"}]], 
    RowBox[{"(", 
     RowBox[{"one", " ", "+", " ", "two", " ", "+", " ", "three"}], ")"}]}], 
   "}"}], " ", "/.", " ", 
  RowBox[{"{", 
   RowBox[{"T", " ", "\[Rule]", " ", 
    RowBox[{
     FractionBox["1", "\[Lambda]"], 
     RowBox[{"Log", "[", "2", "]"}]}]}], "}"}]}]}], "Input",
 CellChangeTimes->{{3.79597522428051*^9, 3.795975245146513*^9}, {
  3.7959762751275063`*^9, 3.795976275868064*^9}, {3.795976355634647*^9, 
  3.795976368217979*^9}, {3.795977012215788*^9, 3.795977035630581*^9}, {
  3.79597833760819*^9, 3.79597834583923*^9}},
 CellLabel->"In[19]:=",ExpressionUUID->"2c5e161d-9f10-470d-909b-e6faa6f31d20"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"-", "1"}], "+", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"T", " ", "\[Lambda]"}]]}], "\[Lambda]"]], "Output",
 CellChangeTimes->{
  3.795975245755308*^9, 3.795976278729088*^9, {3.795976369012906*^9, 
   3.795976377690996*^9}, 3.795977037305262*^9},
 CellLabel->"Out[19]=",ExpressionUUID->"549273fe-e25c-4d37-8dda-8e6d77183cab"],

Cell[BoxData[
 FractionBox[
  RowBox[{"2", " ", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"T", " ", "\[Lambda]"}]], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"T", " ", "\[Lambda]"}]]}], ")"}]}], "\[Lambda]"]], "Output",
 CellChangeTimes->{
  3.795975245755308*^9, 3.795976278729088*^9, {3.795976369012906*^9, 
   3.795976377690996*^9}, 3.795977037335972*^9},
 CellLabel->"Out[20]=",ExpressionUUID->"f768b5d4-9a7b-4349-8ea2-00cf768f177e"],

Cell[BoxData[
 FractionBox[
  RowBox[{"4", " ", 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"2", " ", "T", " ", "\[Lambda]"}]], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"T", " ", "\[Lambda]"}]]}], ")"}]}], "\[Lambda]"]], "Output",
 CellChangeTimes->{
  3.795975245755308*^9, 3.795976278729088*^9, {3.795976369012906*^9, 
   3.795976377690996*^9}, 3.795977037337638*^9},
 CellLabel->"Out[21]=",ExpressionUUID->"09cb71a4-ba79-4313-aea2-42e849941468"],

Cell[BoxData[
 RowBox[{"{", 
  FractionBox[
   RowBox[{"7", " ", "l0prime"}], 
   RowBox[{"2", " ", 
    RowBox[{"Log", "[", "2", "]"}]}]], "}"}]], "Output",
 CellChangeTimes->{
  3.795975245755308*^9, 3.795976278729088*^9, {3.795976369012906*^9, 
   3.795976377690996*^9}, 3.795977037339467*^9},
 CellLabel->"Out[22]=",ExpressionUUID->"186ca54d-f7fa-445e-bdf9-5323ece68907"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"one", " ", "=", " ", 
   RowBox[{"Integrate", "[", 
    RowBox[{
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"\[Lambda]", " ", "t"}]], ",", " ", 
     RowBox[{"{", 
      RowBox[{"t", ",", " ", "0", ",", " ", "T"}], "}"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"two", " ", "=", " ", 
  RowBox[{"Integrate", "[", 
   RowBox[{
    RowBox[{"2", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"\[Lambda]", " ", 
       RowBox[{"(", 
        RowBox[{"t", "-", "T"}], ")"}]}]]}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"t", ",", " ", "T", ",", " ", 
      RowBox[{"2", "T"}]}], "}"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"three", " ", "=", " ", 
  RowBox[{"Integrate", "[", 
   RowBox[{
    RowBox[{"4", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"\[Lambda]", " ", 
       RowBox[{"(", 
        RowBox[{"t", "-", 
         RowBox[{"2", "T"}]}], ")"}]}]]}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"t", ",", " ", 
      RowBox[{"2", "T"}], ",", " ", 
      RowBox[{"3", "T"}]}], "}"}]}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"{", 
   RowBox[{
    FractionBox["l0prime", 
     RowBox[{"6", " ", "T"}]], 
    RowBox[{"(", 
     RowBox[{"one", " ", "+", " ", "two", " ", "+", " ", "three"}], ")"}]}], 
   "}"}], " ", "/.", " ", 
  RowBox[{"{", 
   RowBox[{"T", " ", "\[Rule]", " ", 
    RowBox[{
     FractionBox["1", "\[Lambda]"], 
     RowBox[{"Log", "[", "2", "]"}]}]}], "}"}]}]}], "Input",
 CellChangeTimes->{{3.795978477912219*^9, 3.79597850605229*^9}, {
  3.795979100602355*^9, 3.795979102491531*^9}},
 CellLabel->"In[59]:=",ExpressionUUID->"9aa3efb2-0efa-4b61-8356-f43a1868a1c8"],

Cell[BoxData[
 FractionBox[
  RowBox[{"2", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"T", " ", "\[Lambda]"}]]}], ")"}]}], "\[Lambda]"]], "Output",
 CellChangeTimes->{{3.7959785002442427`*^9, 3.7959785079945583`*^9}, 
   3.7959791031010323`*^9},
 CellLabel->"Out[60]=",ExpressionUUID->"79346a6e-24be-4f17-bfd8-c20a5a4dd1c6"],

Cell[BoxData[
 FractionBox[
  RowBox[{"4", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"T", " ", "\[Lambda]"}]]}], ")"}]}], "\[Lambda]"]], "Output",
 CellChangeTimes->{{3.7959785002442427`*^9, 3.7959785079945583`*^9}, 
   3.7959791031466503`*^9},
 CellLabel->"Out[61]=",ExpressionUUID->"9c7dcc73-ac5c-4b4a-848b-d209439b57b3"],

Cell[BoxData[
 RowBox[{"{", 
  FractionBox[
   RowBox[{"7", " ", "l0prime"}], 
   RowBox[{"6", " ", 
    RowBox[{"Log", "[", "2", "]"}]}]], "}"}]], "Output",
 CellChangeTimes->{{3.7959785002442427`*^9, 3.7959785079945583`*^9}, 
   3.795979103148658*^9},
 CellLabel->"Out[62]=",ExpressionUUID->"84853951-2b19-4990-a6f1-5940cf8174f9"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"n", "[", "t_", "]"}], ":=", " ", 
  RowBox[{"Piecewise", "[", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", 
       RowBox[{
        RowBox[{"t", "\[GreaterEqual]", "0"}], " ", "&&", " ", 
        RowBox[{"t", "\[LessEqual]", "T"}]}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"2", ",", 
       RowBox[{
        RowBox[{"t", ">", "T"}], " ", "&&", " ", 
        RowBox[{"t", "\[LessEqual]", 
         RowBox[{"2", "T"}]}]}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"4", ",", 
       RowBox[{
        RowBox[{"t", ">", 
         RowBox[{"2", "T"}]}], " ", "&&", " ", 
        RowBox[{"t", "\[LessEqual]", 
         RowBox[{"3", "T"}]}]}]}], "}"}]}], "}"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7959775446361*^9, 3.795977580516193*^9}, {
  3.795977669780993*^9, 3.795977735473255*^9}, {3.795977769306691*^9, 
  3.795977805314938*^9}, {3.795977865334571*^9, 3.795977866558529*^9}},
 CellLabel->"In[39]:=",ExpressionUUID->"09d8d545-17f5-4144-8752-a85bf791e37b"],

Cell[BoxData[
 RowBox[{
  RowBox[{"v", "[", "t_", "]"}], ":=", 
  RowBox[{
   FractionBox["l0prime", "2"], 
   RowBox[{"n", "[", "t", "]"}], 
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"\[Lambda]", " ", "t"}]]}]}]], "Input",
 CellChangeTimes->{{3.795977295487571*^9, 3.795977541099827*^9}},
 CellLabel->"In[40]:=",ExpressionUUID->"2e648e65-3e00-4e1d-9715-c8008a6092f5"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   FractionBox["1", 
    RowBox[{"3", " ", "T"}]], 
   RowBox[{"Integrate", "[", 
    RowBox[{
     RowBox[{"v", "[", "t", "]"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"t", ",", "0", ",", " ", 
       RowBox[{"3", "T"}]}], "}"}]}], "]"}]}], "/.", " ", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"T", "\[Rule]", 
     RowBox[{"Log", "[", "2", "]"}]}], ",", " ", 
    RowBox[{"\[Lambda]", "\[Rule]", " ", "1"}]}], "}"}], " "}]], "Input",
 CellChangeTimes->{{3.795977807506804*^9, 3.795977837608708*^9}, {
  3.795977893636868*^9, 3.795977938966851*^9}},
 CellLabel->"In[43]:=",ExpressionUUID->"41912254-7e08-4984-80ae-1ea07ac1b819"],

Cell[BoxData[
 FractionBox[
  RowBox[{"7", " ", "l0prime"}], 
  RowBox[{"2", " ", 
   RowBox[{"Log", "[", "2", "]"}]}]]], "Output",
 CellChangeTimes->{
  3.795977838545001*^9, 3.795977871248138*^9, {3.795977912139659*^9, 
   3.7959779420476913`*^9}},
 CellLabel->"Out[43]=",ExpressionUUID->"9b2dc23b-43cc-4311-a432-eb821410d475"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"v", "[", "t", "]"}], "/.", " ", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"T", "\[Rule]", 
        RowBox[{"Log", "[", "2", "]"}]}], ",", " ", 
       RowBox[{"\[Lambda]", "\[Rule]", " ", "1"}]}], "}"}]}], "}"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", " ", 
     RowBox[{"3", 
      RowBox[{"Log", "[", "2", "]"}]}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.79597785255618*^9, 3.795977855943701*^9}, {
  3.795977961646132*^9, 3.795978018118026*^9}},
 CellLabel->"In[46]:=",ExpressionUUID->"593d4f22-a104-4106-bdd9-b2293b371905"],

Cell[BoxData[
 GraphicsBox[{{}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}, {Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, 
    "DefaultGraphicsInteraction" -> {
     "Version" -> 1.2, "TrackMousePosition" -> {True, False}, 
      "Effects" -> {
       "Highlight" -> {"ratio" -> 2}, "HighlightPoint" -> {"ratio" -> 2}, 
        "Droplines" -> {
         "freeformCursorMode" -> True, 
          "placement" -> {"x" -> "All", "y" -> "None"}}}}, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->
   NCache[{{0, 3 Log[2]}, {0., 0.}}, {{0, 2.0794415416798357`}, {0., 0.}}],
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.795978006124789*^9, 3.7959780340296907`*^9}},
 CellLabel->"Out[46]=",ExpressionUUID->"b6fcb460-8ad6-4049-8af0-8d4986e950eb"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   FractionBox[
    RowBox[{
     SuperscriptBox["2", "m"], "-", "1"}], 
    RowBox[{"2", " ", "m", " ", 
     RowBox[{"Log", "[", "2", "]"}]}]], ",", 
   RowBox[{"{", 
    RowBox[{"m", ",", "1", ",", "10"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.7960467940907183`*^9, 3.7960468746893578`*^9}},
 CellLabel->"In[63]:=",ExpressionUUID->"33e9481d-2573-4a01-a6b8-19214ce4fc9e"],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    TagBox[
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJwVznk41HkAx/HJlclgyuQ+57cpIpIdTeT7oZTJo2YmLFaP2JWOLSppe7St
I0cjlJgkKSJt2VHRsaWSJ7JRzY4eXaIoV2hmGMTGtn+8n/e/L5vIGGGUGo1G
8//W/3+p8F9Ko8lJS1N54tr5vaRRryprvY6c0N0EJflmvaR6MXOgyFBOJIpf
hz/a9JLsKFkp11FOeI2iuVlLesmqV0GsvaFyUvWs77hqbS+R1IWPf6yRk1cu
jhznhF6SmrP7bvM2BXH9JX5E0N1LXBzzeSdblYSTKxDvv91HbCKirup8UBFj
czeTSMkAuZOjMe9w3QSZcW27oScZJGW6WsuKs6eIn2qTha1smLAPHzuff2qa
lEifBzGr5OTCZFya7y0azo+G+Bh8VZBUp54uV2816A7b3dLOHCGMoxlWyofq
CL9SPW22QkX6o3KDrzpqIkDWvHXZ6Bhh1m7mdtZo4eWu2hsT6l9IVo7YUJ+t
jTDPiO0z7ClyTfS1VT+DjolQsVi84yuxUbPj/s7QQeyayKQQAxp6xu4UyuMZ
mGjlTXITZkEVbVAmndEF64jWCxd7dVTSirKSkvRRqxyg+89oIEynkVsYx8T1
z9vqE7q0kBFwjnbZbS5E2iUcNVs6wsMkDbVDc4EnpX71B3XwY2lxh2b1PCz2
0FVK/fSw+GBF9M04Awi8453TJEyUsz4Mb7NjIXJMprl69Tz4fFJ7vHGIhaV7
mj/YO7BwLDHwvGfZfMRe2qLXucAQkj/qBo//YIjJdXntUzxjKJl6InNTIxxY
Wbmy7a0p3nNyFlx8ZoTGWS8MG45YQFWoZO3IM0a6yDZSM8Iab3N3xYkDTTDH
xdo75zs2YniWsnq6KcTxZlszJigs9LySYvvEFC1p2s9YjrYQKfYty0wxw8nW
20ay/YtQtzxgYzDHHOLXAQ9iVPYwv8d6FDZhDj+j8k+8cUdQanRu5J8WmPRt
2SfOdMaGw53TxbstcfQ0h67Y5QLbhv769kVWOKrrsCJtiyukpxjjye1WYDDo
sz+LOOC3ZZArG6whHfZjObG50CPlyWcvWUOyMd6j77k7Ohqvxdjr2iC65LpM
24SgoKAlr3+nDVIPdfeXB3vhIo/GtWuygby98v5pt1V4bLIgXcOejX9o0Rp3
E3zg/fdPAZtT2WgIzOlzebUWZfWSEHkHGxVGDuNVO9fB+M2AtjkoxGaenTHO
9keXDG5NhRSyR7wuH9jKR/EiP8NNRRQU40qxXSwfIYcCVYozFM4EFXHe7udD
are92qyEgnqU+akN6XzcSzzhFFNBocC5yyLiAh+nl/QsNKyh4FmrXr/8Ix8B
okzjn59QMAgdTHwUJQDzXf74xFMKTnVqyYqdAjR/f64tS0phZdHoHqt4Abzf
1+TdbKXwXtfWIT1NAOflHUyd1xQ+W8x3SasQgNHjTL/WQ8Fy75TX6gEBmtzd
+9b0UejovGsvUQqQctzn0Zt+Cq/z/mKbTwkw6RGaqjn0zZcf5KvBEKLvRMqs
kBEK3bUupZpLhCjrz3o3NEqhyf+hxzGOEOGk4H7yGIWhpKIxSyJE20Dlb5Vf
KMxOWSH15QuRi5thXlMU2JX6o93BQqwXP3Bv+5dC69J1JCVCiDmDzaY7pr/5
eKPVttuFaPBq+zIzQ+G6vUnA0z1C/AdHyyWw
       "]]},
     Annotation[#, "Charting`Private`Tag$26795#1"]& ]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{1., 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}, {Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, 
    "DefaultGraphicsInteraction" -> {
     "Version" -> 1.2, "TrackMousePosition" -> {True, False}, 
      "Effects" -> {
       "Highlight" -> {"ratio" -> 2}, "HighlightPoint" -> {"ratio" -> 2}, 
        "Droplines" -> {
         "freeformCursorMode" -> True, 
          "placement" -> {"x" -> "All", "y" -> "None"}}}}, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{1, 10}, {0., 73.79384329278658}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.796046875479794*^9},
 CellLabel->"Out[63]=",ExpressionUUID->"36411e85-dad4-4d46-bb22-e596efcdf40c"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Mod", "[", 
  RowBox[{"8", ",", "3"}], "]"}]], "Input",
 CellChangeTimes->{{3.796057889501051*^9, 3.79605790755339*^9}},
 CellLabel->"In[70]:=",ExpressionUUID->"cdecff5f-e739-452b-8587-690bd75407c0"],

Cell[BoxData["2"], "Output",
 CellChangeTimes->{{3.7960578917208157`*^9, 3.796057907870503*^9}},
 CellLabel->"Out[70]=",ExpressionUUID->"7fcb0492-3d7e-4669-a16a-3eb5c1bc9d09"]
}, Open  ]]
},
WindowSize->{808, 655},
WindowMargins->{{230, Automatic}, {37, Automatic}},
FrontEndVersion->"12.0 for Mac OS X x86 (64-bit) (April 8, 2019)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 1669, 45, 118, "Input",ExpressionUUID->"2c5e161d-9f10-470d-909b-e6faa6f31d20"],
Cell[2252, 69, 388, 9, 53, "Output",ExpressionUUID->"549273fe-e25c-4d37-8dda-8e6d77183cab"],
Cell[2643, 80, 525, 13, 55, "Output",ExpressionUUID->"f768b5d4-9a7b-4349-8ea2-00cf768f177e"],
Cell[3171, 95, 535, 13, 55, "Output",ExpressionUUID->"09cb71a4-ba79-4313-aea2-42e849941468"],
Cell[3709, 110, 375, 9, 54, "Output",ExpressionUUID->"186ca54d-f7fa-445e-bdf9-5323ece68907"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4121, 124, 1687, 49, 118, "Input",ExpressionUUID->"9aa3efb2-0efa-4b61-8356-f43a1868a1c8"],
Cell[5811, 175, 402, 10, 55, "Output",ExpressionUUID->"79346a6e-24be-4f17-bfd8-c20a5a4dd1c6"],
Cell[6216, 187, 402, 10, 55, "Output",ExpressionUUID->"9c7dcc73-ac5c-4b4a-848b-d209439b57b3"],
Cell[6621, 199, 332, 8, 54, "Output",ExpressionUUID->"84853951-2b19-4990-a6f1-5940cf8174f9"]
}, Open  ]],
Cell[6968, 210, 1038, 27, 30, "Input",ExpressionUUID->"09d8d545-17f5-4144-8752-a85bf791e37b"],
Cell[8009, 239, 376, 9, 48, "Input",ExpressionUUID->"2e648e65-3e00-4e1d-9715-c8008a6092f5"],
Cell[CellGroupData[{
Cell[8410, 252, 668, 18, 48, "Input",ExpressionUUID->"41912254-7e08-4984-80ae-1ea07ac1b819"],
Cell[9081, 272, 329, 8, 54, "Output",ExpressionUUID->"9b2dc23b-43cc-4311-a432-eb821410d475"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9447, 285, 658, 17, 30, "Input",ExpressionUUID->"593d4f22-a104-4106-bdd9-b2293b371905"],
Cell[10108, 304, 1691, 45, 231, "Output",ExpressionUUID->"b6fcb460-8ad6-4049-8af0-8d4986e950eb"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11836, 354, 431, 11, 52, "Input",ExpressionUUID->"33e9481d-2573-4a01-a6b8-19214ce4fc9e"],
Cell[12270, 367, 3513, 77, 241, "Output",ExpressionUUID->"36411e85-dad4-4d46-bb22-e596efcdf40c"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15820, 449, 223, 4, 30, "Input",ExpressionUUID->"cdecff5f-e739-452b-8587-690bd75407c0"],
Cell[16046, 455, 175, 2, 68, "Output",ExpressionUUID->"7fcb0492-3d7e-4669-a16a-3eb5c1bc9d09"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

