#!/usr/local/env python

import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import stats
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
import matplotlib
#matplotlib.use("Agg")      # COMMENT THIS FOR PLOTS FOR plt.show(), UNCOMMENT FOR ANIMATIONS
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys

####################################################################################################

class filament(object) :

    #-----------------------------------------------------------------------------------------------

    def __init__(self, t, numRods, l0, com, growthRate, Lb, mu, F0) :
        """ creates a filament object
                numRods : number of rods in the filament
                l0 : initial length of the cells
                com : center of mass of the filaments
                growthRate : initial growth rate of the
                Lb : length at which a filament breaks
                mu : drag per unit lenght parallel to the axis of the rod"""

        self.numRods = numRods
        self.l0 = l0
        self.growthRate = growthRate
        self.T = l0/growthRate * np.log(2)  # cell cycle time
        l = l0 * np.exp( (growthRate/l0) * (t % self.T) )   # cell length
        self.L = numRods * l    # filament length
        self.com = com
        self.Lb = Lb
        self.mu = mu
        self.F0 = F0
        self.broken = [False] * (self.numRods-1)
        self.updateTips()

    #-----------------------------------------------------------------------------------------------

    def updateLength(self, t) :
        """ updates the length of the filament
                t : the current time """
        l = l0 * np.exp( (growthRate/l0) * (t % self.T) )
        self.L = self.numRods * l

        #if self.L > self.Lb : self.broken = True

    #-----------------------------------------------------------------------------------------------

    def cellDivision(self) :
        """ handles cell division """
        self.numRods = int(self.numRods * 2)
        self.broken = [False] * (self.numRods-1)

    #-----------------------------------------------------------------------------------------------

    def updateTips(self) :
        """ calculate the location of the end tips of the filaments """
        self.tip_locations = [ self.com - (self.L / 2), self.com + (self.L / 2) ]

    #-----------------------------------------------------------------------------------------------

    def updateStress(self) :

        stress = []
        for i in range( self.numRods-1 ) :
            q = i / self.numRods
            s = 0.5 * self.mu * q * (1-q) * self.L * self.numRods * self.growthRate
            stress.append(s)
        self.stress = stress

    #-----------------------------------------------------------------------------------------------

    def update(self, t, dt) :
        """ updates the attributes of the filament """
        self.updateLength(t)
        self.updateTips()   # updateTips must be called AFTER updateLength
        self.updateStress()

        a = self.F0 / np.sqrt(2)    # Maxwell distribution parameter in terms of the critical stress
        # check if a break as occured

        if self.numRods > 1 :
            for i in range(self.numRods-1) :

                q = i / self.numRods    # fractional location of the ith joint
                s = self.stress[i]      # stress in the ith joint

                #prob_break = 2/np.pi * s**2 * np.exp( -s**2 / (2 * a**2) ) / a**3   # Maxwell-Boltzmann Distribution
                #prob_break = 1
                prob_break = (np.exp(s/F0) - 1) * dt
                if random.uniform(0,1) < prob_break : self.broken[i] = True

        if len(self.broken) != self.numRods-1 : input("HEY LOOK HERE")


####################################################################################################


growthRate = 0.1    # intitial  growth rate
numRods = 4         # number of initial cells
l0 = 5              # initial cell length

Lb = 2000           # filament breaking length
mu = 2e-9           # drag coefficient per unit length for parallel drag

# critical stress at which breaking probability peaks
#F0 = 0.5 * mu * 0.25 * 32**2 * l0 * growthRate
F0 = 0.75e-7
# ~6e-6 for mu = 1e-7, growthRate = 0.1

t = 0
T = l0/growthRate * np.log(2)       # cell cycle time
dt = T / 5000     # timestep
endTime = 230
random.seed(32)

filaments = [ filament(t, numRods, l0, 0.0, growthRate, Lb, mu, F0) ]     # initiial filament
totalLength = [[t, numRods * l0]]    # array of data points [t, Lt] where Lt is the length from the two farthest tips
totalLengthRate = []
while t < endTime :

    newFilaments =[]
    for f in filaments :

        f.update(t, dt)
        # if the filament has reached the breaking length
        if True in f.broken :
            if sum(f.broken) > 1 : input("UH OH -- More than one breaking event occured")
            ic = (f.numRods-2)/2
            indices = [i for i, x in enumerate(f.broken) if x == True]
            i_break = indices[[np.abs(i - ic) for i in indices].index( min([np.abs(i - ic) for i in indices]) )]

            new_com_1 = f.com - f.L/2 * (1 - ( (i_break+1) / f.numRods ) )
            newFilaments.append( filament(t, i_break+1, l0, new_com_1, growthRate, Lb, mu, F0) )
            new_com_2 = f.com - f.L/2 * (1 - ( (f.numRods + i_break +1 ) / f.numRods) )
            newFilaments.append( filament(t, f.numRods-i_break-1, l0, new_com_2, growthRate, Lb, mu, F0) )

            """
            for i in range(f.numRods-1) :
                lastBreakIndex = 0
                if f.broken[i] :        # check if the ith joint is broken
                    newfilaments.append( filament(t, lastBreakIndex+i+1), f.com )
                    lastBreakIndex = i

            newFilaments.append( filament(t, f.numRods/2, l0, f.com - (f.L/4), growthRate, Lb) )
            newFilaments.append( filament(t, f.numRods/2, l0, f.com + (f.L/4), growthRate, Lb) )"""

        else : newFilaments.append(f)

    filaments = newFilaments

    if (t+dt)%T < t%T  :
        for f in filaments :
            f.cellDivision()   # check if cells divide

    t += dt
    print("Time =", t)

    tip_max = max( [ max(f.tip_locations) for f in filaments ] )
    tip_min = min( [ min(f.tip_locations) for f in filaments ] )
    totalLength.append( [t, tip_max - tip_min] )
    Lt_rate =  (totalLength[-1][1] - totalLength[-2][1]) / (totalLength[-1][0] - totalLength[-2][0])
    totalLengthRate.append( [t, Lt_rate] )

print("Average Rate =", totalLength[-1][1]/endTime, "um/min")

totalLength = np.array(totalLength)
plt.figure(1)
plt.title(r"Tip-to-Tip Expansion Displacement" )
plt.xlabel(r"Time(min)")
plt.ylabel(r"Displacement ($\mu$m)")
plt.plot(totalLength[:,0], totalLength[:,1])

totalLengthRate = np.array(totalLengthRate)
plt.figure(2)
plt.title(r"Tip-to-Tip Expansion Rate" )
plt.xlabel(r"Time(min)")
plt.ylabel(r"Rate of Expansion ($\mu$m/min)")
plt.plot(totalLengthRate[:,0], totalLengthRate[:,1])

plt.show()
