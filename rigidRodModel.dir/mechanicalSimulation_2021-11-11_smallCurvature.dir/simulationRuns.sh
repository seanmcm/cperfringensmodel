#!/bin/bash

for psi in 1 20 40 60 80 100
do
    for a in 2 5 10 15 20 100
    do
        python exponentialGrowth.py $psi $a
    done
done
