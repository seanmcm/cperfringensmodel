#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 09/07/2018

import sys
import numpy as np
import scipy as sp
import matplotlib
import random as rand
import matplotlib as mpl
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pickle

from main import *

def makePlot(file) :

    data = pickle.load(open(file, "rb"))
    #margins, probs = data[0], data[1]
    tVals, LDiffVals = data[0][0], data[0][1]

    #print("tVals", tVals)
    #print("LDiffVals", LDiffVals)

    plt.figure(1)
    #plt.xlim(-12,12)
    plt.xlabel(r"Time (s)")
    plt.ylabel(r"$L_1$ - $L_2$ ($\mu$m)")
    plt.title(r"Null Case")
    plt.scatter(tVals, LDiffVals)
    #plt.show()
    try :
        plt.savefig(str(sys.argv[1])[:-3] + "png")
    except :
        plt.savefig(file[:-3] + "png")
#makePlot("run2_outcomes_100.pkl")
try : makePlot( str(sys.argv[1]) )
except : print("File not provided as argument. Try again")
