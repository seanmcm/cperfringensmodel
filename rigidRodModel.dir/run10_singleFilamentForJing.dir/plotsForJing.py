#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 05/08/2019

import pdb
import numpy as np
from scipy.integrate import ode
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
#import matplotlib.animation as manimation
import sys

#######################################################################################################

def analysis(filename) :

    stiffData = pickle.load(open(filename, "rb"))

    tVec = stiffData[0]

    # organize filament 1 data

    # rawData
    f1_eqns = stiffData[1][0]
    f1_vals = stiffData[1][1]
    f1_qs = stiffData[1][2]
    f1_qdots = stiffData[1][3]
    f1_xdots = stiffData[1][4]
    f1_ydots = stiffData[1][5]
    f1_lateralForces = stiffData[1][6]
    f1_lateralTorques = stiffData[1][7]
    f1_endSpeeds = stiffData[1][8]

    #f1_qVecs = [ [ f1_qs[i][j] for i in range(len(f1_qs)) ] for j in range(len(f1_qs[0])) ]
    #f1_qdotVecs = [ [ f1_qdots[i][j] for i in range(len(f1_qdots)) ] for j in range(len(f1_qdots[0])) ]

    plt.figure(1)
    plt.title("Free End Speed")
    plt.xlabel("Time (min)")
    plt.ylabel("Speed ($\mu$m/min)")

    plt.plot(tVec, f1_endSpeeds)

    plt.show()

    return

#------------------------------------------------------------------------------------

def plotStuff(tVec, f1, f2, label) :
    """ label is a string for what date you are plotting
        ie. qs, qdots, xdots, ydots, etc """


    if label == "lateralForces" or label == "lateralTorques" :
        vec1 = forceDataToVec(f1)
        vec2 = forceDataToVec(f2)
    else:
        vec1 = dataToVec(f1)
        vec2 = dataToVec(f2)

    print("label", label)

    plt.figure(1)
    plt.title("Filament 1 " + label)
    plt.xlabel("Time (min)")
    plt.ylabel(label)
    for i in range( len(vec1) ) :
        plt.plot(tVec, vec1[i])
        if label == "xdots" :
            plt.legend([ "x" + str(i) for i in range(len(vec1[i])) ] )
        if label == "ydots" :
            plt.legend([ "y" + str(i) for i in range(len(vec1[i])) ] )
        elif label == "qs" or label == "qdots" :
            #plt.legend(["x0","y0","theta0","theta1","theta2","theta3"])
            plt.legend(["x0", "y0"] + [ "theta" + str(i) for i in range(len(vec1[i])-2) ] )

    plt.figure(2)
    plt.title("Filament 2 " + label)
    plt.xlabel("Time (min)")
    plt.ylabel(label)
    for i in range( len(vec2) ) :
        plt.plot(tVec, vec2[i])
        #plt.legend(["x0","y0","theta0","theta1","theta2","theta3","theta4"])
        if label == "xdots" :
            plt.legend([ "x" + str(i) for i in range(len(vec2[i])) ] )
        elif label == "ydots" :
            plt.legend([ "y" + str(i) for i in range(len(vec2[i])) ] )
        elif label == "qs" or label == "qdots" :
            #plt.legend(["x0","y0","theta0","theta1","theta2","theta3"])
            plt.legend(["x0", "y0"] + [ "theta" + str(i) for i in range(len(vec2[i])-2) ] )


    plt.show()

    return

#------------------------------------------------------------------------------------

def dataToVec(data) :
    """ takes data and organizes it for plotting """
    return [ [ data[i][j] for i in range(len(data)) ] for j in range(len(data[0])) ]

#------------------------------------------------------------------------------------

def forceDataToVec(data) :
    """ coverts force/torque data and formats it for plotting """

    newData = []
    for j in range(len(data[-1])) :
        temp = []
        for i in range( len(data) ) :
            try: temp.append( data[i][j] )
            except IndexError : temp.append(0)
        newData.append(temp)
    return newData

####################################################################################################

try :
    filename = str(sys.argv[1])
except IndexError :
    print("ERROR: No file given -> try again")
    exit()
analysis(filename)
