#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 05/08/2019

import pdb
import numpy as np
from scipy.integrate import ode
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
#import random
#from random import randrange
import math
import time
import csv
import os
import pickle
#from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
#import matplotlib.animation as manimation
import sys

#######################################################################################################

def analysis(filename, label) :

    stiffData = pickle.load(open(filename, "rb"))

    tVec = stiffData[0]

    # organize filament 1 data

    # rawData
    f1_eqns = stiffData[1][0]
    f1_vals = stiffData[1][1]
    f1_qs = stiffData[1][2]
    f1_qdots = stiffData[1][3]
    f1_xdots = stiffData[1][4]
    f1_ydots = stiffData[1][5]
    f1_lateralForces = stiffData[1][6]
    f1_lateralTorques = stiffData[1][7]

    #f1_qVecs = [ [ f1_qs[i][j] for i in range(len(f1_qs)) ] for j in range(len(f1_qs[0])) ]
    #f1_qdotVecs = [ [ f1_qdots[i][j] for i in range(len(f1_qdots)) ] for j in range(len(f1_qdots[0])) ]

    f2_eqns = stiffData[2][0]
    f2_vals = stiffData[2][1]
    f2_qs = stiffData[2][2]
    f2_qdots = stiffData[2][3]
    f2_xdots = stiffData[2][4]
    f2_ydots = stiffData[2][5]
    f2_lateralForces = stiffData[2][6]
    f2_lateralTorques = stiffData[2][7]

    #f2_qVecs = [ [ f2_qs[i][j] for i in range(len(f2_qs)) ] for j in range(len(f2_qs[0])) ]
    #f2_qdotVecs = [ [ f2_qdots[i][j] for i in range(len(f2_qdots)) ] for j in range(len(f2_qdots[0])) ]

    if label == "qs" : plotStuff(tVec, f1_qs, f2_qs, label)
    elif label == "qdots" : plotStuff(tVec, f1_qdots, f2_qdots, label)
    elif label == "xdots" : plotStuff(tVec, f1_xdots, f2_xdots, label)
    elif label == "ydots" : plotStuff(tVec, f1_ydots, f2_ydots, label)
    elif label == "lateralForces" : plotStuff(tVec, f1_lateralForces, f2_lateralForces, label)
    elif label == "lateralTorques" : plotStuff(tVec, f1_lateralTorques, f2_lateralTorques, label)
    else :
        print("Not a valid label, try qs, qdots, xdots, or ydots")
        exit()

    return

#------------------------------------------------------------------------------------

def plotStuff(tVec, f1, f2, label) :
    """ label is a string for what date you are plotting
        ie. qs, qdots, xdots, ydots, etc """


    if label == "lateralForces" or label == "lateralTorques" :
        vec1 = forceDataToVec(f1)
        vec2 = forceDataToVec(f2)
    else:
        vec1 = dataToVec(f1)
        vec2 = dataToVec(f2)

    print("label", label)

    plt.figure(1)
    plt.title("Filament 1 " + label)
    plt.xlabel("Time (min)")
    plt.ylabel(label)
    for i in range( len(vec1) ) :
        plt.plot(tVec, vec1[i])
        if label == "xdots" :
            plt.legend([ "x" + str(i) for i in range(len(vec1[i])) ] )
        if label == "ydots" :
            plt.legend([ "y" + str(i) for i in range(len(vec1[i])) ] )
        elif label == "qs" or label == "qdots" :
            #plt.legend(["x0","y0","theta0","theta1","theta2","theta3"])
            plt.legend(["x0", "y0"] + [ "theta" + str(i) for i in range(len(vec1[i])-2) ] )

    plt.figure(2)
    plt.title("Filament 2 " + label)
    plt.xlabel("Time (min)")
    plt.ylabel(label)
    for i in range( len(vec2) ) :
        plt.plot(tVec, vec2[i])
        #plt.legend(["x0","y0","theta0","theta1","theta2","theta3","theta4"])
        if label == "xdots" :
            plt.legend([ "x" + str(i) for i in range(len(vec2[i])) ] )
        elif label == "ydots" :
            plt.legend([ "y" + str(i) for i in range(len(vec2[i])) ] )
        elif label == "qs" or label == "qdots" :
            #plt.legend(["x0","y0","theta0","theta1","theta2","theta3"])
            plt.legend(["x0", "y0"] + [ "theta" + str(i) for i in range(len(vec2[i])-2) ] )


    plt.show()

    return

#------------------------------------------------------------------------------------

def plotStuffAnimated(tVec, f1, f2, label) :
    """ label is a string for what date you are plotting
        ie. qs, qdots, xdots, ydots, etc """

    plt.ion()

    if label == "lateralForces" or label == "lateralTorques" :
        vec1 = forceDataToVec(f1)
        vec2 = forceDataToVec(f2)
    else:
        vec1 = dataToVec(f1)
        vec2 = dataToVec(f2)

    print("label", label)

    fig1 = plt.figure(1)
    ax = fig1.add_subplot(111)
    for i in range( len(vec1) ) :
        line1, = ax.plot(tVec, vec1[i], )
    plt.title("Filament 1 " + label)
    plt.xlabel("Time (min)")
    plt.ylabel(label)
    for i in range( len(vec1) ) :
        plt.plot(tVec, vec1[i])
        if label == "xdots" :
            plt.legend([ "x" + str(i) for i in range(len(vec1[i])) ] )
        if label == "ydots" :
            plt.legend([ "y" + str(i) for i in range(len(vec1[i])) ] )
        elif label == "qs" or label == "qdots" :
            #plt.legend(["x0","y0","theta0","theta1","theta2","theta3"])
            plt.legend(["x0", "y0"] + [ "theta" + str(i) for i in range(len(vec1[i])-2) ] )

    plt.figure(2)
    plt.title("Filament 2 " + label)
    plt.xlabel("Time (min)")
    plt.ylabel(label)
    for i in range( len(vec2) ) :
        plt.plot(tVec, vec2[i])
        #plt.legend(["x0","y0","theta0","theta1","theta2","theta3","theta4"])
        if label == "xdots" :
            plt.legend([ "x" + str(i) for i in range(len(vec2[i])) ] )
        elif label == "ydots" :
            plt.legend([ "y" + str(i) for i in range(len(vec2[i])) ] )
        elif label == "qs" or label == "qdots" :
            #plt.legend(["x0","y0","theta0","theta1","theta2","theta3"])
            plt.legend(["x0", "y0"] + [ "theta" + str(i) for i in range(len(vec2[i])-2) ] )


    plt.show()

    return


#------------------------------------------------------------------------------------

def dataToVec(data) :
    """ takes data and organizes it for plotting """
    return [ [ data[i][j] for i in range(len(data)) ] for j in range(len(data[0])) ]

#------------------------------------------------------------------------------------

def forceDataToVec(data) :
    """ coverts force/torque data and formats it for plotting """

    newData = []
    for j in range(len(data[-1])) :
        temp = []
        for i in range( len(data) ) :
            try: temp.append( data[i][j] )
            except IndexError : temp.append(0)
        newData.append(temp)
    return newData

####################################################################################################

"""try :
    filename = str(sys.argv[1])
except IndexError :
    print("ERROR: No file given -> try again")
    exit()
try :
    label = str(sys.argv[2])
except IndexError :
    print("ERROR: No label given (qs, qdots, xdots, ydots, etc.)")
    exit()
analysis(filename, label)"""
