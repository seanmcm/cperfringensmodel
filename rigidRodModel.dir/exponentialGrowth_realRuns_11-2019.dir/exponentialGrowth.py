#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 05/09/2019

import sys
import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
#import plotsV4
#import plotsV4_withSites_Single
from FilamentV5 import *

################################################################################
# SIMULATION NOTES
#   TIME-SCALE: minutes
#   LENGTH-SCALE: micrometers
################################################################################

################################################################################
# ODE FUNCTION
################################################################################

def dqdt(t, q, growthRate, kb, mu, nu, epsilon, width, epsilonLJ, rm, Lc) :
    """ the ODE function
        returns an array of the qDots of the N+2 generalized coordinate at time
        t to be used in solving dq/dt = qDot
            t: time
            q: array of the form [x0, y0 , theta0, theta1, ... , thetaN] """

    global lambdaVals
    global realCall
    global filaments
    global dt

    qLengths = [ len( filament.q ) for filament in filaments ]
    for i in range( len(qLengths) ) :
        #print("Updating Filament", filaments[i] )
        #lt = length(t, filaments[i].l0, growthRate)
        if i == 0 :
            qNew = q[ : qLengths[0] ]
        else :
            qNew = q[ qLengths[i-1] : qLengths[i-1] + qLengths[i] ]
        filaments[i].qUpdate(qNew)

    #filaments = myFilamentList.fs
    #print("look here!", myFilaments)
    #filaments = myFilaments[0]
    #print("LOOK HERE", filaments)

    #print("ode function call here")

    #oldFilaments = []
    tick = time.time()
    for filament in filaments :

        #oldFilaments.append( filament.copy() )
        numRods = len(filament.q) - 2
        l0 = filament.l0

        # set parameters
        #filament.lt = np.array( [filament.length(t, l0, growthRate)] * numRods )
        #filament.lp = np.array( [growthRate] * numRods )
        filament.lt = np.array( [filament.length(t, l0, growthRate)] * numRods )
        filament.lp = np.array( [filament.growthRateFunc(t, l0, growthRate)] * numRods )
        #print("check length/growth", t, filament.lt[0], filament.lp[1], filament.lastAddTime)

        #kb = kb0
        #mu = dragCoefficient(t, mu0, filament.lt[0], l0)
        #nu = dragCoefficient(t, nu0, filament.lt[0], l0)
        #epsilon = rotDragCoefficient(t, epsilon0, filament.lt[0], l0)

        # update qdots based on single filament growth dynamics
        #if realCall == True : print("Filament =", filament, "Length", filament.lt)
        filament.growthDynamics(t, kb, mu, nu, epsilon)
        #growth_qdots = np.concatenate((growth_qdots, filament.qdots), axis=None)


    #print("before lateral", filaments[0], filaments[0].qdots)


    """ THERE MIGHT BE A SERIOUS ISSUE HERE !!!
        Lateral Forces are problematic """
    """for i in range( len(filaments) ) :
        for j in range( len(filaments) ) :

            if i == j : pass
            #if j <= i :
            else :
                # update qdots based on lateral interactions
                #filaments[i].lateralDynamics(filaments[j], width, kon, koff0, ks, Lmax, Lc, Fc, dt )
                filaments[i].lateralDynamicsLJ( filaments[j], epsilonLJ, rm, Lc , dt )"""

    # solve for the qdots in each filament
    for filament in filaments :
        filament.solve()
        #filament.storeData(t)

    #print("after lateral", filaments[0], filaments[0].qdots)

    qdots = np.array([])
    for filament in filaments :
        qdots = np.concatenate((qdots, filament.qdots), axis=None)


    #lateral_qdots = np.array( lateral_qdots )

    #qdots = growth_qdots #+ lateral_qdots
    realCall = False
    tock = time.time()
    #print("One interation time", tock-tick)
    #print("timeStep", dt)
    #print("check bindingSites0", [len(filaments[0].bindingSites[i]) for i in range(len(filaments[0].bindingSites))] )
    #print("check bindingSites1", [len(filaments[1].bindingSites[i]) for i in range(len(filaments[1].bindingSites))] )
    return qdots


################################################################################
# ODE SOLVER
################################################################################

def main(endTime, numRods, growthRate, angle, mu0, nu0, epsilon0, kb0, width, epsilonLJ, rm, Lc) :
    """ simulation C. Perfringen bacteria """

    global lambdaVals
    global realCall
    global lastAddTime
    global filaments
    global dt

    t = 0.0

    # determine the name of the python file, remove '.py' and add '.pkl'
    #paramString = "_mu" + str(mu0) + "_nu" + "{:.2e}".format(nu0) + "_ep" + str(int(epsilon0)) + "_kb" + str(kb0) + "_angle" + str(angle) + "_endTime" + str(t + endTime)
    paramString = "_epsilonLJ" + str(epsilonLJ) + "_rm" + str(rm) + "_Lc" + str(Lc) + "_endTime" + str(t + endTime)
    outputFile = "data.dir/" + os.path.basename(__file__)[:-3] + paramString + ".pkl"
    outputFile2 = "testData.dir/" + os.path.basename(__file__)[:-3] + "_test" + paramString + ".pkl"
        # "data.dir/" puts it in the data.dir directory

    # constants ----------------------------------------------------------------

    l0 =  5.0                         # initial length of the rigid rods
    #totalLength0 = numRods * l0     # initial total length of filament
    #totalLength0 = (len(q0)-2) * l0
    #totalLength0Other = (len(qOther)-2) * l0

    # integration --------------------------------------------------------------

    #q0 = q1 + q2
    #q0 = np.array(q0)
    q0 = np.array(q1)

    #sol = []                        # array for the data
    #sol.append( [t, [filaments[0].nodePos, filaments[1].nodePos] ] )
    #sol.append( [t, [filaments[0].nodePos] ] )

    for filament in filaments : filament.sol.append( [t, [filaments[0].nodePos] ] )

    # data for stiffness check
    # format = [ [times], [ [f1_eqns], [f1_vals], [f1_qs], [f1_qdots], [f1_xdots], [f1_ydots], [f1_latForces], [f1_latTorques] ], [ [f2_eqns], [f2_vals], [f2_qs], [f2_qdots], [f2_xdots], [f2_ydots], [f2_latForces], [f2_latTorques] ]
    #stiffData = [ [], [ [], [], [], [], [], [], [], [] ], [ [], [], [], [], [], [], [], [] ] ]
    #stiffData = [ [], [ [], [], [], [], [], [], [], [], [], [], [] ] ]


    #solOther = []                        # array for the data
    #solOther.append([t, *qOther])

    try : addTimeInterval = l0/growthRate * np.log(2) #l0 / growthRate
    except ZeroDivisionError : addTimeInterval = np.inf

    params = l0, growthRate, kb0, mu0, nu0, epsilon0
    sol = [params]

    while t < endTime :

        print(t < endTime)

        if endTime <= (t + addTimeInterval) :
            nextStopTime = endTime
            addNodesFlag = False
        else:
            nextStopTime = t + addTimeInterval
            addNodesFlag = True

        print("Adding Nodes?", addNodesFlag)
        if addNodesFlag : print("ADDING NODES AT t =", nextStopTime)
        else : print("STOPPING AT t =", nextStopTime)

        t_span = [t, nextStopTime]                      # start time to endtime
        t_pts = np.arange( t, nextStopTime, 0.01)       # times with guaranteed
        t_pts[-1] = nextStopTime                        # ensure last time is the next stop time (numerical inaccracies messed this up)

        func = lambda time, qs : dqdt(time, qs, growthRate, kb0, mu0, nu0, epsilon0, width, epsilonLJ, rm, Lc)
        s = solve_ivp(func, t_span, q0, t_eval=t_pts)
        sol.append(s)
        t = s.t[-1]     # set current time to last time point of the of the solver

        #params = growthRate, kb0, mu0, nu0, epsilon0, width, kon, kOff0, ks, Lmax, Lc, Fc
        """
        params = growthRate, kb0, mu0, nu0, epsilon0, width, epsilonLJ, rm, Lc

        backend = 'vode'
        #backend = 'dopri5'
        # backend = 'dop853'
        #backend = 'RK45'
        solver = ode(dqdt).set_integrator(backend)
        #solver = ode( lambda time,vars:dqdt(time,vars,*params) ).set_integrator(backend)
        #solverOther = ode(dqdt).set_integrator(backend)

        #print("q0 =", q0)
        #solver.set_initial_value(q0, t).set_f_params(l0, growthRate, kb0, mu0, nu0, epsilon0, totalLength0)
        solver.set_initial_value(q0, t).set_f_params(growthRate, kb0, mu0, nu0, epsilon0, width, epsilonLJ, rm, Lc)
        #solver.set_initial_value(q0, t)
        #solverOther.set_initial_value(qOther, t).set_f_params(q0, l0_other, l0, growthRate, kb0, mu0, nu0, epsilon0, totalLength0)


        #solver.integrate(nextStopTime)

        print("StopTime", nextStopTime)

        #while ( solver.successful() and solverOther.successful() ) and solver.t < nextStopTime :
        while solver.successful() and solver.t < nextStopTime :

            realCall = True

            solver.integrate(endTime, step=True)
            dt = solver.t - t

            # update each filament's attributes based on the new q solutions -----------------------
            # store the node positions in an array and save these positions

            solutions = []
            bothBindX = []
            bothBindY = []
            stiffData[0].append(t)

            qLengths = [ len( filament.q ) for filament in filaments ]
            for i in range( len(qLengths) ) :
                #print("Updating Filament", filaments[i] )
                #lt = length(t, filaments[i].l0, growthRate)
                if i == 0 :
                    qNew = solver.y[ : qLengths[0] ]
                else :
                    qNew = solver.y[ qLengths[i-1] : qLengths[i-1] + qLengths[i] ]
                filaments[i].qUpdate( qNew )
                print("CHECKING LENGTH", filaments[i].lt[0])
                filaments[i].updateNodesPos()
                print("CHECKING Positions", filaments[i].nodePos)
                filaments[i].updateCom()
                newSiteCells = filaments[i].updateBindingSites(dt)
                filaments[i].updateSpacialDots()
                filaments[i].updateEndSpeed()
                #if len(newSiteCells) != 0 :
                    #print("New sites here:", newSiteCells)
                    #print("Before", [ filaments[i].qdots_old[m] for m in newSiteCells ] )
                    #print("After", [ filaments[i].qdots[m] for m in newSiteCells ] )
                    #input("press enter to continue")
                #if dt < 1e-4 and t != 0 :
                    #print("Before", filaments[i].qdots_old )
                    #print("After", filaments[i].qdots )
                    #print("difference", filaments[i].qdots - filaments[i].qdots_old)
                    #pass

                #bindX = [[filaments[i].bindingSites[m][n].bx for n in range(len(filaments[i].bindingSites[m]))] for m in range(len(filaments[i].bindingSites))]

                bindX = []
                for m in range( len(filaments[i].bindingSites) ) :
                    for n in range( len(filaments[i].bindingSites[m]) ) :
                        bindX.append( filaments[i].bindingSites[m][n].bx )
                bothBindX.append(bindX)

                #bindY = [[filaments[i].bindingSites[m][n].by for n in range(len(filaments[i].bindingSites[m]))] for m in range(len(filaments[i].bindingSites))]

                bindY = []
                for m in range( len(filaments[i].bindingSites) ) :
                    for n in range( len(filaments[i].bindingSites[m]) ) :
                        bindY.append( filaments[i].bindingSites[m][n].by )
                bothBindY.append(bindY)

                solutions.append(filaments[i].nodePos)

                stiffData[i+1][0].append(filaments[i].eqns)
                stiffData[i+1][1].append(filaments[i].vals)
                stiffData[i+1][2].append(filaments[i].q)
                stiffData[i+1][3].append(filaments[i].qdots)
                stiffData[i+1][4].append(filaments[i].xdots)
                stiffData[i+1][5].append(filaments[i].ydots)
                stiffData[i+1][6].append(filaments[i].lateralForces)
                stiffData[i+1][7].append(filaments[i].lateralTorques)
                stiffData[i+1][8].append(filaments[i].endSpeed)
                stiffData[i+1][9].append(filaments[i].stressX)
                stiffData[i+1][10].append(filaments[i].stressY)

                try : print("filament", i+1, filaments[i].lateralForces[0] )
                except IndexError : pass

                #filaments[i].eqns_old = filaments[i].eqns
                #filaments[i].vals_old = filaments[i].vals
                #filaments[i].qdots_old = filaments[i].qdots.copy()

            print("stiff length", len(stiffData))
            sol.append( [solver.t, solutions, bothBindX, bothBindY] )
            print(len(sol[-1]))

            #---------------------------------------------------------------------------------------

            #print([solver.t, *solver.y])
            print("\n---------------------------------------------------------------------------\n")
            print("Time =", solver.t, "\t dt =", dt)

            #t = sol[-1][0]
            t = solver.t

            #if t > 1e-3 and dt < 1e-4 :
                #print("time step is too small")
                #input("stopza")
        """

        #addNodesFlag = False      # uncomment to turn off cell divsion feature
        if addNodesFlag :

            lastAddTime = t

            #print("BEGIN ADDING NODES")

            for filament in filaments :
                filament.cellDivision()
                filament.lastAddTime = lastAddTime

            lOld = l0 #length(t, l0, growthRate)
            #print("length before addition", length(t, lOld, growthRate))
            lNew = lOld / 2
            #totalLength = (len(q0) - 2) * lNew

            #muOld = dragCoefficient(t, mu0, lNew, lOld)
            #nuOld = dragCoefficient(t, nu0, lNew, lOld)
            #epsilonOld = rotDragCoefficient(t, epsilon0, lNew, lOld)

            #mu0 = muOld / 2
            #nu0 = nuOld / 2
            #epsilon0 = epsilonOld / 8

            """q0 = [ q[0] - lNew * np.cos(q[2]), q[1] - lNew * np.sin(q[2])]
            for i in range( 2 , len(q) ) :
                q0.append(q[i])
                q0.append(q[i])"""

            #q0 = np.concatenate( (filaments[0].q, filaments[1].q) , axis=None )
            q0 = filaments[0].q

            #l0 = lNew
            #print("length after addition", length(t, lNew, growthRate))
            #totalLength0 = l0 * len(q0)
            #numRods *= 2

            #print("DONE ADDING NODES")

    print("Integration Complete")

    #stiffData = filaments[0].stiffData

    #pickle.dump(stiffData, open(outputFile2, "wb"))

    #pickle.dump(data, open(outputFile, "wb"))
    pickle.dump(sol, open(outputFile, "wb"))

    #plotsV4.configurationPlot(outputFile)            # generate configuration plot
    #plotsV4_withSites_Single.configurationPlot(outputFile)  # generate configuration plot w/ binding sites
    #plotsV4.constraintForcePlot(outputFile)          # generate constraint force plots

    #os.remove("equations.pkl")  # remove equation file, you don't need it anymore

    return


################################################################################

random.seed(7)

startTime = time.clock()

dt = 0.0

mu0 = 1e-9
#muVals = 0.000000001
kb0 = float(sys.argv[2])
#kb0 = 1e-7#[1e-8, 5e-8, 1e-7, 5e-7, 1e-6, 5e-6, 1e-5]
nu0 = 100*mu0#[100*mu0, 1000*mu0] # [2*mu0, 5*mu0, 10*mu0,

growthRate = 0.1        # rate of cell growth (micrometers/minutes)
endTime = 90.0            # total simulation time
numRods = 5             # initial number of rods
angle = 90              # angle subtended by the initial filament configuration (in degrees)

#mu0 = 1e-9             # initial parallel drag coefficient
#kb0 = 10000000         # initial angular spring constant

#for nu0 in nuVals :
    #for kb0 in kbVals :

#nu0 = mu0 * 100        # initial perpendiculat drag coefficient
epsilon0 = nu0 / 10     # initial angular drag coefficient

width = 0.5             # cell width (micrometers)
"""kon = 33             # binding site association rate
kOff0 = 5               # dissociation rate for bonded sites
ks = 1e-8               # spring constant between two bonded sites
Lmax = 2 * width        # maximum length at which a bond can form
Lc = 2.5 * width        # characteristic distance between two bonded sites
Fc = ks * (Lc - width)  # characteristic force between binding sites"""
epsilonLJ = 0.0
rm = 3 * width
Lc = 10 * width
#Lc = 2.5 * 2**(-1/6) * rm

lambdaVals = [None]     # none since the inital lambda values are unknown
realCall = False        # flag used to determine if a call to dqdt is real
lastAddTime = 0         # time of the last cell division

#q1 = [0, 0, 0, np.pi/8, np.pi/4]
#q2 = [0, -2.5, 0, np.pi/8, np.pi/4]
#q2 = [0, -3, 0, np.pi/6, np.pi/4, np.pi/3]
#q2 = [3, -3, -np.pi/8, -np.pi/16, np.pi/10, np.pi/6]

# similar curvature
#q1 = [0, 0, 0, np.pi/6, np.pi/4, np.pi/3]
#q2 = [0.5, -1, 0, np.pi/6, np.pi/4, np.pi/3, np.pi/2.5]

#straight configuration
#q1 = [0, 0, 0, 0, 0, 0, 0]
#q2 = [-1, -1, 0, 0, 0, 0, 0, 0]

# two cell test
# q1 = [0, 0] + [ i * np.pi/18 for i in range(-6, 7) ]


#angle = 90
#angle *= np.pi/180      # convert angle from degrees to radians

"""q1 = [0,0,-angle/2]

step = angle / (numRods-1)
for i in range(3, numRods+2) :
    q1.append(q1[-1] + step)
"""
#q2 = [1, -1, 0]

#q1 = [0.0 ,0.0, 0.0, 0.0, 0.0]
#q1 = [0.0,0.0,-35*np.pi/180, 0.0, 35*np.pi/180]

# six cells with small perturbation at center
angle = float(sys.argv[1])      # horizontal tilt of the two cells in the perturbation
q1 = [0.0,0.0, 0.0, 0.0, -1 * angle * np.pi/180, angle * np.pi/180, 0.0, 0.0]

#filaments = [Filament(q1, 5, 50, growthRate), Filament(q2, 7, 70, growthRate)]
filaments = [Filament(q1, 5, 0, growthRate)]

print("NEW CALL:", nu0, kb0)
#main(endTime, numRods, growthRate, angle, mu0, nu0, epsilon0, kb0, width, kon, kOff0, ks, Lmax, Lc, Fc)
main(endTime, numRods, growthRate, angle, mu0, nu0, epsilon0, kb0, width, epsilonLJ, rm, Lc)

# Calulating runtime, output in form "xx hrs, xx mins, xx secs"
totalRuntime = time.clock()-startTime
mins = round(totalRuntime//60)
secs = round(totalRuntime % 60)
if mins >= 60 :
    hrs = round(mins // 60)
    mins = round(mins % 60)
    print( "Runtime =", hrs, "hrs", mins, "minutes", secs, "seconds" )
else : print( "Runtime =", mins, "minutes", secs, "seconds" )

print("Sweet Baby Jesus, IT WORKS!")


################################################################################
