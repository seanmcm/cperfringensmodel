#!/bin/bash

date
for psi in 1 100
do
    for a in 1 50 100
    do
        for i in 1 2 3 4 5 6 7 8 9 10
        do
            #                           psi  a  t   seed
            python exponentialGrowth.py $psi $a 100 $i
        done
    done
done
wait
date
echo "Complete"
