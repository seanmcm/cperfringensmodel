#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 04/28/2019

import pdb
import numpy as np
from scipy.integrate import ode
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
#import matplotlib.animation as manimation
import sys


def analysis(filename) :

    stiffData = pickle.load(open(filename, "rb"))

    tVec = stiffData[0]

    # organize filament 1 data

    # rawData
    f1_eqns = stiffData[1][0]
    f1_vals = stiffData[1][1]
    f1_qs = stiffData[1][2]
    f1_qdots = stiffData[1][3]

    f1_qVecs = [ [ f1_qs[i][j] for i in range(len(f1_qs)) ] for j in range(len(f1_qs[0])) ]
    f1_qdotVecs = [ [ f1_qdots[i][j] for i in range(len(f1_qdots)) ] for j in range(len(f1_qdots[0])) ]

    f2_eqns = stiffData[2][0]
    f2_vals = stiffData[2][1]
    f2_qs = stiffData[2][2]
    f2_qdots = stiffData[2][3]

    f2_qVecs = [ [ f2_qs[i][j] for i in range(len(f2_qs)) ] for j in range(len(f2_qs[0])) ]
    f2_qdotVecs = [ [ f2_qdots[i][j] for i in range(len(f2_qdots)) ] for j in range(len(f2_qdots[0])) ]

    qdotVecs = f1_qdotVecs + f2_qdotVecs

    plt.figure(1)
    plt.title("Filament 1 qs")
    plt.xlabel("Time (min)")
    plt.ylabel("qdots")
    for i in range( len(f1_qVecs) ) :
        plt.plot(tVec, f1_qVecs[i])
    plt.legend(["x0","y0","theta0","theta1","theta2","theta3"])

    plt.figure(2)
    plt.title("Filament 1 qdots")
    plt.xlabel("Time (min)")
    plt.ylabel("qdots")
    for i in range( len(f1_qdotVecs) ) :
        plt.plot(tVec, f1_qdotVecs[i])
    plt.legend(["x0","y0","theta0","theta1","theta2","theta3"])

    plt.figure(3)
    plt.title("Filament 2 qs")
    plt.xlabel("Time (min)")
    plt.ylabel("qdots")
    for i in range( len(f2_qVecs) ) :
        plt.plot(tVec, f2_qVecs[i])
    plt.legend(["x0","y0","theta0","theta1","theta2","theta3"])

    plt.figure(4)
    plt.title("Filament 2 qdots")
    plt.xlabel("Time (min)")
    plt.ylabel("qdots")
    for i in range( len(f2_qdotVecs) ) :
        plt.plot(tVec, f2_qdotVecs[i])
    plt.legend(["x0","y0","theta0","theta1","theta2","theta3"])

    plt.show()



####################################################################################################

try :
    filename = str(sys.argv[1])
    analysis(filename)
    #constraintForcePlot(filename)
except IndexError :
    print("If loading package, continue, otherwise\nERROR: No file given -> try again")
