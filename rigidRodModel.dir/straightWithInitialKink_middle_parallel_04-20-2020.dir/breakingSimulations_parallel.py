#!/usr/local/env python

import os
import pickle
import numpy as np
from multiprocessing import Pool

from exponentialGrowth import main
from dataManagement import data_main

def simulationSet(args) :

    originalArgs = args
    print("STARTING:", originalArgs)

    numRods = 40
    args.append(numRods)

    flag = "true"
    while flag == "true" :

        main(args)
        data_main()

        f1 = open("endtime.txt", "r")
        endtime = f1.readlines(5)[0]
        f1.close()
        os.remove("endtime.txt")

        f2= open("endtimeFlag.txt", "r")
        flag = f2.readlines(5)[0]
        f1.close()
        os.remove("endtimeFlag.txt")

        print(flag, endtime, originalArgs)

        numRods += 20
        args[3] = numRods

    os.remove("current.pkl")

    print("FINISHED:", originalArgs)
    return


kb_mu_ratios = [1, 15, 30, 45, 60, 75, 90, 100, 125, 150, 175, 200]  # argument 1
rates = [0.05, 0.1, 0.2, 0.3]                                       # argument 2
angles = [5, 10, 15, 20, 30]                                        # argument 3

paramsets = []
for angle in angles :
    #print("angle =", angle)
    for kb_mu_ratio in kb_mu_ratios :
        #print("kb_mu_ratio =", kb_mu_ratio)
        for rate in rates :
            #print("rate =", rate)
            paramsets.append([kb_mu_ratio, rate, angle])

pool = Pool(8)
pool.map(simulationSet, paramsets)
