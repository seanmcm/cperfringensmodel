#import pdb
import numpy as np
import scipy as sp
from scipy import signal
from scipy import integrate
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import optimize
from scipy.interpolate import interp1d
from scipy import interpolate
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
#import matplotlib.animation as manimation
import sys
import pandas as pd
import seaborn as sns

plt.style.use('./mystyle.mplstyle')

# import the predicted critical kinking length
data = []
with open("data_test.csv") as f :
    csv_reader = csv.reader(f, delimiter=',')
    for row in csv_reader :
        row = [ float(x) for x in row ]     # convert the values form strings to floats
        row = np.array(row)                 # convert the list to a numpy array
        data.append( row )
data = np.array(data)

# upack the parameters
psi, a, l0, r, kb, mu, nu, epsilon, numRods0, angle, rel_tol, abs_tol, seed_val = tuple(data[0])
states = data[1:]

# determine the number of cells in the system based on the length of the state array
numRods = lambda state : int( (len(state) - 3) // 4 )

# sort the state array values
t = np.array([state[0] for state in states])
ls = np.array([state[1] for state in states])
x0 = np.array([state[2] for state in states])
y0 = np.array([state[3] for state in states])
thetas = np.array([np.array(state[4:numRods(state)+4]) for state in states])
x0dots = np.array([state[numRods(state)+4] for state in states])
y0dots = np.array([state[numRods(state)+5] for state in states])
thetadots = np.array([np.array(state[numRods(state)+6:2*numRods(state)+6]) for state in states])
Fxs = np.array([np.array(state[2*numRods(state)+6:3*numRods(state)+5]) for state in states])
Fys = np.array([np.array(state[3*numRods(state)+5:]) for state in states])

print(x0dots[9])

T_trans = np.zeros( len(t) )
T_rot = np.zeros( len(t) )
T_growth = np.zeros( len(t) )
V = np.zeros( len(t) )

for i in range(len(t)) :

    numRods = len(thetas[i])
    l = ls[i]

    # compute the xdots and ydots
    xdots, ydots = np.zeros(numRods), np.zeros(numRods)
    xdots[0], ydots[0] = x0dots[i], y0dots[i]
    theta = thetas[i]
    for j in range( 1, len(xdots) ) :

        xdots[j] = xdots[j-1] + 0.5 * ( l * r * np.cos(thetas[i][j-1]) - l * thetadots[i][j-1] * np.sin(thetas[i][j-1]) + l * r * np.cos(thetas[i][j]) - l * thetadots[i][j] * np.sin(thetas[i][j]) )

        ydots[j] = ydots[j-1] + 0.5 * ( l * r * np.sin(thetas[i][j-1]) + l * thetadots[i][j-1] * np.cos(thetas[i][j-1]) + l * r * np.sin(thetas[i][j]) + l * thetadots[i][j] * np.cos(thetas[i][j]) )

    # translational kinetic energy
    T_trans[i] = sum( 0.5 * l * ( xdots**2 + ydots**2 ) )

    # rotational kinetic energy
    T_rot[i] = sum( l**3 * thetadots[i]**2 / 24 )

    # growth kinetic energy
    T_growth[i] = numRods * ( l**2 * r / 24 )

    # potential energy
    V[i] = sum([ 0.5 * kb * (thetas[i][j+1] - thetas[i][j]) for j in range(numRods-1) ])


T = T_trans + T_rot + T_growth
tdiv = np.log(2) / r

fig, axs = plt.subplots(2,2)

axs[0,0].set_xlabel("Time")
axs[0,0].set_ylabel("Total energy")
axs[0,0].plot(t, T+V, label=r"$T+V$")
axs[0,0].vlines([tdiv, 2*tdiv], ymin=min(T+V), ymax=max(T+V), linestyles='dashed')
axs[0,0].set_yscale('log')

axs[0,1].set_xlabel("Time")
axs[0,1].set_ylabel("Potential energy")
axs[0,1].plot(t, V, label=r"$V$")
axs[0,1].vlines([tdiv, 2*tdiv], ymin=min(V), ymax=max(V), linestyles='dashed')

axs[1,0].set_xlabel("Time")
axs[1,0].set_ylabel("Kinetic energy")
axs[1,0].plot(t, T, label=r"$T$")
axs[1,0].vlines([tdiv, 2*tdiv], ymin=min(T), ymax=max(T), linestyles='dashed')
axs[1,0].set_yscale('log')

axs[1,1].set_xlabel("Time")
axs[1,1].set_ylabel("Kinetic energies")
#axs[1,1].plot(t, T, label=r"$T$")
axs[1,1].plot(t, T_trans, label=r"$T_{trans}$")
axs[1,1].plot(t, T_rot, label=r"$T_{rot}$")
axs[1,1].plot(t, T_growth, label=r"$T_{growth}$")
axs[1,1].vlines([tdiv, 2*tdiv], ymin=min(T_trans), ymax=max(T_trans), linestyles='dashed')
axs[1,1].legend(loc="best")
axs[1,1].set_yscale('log')


fig.tight_layout()
plt.savefig("stress_check.dir/energy.pdf")
plt.clf(), plt.close()



# insert zeros into the force arrays
Fxs = np.array([np.insert(Fx, [0, len(Fx)], [0,0]) for Fx in Fxs])
Fys = np.array([np.insert(Fy, [0, len(Fy)], [0,0]) for Fy in Fys])

Fmags = [ np.sqrt(Fxs[i]**2 + Fys[i]**2) for i in range(len(Fxs)) ]

stress_straight = lambda t : numRods0**2 * np.exp( 2*r*t ) / 8
center_stress = np.array( [Fmag[len(Fmag)//2] for Fmag in Fmags] ) / (mu * l0**2 * r)

with open('stress_check.dir/out.txt', 'w') as f:
    for i in range(len(t)) :
        print(f't = {t[i]} \t F = {center_stress[i]}', file=f)

"""for i in range(len(t)) :

    Fx = Fxs[i]
    zeta = np.linspace(-1, 1, endpoint=True, num=len(Fx))
    plt.plot(zeta, Fx)
    plt.title(f"t = {t[i]}")
    plt.savefig(f"stress_check.dir/stressX.dir/stressX_{round(t[i])}_{i}.pdf")
    plt.clf()
    plt.close()"""

for time, cs, ss in zip(t, center_stress, stress_straight(t) ) : print(time,cs, ss)

plt.plot(t, center_stress)
#plt.plot(t, stress_straight(t) )
plt.savefig("stress_check.dir/center_stress.pdf")
