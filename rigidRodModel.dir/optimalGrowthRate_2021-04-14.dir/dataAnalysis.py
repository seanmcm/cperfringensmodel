#!/usr/local/env python

import numpy as np
import scipy as sp
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy.optimize import fsolve
from scipy.stats import gamma
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
from random import randrange
import statistics as stat
import math
import time
import csv
import os
#import pickle
import dill as pickle
import matplotlib
#matplotlib.use("Agg")      # COMMENT THIS FOR PLOTS FOR plt.show(), UNCOMMENT FOR ANIMATIONS
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys


# formats figures with larger fonts
plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'figure.autolayout': True})

psiVals, kbVals = [], []

expRateMeans, expRateStDevs = [], []
avgNumBreaks, stDevNumBreaks = [], []
meanBreakLens = []
#for kbVal in kbVals :
#for psiVal in psiVals :
for filename in sys.argv[1:] :

    #dataFile = open(f"dataFiles.dir/simulationData_sync{sync}_psi{psiVal}.pkl", "rb")
    dataFile = open(filename, "rb")
    dataSets = pickle.load(dataFile)
    dataFile.close()

    #kbVal = psiVal * (mu * l0**2 * growthRate)

    chainData = []

    expRateVals = []
    breakCount = []
    breakLenVals = []

    for simulationData in dataSets :

    #for i in range(mc_steps) :

        # setup output based on option chosen
        """if outOpt == "file" :
            filestr = f"simulationDetails.dir/details_psi{psiVal}-{i}.txt"
            try : os.remove(filestr)
            except : pass
            f = open(filestr, "w")
            sys.stdout = f
        elif outOpt == "none" :
            sys.stdout = open(os.devnull, "w")"""

        #print(f"Phenomenologicial Simulation: Psi = {psiVal}, Run No. {i}")

        #expRate, numBreaks, breakLens, times, displacementVec, timesDiv, displacementVecDiv, timesBreak, displacementVecBreak, chainLens, chainTips, filaments = main(sync, endTime, numRods, l0, growthRate, mu, kbVal, Fcrit, kbDist)

        #dataFile = open(f"dataFiles.dir/simulationData_sync{sync}_psi{psiVal}-{i}.pkl", "rb")
        #endTime, sync, psiVal, i, seedVal, numRods, l0, growthRate, mu, kbVal, kbStd, divLenStd, expRate, numBreaks, breakLens, times, displacementVec, timesDiv, displacementVecDiv, timesBreak, displacementVecBreak, chainLens, chainTips, filaments = pickle.load(dataFile)
        #dataFile.close()

        #simulationData = main(sync, endTime, numRods, l0, growthRate, mu, kbVal, Fcrit, kbDist)
        #dataFile = open(f"dataFiles.dir/simulationData_sync{sync}_psi{psiVal}-{i}.pkl", "wb")
        #pickle.dump(simulationData, dataFile)
        #dataFile.close()

        endTime, sync, psiVal, i, seedVal, numRods, l0, growthRate, mu, kbVal, kbStd, divLenStd, expRate, numBreaks, breakLens, times, displacementVec, timesDiv, displacementVecDiv, timesBreak, displacementVecBreak, chainLens, chainTips, filaments = simulationData


        chainData.append([times, chainLens, chainTips])

        #if outOpt == "file" : f.close()
        #sys.stdout = sys.__stdout__

        expRate = (displacementVecBreak[-1] - displacementVecBreak[0]) / (timesBreak[-1] - timesBreak[0])

        expRateVals.append( expRate )
        breakCount.append(numBreaks)
        breakLenVals.append(stat.mean(breakLens))

        plt.clf()
        #plt.title(rf"Expansion Distance ($\Psi$ = {psiVal})" )
        plt.title(rf"$\Psi$ = {psiVal}" )
        plt.xlabel(r"Time (min)")
        plt.ylabel(r"Expansion Distance $D(t)$ ($\mu$m)")
        plt.plot(times, displacementVec, label=r"D(t)", lw=3)
        #timeRange = int(500 + 25*(psiVal-1))
        timeRange = len( [ timePt for timePt in times if (timePt <= np.log( max(displacementVec) / (numRods * l0) ) * l0 / growthRate ) ] )
        plt.plot(np.array(times[:timeRange]), numRods * l0 * np.exp(growthRate * np.array(times[:timeRange]) / l0), label="Exponential Growth", ls=':', lw=3)
        plt.plot(timesBreak[:-1], displacementVecBreak[:-1], 'o', label="Breaks", alpha=0.75, marker='|', ms=20)
        plt.plot(timesDiv, displacementVecDiv, 'o', label="Divisions", ms=5)
        plt.legend(loc='upper left', fontsize=12)
        plt.savefig(f"plots.dir/trajectories.dir/displacementVsTime_psi{psiVal}_{i}.pdf")

        expRateVec = [ (displacementVec[i+1]-displacementVec[i])/(times[i+1]-times[i]) for i in range(len(displacementVec)-1)]

        plt.clf()
        plt.title(rf"Expansion Rate ($\Psi$ = {psiVal})" )
        plt.xlabel(r"Time (min)")
        plt.ylabel(r"Expansion Rate $D'(t)$ ($\mu$m/min)")
        plt.plot(times[1:], expRateVec, lw=3)
        plt.savefig(f"plots.dir/trajectories.dir/expansionRateVsTime_psi{psiVal}_{i}.pdf")

        #if (psiVal == 2 and i == 2) : input("stop here")

        allFc = []
        for f in filaments : allFc += f.fc

        plt.clf()
        n, bins, patches = plt.hist(x=allFc, bins='auto', color='#0504aa', alpha=0.7, rwidth=0.85)

        plt.xlabel('Critical Force Value')
        plt.ylabel('Frequency')
        plt.title('Distribution of Critical Breaking Forces')

        plt.savefig(f"plots.dir/criticalForceHistograms.dir/critForceDist_psi{psiVal}-{i}.pdf")

    pickle.dump(chainData, open(f"plots.dir/chainData.dir/chainData_psi{psiVal}.pkl", "wb"))

    print("Done Psi =", psiVal)
    psiVals.append(psiVal)
    kbVals.append(kbVal)
    expRateMeans.append(stat.mean(expRateVals))
    expRateStDevs.append(stat.stdev(expRateVals))
    avgNumBreaks.append(stat.mean(breakCount))
    stDevNumBreaks.append(stat.stdev(breakCount))
    meanBreakLens.append(stat.mean(breakLenVals))

kb = kbVals[0]
r = growthRate / l0

#---------------------------------------------------------------------------------------------------

plt.clf()
plt.title(r"Expansion Rate Varies with $\Psi$" )
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Average Expansion Rate ($\mu$m/min)")
#plt.plot(psiVals, expRateMeans)
plt.errorbar(psiVals, expRate, yerr=None, fmt='-o', ecolor='r', lw=3, ms=10)
#plt.xscale('log')
#plt.yscale('log')
#plt.axvline(x=4e5, color='r')
#plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
plt.savefig(f"plots.dir/averageExpansionRate.pdf")

#---------------------------------------------------------------------------------------------------

plt.clf()
plt.title(r"Number of Breaks vs. $\Psi$" )
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Number of Breaks")
#plt.plot(psiVals, expRateMeans)
plt.errorbar(psiVals, avgNumBreaks, yerr=None, fmt='-o', ecolor='r', lw=3, ms=10)
#plt.xscale('log')
#plt.yscale('log')
#plt.axvline(x=4e5, color='r')
#plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
plt.savefig("plots.dir/numBreaksVsPsi.pdf")

#---------------------------------------------------------------------------------------------------

plt.clf()
plt.title(r"Average Break Length Varies with $\Psi$" )
#plt.xlabel(r"Parallel Drag Coefficient $\mu$ (kg / $\mu$m min)")
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Breaking Length ($\mu$m)")
plt.plot(psiVals, meanBreakLens, 'o', lw=3, ms=12, markeredgewidth=3, fillstyle='none', label="Simulation")
#plt.errorbar(psiVals, meanBreakLens, yerr=[], fmt='-o', ecolor='r')
#plt.axvline(x=4e5, color='r')
#plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
Fcrit = lambda psi : 3.4926004228086316 * psi + 0.4179497997198875
f = lambda psi : 2 * l0 * np.sqrt( 2 * Fcrit(psi) )
psiVals2 = np.linspace(1, 10, 1000)
plt.plot(psiVals2 , f(psiVals2), 'r', lw=3, label="Theory")
plt.legend(loc="upper left")

plt.savefig("plots.dir/breakLenVsPsi.pdf")

#---------------------------------------------------------------------------------------------------

plt.clf()
#plt.title(r"Average Expansion Rate vs. Average Break")
plt.xlabel(r"Average Breaking Length ($\mu$m)")
plt.ylabel(r"Average Expansion Rate ($\mu$m/min)")
plt.plot(meanBreakLens, expRateMeans, 'o', lw=3, ms=12, markeredgewidth=3, fillstyle='none', label="Simulation")
f = lambda Lc : (Lc * r) / (2 * np.log(2))
plt.plot(meanBreakLens, f(np.array(meanBreakLens)), '-r', lw=3, label="Theory")
plt.legend(loc="upper left")

plt.savefig("plots.dir/expansionRateVsBreakLen.pdf")

#---------------------------------------------------------------------------------------------------

ndExpRates = [expRate/(l0 * r) for expRate in expRateMeans]
breakNums = [breakLen/l0 for breakLen in meanBreakLens]

#---------------------------------------------------------------------------------------------------

plt.clf()
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
#plt.ylabel(r"$D'^* = \frac{D'}{l_0 r}$")
plt.ylabel(r"Nondimensionalized Expansion Rate")
plt.plot(psiVals, ndExpRates, '-o', lw=3, ms=12)
plt.savefig(f"plots.dir/ndExpRateVsPsi.pdf")

#---------------------------------------------------------------------------------------------------

plt.clf()
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Breaking Number") #($N = L_c / l_0$)")
plt.plot(psiVals, breakNums , '-o', lw=3, ms=12, markeredgewidth=3, fillstyle='none', label="Simulation")
plt.savefig("plots.dir/breakNumVsPsi.pdf")

#---------------------------------------------------------------------------------------------------

l0Vals = [ np.cbrt( kb / (mu * r * psiVal) ) for psiVal in psiVals ]
muVals = [ kb / (l0**3 * r * psiVal) for psiVal in psiVals ]
rVals = [ kb / (mu * l0**3 * psiVal) for psiVal in psiVals ]

#---------------------------------------------------------------------------------------------------

plt.clf()
fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel(r'$k_b$')
ax1.set_ylabel('Expansion Rate ($\mu$m/min)', color=color)
ax1.plot(kbVals, expRateMeans, '-o', lw=3, ms=12, color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel(r'Breaking Length', color=color)  # we already handled the x-label with ax1
ax2.plot(kbVals, meanBreakLens, '--^', lw=3, ms=10, color=color)
plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.savefig("plots.dir/kbVs_expRate_breakLen.pdf")

#---------------------------------------------------------------------------------------------------

expRateMeans2 = [ ndExpRates[i] * l0Vals[i] * r for i in range(len(ndExpRates)) ]
meanBreakLens2 = [ breakNums[i] * l0Vals[i] for i in range(len(breakNums)) ]

plt.clf()
fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel(r'$l_0$')
ax1.set_ylabel('Expansion Rate ($\mu$m/min)', color=color)
ax1.plot(l0Vals, expRateMeans2, '-o', lw=3, ms=12, color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel(r'Breaking Length', color=color)  # we already handled the x-label with ax1
ax2.plot(l0Vals, meanBreakLens2, '--^', lw=3, ms=10, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.savefig("plots.dir/l0Vs_expRate_breakLen.pdf")

#---------------------------------------------------------------------------------------------------

plt.clf()
fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel(r'$\mu$')
ax1.set_ylabel('Expansion Rate ($\mu$m/min)', color=color)
ax1.plot(muVals, expRateMeans, '-o', lw=3, ms=12, color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel(r'Breaking Length', color=color)  # we already handled the x-label with ax1
ax2.plot(muVals, meanBreakLens, '--^', lw=3, ms=10, color=color)
plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.savefig("plots.dir/muVs_expRate_breakLen.pdf")


#---------------------------------------------------------------------------------------------------

expRateMeans2 = [ ndExpRates[i] * l0 * rVals[i] for i in range(len(ndExpRates)) ]

plt.clf()
fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel(r'$r$')
ax1.set_ylabel('Expansion Rate ($\mu$m/min)', color=color)
ax1.plot(rVals, expRateMeans2, '-o', lw=3, ms=12, color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel(r'Breaking Length', color=color)  # we already handled the x-label with ax1
ax2.plot(rVals, meanBreakLens, '--^', lw=3, ms=10, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.savefig("plots.dir/rVs_expRate_breakLen.pdf")
