#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 10/9/2018

from timeit import default_timer as timer
import numpy as np
import scipy as sp
from scipy import optimize
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle

#from main import *



def rateFunc(Favg, rateMax, fStall, fTrans) :
    return rateMax / ( 1 + sp.exp( 7 * (2*Favg - fStall - fTrans) / (2 * (fStall - fTrans))) )

def forceFunc(i, mu, l, rateVals, q) :
    return 0.5 * mu * sum(l) * sum(rateVals) * q[i] * (1 - q[i])

def forceSolver(vars, *args) :

    l, rateMax, mu, fTrans, fStall, q, scale = args
    L = sum(l)

    mu *= 1e9 * scale
    fTrans *= 1e9
    fStall *= 1e9

    rateVals = vars[:len(vars)//2]
    forces = vars[len(vars)//2:]

    eqns = []

    for i in range( len(vars)//2 ) :

        if i == len(l)-1 :
            eqns.append( rateVals[i] * ( 1 + sp.exp( 7 * (forces[i]-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax )
        else :
            eqns.append( rateVals[i] * ( 1 + sp.exp( 7 * (forces[i+1]+forces[i]-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax )

    for i in range( len(vars)//2 ) :
        eqns.append( forces[i] - 0.5 * mu * sum(l) * sum(rateVals) * q[i] * (1 - q[i]) )

    eqns = tuple(eqns)

    print("Equations?", eqns)

    return eqns

np.random.seed(7)
filaments = pickle.load(open("filaments100.pkl", "rb"))
#l = [6.3, 5.4, 7.1, 4.9, 6.4]
l = np.random.choice(filaments)
#l = [4.1282835906084285, 4.124312671057362, 4.033639909686707, 4.03666968119931, 4.255226417616408, 4.2815699354531676, 4.110462280060422, 4.127082455390594, 4.78912470756352, 4.7553403718606875, 4.967925169658106, 4.969857066123919, 3.9793169711113294, 3.9810497617678813, 4.070349627462596, 4.0665689871426896]
print(l)

q = [0.5]
for i in range( len(l) ) :
    q.append( 0.5 * ( sum(l[:i+1]) / sum(l) + 1 ) )

rateMax = 0.2
mu = 1e-9
fTrans = 5e-9
fStall = 1e-7

scale = 10

params = l, rateMax, mu, fTrans, fStall, q, scale

vars0 = ( [0.1] * int(len(l)) ) + ( [10**-8] * int(len(l)) )
vars0 = tuple(vars0)

sols = sp.optimize.fsolve( forceSolver, vars0, args=params )
#sols = sp.optimize.newton_krylov( lambda vars:forceSolver(vars, *params), vars0 )
#print("message", mesg)
#print("ier", ier)


rateSols = sols[: len(sols)//2]
forceSols = sols[len(sols)//2 :]

avgForces = []
for i in range( len(forceSols) ) :
    try : avgForces.append( (forceSols[i] + forceSols[i+1]) / 2 )
    except : avgForces.append( forceSols[i] / 2 )

print("rates", rateSols)
print("forces", forceSols)
print("lengths", l)
print(len(rateSols), len(avgForces))
plt.scatter(avgForces, rateSols)
X = np.linspace(0, 150, 250, endpoint=True)
V = rateFunc(X/1e9, rateMax, fStall, fTrans)
plt.plot(X,V)
plt.show()
