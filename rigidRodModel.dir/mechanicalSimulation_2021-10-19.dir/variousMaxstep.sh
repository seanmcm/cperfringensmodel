#!/bin/bash

echo "RUNNING"
MAXSTEPS=(1e-2 1e-3 1e-4 1e-5 1e-6 1e-7 1e-8)
for maxstep in "${MAXSTEPS[@]}"
do
    echo "MAXSTEP =" $maxstep
    python exponentialGrowth.py ${maxstep}
done
