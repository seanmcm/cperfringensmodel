#!/bin/bash

#ffmpeg -r 10 -f image2 -s 2400x1800 -i configurationGrid/time%d.0.png -vcodec libx264 -crf 25 -pix_fmt yuv420p configurationGrid.mp4

ffmpeg -r 10 -f image2 -s 1200x900 -i curvature/curvature_t%d.0.png -vcodec libx264 -crf 25 -pix_fmt yuv420p curvatureChange.mp4

ffmpeg -r 10 -f image2 -s 1200x900 -i deltaTheta/deltaTheta_t%d.0.png -vcodec libx264 -crf 25 -pix_fmt yuv420p deltaTheta_center.mp4

ffmpeg -r 10 -f image2 -s 1200x900 -i deltaThetaDot/deltaThetaDot_t%d.0.png -vcodec libx264 -crf 25 -pix_fmt yuv420p deltaThetaDot_center.mp4
