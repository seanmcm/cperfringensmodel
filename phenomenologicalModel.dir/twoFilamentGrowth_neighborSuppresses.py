#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 08/22/2018

import numpy as np
import scipy as sp
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt


# Parameters ##############################################################################

np.random.seed(7)

totalTime = 60  # mins
dt = 0.01   # timestep (minutes)

frac= 0.3
rateWithout = 0.2               # average growth rate of cell without a neighbor
rateWith = rateWithout * 0.6    # average growth rate of cell with a neighbor
avgDivLen = 10                  # average length at which a cell divides


# Initialization ##########################################################################

# FILAMENT #1
l1 = []
for i in range(7) :
    # initial length of a each cell with some random fluctuations
    l1.append( np.random.normal(3*avgDivLen/4, frac * 3*avgDivLen/4) )
L1 = [sum(l1)]

# FILAMENT #2
l2 = []
for i in range(11) :
    # initial length of a each cell with some random fluctuations
    l2.append( np.random.normal(avgDivLen/2, frac * avgDivLen/2) )
L2 = [sum(l2)]


# Simulation ##############################################################################

t = 0
tVec = [t]
while t < totalTime :

    # UPDATE FIRST FILAMENT

    l1Original = l1.copy()
    for i in range(len(l1)) :

        # determine a growth rate with some random fluctuations
        if (L1[-1] < L2[-1]) or ( sum(l1Original[:i]) < L2[-1] ) :
            growthRate = np.random.normal(rateWith, frac * rateWith)
        else : growthRate = np.random.normal(rateWithout, frac * rateWithout)

        l1[i] += growthRate * dt    # update growth

        # handle cell divsion if necessary
        if l1[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
            newLength = l1[i] / 2       # determine the length of the daughter cells
            l1[i] = newLength
            l1.insert(i+1, newLength)   # inset new cell

    L1.append(sum(l1))      # determine new total length


    # UPDATE SECOND FILAMENT

    l2Original = l2.copy()
    for i in range(len(l2)) :

        # determine a growth rate with some random fluctuations
        if (L2[-1] < L1[-1]) or ( sum(l2Original[:i]) < L1[-1] )  :
            growthRate = np.random.normal(rateWith, frac * rateWith)
        else : growthRate = np.random.normal(rateWithout, frac * rateWithout)

        l2[i] += growthRate * dt    # update growth

        # handle cell divsion if necessary
        if l2[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
            newLength = l2[i] / 2       # determine the length of the daughter cells
            l2[i] = newLength
            l2.insert(i+1, newLength)   # inset new cell

    L2.append(sum(l2))      # determine new total length

    t += dt
    print(t)
    tVec.append(t)

tVec = np.array(tVec)
L1 = np.array(L1)
L2 = np.array(L2)
LDiff = L1 - L2


# Plotting ###############################################################################

plt.figure(1)
plt.plot(tVec, LDiff, markersize=0.5)
plt.figure(2)
plt.plot(tVec, L1, tVec, L2, 'r', markersize=0.5)
plt.show()
