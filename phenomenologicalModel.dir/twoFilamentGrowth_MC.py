#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 08/20/2018

import numpy as np
import scipy as sp
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt

np.random.seed(7)

# Parameters ##############################################################################

mcSteps = 100
totalTime = 60  # mins
dt = 0.01   # timestep (minutes)

frac= 0.3
avgRate = 0.2
avgDivLen = 10

# Monte Carlo Simulation ##################################################################

F1_count = 0    # number of times filament 1 is longer
#F2_count = 0    # number of times filament 2 is longer

# Initialization ---------------------------------------------------------------------

# FILAMENT #1
l1_init = []
for i in range(7) :
    # initial length of a each cell with some random fluctuations
    l1_init.append( np.random.normal(3*avgDivLen/4, frac * 3*avgDivLen/4) )
L1_init = [sum(l1_init)]

# FILAMENT #2
l2_init = []
for i in range(11) :
    # initial length of a each cell with some random fluctuations
    l2_init.append( np.random.normal(avgDivLen/2, frac * avgDivLen/2) )
L2_init = [sum(l2_init)]


    # Simulation ---------------------------------------------------------------------

for n in range( mcSteps ) :

    # Should initial conditions be the same for each monte carlo simulation?

    l1 = l1_init.copy()
    L1 = L1_init.copy()

    l2 = l2_init.copy()
    L2 = L2_init.copy()

    t = 0
    tVec = [t]
    while t < totalTime :

        # UPDATE FIRST FILAMENT
        for i in range(len(l1)) :

            # determine a growth rate with some random fluctuations
            growthRate = np.random.normal(avgRate, frac * avgRate)
            l1[i] += growthRate * dt    # update growth

            # handle cell divsion if necessary
            if l1[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
                newLength = l1[i] / 2       # determine the length of the daughter cells
                l1[i] = newLength
                l1.insert(i+1, newLength)   # inset new cell

        L1.append(sum(l1))      # determine new total length


        # UPDATE SECOND FILAMENT
        for i in range(len(l2)) :

            # determine a growth rate with some random fluctuations
            growthRate = np.random.normal(avgRate, frac * avgRate)
            l2[i] += growthRate * dt    # update growth

            # handle cell divsion if necessary
            if l2[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
                newLength = l2[i] / 2       # determine the length of the daughter cells
                l2[i] = newLength
                l2.insert(i+1, newLength)   # inset new cell

        L2.append(sum(l2))      # determine new total length

        t += dt
        tVec.append(t)

    tVec = np.array(tVec)
    L1 = np.array(L1)
    L2 = np.array(L2)
    LDiff = L1 - L2
    finalDiff = LDiff[-1]

    if finalDiff > 0 : F1_count += 1
    print("Run", n+1, "is complete")

F2_count = mcSteps - F1_count
#eq_count = mcSteps - (F1_count + F2_count)

print("\n")
print("Filament 1 was longer", F1_count, "times")
print("Filament 2 was longer", F2_count, "times")
#print("The filam", F1_count, "times")

# Plotting ###############################################################################

"""plt.plot(tVec, LDiff, markersize=0.5)
#plt.plot(tVec, L1, tVec, L2, 'r', markersize=0.5)
plt.show()"""
