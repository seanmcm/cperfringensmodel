#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys


plt.title("Varied Angle")
#plt.title("Varied Location")

plt.axhline(y=0, color='black', linestyle='-')

for i in range(1, len(sys.argv)) :

    outfile = os.path.basename(sys.argv[i])
    filename = sys.argv[i]

    data = pickle.load(open(filename, "rb"))
    #data = pickle.load(open(filename, "rb"))
    #paramstr = r"$" + data[0][2:]
    paramstr = data[0]
    #kb, mu, growthRate, numRods, angle, pert_coefficient = data[0]
    data = np.array(data[1:])

    lengths, thetadotDiffs = data[:,0], data[:,1]
    stress = lengths**2 * float(paramstr.split('=')[1]) * (1-float(paramstr.split('=')[1]))

    plt.xlabel(r"Initial Length ($\mu$m)")
    plt.ylabel("Rate of Change of Pertubation Angle (radians / min)")
    plt.plot(stress, thetadotDiffs, '-o', label=paramstr)

plt.legend(loc="upper right")
plt.savefig('plots.dir/' + outfile[:-4] + '.pdf', format='pdf')
