#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 07/16/2018

import numpy as np
from scipy.integrate import ode
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
#import plotsV2

################################################################################
# SIMULATION NOTES
#   TIME-SCALE: minutes
#   LENGTH-SCALE: micrometers
################################################################################


def coefficientArray(numSites) :

    if numSites == 1 : return [0]
    x = [-0.5]
    while round(x[-1], 1) < 0.5 :
        x.append( x[-1] + (1/(numSites-1)) )
    return(x)


################################################################################
# CLASSES
################################################################################


class Site(object) :

    def __init__(self, f, cellIndex, z) :
        """ creates a new binding site object
                bx (float) : x coordinate of the binding site
                by (float) : y coordinate of the binding site
                f (Filament) : the filament the binding site is in
                bond (Bond) : the associated Bond object if the site is in a bond"""

        self.filament = f
        self.cellIndex = cellIndex
        self.z = z

        self.bx = f.com[cellIndex] + z * f.lt[cellIndex] * np.cos( f.q[cellIndex+2] )
        self.by = f.com[len(f.com)//2 + cellIndex] + z * f.lt[cellIndex] * np.sin( f.q[cellIndex+2] )

        self.bond = None

    def calculateDistance(self, other) :
        """ calculates the distance between the site and another site """
        return np.sqrt( (self.bx - other.bx)**2 + (self.by - other.by)**2 )

    def updatePositions(self) :
        """ updates the coordindates of the binding site """
        xc = self.filament.com[self.cellIndex]
        yc = self.filament.com[len(self.filament.com)//2 + self.cellIndex]
        thetas = self.filament.q[2:]
        length = self.filament.lt[self.cellIndex]

        self.bx = xc + self.z * length * np.cos( thetas[self.cellIndex] )
        self.by = yc + self.z * length * np.sin( thetas[self.cellIndex] )


#===============================================================================


class Bond(object) :

    def __init__(self, site1, site2) :
        """ creates a new Bond object
                site1 (Site) : the first binding site object
                site2 (Site) : the second binding site object """
        self.site1 = site1
        self.site2 = site2
        self.L = self.site1.calculateDistance(self.site2)
        self.force = None

    def calculateDistance(self) :
        """ calculates the distace between the two binding sites """
        self.L = self.site1.calculateDistance(self.site2)

    def calculateForce(self, ks, Leq) :
        """ calculates the magnitude of the force between the bonded site """
        self.force = ks * (self.L - Leq)


#===============================================================================


class Filament(object) :


    #---------------------------------------------------------------------------


    def __init__(self, q, l0, numSites, growthRate) :
        """ Create a new filament object
                q (array) : generalized coordinates
                l0 (float) : initial length of the cells
                numSite (int) : number of binding sites per cells """

        self.q = q
        self.l0 = l0
        self.numSites = numSites
        self.initalTotalLength = l0 * ( len(self.q) - 2 )
        self.bonds = []
        self.qdots = []
        self.lt = [ l0 ] * ( len(self.q) - 2 )
        self.lp = [ growthRate ] * ( len(self.q) - 2 )
        self.kSite = numSites * growthRate / l0
        #self.nextDivisionTime =

        # compute center of mass and node positions
        thetas = q[2:]
        xNodes = [ q[0]-l0/2 * np.cos( q[2] ) ]
        yNodes = [ q[1]-l0/2 * np.sin( q[2] ) ]

        xcom = [ q[0] ]
        ycom = [ q[1] ]

        for i in range( len(q) - 2 ) :

            if i != 0 :
                xcom.append( xcom[-1] + l0/2 * np.cos(thetas[i-1]) + l0/2 *np.cos(thetas[i]) )
                ycom.append( ycom[-1] + l0/2 * np.sin(thetas[i-1]) + l0/2 *np.sin(thetas[i]) )

            xNodes.append( xNodes[-1] + l0 * np.cos( thetas[i] ) )
            yNodes.append( yNodes[-1] + l0 * np.sin( thetas[i] ) )

        self.nodePos = xNodes + yNodes
        self.com = xcom + ycom


        self.zVals = []
        for i in range( len(self.q)-2 ) :
            temp = []
            for j in range(numSites) :
                temp.append( random.random() - 0.5 )
            self.zVals.append(temp)

        bind = []
        xc, yc = q[0], q[1]     # current center of masses
        for i in range( len(q) - 2 ) :
            temp = []
            #for zVal in coefficientArray(numSites) :
            for zVal in self.zVals[i] :
                temp.append( Site(self, i, zVal) )
            bind.append(temp)
            # get center of mass coordinates of next cell
            xc += l0/2 * np.cos(q[i+2])
            yc += l0/2 * np.sin(q[i+2])
        self.bindingSites = bind

        self.eqns = []
        self.vals = []


    #---------------------------------------------------------------------------


    def qUpdate(self, q) :
        """ updates the generalized coodinates q of the cells """
        self.q = q


    #---------------------------------------------------------------------------


    def cellDivision(self) :
        """ handles cell division """

        oldThetas = self.q[2:]
        thetas = []
        for i in range( len(oldThetas) ) :
            thetas.append(oldThetas[i])
            thetas.append(oldThetas[i])

        xOld = self.q[0]
        x = xOld - self.lt[0]/4 * np.cos( thetas[0] )

        yOld = self.q[1]
        y = yOld - self.lt[0]/4 * np.sin( thetas[0] )

        self.q = [x] + [y] + thetas

        oldSites = self.bindingSites.copy()
        newSites = []
        for i in range( len(oldSites) ) :
            cell1, cell2 = [] , []
            for j in range( len(oldSites[i]) ) :
                if oldSites[i][j].z <= 0 :
                    oldSites[i][j].z = 2 * oldSites[i][j].z + 0.5
                    cell1.append( oldSites[i][j] )
                else :
                    oldSites[i][j].z = 2 * oldSites[i][j].z - 0.5
                    cell2.append( oldSites[i][j] )
            newSites.append(cell1)
            newSites.append(cell2)
        self.bindingSites = newSites

        ltOld = list(self.lt)
        ltNew = []
        for i in range( len(ltOld) ) :
            ltNew.append( ltOld[i] )
            ltNew.append( ltOld[i] )
        self.lt = np.array( ltNew )

        self.updateNodesPos()
        self.updateCom()

        for i in range( len(self.bindingSites) ) :
            for site in self.bindingSites[i] :
                site.cellIndex = i
                site.updatePositions()


    #---------------------------------------------------------------------------


    def updateNodesPos(self) :
        """ update the node position values based on the new generalized coordinates """

        #print("updating nodePos!", length)
        thetas = self.q[2:]
        xNodes = [ self.q[0] - self.lt[0]/2 * np.cos( self.q[2] ) ]
        yNodes = [ self.q[1] - self.lt[0]/2 * np.sin( self.q[2] ) ]
        for i in range( len(self.q) - 2 ) :
            xNodes.append( xNodes[-1] + self.lt[i] * np.cos(thetas[i]) )
            yNodes.append( yNodes[-1] + self.lt[i] * np.sin(thetas[i]) )
        self.nodePos = xNodes + yNodes


    #---------------------------------------------------------------------------


    def updateCom(self) :
        """ update the cell center of mass positions """

        thetas = self.q[2:]
        xcom = [ self.q[0] ]
        ycom = [ self.q[1] ]
        for i in range(1, len(self.q) - 2 ) :
            xcom.append( xcom[-1] + self.lt[i-1]/2 * np.cos(thetas[i-1]) + self.lt[i]/2 *np.cos(thetas[i]) )
            ycom.append( ycom[-1] + self.lt[i-1]/2 * np.sin(thetas[i-1]) + self.lt[i]/2 *np.sin(thetas[i]) )
        self.com = xcom + ycom


    #---------------------------------------------------------------------------


    def updateBindingSites(self, dt) :
        """ update the binding site positions based on the new generalized coordinates """

        for i in range( len(self.bindingSites) ) :

            # update exisiting binding sites
            for j in range( len(self.bindingSites[i]) ) :
                self.bindingSites[i][j].updatePositions()

            # add new binding site if necessary
            if random.random() <= self.kSite * dt :
                z = random.random() - 0.5
                self.bindingSites[i].append( Site(self, i , z) )


    #---------------------------------------------------------------------------


    def growthDynamics(self, kb, mu, nu, epsilon) :
        """ updates the qdots for the filament based only on growth dynamics
                l (array) : lengths of the cells in the Filaments
                lp (array) : growth rates for each cell
                kb (float) : angular spring constant
                mu (float) : parallel drag coefficient
                nu (float) : perpendiular drag coefficient
                epsilion (float) : rotational drag coefficient """

        #print("Filament=", self, l[0])

        l = self.lt
        lp = self.lp

        x0 = self.q[0]
        y0 = self.q[1]
        thetas = self.q[2:]

        numRods = len(thetas)
        numNodes = numRods + 1

        totalLength = sum(l)

        #--------------------------------------------------------

        xEqns, yEqns, thetaEqns = [], [], []

        # x_i Euler-Lagrange equations --------------------------------------------------------

        # initialize x_0 E-L equation and x E_L equation array
        xEqn0 = np.zeros(3*numRods)
        xEqn0[0] = ( mu * np.cos(thetas[0])**2 + nu * np.sin(thetas[0])**2 ) * l[0]
        xEqn0[1] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
        xEqn0[numRods + 2] = -1
        xEqns.append(xEqn0)

        # initialize y_0 E-L equation and y E_L equation array
        yEqn0 = np.zeros(3*numRods)
        yEqn0[0] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
        yEqn0[1] = ( mu * np.sin(thetas[0])**2 + nu * np.cos(thetas[0])**2 ) * l[0]
        yEqn0[2*numRods + 1] = -1
        yEqns.append(yEqn0)

        # initialize theta_0 E-L equation and theta E_L equation array
        thetaEqn0 = np.zeros(3*numRods)
        thetaEqn0[2] = epsilon * l[0]**3
        thetaEqn0[numRods + 2] = 0.5 * l[0] * np.sin(thetas[0])
        thetaEqn0[2*numRods + 1] = -0.5 * l[0] * np.cos(thetas[0])
        thetaEqns.append(thetaEqn0)

        #generate the 2 through N-1 x, y, and theta E-L equations
        for i in range(1, numRods) :

            # x_i E_L equations -----------------------------------------------------------

            xEqni = np.zeros(3*numRods)

            xEqni[0] = ( mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[i]

            xEqni[1] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

            xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * np.cos(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[0] * l[i] * np.sin(thetas[0]) )

            for j in range(1, i) :
                xEqni[j+2] = (mu - nu) * l[j] * l[i] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[j] * l[i] * np.sin(thetas[j])

            xEqni[i+2] = -0.5 * nu * l[i]**2 * ( np.sin(thetas[i]) * np.cos(thetas[i])**2 + np.sin(thetas[i])**3 )

            xEqni[i + numRods + 1] = 1

            if i != (numRods-1) :
                xEqni[i + numRods + 2] = -1

            xEqns.append(xEqni)


            # y_i E_L equations -----------------------------------------------------------

            yEqni = np.zeros(3*numRods)

            yEqni[0] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

            yEqni[1] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[i]

            yEqni[2] = 0.5 * ( (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[0] * l[i] * np.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * np.sin(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) )

            for j in range(1, i) :
                yEqni[j+2] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[j] * l[i] * np.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])

            yEqni[i+2] = 0.5 * nu * l[i]**2 * (np.cos(thetas[i]) * np.sin(thetas[i])**2 + np.cos(thetas[i])**3)

            yEqni[i + (2*numRods)] = 1

            if i != (numRods-1) :
                yEqni[i + (2*numRods) + 1] = -1

            yEqns.append(yEqni)


            # theta_i E_L equations -----------------------------------------------------------

            thetaEqni = np.zeros(3*numRods)

            thetaEqni[i+2] = epsilon * l[i]**3

            thetaEqni[i + numRods + 1] = 0.5 * l[i] * np.sin(thetas[i])

            thetaEqni[i + (2*numRods)] = -0.5 * l[i] * np.cos(thetas[i])

            if i != (numRods-1) :
                thetaEqni[i + numRods + 2] = 0.5 * l[i] * np.sin(thetas[i])
                thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * np.cos(thetas[i])

            thetaEqns.append(thetaEqni)

        # combine all E-L equation coeffcients into a single array
        eqns = xEqns + yEqns + thetaEqns
        self.eqns = np.array(eqns)
        # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
        #   are standard arrays

        xVals, yVals, thetaVals = [0], [0], [-kb * (thetas[0]-thetas[1])]

        for i in range(1, numRods) :

            xVal = -(mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i]))
            for j in range(1,i) :
                xVal -= (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] * lp[j] * np.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
            xVals.append(xVal)

            yVal = -(mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i]))
            for j in range(1,i) :
                yVal -= (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] * lp[j] * np.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
            yVals.append(yVal)

            if i == (numRods-1) :
                thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
            else :
                thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
            thetaVals.append(thetaVal)

        vals = xVals + yVals + thetaVals
        self.vals = np.array(vals)


    #---------------------------------------------------------------------------


    def lateralDynamics( self, other, width, kon, koff0, ks, Lmax, Lc, Fc, dt ) :
        """ Out dated version see below for new version of this function which
            uses Lagrangian mechanics instead

            simulates the lateral interaction dynamics on the cell
                other (Filament) : the adjacent filament
                lt (array) : array of cell lengths
                width (float) : width of a single cell
                kon (float) : rate at which bonds form
                koff0 (float) : characteristic rate at which bonds break
                ks (float) : spring constant for force between bonded site
                Lmax (float) : maximum distance at which a bond can form
                Lc (float) : cutoff distance at which a bond immediately breaks
                Fc (float) : characterstic force between bonded site
                dt (float) : time step -- current time minus the previous time """

        #print("check num bonds", len(self.bonds))

        # iterate over all bonds and break old ones if necessary
        if ( len(self.bonds) != 0 ) and ( len(other.bonds) != 0 ) :
            for i in range( len(self.bonds)-1, -1, -1 ) :
                for j in range( len(other.bonds)-1, -1, -1 ) :
                    if self.bonds[i] == other.bonds[j] :

                        self.bonds[i].calculateForce(ks, width)
                        koff = koff0 * np.exp( self.bonds[i].force / Fc )

                        # check sites are within cutoff distance
                        Lc_val = np.sqrt( (self.bonds[i].site1.bx - self.bonds[i].site2.bx)**2 + (self.bonds[i].site1.by - self.bonds[i].site2.by)**2 ) > Lc
                        # determine if a break occurs
                        koffCheck = random.random()
                        koff_val = koffCheck <= koff * dt

                        #print("ratecheck", koffCheck, np.exp( self.bonds[i].force / Fc ) )
                        if Lc_val or koff_val:
                            # remove bond reference from the two sites
                            print("DESTROYING BOND!")
                            self.bonds[i].site1.bond = None
                            self.bonds[i].site2.bond = None
                            # remove the bond objects from the list of bonds
                            del self.bonds[i]
                            del other.bonds[j]
                            break

        # iterate over all binding sites and form new bonds if necessary
        for i in range( len(self.bindingSites) ) :
            for j in range( len(self.bindingSites[i]) ) :

                for m in range( len(other.bindingSites) ) :
                    for n in range( len(other.bindingSites[m]) ) :
                        # check if sites are within maximum distance at which a bond can form
                        if self.bindingSites[i][j].calculateDistance( other.bindingSites[m][n] ) <= Lmax :
                            if random.random() <= kon * dt :
                                newBond = Bond( self.bindingSites[i][j], other.bindingSites[m][n] )
                                newBond.calculateForce(ks, width)
                                self.bonds.append( newBond )
                                other.bonds.append( newBond )
                                #print("creating new bond")


        # update qdots based on torques
        for i in range( len(self.bonds) ) :
            for j in range( len(other.bonds) ) :
                if self.bonds[i] == self.bonds[j] :


                    # update each Euler-Lagrange equation based on the lateral dynamics
                    # note: these terms depend only on the generalized coordinates and not
                    # their derivatives so this only changes the "value" side of the equation

                    if self.bonds[i].site1.filament == self :
                        siteSelf = self.bonds[i].site1
                        siteOther = self.bonds[i].site2
                    else :
                        siteSelf = self.bonds[i].site2
                        siteOther = self.bonds[i].site1
                    xDiff = siteOther.bx - siteSelf.bx
                    yDiff = siteOther.by - siteSelf.by
                    sqrtDist = np.sqrt( xDiff**2 + yDiff**2 )


                    # update the "values" on the self filament

                    index = siteSelf.cellIndex
                    numRods = len(self.q)-2
                    xcom = self.com[:numRods]
                    ycom = self.com[numRods:]

                    # x-coodinate adjustment
                    self.vals[index] += ( -ks * xDiff * ( sqrtDist - width ) ) / sqrtDist

                    # y-coodinate adjustment
                    self.vals[numRods+index] += ( -ks * yDiff * ( sqrtDist - width ) ) / sqrtDist

                    # theta-coodinate adjustment
                    numer = ks * (sqrtDist - width) * ( yDiff * xcom[index] + siteOther.bx * (siteSelf.by - xcom[index]) + siteSelf.bx * ( ycom[index] - siteOther.by ) )
                    #print("numerator is zero", numer == 0)
                    self.vals[2*numRods+index] += numer / sqrtDist


                    # update the "values" on the other filament

                    index = siteOther.cellIndex
                    numRods = len(other.q)-2
                    xcom = other.com[:numRods]
                    ycom = other.com[numRods:]

                    # x-coodinate adjustment
                    other.vals[index] += ( ks * xDiff * ( sqrtDist - width ) ) / sqrtDist

                    # y-coodinate adjustment
                    other.vals[numRods+index] += ( ks * yDiff * ( sqrtDist - width ) ) / sqrtDist

                    # theta-coodinate adjustment
                    numer = -ks * (sqrtDist - width) * ( yDiff * xcom[index] + siteOther.bx * (siteSelf.by - xcom[index]) + siteSelf.bx * ( ycom[index] - siteOther.by ) )
                    #numer = -numer
                    #numer = ks * (sqrtDist - width) * ( -yDiff * xcom[index] + siteSelf.bx * (siteOther.by - xcom[index]) + siteOther.bx * ( ycom[index] - siteSelf.by ) )
                    other.vals[2*numRods+index] += numer / sqrtDist


    #---------------------------------------------------------------------------


    def solve( self ) :
        """ solves the filament dynamics once both the growth and lateral dynamics have been computed
            # solve the system of equations
            #   solve Ax = v where A is formed from eqns, v is vals and
            #   x are the unknowns [xdot_0, ydot_0, thetadot_i, lambdax_i, lambday_i]"""

        numRods = len(self.q)-2

        sols = np.linalg.solve(self.eqns, self.vals)

        """# array of the constraint force valse
        lambdaX = sols[numRods+2 : 2*numRods+1]
        lambdaY = sols[2*numRods+1:]

        # append constraint force values to global array of force values
        #   only append when call is 'real' since solver makes additional calls
        if realCall == True :
            temp = []
            for i in range( len(lambdaX) ) :
                temp.append( [ lambdaX[i], lambdaY[i] ] )
            lambdaVals.append(temp)
        realCall = False    # set to false so 'non-real' calls are not included"""

        self.qdots = sols[:numRods+2]
