#import pdb
import numpy as np
import scipy as sp
from scipy import signal
from scipy import integrate
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import optimize
from scipy.interpolate import interp1d
from scipy import interpolate
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys
import pandas as pd
import seaborn as sns

####################################################################################################
# Classes
####################################################################################################

class ComputeCurvature:
    def __init__(self):
        """ Initialize some variables """
        self.xc = 0  # X-coordinate of circle center
        self.yc = 0  # Y-coordinate of circle center
        self.r = 0   # Radius of the circle
        self.xx = np.array([])  # Data points
        self.yy = np.array([])  # Data points

    def calc_r(self, xc, yc):
        """ calculate the distance of each 2D points from the center (xc, yc) """
        return np.sqrt((self.xx-xc)**2 + (self.yy-yc)**2)

    def f(self, c):
        """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
        ri = self.calc_r(*c)
        return ri - ri.mean()

    def df(self, c):
        """ Jacobian of f_2b
        The axis corresponding to derivatives must be coherent with the col_deriv option of leastsq"""
        xc, yc = c
        df_dc = np.empty((len(c), self.xx.size))

        ri = self.calc_r(xc, yc)
        df_dc[0] = (xc - self.xx)/ri                   # dR/dxc
        df_dc[1] = (yc - self.yy)/ri                   # dR/dyc
        df_dc = df_dc - df_dc.mean(axis=1)[:, np.newaxis]
        return df_dc

    def fit(self, xx, yy):
        self.xx = xx
        self.yy = yy
        center_estimate = np.r_[np.mean(xx), np.mean(yy)]
        center = optimize.leastsq(self.f, center_estimate, Dfun=self.df, col_deriv=True)[0]

        self.xc, self.yc = center
        ri = self.calc_r(*center)
        self.r = ri.mean()

        return 1 / self.r  # Return the curvature

    def circleFit(self, xx, yy):
        self.xx = xx
        self.yy = yy
        center_estimate = np.r_[np.mean(xx), np.mean(yy)]
        center = optimize.leastsq(self.f, center_estimate, Dfun=self.df, col_deriv=True)[0]

        self.xc, self.yc = center
        ri = self.calc_r(*center)
        self.r = ri.mean()

        return self.r, self.xc, self.yc   # return the properties of the best fit circle


####################################################################################################
# Filament Dynamics Functions
####################################################################################################

def length(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return l0 * np.exp( (growthRate/l0) * (t - lastAddTime) )


def growthRateFunc(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return growthRate * np.exp( (growthRate/l0) * (t - lastAddTime) )

#---------------------------------------------------------------------------------------------------

def growthDynamics(t, q, lt, kb, mu, nu, epsilon, growthRate) :
    """ updates the qdots for the filament based only on growth dynamics
            l (array) : lengths of the cells in the Filaments
            lp (array) : growth rates for each cell
            kb (float) : angular spring constant
            mu (float) : parallel drag coefficient
            nu (float) : perpendiular drag coefficient
            epsilion (float) : rotational drag coefficient """

    l = [lt] * ( len(q) - 2 )
    lp = [ growthRate ] * ( len(q) - 2 )

    x0 = q[0]
    y0 = q[1]
    thetas = q[2:]

    numRods = len(thetas)
    numNodes = numRods + 1

    #totalLength = sum(l)

    #--------------------------------------------------------

    xEqns, yEqns, thetaEqns = [], [], []

    # x_i Euler-Lagrange equations --------------------------------------------------------

    # initialize x_0 E-L equation and x E_L equation array
    xEqn0 = np.zeros(3*numRods)
    xEqn0[0] = ( mu * np.cos(thetas[0])**2 + nu * np.sin(thetas[0])**2 ) * l[0]
    xEqn0[1] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    if numRods != 1 : xEqn0[numRods + 2] = -1
    xEqns.append(xEqn0)

    # initialize y_0 E-L equation and y E_L equation array
    yEqn0 = np.zeros(3*numRods)
    yEqn0[0] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    yEqn0[1] = ( mu * np.sin(thetas[0])**2 + nu * np.cos(thetas[0])**2 ) * l[0]
    if numRods != 1 : yEqn0[2*numRods + 1] = -1
    yEqns.append(yEqn0)

    # initialize theta_0 E-L equation and theta E_L equation array
    thetaEqn0 = np.zeros(3*numRods)
    thetaEqn0[2] = epsilon * l[0]**3
    if numRods != 1 :
        thetaEqn0[numRods + 2] = 0.5 * l[0] * np.sin(thetas[0])
        thetaEqn0[2*numRods + 1] = -0.5 * l[0] * np.cos(thetas[0])
    thetaEqns.append(thetaEqn0)

    #generate the 2 through N-1 x, y, and theta E-L equations
    for i in range(1, numRods) :

        # x_i E_L equations -----------------------------------------------------------

        xEqni = np.zeros(3*numRods)

        xEqni[0] = ( mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[i]

        xEqni[1] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * np.cos(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[0] * l[i] * np.sin(thetas[0]) )

        for j in range(1, i) :
            xEqni[j+2] = (mu - nu) * l[j] * l[i] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[j] * l[i] * np.sin(thetas[j])

        xEqni[i+2] = -0.5 * nu * l[i]**2 * ( np.sin(thetas[i]) * np.cos(thetas[i])**2 + np.sin(thetas[i])**3 )

        xEqni[i + numRods + 1] = 1

        if i != (numRods-1) :
            xEqni[i + numRods + 2] = -1

        xEqns.append(xEqni)


        # y_i E_L equations -----------------------------------------------------------

        yEqni = np.zeros(3*numRods)

        yEqni[0] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        yEqni[1] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[i]

        yEqni[2] = 0.5 * ( (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[0] * l[i] * np.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * np.sin(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) )

        for j in range(1, i) :
            yEqni[j+2] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[j] * l[i] * np.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])

        yEqni[i+2] = 0.5 * nu * l[i]**2 * (np.cos(thetas[i]) * np.sin(thetas[i])**2 + np.cos(thetas[i])**3)

        yEqni[i + (2*numRods)] = 1

        if i != (numRods-1) :
            yEqni[i + (2*numRods) + 1] = -1

        yEqns.append(yEqni)


        # theta_i E_L equations -----------------------------------------------------------

        thetaEqni = np.zeros(3*numRods)

        thetaEqni[i+2] = epsilon * l[i]**3

        thetaEqni[i + numRods + 1] = 0.5 * l[i] * np.sin(thetas[i])

        thetaEqni[i + (2*numRods)] = -0.5 * l[i] * np.cos(thetas[i])

        if i != (numRods-1) :
            thetaEqni[i + numRods + 2] = 0.5 * l[i] * np.sin(thetas[i])
            thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * np.cos(thetas[i])

        thetaEqns.append(thetaEqni)


        # stress calculations --------------------------------------------------------------
        #self.stressX.append( 0.5 * ( l[i-1]*np.cos(thetas[i-1]) - l[i]*np.sin(thetas[i]) ) )
        #self.stressY.append( 0.5 * ( -l[i-1]*np.sin(thetas[i-1]) + l[i]*np.cos(thetas[i]) ) )

    # combine all E-L equation coeffcients into a single array
    eqns = np.array( xEqns + yEqns + thetaEqns )

    # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
    #   are standard arrays
    #print(eqns)
    #input("stopza")

    if numRods == 1 : xVals, yVals, thetaVals = [0.0], [0.0], [0.0]
    else : xVals, yVals, thetaVals = [0.0], [0.0], [-kb * (thetas[0]-thetas[1])]

    for i in range(1, numRods) :

        xVal = -(mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i]))
        for j in range(1,i) :
            xVal -= (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] * lp[j] * np.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        xVals.append(xVal)

        yVal = -(mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i]))
        for j in range(1,i) :
            yVal -= (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] * lp[j] * np.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        yVals.append(yVal)

        if i == (numRods-1) :
            thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
        else :
            thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
        thetaVals.append(thetaVal)

    vals = np.array( xVals + yVals + thetaVals )


    numRods = len(q)-2

    sols = np.linalg.solve(eqns, vals)

    return sols

####################################################################################################

comp_curv = ComputeCurvature()

print("LOADING DATA...")
count = 1

simulationSets = []
for file in sys.argv[1:] :

    try :
        filename = str(file)
    except IndexError :
        print("If loading package, continue, otherwise\nERROR: No input file given -> try again")
        exit()

    #sol = pickle.load(open("data.dir/exponentialGrowth_epsilonLJ0.0_rm1.5_Lc5.0_endTime180.0.pkl", "rb"))
    sol = pickle.load(open(filename, "rb"))
    l0, growthRate, kb, mu, nu, epsilon, numRods_init, angle, rtol, atol, seed_val = sol[0]
    psi = int(kb / (mu * l0**2 * growthRate))
    a = int(nu/mu)
    b = epsilon/mu
    sol = sol[1:]

    #print( sum( [ sum( [ len(sol[i].y[j]) for j in range( len(sol[i].y) ) ] ) for i in range( len(sol) ) ] ) )
    #print( len(sol[0].y[0]) )
    #print( len(sol[0].t) )
    # use these three lines to coarsen data if desired
    n = 100     # take every nth output
    for s in sol :
        s.t = s.t[0::n]
        s.y = np.array( [ s.y[i][0::n] for i in range(len(s.y)) ] )
    #print( sum( [ sum( [ len(sol[i].y[j]) for j in range( len(sol[i].y) ) ] ) for i in range( len(sol) ) ] ) )
    #print( len(sol[0].y[0]) )
    #print( len(sol[0].t) )



    print(f"{count} ORGANIZING DATA ... Psi = {psi} \t a = {a}")
    count += 1

    t = np.concatenate( [s.t for s in sol], axis=None )

    minX, maxX, minY, maxY = 1E100, 0, 1E100, 0
    qs, qdot, lambdaX, lambdaY, nodeX, nodeY = [], [], [], [], [], []
    lastAddTime = [sol[0].t[0]]
    ltVec, lpVec = [], []

    qVec = []
    for i in range( len(sol[-1].y) ) : qVec.append( [None] * len(t) )

    k = 0   # time index

    for s in sol :

        #print("starting a new sol")

        q = [ s.y[:,i] for i in range(len(s.y[0])) ]
        numRods = len(q[0]) - 2

        lt = [ [ length(time, l0, growthRate, lastAddTime[-1]) ] * numRods for time in s.t ]
        ltVec += lt
        lp = [ [ growthRateFunc(time, l0, growthRate, lastAddTime[-1]) ] * numRods for time in s.t ]
        lpVec += lp

        # interating over time
        for i in range(len(q)) :

            numRods = len(q[i])-2
            lt = length(s.t[i], l0, growthRate, lastAddTime[-1])
            lp = growthRateFunc(s.t[i], l0, growthRate, lastAddTime[-1])

            # organize q
            for j in range( len(s.y) ) :
                qVec[j][k] = s.y[j][i]
            k += 1

            # compute qdots and Lagrange multipliers
            sols = growthDynamics(s.t[i], q[i], lt, kb, mu, nu, epsilon, lp)

            # organize q arrays
            qs.append(q[i])

            # organize qdots
            qdots = sols[: numRods+2]
            qdot.append(qdots)

            # organize Lagrange multipliers
            lambdaXs = sols[numRods+2 : 2*numRods+1]
            lambdaX.append(lambdaXs)
            lambdaYs = sols[2*numRods+1 :]
            lambdaY.append(lambdaYs)

            # calculate node positions
            thetas = q[i][2:]
            xNodes = [ q[i][0] - lt/2 * np.cos( thetas[0] ) ]
            yNodes = [ q[i][1] - lt/2 * np.sin( thetas[0] ) ]
            for i in range( len(q[i]) - 2 ) :
                xNodes.append( xNodes[-1] + lt * np.cos(thetas[i]) )
                yNodes.append( yNodes[-1] + lt * np.sin(thetas[i]) )
                if min(xNodes) < minX : minX = min(xNodes)
                if max(xNodes) > maxX : maxX = max(xNodes)
                if min(yNodes) < minY : minY = min(yNodes)
                if max(yNodes) > maxY : maxY = max(yNodes)
            nodeX.append(xNodes)
            nodeY.append(yNodes)

        lastAddTime.append(s.t[-1])

    d = {
        "t" : t,
        "psi" : [psi] * len(t),
        "a" : [a] * len(t),
        "q" : qs,
        "qdot" : qdot,
        "Fx" : lambdaX,
        "Fy" : lambdaY,
        "xNode" : nodeX,
        "yNode" : nodeY,
        "lt" : ltVec,
        "ldot" : lpVec
    }

    curvatures, deltaThetas, deltaThetaDots = [], [], []

    for i in range( len(d["t"]) ) :

        numRods = len(d["q"][i])-2
        numNodes = numRods - 1

        # compute curvature
        numMiddle = 7   # number of nodes in the "middle"; always use even numCells so this should be odd
        index = numMiddle + ( (numNodes - numMiddle) // 2 )
        x = np.array( d["xNode"][i][-index:index] )
        y = np.array( d["yNode"][i][-index:index] )
        curvature = comp_curv.fit(x, y)
        curvatures.append(curvature)

        # compute deltaTheta
        thetas = d["q"][i][2:]
        deltaTheta = thetas[numRods//2] - thetas[numRods//2-1]
        deltaThetas.append(deltaTheta * 180 / np.pi)

        # compute deltaThetaDot
        thetaDots = d["qdot"][i][2:]
        deltaThetaDot = thetaDots[numRods//2] - thetaDots[numRods//2-1]
        deltaThetaDots.append(deltaThetaDot * 180 / np.pi)

    # compute the difference of the curvature form the initial value at each times
    curvatureDiffs = list( 100 * (np.array(curvatures) - np.array( [curvatures[0]] * len(curvatures) ) ) / curvatures[0] )

    # compute the rate of change of the curvatures as a function of time
    curvatureRates = [None] + [(curvatures[i+1] - curvatures[i]) / (d["t"][i+1] - d["t"][i]) for i in range( len(d["t"])-1 )]

    d["curvature"] = curvatures
    d["curvatureDiff"] = curvatureDiffs
    d["curvatureRate"] = curvatureRates
    d["deltaTheta"] = deltaThetas
    d["deltaThetaDot"] = deltaThetaDots

    #configurationPlot(t, nodeX, nodeY, minX, maxX, minY, maxY, psi, a, b, numRods_init, angle)

    params = ( psi, a, b, l0, numRods_init, angle, seed_val )
    plotParams = ( minX, maxX, minY, maxY )
    simulationSets.append( (params, plotParams, pd.DataFrame(d)) )

f = open("data_processed.pkl", 'wb')
pickle.dump(simulationSets, f)
f.close()
