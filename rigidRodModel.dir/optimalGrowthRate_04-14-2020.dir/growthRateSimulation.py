#!/usr/local/env python

import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import stats
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
import matplotlib
#matplotlib.use("Agg")      # COMMENT THIS FOR PLOTS FOR plt.show(), UNCOMMENT FOR ANIMATIONS
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys

####################################################################################################

class filament(object) :

    #-----------------------------------------------------------------------------------------------

    def __init__(self, t, numRods, l0, com, growthRate, Lb) :
        """ creates a filament object
                numRods : number of rods in the filament
                l0 : initial length of the cells
                com : center of mass of the filaments
                growthRate : initial growth rate of the
                Lb : length at which a filament breaks """

        self.numRods = numRods
        self.T = l0/growthRate * np.log(2)  # cell cycle time
        l = l0 * np.exp( (growthRate/l0) * (t % self.T) )   # cell length
        self.L = numRods * l    # filament length
        self.com = com
        self.Lb = Lb
        self.broken = False
        self.updateTips()

    #-----------------------------------------------------------------------------------------------

    def updateLength(self, t) :
        """ updates the length of the filament
                t : the current time """
        l = l0 * np.exp( (growthRate/l0) * (t % self.T) )
        self.L = self.numRods * l

        if self.L > self.Lb : self.broken = True

    #-----------------------------------------------------------------------------------------------

    def cellDivision(self) :
        """ handles cell division """
        self.numRods = int(self.numRods * 2)

    #-----------------------------------------------------------------------------------------------

    def updateTips(self) :
        """ calculate the location of the end tips of the filaments """
        self.tip_locations = [ self.com - (self.L / 2), self.com + (self.L / 2) ]

    #-----------------------------------------------------------------------------------------------

    def update(self, t) :
        """ updates the attributes of the filament """
        self.updateLength(t)
        self.updateTips()   # updateTips must be called AFTER updateLength

####################################################################################################


growthRate = 0.1    # intitial  growth rate
numRods = 2         # number of initial cells
l0 = 5              # initial cell length

Lb = 2000           # filament breaking length

t = 0
T = l0/growthRate * np.log(2)       # cell cycle time
dt = T / 30     # timestep
endTime = 400
filaments = [ filament(t, numRods, l0, 0.0, growthRate, Lb) ]     # initiial filament
totalLength = [[t, numRods * l0]]    # array of data points [t, Lt] where Lt is the length from the two farthest tips
totalLengthRate = []
while t < endTime :

    newFilaments =[]
    for f in filaments :

        f.update(t)
        # if the filament has reached the breaking length
        if f.broken :
            newFilaments.append( filament(t, f.numRods/2, l0, f.com - (f.L/4), growthRate, Lb) )
            newFilaments.append( filament(t, f.numRods/2, l0, f.com + (f.L/4), growthRate, Lb) )
        else : newFilaments.append(f)

    filaments = newFilaments

    if (t+dt)%T < t%T  :
        for f in filaments :
            f.cellDivision()   # check if cells divide

    t += dt
    print("Time =", t)

    tip_max = max( [ max(f.tip_locations) for f in filaments ] )
    tip_min = min( [ min(f.tip_locations) for f in filaments ] )
    totalLength.append( [t, tip_max - tip_min] )
    Lt_rate =  (totalLength[-1][1] - totalLength[-2][1]) / (totalLength[-1][0] - totalLength[-2][0])
    totalLengthRate.append( [t, Lt_rate] )

totalLength = np.array(totalLength)
plt.figure(1)
plt.plot(totalLength[:,0], totalLength[:,1])

totalLengthRate = np.array(totalLengthRate)
plt.figure(2)
plt.plot(totalLengthRate[:,0], totalLengthRate[:,1])

plt.show()
