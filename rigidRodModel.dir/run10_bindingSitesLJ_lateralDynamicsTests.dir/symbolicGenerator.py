#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# Euler-Lagrange Equation Generator (no longer in use)
# Final Version
# 06/13/2019

import pickle
import numpy as np
#import sympy as sym
#from sympy import Symbol, symbols
from sympy import *
from sympy.physics.vector import dynamicsymbols
#from IPython.display import Math


def filamentLagrangian( n ) :
    """ gives the Lagrangian of a filament of n cells """

    t = symbols('t')
    rho = symbols('rho')
    kb = symbols('kb')

    l = [dynamicsymbols('l' + str(i)) for i in range(n) ]
    lp = [dynamicsymbols('l' + str(i), 1) for i in range(n) ]
    x = [dynamicsymbols('x' + str(i)) for i in range(n) ]
    xdot = [dynamicsymbols('x' + str(i), 1) for i in range(n) ]
    y = [dynamicsymbols('y' + str(i)) for i in range(n) ]
    ydot = [dynamicsymbols('y' + str(i), 1) for i in range(n) ]
    theta = [dynamicsymbols('theta' + str(i)) for i in range(n) ]
    thetadot = [dynamicsymbols('theta' + str(i), 1) for i in range(n) ]

    q = x + y + theta
    qdot = xdot + ydot + thetadot

    T_linear = 0.5 * rho * sum( [ l[i] * (xdot[i]**2 + ydot[i]**2) for i in range(n) ] )
    T_rotational = 1/24 * rho * sum( [ l[i]**3 * thetadot[i]**2 for i in range(n) ] )
    T_growth = 1/24 * rho * sum( [ l[i] * lp[i] for i in range(n) ] )
    T = T_linear + T_rotational + T_growth

    V = 0.5 * kb * sum( [ theta[i]**2 + theta[i+1]**2 - 2 * theta[i] * theta[i+1] for i in range(n-1) ] )

    L = T - V

    constraintEqns, lagrangeMultipliers = freeEndsConstraints(n, x, y, theta, l)

    R = rayleighDissipationFunction(n, xdot, ydot, thetadot, theta, l)

    eulerLagrangeEqns(L, q, lagrangeMultipliers, constraintEqns, R)

    return


def freeEndsConstraints(n, x, y, theta, l) :
    """ generates the constraint equations for a single filament
        containing n cells with both ends free
        x, y, and theta are arrays of the generalized coordinates
        l is the array of cell lengths """

    xConstraintEqns, yConstraintEqns = [], []
    xLagrangeMultipliers, yLagrangeMultipliers = [], []
    for i in range(n-1) :
        xConstraintEqns.append( x[i] - x[i+1] + 0.5 * ( l[i] * cos(theta[i]) + l[i+1] * cos(theta[i+1]) ) )
        xLagrangeMultipliers.append( symbols('lambdaX'+ str(i)) )
        yConstraintEqns.append( y[i] - y[i+1] + 0.5 * ( l[i] * sin(theta[i]) + l[i+1] * sin(theta[i+1]) ) )
        yLagrangeMultipliers.append( symbols('lambdaY'+ str(i)) )

    constraintEqns = xConstraintEqns + yConstraintEqns
    lagrangeMultipliers = xLagrangeMultipliers + yLagrangeMultipliers

    return constraintEqns, lagrangeMultipliers


def rayleighDissipationFunction(n, xdot, ydot, thetadot, theta, l) :
    """ generates the Rayleigh Dissipation Function for a single
        filament containing n cells
        x, y, and theta are arrays of the generalized coordinates
        l is the array of cell lengths """

    mu, nu, epsilon = symbols('mu nu epsilon' )

    xdot_para = [ xdot[i] * cos(theta[i]) for i in range( len(xdot) ) ]
    xdot_perp = [ -xdot[i] * sin(theta[i]) for i in range( len(xdot) ) ]
    ydot_para = [ ydot[i] * sin(theta[i]) for i in range( len(ydot) ) ]
    ydot_perp = [ ydot[i] * cos(theta[i]) for i in range( len(ydot) ) ]

    R = 0.5 * sum( [ mu * l[i] * (xdot_para[i] + ydot_para[i])**2 + nu * l[i] * (xdot_perp[i] + ydot_perp[i])**2 + epsilon * l[i]**3 * thetadot[i]**2 for i in range(n) ] )

    return R


def eulerLagrangeEqns( L, q, cVars, cEqns, R ) :
    """ Generates the Euler-Lagrange equations given:
            L : Lagrangian
            qVars : the generalized coordindate functions
            qdotsVars : derivatives of the generalized coordinate functions
            cVars : the Lagrange multipliers
            cEqns : the constraint equations
            R : the Rayleigh dissipation function """

    t = symbols('t')
    qdot = [ diff( qVar, t ) for qVar in q ]
    n = len(q) // 3

    x, y, theta = q[:len(q)//3], q[ len(q)//3 : 2*len(q)//3 ], q[2*len(q)//3 :]
    xdot, ydot, thetadot = qdot[:len(qdot)//3], qdot[ len(qdot)//3 : 2*len(qdot)//3 ], qdot[2*len(qdot)//3 :]

    xCEqns, yCEqns = cEqns[:len(cEqns)//2],  cEqns[len(cEqns)//2:]
    xTemp = [ solve(diff(xCEqns[i], t), xdot[i+1])[0] for i in range( len(xCEqns) ) ]
    yTemp = [ solve(diff(yCEqns[i], t), ydot[i+1])[0] for i in range( len(yCEqns) ) ]


    xCEqnsTD = [xTemp[0]]
    yCEqnsTD = [yTemp[0]]
    for i in range( 1, len(xTemp) ) :
        xCEqnsTD.append( xTemp[i].subs(xdot[i], xTemp[i-1]) )
        yCEqnsTD.append( yTemp[i].subs(ydot[i], yTemp[i-1]) )

    #pprint(xCEqnsTD)

    eqns = []
    #for q, qdot in zip( [x,y,theta], [xdot,ydot,thetadot] ) :
    for q, qdot in zip( [x], [xdot] ) :
        for i in range( len(q) ) :
            if i == 0 : # first cell
                eqn = diff(diff(L, qdot[i]), t) - diff(L, q[i]) - cVars[i] * diff(cEqns[i], q[i]) - cVars[i+n-1] * diff(cEqns[i+n-1], q[i]) + diff(R, qdot[i])
            elif i == len(q)-1 :  # last cell
                eqn = diff(diff(L, qdot[i]), t) - diff(L, q[i]) - cVars[i-1] * diff(cEqns[i-1], q[i]) - cVars[i+n-2] * diff(cEqns[i+n-2], q[i]) + diff(R, qdot[i])
                eqn = eqn.subs(xdot[i], xCEqnsTD[i-1])
                eqn = eqn.subs(ydot[i], yCEqnsTD[i-1])
            else:
                eqn = diff(diff(L, qdot[i]), t) - diff(L, q[i]) - cVars[i] * diff(cEqns[i], q[i]) - cVars[i-1] * diff(cEqns[i-1], q[i]) - cVars[i+n-1] * diff(cEqns[i+n-1], q[i]) - cVars[i+n-2] * diff(cEqns[i+n-2], q[i]) + diff(R, qdot[i])
                eqn = eqn.subs(xdot[i], xCEqnsTD[i-1])
                eqn = eqn.subs(ydot[i], yCEqnsTD[i-1])

            eqn = eqn.subs(symbols('rho'), 0)

            #eqn = collect( expand(eqn), [xdot[i], ydot[i]] )
            eqns.append(eqn)

    #pprint( eqns )

filamentLagrangian(3)
