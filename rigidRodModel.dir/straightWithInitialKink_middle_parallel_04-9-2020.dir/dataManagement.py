#!/usr/local/env python

import os
import pickle
import numpy as np


def data_main() :

    #print("RUNNING DATA MANAGMENT")

    newData = pickle.load(open("out.pkl", "rb"))
    os.remove("out.pkl")

    params = newData[0]     # [kb-mu-ratio, growthRate, angle]
    dataPt = newData[1]     # [init_length, endTime]
    endTime = dataPt[1]

    flag = endTime > 0.25
    f1, f2 = open("endtime.txt", 'w'), open("endtimeFlag.txt", 'w')
    f1.write(str(endTime)); f2.write(str(flag).lower())
    f1.close(); f2.close()

    try :
        data = pickle.load(open("current.pkl", "rb"))
        os.remove("current.pkl")
    except FileNotFoundError :
        data = []
    data.append(dataPt)
    pickle.dump(data, open("current.pkl", "wb"))

    if flag == False :
        try :
            dataSets = pickle.load(open("data.dir/data.pkl", "rb"))
            os.remove("data.dir/data.pkl")
        except FileNotFoundError :
            dataSets = []
        dataSets.append([params, data])
        pickle.dump(dataSets, open("data.dir/data.pkl", "wb"))
