#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 08/30/2018

import numpy as np
import scipy as sp
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt


# Functions ###############################################################################

def getForce(mu, L, LPrime, q) :
    """ determines the constraint force for the point
        located at l = q L """
    return 0.5 * mu * L * LPrime * q * (1-q)

def getGrowthRate(avgRate, frac, random) :
    """ determines the growth rate of an individual
        cell, if random = true then stochastic
        effects are inlcuded in the calculation """
    if random :
        return np.random.normal(avgRate, frac * avgRate)
    else : return avgRate

def updateLengths(dt, rateWithout, rateWith, frac, random, option) :
    """ updates the length of cell """

    if option == 0 :s
        growthRate = getGrowthRate(rateWithout, frac, random)

    """if option == 1 or option == 2 :
        if (L1[-1] < L2[-1]) or ( sum(l1Original[:i]) < L2[-1] ) :
            #growthRate = np.random.normal(rateWith, frac * rateWith)
            growthRate = getGrowthRate(rateWith, frac, random)
        #else : growthRate = np.random.normal(rateWithout, frac * rateWithout)
        else : growthRate = getGrowthRate(rateWith, frac, random)

    if option == 3 :
        q = sum(l1Original[:i+1]) / (2 * L1[-1]) + 0.5
        force = getForce(mu, L1[-1], rateWithout, q)
        growthRate = (1 - (0.4 * (force/fmax))) * rateWithout"""

    return growthRate * dt

# Parameters ##############################################################################

np.random.seed(7)

"""
Dictate the desired phenomenological effects
Choose an option for the model :
  0 : standard growth (no other effects)
  1 : neighbor suppresses growth
  2 : neighbor promotes growth
  3 : force dependent growth rate """
option = 0

random = False       # stochastic effects flag

mcSteps = 1
totalTime = 60      # minutes
dt = 0.01           # timestep (minutes)

frac = 0.3          # fraction for determining stdev for random number from normal dist
mu = 1e-9           # drag coefficient per length
avgDivLen = 10      # average length at which a cell divides


# Monte Carlo Simulation ##################################################################

# Initialization ---------------------------------------------------------------------

# determine average growth rates
rateWithout = 0.2                   # average growth rate of cell without a neighbor
if option == 0 :
    rateWith = rateWithout          # average growth rate of cell with a neighbor
if option == 1 :
    rateWith = 0.6 * rateWithout
if option == 2 :
    rateWith = 1.6 * rateWithout
if option == 3 :
    rateWith = rateWithout

# inialize five filaments: pick two at random

ls = [[avgDivLen/2]] * 5

t_init =
while t < t_init
for l in ls :
    for i in range( len(l) ) :

        l[i] += updateLength(dt, rateWithout, rateWith, frac, random, option)

        if random :
            if l[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
                newLength = l[i] / 2       # determine the length of the daughter cells
                l[i] = newLength
                l.insert(i+1, newLength)   # inset new cell
        else:
            if l[i] >= avgDivLen :
                newLength = l[i] / 2       # determine the length of the daughter cells
                l[i] = newLength
                l.insert(i+1, newLength)   # inset new cell
