#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 08/30/2018

import numpy as np
import scipy as sp
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle


# Functions ###############################################################################

def getForce(mu, L, LPrime, q) :
    """ determines the constraint force for the point
        located at l = q L """
    return 0.5 * mu * L * LPrime * q * (1-q)

def getGrowthRate(avgRate, frac, random) :
    """ determines the growth rate of an individual
        cell, if random = true then stochastic
        effects are inlcuded in the calculation """
    if random :
        return np.random.normal(avgRate, frac * avgRate)
    else : return avgRate

# Parameters ##############################################################################

np.random.seed(7)

"""
Dictate the desired phenomenological effects
Choose an option for the model :
  0 : standard growth (no other effects)
  1 : neighbor suppresses growth
  2 : neighbor promotes growth
  3 : force dependent growth rate """
option = 0

random = True       # stochastic effects flag

mcSteps = 1
totalTime = 60      # minutes
dt = 0.01           # timestep (minutes)

frac = 0.3          # fraction for determining stdev for random number from normal dist

mu = 1e-9           # drag coefficient per length

# determine growth rate
rateWithout = 0.2                   # average growth rate of cell without a neighbor
#if option == 0 :
    #rateWith = rateWithout          # average growth rate of cell with a neighbor
if option == 1 :
    rateWith = 0.6 * rateWithout
if option == 2 :
    rateWith = 1.6 * rateWithout
#if option == 3 :
    #rateWith = rateWithout

avgDivLen = 10                      # average length at which a cell divides


# Monte Carlo Simulation ##################################################################

#F1_count = 0    # number of times filament 1 is longer
#F2_count = 0    # number of times filament 2 is longer

# Initialization ---------------------------------------------------------------------

"""# FILAMENT #1
l1_init = []
for i in range(7) :
    # initial length of a each cell with some random fluctuations
    if random : l1_init.append( np.random.normal(3*avgDivLen/4, frac * 3*avgDivLen/4) )
    else : l1_init.append(3*avgDivLen/4)
L1_init = [sum(l1_init)]

# FILAMENT #2
l2_init = []
for i in range(11) :
    # initial length of a each cell with some random fluctuations
    if random : l2_init.append( np.random.normal(avgDivLen/2, frac * avgDivLen/2) )
    else : l2_init.append(avgDivLen/2)
L2_init = [sum(l2_init)]"""

n1 = 8
n2 = 12

#marginFrac = 0.1                       # frac of typically new cell length (avgDivLen/2)
results = []
#for marginFrac in [-1, -0.5, -0.25, 0, 0.25, 0.5, 1] :
#for marginFrac in [-1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1] :
for marginFrac in [0] :

    #np.random.seed(seedNo)

    filaments = pickle.load(open("filaments100.pkl", "rb"))

    l1_init = np.random.choice(filaments)
    l2_init = np.random.choice(filaments)

    # ensure L1 is always the initially longer filament
    if sum(l2_init) > sum(l1_init) :
        temp = l1_init
        l1_init = l2_init
        l2_init = temp

    L1_init = [sum(l1_init)]
    L2_init = [sum(l2_init)]

    margin = L1_init[0] - L2_init[0]

    F1_count = 0    # number of times filament 1 is longer

    """margin = avgDivLen/2 * marginFrac       # initial difference between the two filaments (L1 - L2)
    L1_init = [75]
    L2_init = [L1_init[0] - margin]

    l1_init = [L1_init[0] / n1] * n1
    l2_init = [L2_init[0] / n2] * n2"""

    """# FILAMENT #1
    l1_init = []
    for i in range(7) :
        # initial length of a each cell with some random fluctuations
        if random : l1_init.append( np.random.normal(3*avgDivLen/4, frac * 3*avgDivLen/4) )
        else : l1_init.append(3*avgDivLen/4)
    L1_init = [sum(l1_init)]

    # FILAMENT #2
    l2_init = []
    for i in range(11) :
        # initial length of a each cell with some random fluctuations
        if random : l2_init.append( np.random.normal(avgDivLen/2, frac * avgDivLen/2) )
        else : l2_init.append(avgDivLen/2)
    L2_init = [sum(l2_init)]"""

    #print(l1_init)

    # Simulation ##############################################################################

    for n in range( mcSteps ) :

        l1 = l1_init.copy()
        L1 = L1_init.copy()

        l2 = l2_init.copy()
        L2 = L2_init.copy()

        t = 0
        tVec = [t]
        while t < totalTime :
            print(t)
            # UPDATE FIRST FILAMENT

            fmax = getForce(mu, L1[-1], rateWithout, 0.5)

            l1Original = l1.copy()
            for i in range(len(l1)) :

                # determine a growth rate with some random fluctuations ----------------------------

                if option == 0 :
                    growthRate = getGrowthRate(rateWithout, frac, random)

                if option == 1 or option == 2 :
                    if (L1[-1] < L2[-1]) or ( sum(l1Original[:i]) < L2[-1] ) :
                        #growthRate = np.random.normal(rateWith, frac * rateWith)
                        growthRate = getGrowthRate(rateWith, frac, random)
                    #else : growthRate = np.random.normal(rateWithout, frac * rateWithout)
                    else : growthRate = getGrowthRate(rateWith, frac, random)

                if option == 3 :
                    q = sum(l1Original[:i+1]) / (2 * L1[-1]) + 0.5
                    force = getForce(mu, L1[-1], rateWithout, q)
                    growthRate = (1 - (0.4 * (force/fmax))) * rateWithout

                l1[i] += growthRate * dt    # update growth

                #----------------------------------------------------------------------------------

                # handle cell divsion if necessary
                if random :
                    if l1[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
                        newLength = l1[i] / 2       # determine the length of the daughter cells
                        l1[i] = newLength
                        l1.insert(i+1, newLength)   # inset new cell
                else:
                    if l1[i] >= avgDivLen :
                        newLength = l1[i] / 2       # determine the length of the daughter cells
                        l1[i] = newLength
                        l1.insert(i+1, newLength)   # inset new cell

            L1.append(sum(l1))      # determine new total length


            # UPDATE SECOND FILAMENT

            l2Original = l2.copy()
            for i in range(len(l2)) :

                """# determine a growth rate with some random fluctuations
                if (L2[-1] < L1[-1]) or ( sum(l2Original[:i]) < L1[-1] )  :
                    growthRate = np.random.normal(rateWith, frac * rateWith)
                else : growthRate = np.random.normal(rateWithout, frac * rateWithout)

                l2[i] += growthRate * dt    # update growth"""

                # determine a growth rate with some random fluctuations ----------------------------

                if option == 0 :
                    #growthRate = getGrowthRate(rateWithout, frac, random)
                    growthRate = 0.2 * np.exp(t/60)

                if option == 1 or option == 2 :
                    if (L2[-1] < L1[-1]) or ( sum(l2Original[:i]) < L1[-1] )  :
                        #growthRate = np.random.normal(rateWith, frac * rateWith)
                        growthRate = getGrowthRate(rateWith, frac, random)
                    #else : growthRate = np.random.normal(rateWithout, frac * rateWithout)
                    else : growthRate = getGrowthRate(rateWith, frac, random)

                if option == 3 :
                    q = sum(l2Original[:i+1]) / (2 * L1[-1]) + 0.5
                    force = getForce(mu, L2[-1], rateWithout, q)
                    growthRate = (1 - (0.4 * (force/fmax))) * rateWithout

                l2[i] += growthRate * dt    # update growth

                #----------------------------------------------------------------------------------

                """# handle cell divsion if necessary
                if l2[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
                    newLength = l2[i] / 2       # determine the length of the daughter cells
                    l2[i] = newLength
                    l2.insert(i+1, newLength)   # inset new cell"""

                # handle cell divsion if necessary
                if random :
                    if l2[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
                        newLength = l2[i] / 2       # determine the length of the daughter cells
                        l2[i] = newLength
                        l2.insert(i+1, newLength)   # inset new cell
                else:
                    if l2[i] >= avgDivLen :
                        newLength = l2[i] / 2       # determine the length of the daughter cells
                        l2[i] = newLength
                        l2.insert(i+1, newLength)   # inset new cell

            L2.append(sum(l2))      # determine new total length

            t += dt
            #print(t)
            tVec.append(t)

        tVec = np.array(tVec)
        L1 = np.array(L1)
        L2 = np.array(L2)
        LDiff = L1 - L2
        finalDiff = L1[-1] - L2[-1]

        if finalDiff > 0 : F1_count += 1
        print("Run", n+1, "is complete")

    F2_count = mcSteps - F1_count
    #eq_count = mcSteps - (F1_count + F2_count)
    results.append([margin, F1_count])
    print("done marginFrac =", marginFrac, "\nResults:", results)
    plt.figure(1)
    plt.plot(tVec, LDiff, markersize=0.5)
    plt.figure(2)
    plt.plot(tVec, L1, tVec, L2, 'r', markersize=0.5)
    plt.show()


print("\nFinal Results:\n", results)


print("\n")
if (L1_init[0] - L2_init[0]) > 0 :
    print("Filament 1 was longer initially by", L1_init[0] - L2_init[0])
else : print("Filament 2 was longer initially by", L2_init[0] - L1_init[0])
"""print("\nMonte Carlo Results:")
print("Filament 1 was longer", F1_count, "times")
print("Filament 2 was longer", F2_count, "times")"""
#print("The filam", F1_count, "times")

# Plotting ###############################################################################
print()

plt.figure(1)
plt.plot(tVec, LDiff, markersize=0.5)
plt.figure(2)
plt.plot(tVec, L1, tVec, L2, 'r', markersize=0.5)
plt.show()
