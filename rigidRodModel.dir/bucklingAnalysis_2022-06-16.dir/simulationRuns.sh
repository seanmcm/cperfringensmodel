#!/bin/bash

date
for psi in 50
do
    for a in 1 2 5 10 20 50 100
    do
        #                                a time N
        python exponentialGrowth.py $psi $a 100
    done
done
wait
date
echo "Complete"
