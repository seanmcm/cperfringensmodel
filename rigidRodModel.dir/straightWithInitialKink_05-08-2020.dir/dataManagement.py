#!/usr/local/env python

import os
import pickle
import numpy as np

print("RUNNING DATA MANAGMENT")

try :
    newData = pickle.load(open("out.pkl", "rb"))
except :
    f1 = open("endtimeFlag.txt", 'w')
    f1.write("true")
    f1.close()
os.remove("out.pkl")

params = newData[0]     # [kb-mu-ratio, growthRate, angle]
dataPt = newData[1]     # [init_length, endTime]
endTime = dataPt[1]

flag = endTime > 1.0
f1, f2 = open("endtime.txt", 'w'), open("endtimeFlag.txt", 'w')
f1.write(str(endTime)); f2.write(str(flag).lower())
f1.close(); f2.close()

try :
    data = pickle.load(open("current.pkl", "rb"))
    os.remove("current.pkl")
except FileNotFoundError :
    data = []
data.append(dataPt)
pickle.dump(data, open("current.pkl", "wb"))

if flag == False :
    try :
        dataSets = pickle.load(open("data.dir/data.pkl", "rb"))
        os.remove("data.dir/data.pkl")
    except FileNotFoundError :
        dataSets = []
    dataSets.append([params, data])
    pickle.dump(dataSets, open("data.dir/data.pkl", "wb"))
