Buckling analysis simulations
Directory Initiation: 04/12/2022

Goal:
- Set up simulations that yield a buckled chain
- Use from the chain configuration, estimate the curvature as a function of location
- Use the form of this curvature function to determine the basis functions in the continuation of John Neu's buckling analysis

04/12/2022
- Setting up 9 new simulations to determine the curvature
- Consider straight chains initialized with a center perturbation
- Use Psi = 5, 10, 20
- Use a = 1
- Use N = 8, 16, 32 (short, medium, long)
- Simulation times:
    * t = 102 mins (3 cell cycles) for the short chains
    * t = 68 mins (2 cell cycles) for the medium chains
    * t = 34 mins (1 cell cycle) for the long chains
- The simulations were run at 4:50pm ET on 4/12/22
