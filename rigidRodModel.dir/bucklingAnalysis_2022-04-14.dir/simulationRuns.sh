#!/bin/bash

date
for psi in 1 2 5 10 20 50
do
    #                               a time N
    python exponentialGrowth.py $psi 2 68 8 &
done
wait
date
echo "Complete"
