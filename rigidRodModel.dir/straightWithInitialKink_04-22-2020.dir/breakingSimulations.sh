#!/bin/bash

echo "RUNNING"

# 1 15 30 45 60 75 90 100 125 150 175 200)   # argument 1
# 0.05 0.1 0.2 0.3)                                # argument 2
# 5 10 15 20 30)                                   # argument 3

KB_MU_RATIOS=(1 15 30 45 60 75 90 100 125 150 175 200)
RATES=(0.05 0.1 0.2 0.3)
ANGLES=(5 10 15 20 30)

for angle in "${ANGLES[@]}"
do
    echo "NEW ANGLE!"
    for kb_mu_ratio in "${KB_MU_RATIOS[@]}"
    do
        echo "NEW KB_MU_RATIO!"
        for rate in "${RATES[@]}"
        do
            echo "NEW RATE!"
            echo "angle =" $angle
            echo "rate =" $rate
            echo "kb_mu_ratio =" ${kb_mu_ratio}
            flag="true"
            numrods=4
            #python -c "import pickle; pickle.dump([], open('data.dir/tempData.pkl', 'wb'))"
            #mv data.dir/tempData.pkl data.dir/data_kb-mu-ratio${kb_mu_ratio}_growthRate${rate}_angle${angle}.pkl
            while [ $flag = "true" ]
            do
                echo $numrods
                python exponentialGrowth.py ${kb_mu_ratio} $rate $angle $numrods
                python dataManagement.py

                file1="endtime.txt"
                file2="endtimeFlag.txt"
                endtime=$(cat "$file1")
                flag=$(cat "$file2")
                echo $flag $endtime
                rm endtime.txt endtimeFlag.txt

                numrods=$(( numrods + 4 ))

                duration=$SECONDS
                echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
            done
            rm current.pkl
        done
    done
done

echo "SIMULATIONS COMPLETE!"
