import csv
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

#---------------------------------------------------------------------------------------------------

def break_check_heatmap() :

    break_checks = []
    with open("output.dir/crit_stress_check_buckling.csv") as f :
        csv_reader = csv.reader(f, delimiter=',')
        for row in csv_reader :
            vals = np.array( [float(x) for x in row] )
            break_checks.append( vals )
    break_checks = np.array( break_checks )

    param_vals = [1,2,5,10,20,50,100]
    ax = sns.heatmap(break_checks, vmin=0, vmax=1, xticklabels=param_vals, yticklabels=param_vals, cmap='coolwarm', cbar=False)

    #plt.text(10, 2, "Chain breaks")
    #plt.text(1, 100 , "Chain survives")

    plt.title("Does the stress exceed the crtical values?")
    ax.set_xlabel(r"$a$")
    ax.set_ylabel(r"$\Psi$", rotation=0)
    #plt.xticks(fontsize=16)
    plt.yticks(rotation=0)

    ax.invert_yaxis()
    ax.set_aspect('equal')

    plt.savefig("output.dir/breakage_heatmap.pdf")

#---------------------------------------------------------------------------------------------------

def efficiency_heatmap() :

    break_checks = []
    with open("output.dir/crit_stress_check_buckling.csv") as f :
        csv_reader = csv.reader(f, delimiter=',')
        for row in csv_reader :
            vals = np.array( [float(x) for x in row] )
            break_checks.append( vals )
    break_checks = np.array( break_checks )

    exp_rel = []
    with open("output.dir/exp_efficiency.csv") as f :
        csv_reader = csv.reader(f, delimiter=',')
        for row in csv_reader :
            vals = np.array( [float(x)-1 if float(x) < 1 else 0 for x in row] )
            exp_rel.append( vals )
    exp_rel = np.array( exp_rel )

    param_vals = [1,2,5,10,20,50,100]
    ax = sns.heatmap(exp_rel, xticklabels=param_vals, yticklabels=param_vals, cmap='coolwarm_r', cbar=True, annot=True)

    plt.title("Efficiency relative to break limited expansion")
    ax.set_xlabel(r"$a$")
    ax.set_ylabel(r"$\Psi$", rotation=0)
    #plt.xticks(fontsize=16)
    plt.yticks(rotation=0)

    ax.invert_yaxis()
    ax.set_aspect('equal')

    plt.savefig("output.dir/efficiency_heatmap.pdf")

#---------------------------------------------------------------------------------------------------

plt.style.use('./mystyle.mplstyle')
break_check_heatmap()
efficiency_heatmap()
