 #!/bin/bash

echo "RUNNING"


#KBS=(2.5e-8 2.5e-7 2.5e-6 2.5e-5 2.5e-4 2.5e-3 2.5e-2 0.25 2.5)
KBS=(0.001)
#KBS=()
#KBS=(0)

MUS=(1e-5)
#MUS=(1e100)

RATES=(0.02)
#RATES=(0.0333 0.05 0.1 0.2 0.3)

ANGLES=(2)
#ANGLES=(1 1.5 2 2.5 3 3.5 4 4.5 5)

#QS=(0.5)
QS=(0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5)

AS=(2)
#AS=(25 50 75 100 125 150 175 200)

#L0S=(5 10 15 20 25 30 35 40 45 50)
L0S=(5)

#BS=(0.1)
#BS=(0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1)

for q in "${QS[@]}"
do
    echo "NEW Q (LOCATION)!"
    for angle in "${ANGLES[@]}"
    do
        echo "NEW ANGLE!"
        for kb in "${KBS[@]}"
        do
            echo "NEW KB!"
            for mu in "${MUS[@]}"
            do
                echo "NEW MU!"
                for rate in "${RATES[@]}"
                do
                    echo "NEW RATE!"
                    for a in "${AS[@]}"
                    do
                        echo"NEW L0!"
                        for l in "${L0S[@]}"
                        do
                            echo "NEW A!"
                            echo "q =" $q
                            echo "angle =" $angle
                            echo "rate =" $rate
                            echo "mu =" $mu
                            echo "kb =" $kb
                            echo "a =" $a
                            echo "l =" $l
                            flag="true"
                            # use N = 20-40 for varied angle
                            # use N = 30-50 for varied location
                            numrods=4
                            maxnumrods=40
                            #python -c "import pickle; pickle.dump([], open('data.dir/tempData.pkl', 'wb'))"
                            #mv data.dir/tempData.pkl data.dir/data_kb-mu-ratio${kb_mu_ratio}_growthRate${rate}_angle${angle}.pkl
                            while [ $numrods -le $maxnumrods ]
                            do
                                echo $numrods
                                python criticalForceCalculations.py $kb $mu $a $rate $numrods $angle $q $l
                                numrods=$(( numrods + 2 ))
                            done
                            duration=$SECONDS
                            echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
                        done
                    done
                done
            done
        done
    done
done

#python criticalForcePlotting.py data*.pkl
mv data*.pkl data.dir

echo "SIMULATIONS COMPLETE!"
