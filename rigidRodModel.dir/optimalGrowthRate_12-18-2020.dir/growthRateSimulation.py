#!/usr/local/env python

import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import stats
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
from random import randrange
import statistics as stat
import math
import time
import csv
import os
import pickle
import matplotlib
#matplotlib.use("Agg")      # COMMENT THIS FOR PLOTS FOR plt.show(), UNCOMMENT FOR ANIMATIONS
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys

####################################################################################################

class filament(object) :

    #-----------------------------------------------------------------------------------------------

    def __init__(self, t, numRods, l0, com, growthRate, mu, kb, Fcrit, kbDist) :
        """ creates a filament object
                numRods : number of rods in the filament
                l0 : initial length of the cells
                com : center of mass of the filaments
                growthRate : initial growth rate of the
                Lb : length at which a filament breaks
                mu : drag per unit lenght parallel to the axis of the rod
                kb : angular spring constant
                Fcrit : breaking force as a function of psi"""

        self.numRods = numRods
        self.l0 = l0
        self.growthRate = growthRate
        self.T = l0/growthRate * np.log(2)  # cell cycle time
        l = l0 * np.exp( (growthRate/l0) * (t % self.T) )   # cell length
        self.L = numRods * l    # filament length
        self.com = com
        #self.Lb = Lb
        self.mu = mu
        self.kb = kb
        self.Fcrit = Fcrit

        self.kbDist = kbDist

        kbs =  [ self.kbDist(self.kb) for i in range(self.numRods-1) ]
        self.fc = [self.Fcrit(kbs[i]/(self.mu * self.l0**2 * self.growthRate)) * (self.mu * self.l0 * self.growthRate) for i in range(len(kbs)) ]
        self.broken = [False] * (self.numRods-1)

        self.lastDivTime = t

        self.updateStress()
        self.updateTips()

    #-----------------------------------------------------------------------------------------------

    def updateLength(self, t) :
        """ updates the length of the filament
                t : the current time """
        #l = self.l0 * np.exp( (self.growthRate/self.l0) * (t % self.T) )

        l = self.l0 * np.exp( (self.growthRate/self.l0) * (t - self.lastDivTime) )

        self.L = self.numRods * l

        #if self.L > self.Lb : self.broken = True

    #-----------------------------------------------------------------------------------------------

    def cellDivision(self) :
        """ handles cell division """

        self.numRods = int(self.numRods * 2)
        self.broken = [False] * (self.numRods-1)

        oldFc = self.fc
        newFc = []
        for i in range( len(oldFc) ) :
            newFc.append( self.Fcrit( kbDist(self.kb) / (self.mu * self.l0**2 * self.growthRate) ) * (self.mu * self.l0 * self.growthRate) )
            newFc.append(oldFc[i])
        newFc.append( self.Fcrit( kbDist(self.kb) / (self.mu * self.l0**2 * self.growthRate) ) * (self.mu * self.l0 * self.growthRate) )

        self.fc = newFc

    #-----------------------------------------------------------------------------------------------

    def updateTips(self) :
        """ calculate the location of the end tips of the filaments """
        self.tip_locations = [ self.com - (self.L / 2), self.com + (self.L / 2) ]

    #-----------------------------------------------------------------------------------------------

    def updateStress(self) :

        stress = []
        for i in range(1, self.numRods ) :
            q = i / self.numRods
            #s = 0.5 * self.mu * q * (1-q) * self.L * self.numRods * self.growthRate
            s = 0.5 * self.mu * q * (1-q) * self.L**2 * self.growthRate / self.l0

            #s = 0.5 * self.mu * q * (1-q) * ( self.numRods * self.l0 * np.exp( (self.growthRate/self.l0) * (t % self.T) ) )**2 * self.growthRate / self.l0

            #s = 0.5 * q * (1-q) * self.numRods**2
            stress.append(s)
        self.stress = stress

    #-----------------------------------------------------------------------------------------------

    def update(self, t, dt, eventBased) :
        """ updates the attributes of the filament """

        self.updateLength(t)
        self.updateTips()   # updateTips must be called AFTER updateLength
        self.updateStress()

        #a = self.F0 / np.sqrt(2)    # Maxwell distribution parameter in terms of the critical stress
        # check if a break as occured

        if not eventBased :

            if self.numRods > 1 :
                #print("Diffs Before:", np.array(self.stress) - np.array( self.fc ) )
                for i in range(self.numRods-1) :

                    q = i / self.numRods    # fractional location of the ith joint
                    s = self.stress[i]      # stress in the ith joint

                    #prob_break = 2/np.pi * s**2 * np.exp( -s**2 / (2 * a**2) ) / a**3   # Maxwell-Boltzmann Distribution
                    #prob_break = 1
                    #prob_break = (np.exp(s/self.F0) - 1) * dt
                    #if random.uniform(0,1) < prob_break : self.broken[i] = True

                    if self.stress[i] > self.fc[i]  :
                        #print(i, self.stress[i] - self.fc[i])
                        self.broken[i] = True

                    #if s > self.fc[i] : self.broken[i] = True

        #print("Flags:", self.broken)
        if len(self.broken) != self.numRods-1 : input("HEY LOOK HERE")


####################################################################################################

def main(eventBased, endTime, numRods, l0, growthRate, mu, kb, Fcrit, kbDist) :

    #growthRate = 0.1    # intitial  growth rate
    #numRods = 4         # number of initial cells
    #l0 = 5              # initial cell length

    #Lb = 2000           # filament breaking length
    #mu = 1e-9           # drag coefficient per unit length for parallel drag

    # critical stress at which breaking probability peaks
    #F0 = 0.5 * mu * 0.25 * 32**2 * l0 * growthRate
    #F0 = 0.75e-7
    # ~6e-6 for mu = 1e-7, growthRate = 0.1

    t = 0
    T = l0/growthRate * np.log(2)       # cell cycle time
    dt = T / 5000     # timestep
    #endTime = 350
    nextDivTime = t + T
    nextEventTime = nextDivTime
    lastDivTime = 0.0

    numBreaks = 0
    breakLens = []


    filaments = [ filament(t, numRods, l0, 0.0, growthRate, mu, kb, Fcrit, kbDist) ]     # initiial filament
    totalLength = [[t, numRods * l0]]    # array of data points [t, Lt] where Lt is the length from the two farthest tips
    totalLengthRate = []
    while t < endTime :

        print("Time =", t)

        if eventBased :

            breakEventTimes = []
            for f in filaments :
                for i in range(len(f.stress)) :
                    q = (i+1) / f.numRods
                    nextBreakTime = (f.l0 / (2 * f.growthRate)) * np.log( 2 * f.fc[i] / ( f.growthRate * f.mu * q * (1-q) * f.numRods**2 * f.l0 ) )
                    breakEventTimes.append(nextBreakTime + lastDivTime)
            nextBreakTime = min(breakEventTimes)

            index = breakEventTimes.index(nextBreakTime)
            for i in range(len(filaments)) :
                if index > len(filaments[i].stress) :
                    index -= len(filaments[i].stress)
                else :
                    f_index = i
                    break_index = index
                    break
            #print("NEXT BREAK: Filament", f_index, ", Node", break_index)

            if nextBreakTime < nextDivTime :
                print("BREAK")
                numBreaks += 1
                t = nextBreakTime

                filaments[f_index].broken[break_index] = True

                newFilaments = []
                for f in filaments :

                    f.update(t, dt, eventBased)

                    # if the filament has reached the breaking length
                    if True in f.broken :
                        breakLens.append(f.L)

                        if sum(f.broken) > 1 :
                            #print("Fcrits", f.fc)
                            #print("stress", f.stress)
                            #print("F-Fc:", np.array(f.stress) - np.array(f.fc) )
                            #print("Flags:", f.broken)
                            input("UH OH -- More than one breaking event occured")
                        ic = (f.numRods-2)/2
                        indices = [i for i, x in enumerate(f.broken) if x == True]
                        i_break = indices[[np.abs(i - ic) for i in indices].index( min([np.abs(i - ic) for i in indices]) )]

                        oldFc = f.fc

                        new_com_1 = f.com - f.L/2 * (1 - ( (i_break+1) / f.numRods ) )
                        f1 = filament(t, i_break+1, l0, new_com_1, growthRate, mu, kb, Fcrit, kbDist)
                        f1.fc = oldFc[:i_break]
                        newFilaments.append(f1)

                        new_com_2 = f.com - f.L/2 * (1 - ( (f.numRods + i_break +1 ) / f.numRods) )
                        f2 = filament(t, f.numRods-i_break-1, l0, new_com_2, growthRate, mu, kb, Fcrit, kbDist)
                        f2.fc = oldFc[i_break+1:]
                        newFilaments.append(f2)

                    else : newFilaments.append(f)

                filaments = newFilaments

            else :
                #print("DIV")
                t = nextDivTime
                for f in filaments :
                    f.update(t, dt, eventBased)
                    f.cellDivision()   # check if cells divide
                    f.lastDivTime = t
                    f.update(t, dt, eventBased)
                lastDivTime = t
                nextDivTime = t + T

            tip_max = max( [ max(f.tip_locations) for f in filaments ] )
            tip_min = min( [ min(f.tip_locations) for f in filaments ] )
            totalLength.append( [t, tip_max - tip_min] )
            Lt_rate =  (totalLength[-1][1] - totalLength[-2][1]) / (totalLength[-1][0] - totalLength[-2][0])
            totalLengthRate.append( [t, Lt_rate] )

            #input("step complete")

        else:

            newFilaments =[]
            for f in filaments :

                f.update(t, dt, eventBased)
                # if the filament has reached the breaking length
                if True in f.broken :

                    #input("BREAK!")

                    if sum(f.broken) > 1 :
                        print("Fcrits", f.fc)
                        print("stress", f.stress)
                        #input("UH OH -- More than one breaking event occured")
                    ic = (f.numRods-2)/2
                    indices = [i for i, x in enumerate(f.broken) if x == True]
                    i_break = indices[[np.abs(i - ic) for i in indices].index( min([np.abs(i - ic) for i in indices]) )]

                    oldFc = f.fc

                    new_com_1 = f.com - f.L/2 * (1 - ( (i_break+1) / f.numRods ) )
                    f1 = filament(t, i_break+1, l0, new_com_1, growthRate, mu, kb, Fcrit, kbDist)
                    f1.fc = oldFc[:i_break]
                    newFilaments.append(f1)

                    new_com_2 = f.com - f.L/2 * (1 - ( (f.numRods + i_break +1 ) / f.numRods) )
                    f2 = filament(t, f.numRods-i_break-1, l0, new_com_2, growthRate, mu, kb, Fcrit, kbDist)
                    f2.fc = oldFc[i_break+1:]
                    newFilaments.append(f2)

                    """
                    for i in range(f.numRods-1) :
                        lastBreakIndex = 0
                        if f.broken[i] :        # check if the ith joint is broken
                            newfilaments.append( filament(t, lastBreakIndex+i+1), f.com )
                            lastBreakIndex = i

                    newFilaments.append( filament(t, f.numRods/2, l0, f.com - (f.L/4), growthRate, Lb) )
                    newFilaments.append( filament(t, f.numRods/2, l0, f.com + (f.L/4), growthRate, Lb) )"""

                else : newFilaments.append(f)

            filaments = newFilaments

            if (t+dt)%T < t%T  :
                for f in filaments :
                    f.cellDivision()   # check if cells divide

            t += dt
            print("Time =", t)

            tip_max = max( [ max(f.tip_locations) for f in filaments ] )
            tip_min = min( [ min(f.tip_locations) for f in filaments ] )
            totalLength.append( [t, tip_max - tip_min] )
            Lt_rate =  (totalLength[-1][1] - totalLength[-2][1]) / (totalLength[-1][0] - totalLength[-2][0])
            totalLengthRate.append( [t, Lt_rate] )

    print("Average Rate =", totalLength[-1][1]/endTime, "um/min")
    print("Number of breaks:", numBreaks)
    return totalLength[-1][1]/endTime, numBreaks, breakLens

    """totalLength = np.array(totalLength)
    plt.figure(1)
    plt.title(r"Tip-to-Tip Expansion Displacement" )
    plt.xlabel(r"Time(min)")
    plt.ylabel(r"Displacement ($\mu$m)")
    plt.plot(totalLength[:,0], totalLength[:,1])

    totalLengthRate = np.array(totalLengthRate)
    plt.figure(2)
    plt.title(r"Tip-to-Tip Expansion Rate" )
    plt.xlabel(r"Time(min)")
    plt.ylabel(r"Rate of Expansion ($\mu$m/min)")
    plt.plot(totalLengthRate[:,0], totalLengthRate[:,1])

    plt.show() """


# SIMULATION PARAMETER
np.random.seed(32)
eventBased = True
mc_steps = 10
endTime = 350

# PHYSICAL PARAMETERS
numRods = 4         # number of initial cells
l0 = 5              # initial cell length
growthRate = 0.1    # intitial  growth rate
mu = 10           # drag coefficient per unit length for parallel drag

Fcrit = lambda psi : 3.466248151546605 * psi + 12.20633912763862
kbDist = lambda kb : abs(np.random.normal(kb, 0.1 * kb))

"""#kbVals = [1e-8, 2e-8, 3e-8, 4e-8, 5e-8, 6e-8, 7e-8, 8e-8, 9e-8, 1e-7]
#psiVals = [kbVal / (mu * l0**2 * growthRate) for kbVal in kbVals]

psiVals = [4, 8, 12, 16, 20, 24, 28, 32, 36, 40]
kbVals = [psiVal * (mu * l0**2 * growthRate) for psiVal in psiVals]

# SIMULATION CALLS
expRateVals = []
for kbVal in kbVals :
    expRate = main(eventBased, endTime, numRods, l0, growthRate, mu, kbVal, Fcrit, kbDist)
    expRateVals.append( expRate )

plt.title(r"Expansion Rate vs. $\Psi$" )
plt.xlabel(r"$\Psi (\frac{k_b}{\mu l_0^3 r})$")
plt.ylabel(r"Average Expansion Rate ($\mu$m/min)")
plt.plot(psiVals, expRateVals)
plt.savefig("plots.dir/averageExpansionRate.pdf")


#kbVals = [1e-7, 2e-7, 3e-7, 4e-7, 5e-7, 6e-7, 7e-7, 8e-7, 9e-7, 1e-6]
#psiVals = [kbVal / (mu * l0**2 * growthRate) for kbVal in kbVals]

psiVals = [40, 80, 120, 160, 200, 240, 280, 320, 360, 400]
kbVals = [psiVal * (mu * l0**2 * growthRate) for psiVal in psiVals]

# SIMULATION CALLS
expRateVals = []
for kbVal in kbVals :
    expRate = main(eventBased, endTime, numRods, l0, growthRate, mu, kbVal, Fcrit, kbDist)
    expRateVals.append( expRate )

plt.title(r"Expansion Rate vs. $\Psi$" )
plt.xlabel(r"$\Psi (\frac{k_b}{\mu l_0^3 r})$")
plt.ylabel(r"Average Expansion Rate ($\mu$m/min)")
plt.plot(psiVals, expRateVals)
plt.savefig("plots.dir/averageExpansionRate2.pdf")"""



#kbVals = [1e-9, 5e-9, 1e-8, 5e-8, 1e-7, 5e-7, 1e-6, 5e-6, 1e-5, 5e-5, 1e-4]
#psiVals = [kbVal / (mu * l0**2 * growthRate) for kbVal in kbVals]

#psiVals = [0.4, 2, 4, 20, 40, 2e2, 4e2, 2e3, 4e3, 2e4, 4e4, 2e5, 4e5]
#psiVals = [1e5, 2e5, 3e5, 4e5, 5e5, 6e5, 7e5, 8e5, 9e5, 10e5, 11e5, 12e5, 13e5, 14e5, 15e5, 16e5, 17e5, 18e5, 19e5, 20e5, 21e5, 22e5, 23e5, 24e5]
psiVals = [1e3, 2e3, 3e3, 4e3, 5e3, 6e3, 7e3, 8e3, 9e3, 10e3, 11e3, 12e3, 13e3, 14e3, 15e3, 16e3, 17e3, 18e3, 19e3, 20e3, 21e3, 22e3, 23e3, 24e3]
kbVals = [psiVal * (mu * l0**2 * growthRate) for psiVal in psiVals]
muVals = [1e5 / (psiVal * l0**2 * growthRate) for psiVal in psiVals]

# SIMULATION CALLS
expRateMeans, expRateStDevs = [], []
avgNumBreaks, stDevNumBreaks = [], []
meanBreakLens = []
for kbVal in kbVals :
    vals = []
    breakCount = []
    breakLenVals = []
    for i in range(mc_steps) :
        sys.stdout = open(os.devnull, "w")
        sys.stderr = open(os.devnull, "w")
        expRate, numBreaks, breakLens = main(eventBased, endTime, numRods, l0, growthRate, mu, kbVal, Fcrit, kbDist)
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__
        vals.append( expRate )
        breakCount.append(numBreaks)
        breakLenVals.append(stat.mean(breakLens))
    print("Done kb =", kbVal)
    expRateMeans.append(stat.mean(vals))
    expRateStDevs.append(stat.stdev(vals))
    avgNumBreaks.append(stat.mean(breakCount))
    stDevNumBreaks.append(stat.stdev(breakCount))
    meanBreakLens.append(stat.mean(breakLenVals))



plt.clf()
plt.title(r"Expansion Rate vs. $\Psi$" )
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Average Expansion Rate ($\mu$m/min)")
#plt.plot(psiVals, expRateMeans)
plt.errorbar(psiVals, expRateMeans, yerr=expRateStDevs, fmt='-o', ecolor='r')
#plt.axvline(x=4e5, color='r')
plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
plt.savefig("plots.dir/averageExpansionRate4.pdf")

plt.clf()
plt.title(r"Number of Breaks vs. $\Psi$" )
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Number of Breaks")
#plt.plot(psiVals, expRateMeans)
plt.errorbar(psiVals, avgNumBreaks, yerr=stDevNumBreaks, fmt='-o', ecolor='r')
#plt.axvline(x=4e5, color='r')
plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
plt.savefig("plots.dir/numBreaksVsPsi.pdf")

plt.clf()
plt.title(r"Avg. Break Len vs. $\Psi$" )
#plt.xlabel(r"Parallel Drag Coefficient $\mu$ (kg / $\mu$m min)")
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Break Len")
plt.plot(psiVals, meanBreakLens, '-o')
#plt.errorbar(psiVals, meanBreakLens, yerr=[], fmt='-o', ecolor='r')
#plt.axvline(x=4e5, color='r')
plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
plt.savefig("plots.dir/breakLenVsPsi.pdf")
