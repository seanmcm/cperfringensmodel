#!/bin/bash

echo "RUNNING"

KBS=(1e-7)
MUS=(1e-9)
RATES=(0.1)

#ANGLES=(2)
ANGLES=(1 1.5 2 2.5 3 3.5 4 4.5 5)

QS=(0.2)
#QS=(0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5)
#QS=(0.01 0.02 0.03 0.04 0.05 0.06 0.07 0.08 0.09 0.1 0.11 0.12 0.13 0.14 0.15 0.16 0.17 0.18 0.19 0.2 0.21 0.22 0.23 0.24 0.25 0.26 0.27 0.28 0.29 0.3 0.31 0.32 0.33 0.34 0.35 0.36 0.37 0.38 0.39 0.4 0.41 0.42 0.43 0.44 0.45 0.46 0.47 0.48 0.49 0.5)

for q in "${QS[@]}"
do
    echo "NEW Q (LOCATION)!"
    for angle in "${ANGLES[@]}"
    do
        echo "NEW ANGLE!"
        for kb in "${KBS[@]}"
        do
            echo "NEW KB!"
            for mu in "${MUS[@]}"
            do
                echo "NEW MU!"
                for rate in "${RATES[@]}"
                do
                    echo "NEW RATE!"
                    echo "q =" $q
                    echo "angle =" $angle
                    echo "rate =" $rate
                    echo "mu =" $mu
                    echo "kb =" $kb
                    flag="true"
                    numrods=36
                    maxnumrods=56
                    #python -c "import pickle; pickle.dump([], open('data.dir/tempData.pkl', 'wb'))"
                    #mv data.dir/tempData.pkl data.dir/data_kb-mu-ratio${kb_mu_ratio}_growthRate${rate}_angle${angle}.pkl
                    while [ $numrods -le $maxnumrods ]
                    do
                        echo $numrods
                        python criticalForceCalculations.py $kb $mu $rate $numrods $angle $q
                        numrods=$(( numrods + 2 ))

                    done
                    duration=$SECONDS
                    echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
                done
            done
        done
    done
done

#python criticalForcePlotting.py data*.pkl
mv data*.pkl data.dir

echo "SIMULATIONS COMPLETE!"
