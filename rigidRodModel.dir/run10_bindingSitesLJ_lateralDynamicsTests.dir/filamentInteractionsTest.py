#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# Filament interactions test
# 10/26/2018

import numpy as np
from scipy.integrate import ode
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
#import plotsV2


def dqdt(t, q, tauTheta1, Rthresh) :
    """ returns array of qdots for the solving of dq/dt = qdot
        q is an array of generalized cordinates
        t is time (float)
        tauTheta1: relaxation time for rotation due to alginment with neighbors
        Rthresh: threshold distance for neighbor alignment """

    xVals = q[ : len(q)//3 ]
    yVals = q[ len(q)//3 : len(q)//3 * 2 ]
    thetaVals = q[ len(q)//3 * 2 : ]

    xDots = []
    yDots = []
    for i in range( len(q)//3 ) :
        xDots.append(0)
        yDots.append(0)

    thetaDots =[]
    for i in range( len(thetaVals) ) :
        qdot = 0
        for j in range( len(thetaVals) ) :
            if i == j : pass    # when the two cells are the same cell
            else :
                r = np.sqrt( (xVals[i]-xVals[j])**2 + (yVals[i]-yVals[j])**2 )
                if r < Rthresh :    # if the cell are with Rthresh of each other
                    qdot -= np.sin( 2 * (thetaVals[i]-thetaVals[j]) / tauTheta1 )   # add contribution to qdot
        thetaDots.append( qdot )

    qDots = xDots + yDots + thetaDots
    return qDots

####################################################################################################

tauTheta1 = 10
Rthresh = 100

N = 2   # number of cells
l = 5   # cell length (in micrometers)

cell1 = [0, 0, 0]               # [xpos, ypos, theta from horizontal]
cell2 = [l/2, -l/4, np.pi/6]

q0 = cell1[:N] + cell2[:N] + [cell1[N]] + [cell2[N]]
print("q0 =", q0)

t = 0
endTime = 10

sol = []
sol.append([t, *q0])

backend = 'vode'
#backend = 'dopri5'
#backend = 'dop853'
solver = ode(dqdt).set_integrator(backend)

#func = lambda t, q : dqdt(t, q, tauTheta1, Rthresh)
#solver = ode(func).set_integrator(backend)
#solver.set_initial_value(q0, t).set_f_params()

solver.set_initial_value(q0, t).set_f_params(tauTheta1, Rthresh)
#solver.integrate(nextStopTime)

print("StopTime", endTime)
while solver.successful() and solver.t < endTime :
    realCall = True
    solver.integrate(endTime, step=True)
    sol.append([solver.t, *solver.y])
    #print([solver.t, *solver.y])
    print("Time =", solver.t)

#t = sol[-1][0]

for s in sol :
    print("t =", s[0], "theta1 =", s[5], "theta2 =", s[6])
