#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 09/07/2018

import numpy as np
import scipy as sp
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle as pkl

# initialize parameters

def initialization(totalTime=60, option=0, random=True) :

    np.random.seed(7)

    """
    Dictate the desired phenomenological effects
    Choose an option for the model :
      0 : standard growth (no other effects)
      1 : neighbor suppresses growth
      2 : neighbor promotes growth
      3 : force dependent growth rate """
    #option = 0

    #random = False       # stochastic effects flag

    #mcSteps = 1
    #totalTime = to      # minutes
    dt = 0.01           # timestep (minutes)

    frac = 0.3          # fraction for determining stdev for random number from normal dist
    mu = 1e-9           # drag coefficient per length
    avgDivLen = 10      # average length at which a cell divides

    # determine average growth rates
    rateWithout = 0.2                   # average growth rate of cell without a neighbor
    if option == 0 :
        rateWith = rateWithout          # average growth rate of cell with a neighbor
    if option == 1 :
        rateWith = 0.6 * rateWithout
    if option == 2 :
        rateWith = 1.6 * rateWithout
    if option == 3 :
        rateWith = rateWithout

    return dt, totalTime, frac, mu, avgDivLen, rateWithout, rateWith, option, random

def getForce(mu, L, LPrime, q) :
    """ determines the constraint force for the point
        located at l = q L """
    return 0.5 * mu * L * LPrime * q * (1-q)

def getGrowthRate(avgRate, frac, random) :
    """ determines the growth rate of an individual
        cell, if random == true then stochastic
        effects are inlcuded in the calculation """
    if random :
        return np.random.normal(avgRate, frac * avgRate)
    else : return avgRate


def main(option=0) :

    if option == 0 :
        return
    elif option == 1 :
        print("Development in progress")
        return
    elif option == 2 :
        print("Development in progress")
        return
    else :
        print("Development in progress")
        return
