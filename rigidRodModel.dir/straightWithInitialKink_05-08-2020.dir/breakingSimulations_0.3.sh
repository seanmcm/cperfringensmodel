#!/bin/bash

# 1 15 30 45 60 75 90 100 125 150 175 200)   # argument 1
# 0.05 0.1 0.2 0.3)                                # argument 2
# 5 10 15 20 30)                                   # argument 3

rm current.pkl out.pkl endtime.txt endtimeFlag.txt      # clear any old uneeded files
echo "RUNNING"

KBS=(1e-7)
MUS=(1e-9)
RATES=(0.1)
ANGLES=(20)

INDICIES=(0 1 2 3 4 5 6 7 8)
QS=(0.21 0.22 0.23 0.24 0.25 0.26 0.27 0.28 0.29 0.3)

for q in "${QS[@]}"
do
    echo "NEW Q (LOCATION)!"
    for angle in "${ANGLES[@]}"
    do
        echo "NEW ANGLE!"
        for kb in "${KBS[@]}"
        do
            echo "NEW KB!"
            for mu in "${MUS[@]}"
            do
                echo "NEW MU!"
                for rate in "${RATES[@]}"
                do
                    echo "NEW RATE!"
                    echo "q =" $q
                    echo "angle =" $angle
                    echo "rate =" $rate
                    echo "mu =" $mu
                    echo "kb =" $kb
                    flag="true"
                    numrods=30
                    #python -c "import pickle; pickle.dump([], open('data.dir/tempData.pkl', 'wb'))"
                    #mv data.dir/tempData.pkl data.dir/data_kb-mu-ratio${kb_mu_ratio}_growthRate${rate}_angle${angle}.pkl
                    while [ $flag = "true" ]
                    do
                        echo $numrods
                        python exponentialGrowth.py $kb $mu $rate $numrods $angle $q
                        python dataManagement.py

                        file1="endtime.txt"
                        file2="endtimeFlag.txt"
                        endtime=$(cat "$file1")
                        flag=$(cat "$file2")
                        echo $flag $endtime
                        rm endtime.txt endtimeFlag.txt

                        numrods=$(( numrods + 10 ))

                        duration=$SECONDS
                        echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
                    done
                    rm current.pkl
                done
            done
        done
    done
done

echo "SIMULATIONS COMPLETE!"
