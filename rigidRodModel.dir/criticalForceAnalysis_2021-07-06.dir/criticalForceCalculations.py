#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys


####################################################################################################
# Filament Dynamics Functions
####################################################################################################

def length(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return l0 * np.exp( (growthRate/l0) * (t - lastAddTime) )


def growthRateFunc(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return growthRate * np.exp( (growthRate/l0) * (t - lastAddTime) )

#---------------------------------------------------------------------------------------------------

def growthDynamics(t, q, lt, kb, mu, nu, epsilon, growthRate) :
    """ updates the qdots for the filament based only on growth dynamics
            l (array) : lengths of the cells in the Filaments
            lp (array) : growth rates for each cell
            kb (float) : angular spring constant
            mu (float) : parallel drag coefficient
            nu (float) : perpendiular drag coefficient
            epsilion (float) : rotational drag coefficient """

    l = [lt] * ( len(q) - 2 )
    lp = [ growthRate ] * ( len(q) - 2 )

    x0 = q[0]
    y0 = q[1]
    thetas = q[2:]

    numRods = len(thetas)
    numNodes = numRods + 1

    #totalLength = sum(l)

    #--------------------------------------------------------

    xEqns, yEqns, thetaEqns = [], [], []

    # x_i Euler-Lagrange equations --------------------------------------------------------

    # initialize x_0 E-L equation and x E_L equation array
    xEqn0 = np.zeros(3*numRods)
    xEqn0[0] = ( mu * np.cos(thetas[0])**2 + nu * np.sin(thetas[0])**2 ) * l[0]
    xEqn0[1] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    if numRods != 1 : xEqn0[numRods + 2] = -1
    xEqns.append(xEqn0)

    # initialize y_0 E-L equation and y E_L equation array
    yEqn0 = np.zeros(3*numRods)
    yEqn0[0] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    yEqn0[1] = ( mu * np.sin(thetas[0])**2 + nu * np.cos(thetas[0])**2 ) * l[0]
    if numRods != 1 : yEqn0[2*numRods + 1] = -1
    yEqns.append(yEqn0)

    # initialize theta_0 E-L equation and theta E_L equation array
    thetaEqn0 = np.zeros(3*numRods)
    thetaEqn0[2] = epsilon * l[0]**3
    if numRods != 1 :
        thetaEqn0[numRods + 2] = 0.5 * l[0] * np.sin(thetas[0])
        thetaEqn0[2*numRods + 1] = -0.5 * l[0] * np.cos(thetas[0])
    thetaEqns.append(thetaEqn0)

    #generate the 2 through N-1 x, y, and theta E-L equations
    for i in range(1, numRods) :

        # x_i E_L equations -----------------------------------------------------------

        xEqni = np.zeros(3*numRods)

        xEqni[0] = ( mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[i]

        xEqni[1] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * np.cos(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[0] * l[i] * np.sin(thetas[0]) )

        for j in range(1, i) :
            xEqni[j+2] = (mu - nu) * l[j] * l[i] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[j] * l[i] * np.sin(thetas[j])

        xEqni[i+2] = -0.5 * nu * l[i]**2 * ( np.sin(thetas[i]) * np.cos(thetas[i])**2 + np.sin(thetas[i])**3 )

        xEqni[i + numRods + 1] = 1

        if i != (numRods-1) :
            xEqni[i + numRods + 2] = -1

        xEqns.append(xEqni)


        # y_i E_L equations -----------------------------------------------------------

        yEqni = np.zeros(3*numRods)

        yEqni[0] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        yEqni[1] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[i]

        yEqni[2] = 0.5 * ( (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[0] * l[i] * np.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * np.sin(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) )

        for j in range(1, i) :
            yEqni[j+2] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[j] * l[i] * np.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])

        yEqni[i+2] = 0.5 * nu * l[i]**2 * (np.cos(thetas[i]) * np.sin(thetas[i])**2 + np.cos(thetas[i])**3)

        yEqni[i + (2*numRods)] = 1

        if i != (numRods-1) :
            yEqni[i + (2*numRods) + 1] = -1

        yEqns.append(yEqni)


        # theta_i E_L equations -----------------------------------------------------------

        thetaEqni = np.zeros(3*numRods)

        thetaEqni[i+2] = epsilon * l[i]**3

        thetaEqni[i + numRods + 1] = 0.5 * l[i] * np.sin(thetas[i])

        thetaEqni[i + (2*numRods)] = -0.5 * l[i] * np.cos(thetas[i])

        if i != (numRods-1) :
            thetaEqni[i + numRods + 2] = 0.5 * l[i] * np.sin(thetas[i])
            thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * np.cos(thetas[i])

        thetaEqns.append(thetaEqni)


        # stress calculations --------------------------------------------------------------
        #self.stressX.append( 0.5 * ( l[i-1]*np.cos(thetas[i-1]) - l[i]*np.sin(thetas[i]) ) )
        #self.stressY.append( 0.5 * ( -l[i-1]*np.sin(thetas[i-1]) + l[i]*np.cos(thetas[i]) ) )

    # combine all E-L equation coeffcients into a single array
    eqns = np.array( xEqns + yEqns + thetaEqns )

    # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
    #   are standard arrays
    #print(eqns)
    #input("stopza")

    if numRods == 1 : xVals, yVals, thetaVals = [0.0], [0.0], [0.0]
    else : xVals, yVals, thetaVals = [0.0], [0.0], [-kb * (thetas[0]-thetas[1])]

    for i in range(1, numRods) :

        xVal = -(mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i]))
        for j in range(1,i) :
            xVal -= (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] * lp[j] * np.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        xVals.append(xVal)

        yVal = -(mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i]))
        for j in range(1,i) :
            yVal -= (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] * lp[j] * np.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        yVals.append(yVal)

        if i == (numRods-1) :
            thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
        else :
            thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
        thetaVals.append(thetaVal)

    vals = np.array( xVals + yVals + thetaVals )


    numRods = len(q)-2

    sols = np.linalg.solve(eqns, vals)

    return sols


####################################################################################################
# Main Calculation Setup
####################################################################################################

# import parameters
pert_coefficient = float(sys.argv[1])       # coefficient of the pertubation location ( 0 < q < 1 )
angle = float(sys.argv[2])                  # size of the perturbation
numRods = int(sys.argv[3])                  # initial number of cells
l0 = float(sys.argv[4])
r = float(sys.argv[5])                      # indiviual cell growth rate at begining of cell cycle
kb = float(sys.argv[6])                     # angular spring constant
mu = float(sys.argv[7])                     # drag per unit length parallel to the rod
a = float(sys.argv[8])                      # ratio of parallel drag / perpendicular drag
b = 1/float(sys.argv[9])                    # ratio of parallel drag / rotational  drag

#print(f"b = {b}")

"""kb = float(sys.argv[1])                     # angular spring constant
mu = float(sys.argv[2])                     # drag per unit length parallel to the rod
a = float(sys.argv[3])                      # ratio of parallel drag / perpendicular drag
b = 1/12 #float(sys.argv[4])                 # ratio of parallel drag / rotational  drag
nu =  mu * a                              # drag per unit length perpendicular to the rod
epsilon = nu * b                             # rotational drag per unit length
r = float(sys.argv[4])             # indiviual cell growth rate at begining of cell cycle
numRods = int(sys.argv[5])                  # initial number of cells
angle = float(sys.argv[6])                  # size of the perturbation
pert_coefficient = float(sys.argv[7])       # coefficient of the pertubation location ( 0 < q < 1 )
l0 = float(sys.argv[8])"""

# other user paramters / useful values
nu = a * mu
epsilon = b * nu
growthRate = l0 * r

psi = kb / (mu * l0**3 * r)

#params = (kb, mu, a, b, r, numRods, angle, pert_coefficient, l0)
params = (psi, l0, r, kb, mu, a, b, numRods, angle, pert_coefficient)

#filename = "data_kb" + str(kb) + "_mu" + str(mu) + "_a" + str(a) + "_b" + str(b) + "_rate" + str(r) + "_angle" + str(angle) + "_q" + str(pert_coefficient) + ".pkl"

filename = f"data_l0{l0}_r{r}_kb{kb}_mu{mu}_a{a}_b{round(b,3)}_loc{pert_coefficient}_angle{angle}.pkl"

#paramstr = r"$k_b$ = " + str(kb) + r", $\mu$ = " + str(mu) + r", $G_r$ = " + str(growthRate) + r", $\theta$ = " + str(angle) + r", $q$ =" + str(pert_coefficient)

# VARIED PARAMETER STRING
#paramstr = r"$\theta$ = " + str(angle)
#paramstr = r"$q$ = " + str(pert_coefficient)

angle *= np.pi / 180      # convet angle to radians

# index of the first perturbed cell
pert_index = int( (numRods * pert_coefficient) - 1 )      # convert coefficient to index

q = [0.0] * (numRods+2)
q[pert_index + 2] = -angle
q[pert_index + 3] = angle

#l0 = 5.0
lt = length(0.0, l0, growthRate, 0.0)
lp = growthRateFunc(0.0, l0, growthRate, 0.0)

# compute qdots and Lagrange multipliers
sols = growthDynamics(0.0, q, lt, kb, mu, nu, epsilon, lp)

# organize qdots
qdots = sols[: numRods+2]
thetadots = qdots[2:]

# organize Lagrange multipliers
lambdaXs = sols[numRods+2 : 2*numRods+1]
lambdaYs = sols[2*numRods+1 :]

# calculate node positions
minX, maxX, minY, maxY = 1E100, 0, 1E100, 0
thetas = q[2:]
xNodes = [ q[0] - lt/2 * np.cos( thetas[0] ) ]
yNodes = [ q[1] - lt/2 * np.sin( thetas[0] ) ]
for i in range( len(q) - 2 ) :
    xNodes.append( xNodes[-1] + lt * np.cos(thetas[i]) )
    yNodes.append( yNodes[-1] + lt * np.sin(thetas[i]) )
    if min(xNodes) < minX : minX = min(xNodes)
    if max(xNodes) > maxX : maxX = max(xNodes)
    if min(yNodes) < minY : minY = min(yNodes)
    if max(yNodes) > maxY : maxY = max(yNodes)

#thetadotDiff = qdots[pert_index+2] - qdots[pert_index+3]
straightStress = 0.5 * mu * (numRods * l0)**2 * (growthRate/l0) * pert_coefficient * (1-pert_coefficient)
stressDiff = lambdaXs[pert_index] - straightStress


try :
    data = pickle.load(open(filename, "rb"))
    os.remove(filename)
except FileNotFoundError :
    #data = [params]
    data = [ params, [], [], [], [], [], [] ]
#data.append([numRods * l0, stressDiff])

#thetadotVals = [thetadots[0], thetadots[int(numRods * 0.1)], thetadots[int(numRods * 0.2)], thetadots[int(numRods * 0.3)], thetadots[int(numRods * 0.4)], thetadots[int(numRods * 0.5)]]
#lambdaXVals = [lambdaXs[0], lambdaXs[int(numRods * 0.1)], lambdaXs[int(numRods * 0.2)], lambdaXs[int(numRods * 0.3)], lambdaXs[int(numRods * 0.4)], lambdaXs[int(numRods * 0.5)]]
#lambdaYVals = [lambdaYs[0], lambdaYs[int(numRods * 0.1)], lambdaYs[int(numRods * 0.2)], lambdaYs[int(numRods * 0.3)], lambdaYs[int(numRods * 0.4)], lambdaYs[int(numRods * 0.5)]]

data[1].append(numRods * l0)
data[2].append(qdots[0])
data[3].append(qdots[1])
data[4].append( np.array(thetadots) )
data[5].append( np.array(lambdaXs) )
data[6].append( np.array(lambdaYs) )

pickle.dump(data, open(filename, "wb"))
