#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 07/16/2018

import numpy as np
from scipy.integrate import ode
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
import plotsV2
import plotsV3

################################################################################
# SIMULATION NOTES
#   TIME-SCALE: minutes
#   LENGTH-SCALE: micrometers
################################################################################

################################################################################
# FUNCTIONS
################################################################################

def length(t, l0, growthRate) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    global lastAddTime
    return l0 + growthRate * ( t - lastAddTime )

    # Something might be funky with the growth rate

def springConstant(t, k0, length, l0) :
    """ returns the linear spring constant at time t
            t: time
            k0: the initial linear spring constant (value at t = 0)
            length: the current equilibrium length
            l0: the initial spring equilibrium length (value at t = 0) """
    return k0 * l0 / length

def dragCoefficient(t, gamma0, l, l0) :
    """ returns the drag coefficient as time t
            t: time
            gamma0: the initial drag coefficient (value at time t =0 )
            totalLength: total length of the filament at time t
            totalLength0: total length of the filament at time t = 0 """
    return gamma0 * l0 / l

def rotDragCoefficient(t, gamma0, l, l0) :
    """ returns the drag coefficient as time t
            t: time
            gamma0: the initial drag coefficient (value at time t =0 )
            totalLength: total length of the filament at time t
            totalLength0: total length of the filament at time t = 0 """
    return gamma0 * (l0 / l)**3

def shortestDistance(x0, y0, x1, y1, x2, y2) :
    """ returns the shortest distance from a point (x0, y0) to a
        line segment with end points (x1, y1) and (x2, y2) """

    """d1 = np.sqrt( (x1-x0)**2 + (y1-y0)**2 )
    d2 = np.sqrt( (x2-x0)**2 + (y2-y0)**2 )

    a = y2 - y1
    b = x1 - x2
    c = x2 * y1 - x1 * y2
    numer = np.abs( a * x0 + b * y0 + c )
    denom = np.sqrt( a**2 + b**2 )
    d3 = numer/denom

    return min([d1, d2, d3])"""

    Ax = x0 - x1
    Ay = y0 - y1
    Bx = x2 - x1
    By = y2 - y1

    dot = (Ax * Bx) + (Ay * By)
    lengthSqrd = Bx**2 + By**2
    param = dot / lengthSqrd

    if param < 0 : xx, yy = x1, y1
    elif param > 1 : xx, yy = x2, y2
    else :
        xx = x1 + param * Bx
        yy = y1 + param * By

    dx = x0 - xx
    dy = y0 - yy

    return np.sqrt( dx**2 + dy**2 )

################################################################################
# ODE FUNCTION
################################################################################

def dqdt(t, q, l0, growthRate, kb0, mu0, nu0, epsilon0, numCells, Rthresh, tauTheta) :
    """ the ODE function
        returns an array of the qDots of the N+2 generalized coordinate at time
        t to be used in solving dq/dt = qDot
            t: time
            q: array of the form [x0, y0 , theta0, theta1, ... , thetaN] """

    global lambdaVals
    global realCall

    q1 = q[:numCells[0]+2]
    q2 = q[numCells[0]+2:]
    qs = [q1, q2]

    qPairs = []
    for i in range( len(qs)) :
        for j in range( len(qs) ) :
            if i == j : pass
            else :
                qPairs.append( [ qs[i], qs[j] ] )

    """lPairs = []
    for i in range( len(l0s)) :
        for j in range( len(l0s) ) :
            if i == j : pass
            else :
                lPairs.append( [ l0s[i], l0s[j] ] )"""
    lPairs = [l0, l0]

    solutions = []
    for z in range( len(qPairs) ) :

        q = qPairs[z][0]
        qOther = qPairs[z][1]

        l0 = lPairs[0]
        l0_other = lPairs[1]

        # initizations
        #n = ReferenceFrame('n')     # defines vector reference frame
        random.seed(1)

        # convert the position array
        x0 = q[0]
        y0 = q[1]
        thetas = q[2:]

        numRods = len(thetas)
        numNodes = numRods + 1

        # calulate spring length, spring constant, and drag coefficients
        lt = length(t, l0, growthRate)
        l = np.array([lt]*numRods)
        lp = np.array([growthRate]*numRods)
        totalLength = sum(l)
        kb = kb0
        mu = dragCoefficient(t, mu0, lt, l0)
        nu = dragCoefficient(t, nu0, lt, l0)
        epsilon = rotDragCoefficient(t, epsilon0, lt, l0)
        Rthresh = l[0]/2


        xEqns, yEqns, thetaEqns = [], [], []

        # x_i Euler-Lagrange equations --------------------------------------------------------

        # initialize x_0 E-L equation and x E_L equation array
        xEqn0 = np.zeros(3*numRods)
        xEqn0[0] = ( mu * np.cos(thetas[0])**2 + nu * np.sin(thetas[0])**2 ) * l[0]
        xEqn0[1] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
        if  numRods != 1 :
            xEqn0[numRods + 2] = -1
        xEqns.append(xEqn0)

        # initialize y_0 E-L equation and y E_L equation array
        yEqn0 = np.zeros(3*numRods)
        yEqn0[0] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
        yEqn0[1] = ( mu * np.sin(thetas[0])**2 + nu * np.cos(thetas[0])**2 ) * l[0]
        if numRods != 1 :
            yEqn0[2*numRods + 1] = -1
        yEqns.append(yEqn0)

        # initialize theta_0 E-L equation and theta E_L equation array
        thetaEqn0 = np.zeros(3*numRods)
        thetaEqn0[2] = epsilon * l[0]**3
        if numRods != 1 :
            thetaEqn0[numRods + 2] = 0.5 * l[0] * np.sin(thetas[0])
            thetaEqn0[2*numRods + 1] = -0.5 * l[0] * np.cos(thetas[0])
        thetaEqns.append(thetaEqn0)

        #generate the 2 through N-1 x, y, and theta E-L equations
        for i in range(1, numRods) :

            # x_i E_L equations -----------------------------------------------------------

            xEqni = np.zeros(3*numRods)

            xEqni[0] = ( mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[i]

            xEqni[1] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

            xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * np.cos(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[0] * l[i] * np.sin(thetas[0]) )

            for j in range(1, i) :
                xEqni[j+2] = (mu - nu) * l[j] * l[i] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[j] * l[i] * np.sin(thetas[j])

            xEqni[i+2] = -0.5 * nu * l[i]**2 * ( np.sin(thetas[i]) * np.cos(thetas[i])**2 + np.sin(thetas[i])**3 )

            xEqni[i + numRods + 1] = 1

            if i != (numRods-1) :
                xEqni[i + numRods + 2] = -1

            xEqns.append(xEqni)


            # y_i E_L equations -----------------------------------------------------------

            yEqni = np.zeros(3*numRods)

            yEqni[0] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

            yEqni[1] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[i]

            yEqni[2] = 0.5 * ( (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[0] * l[i] * np.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * np.sin(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) )

            for j in range(1, i) :
                yEqni[j+2] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[j] * l[i] * np.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])

            yEqni[i+2] = 0.5 * nu * l[i]**2 * (np.cos(thetas[i]) * np.sin(thetas[i])**2 + np.cos(thetas[i])**3)

            yEqni[i + (2*numRods)] = 1

            if i != (numRods-1) :
                yEqni[i + (2*numRods) + 1] = -1

            yEqns.append(yEqni)


            # theta_i E_L equations -----------------------------------------------------------

            thetaEqni = np.zeros(3*numRods)

            thetaEqni[i+2] = epsilon * l[i]**3

            thetaEqni[i + numRods + 1] = 0.5 * l[i] * np.sin(thetas[i])

            thetaEqni[i + (2*numRods)] = -0.5 * l[i] * np.cos(thetas[i])

            if i != (numRods-1) :
                thetaEqni[i + numRods + 2] = 0.5 * l[i] * np.sin(thetas[i])
                thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * np.cos(thetas[i])

            thetaEqns.append(thetaEqni)

        # combine all E-L equation coeffcients into a single array
        eqns = xEqns + yEqns + thetaEqns
        eqns = np.array(eqns)
        # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
        #   are standard arrays

        if numRods == 1 :
            xVals, yVals, thetaVals = [0], [0], [0]
        else :
            xVals, yVals, thetaVals = [0], [0], [-kb * (thetas[0]-thetas[1])]

        for i in range(1, numRods) :

            xVal = -(mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i]))
            for j in range(1,i) :
                xVal -= (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] * lp[j] * np.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
            xVals.append(xVal)

            yVal = -(mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i]))
            for j in range(1,i) :
                yVal -= (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] * lp[j] * np.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
            yVals.append(yVal)

            if i == (numRods-1) :
                thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
            else :
                thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
            thetaVals.append(thetaVal)

        vals = xVals + yVals + thetaVals
        vals = np.array(vals)

        # solve the system of equations
        #   solve Ax = v where A is formed from eqns, v is vals and
        #   x are the unknowns [xdot_0, ydot_0, thetadot_i, lambdax_i, lambday_i]
        sols = np.linalg.solve(eqns, vals)


        """"""
        
        # DISTANCES BASED ON NODES

        x0, y0, x0Other, y0Other = q[0], q[1], qOther[0], qOther[1]
        theta0, theta0Other = q[2], qOther[2]

        l = [length(t, l0, growthRate)] * int( len(q[2:]) )
        lOther = [length(t, l0_other, growthRate)] * int( len(qOther[2:]) )

        x1, y1, xOther, yOther = [], [], [], []

        x1.append( x0 - (l[0] * np.cos(theta0))/2 )
        x1.append( x0 + (l[0] * np.cos(theta0))/2 )
        y1.append( y0 - (l[0] * np.sin(theta0))/2 )
        y1.append( y0 + (l[0] * np.sin(theta0))/2 )

        i = 1
        for theta in q[3:] :
            x1.append( x1[-1] + l[i] * np.cos(theta) )
            y1.append( y1[-1] + l[i] * np.sin(theta) )
            i += 1

        xOther.append( x0Other - (l[0] * np.cos(theta0Other))/2 )
        xOther.append( x0Other + (l[0] * np.cos(theta0Other))/2 )
        yOther.append( y0Other - (l[0] * np.sin(theta0Other))/2 )
        yOther.append( y0Other + (l[0] * np.sin(theta0Other))/2 )

        i = 1
        for theta in qOther[3:] :
            xOther.append( xOther[-1] + l[i] * np.cos(theta) )
            yOther.append( yOther[-1] + l[i] * np.sin(theta) )
            i += 1

        rs = []
        for i in range( len(l) ) :
            row = []
            for j in range( len(lOther) ) :
                d1 = shortestDistance(x1[i], y1[i], xOther[j], yOther[j], xOther[j+1], yOther[j+1])
                d2 = shortestDistance(x1[i+1], y1[i+1], xOther[j], yOther[j], xOther[j+1], yOther[j+1])
                row.append( min( [d1, d2] ) )
            rs.append(row)

        """

        # DISTANCES BASED ON CENTER OF MASSES

        x0, y0, x0Other, y0Other = q[0], q[1], qOther[0], qOther[1]
        theta0, theta0Other = q[2], qOther[2]

        x1, y1, xOther, yOther = [x0], [y0], [x0Other], [y0Other]

        l = [length(t, l0, growthRate)] * int( len(q[2:]) )
        lOther = [length(t, l0_other, growthRate)] * int( len(qOther[2:]) )

        for i in range( 1, len(thetas) ) :
            x1.append( x1[-1] + l[i-1]/2 * np.cos(thetas[i-1]) + l[i]/2 * np.cos(thetas[i]) )
            y1.append( y1[-1] + l[i-1]/2 * np.sin(thetas[i-1]) + l[i]/2 * np.sin(thetas[i]) )

        thetasOther = qOther[2:]
        for i in range( 1, len(thetasOther) ) :
            xOther.append( xOther[-1] + l[i-1]/2 * np.cos(thetasOther[i-1]) + l[i]/2 * np.cos(thetasOther[i]) )
            yOther.append( yOther[-1] + l[i-1]/2 * np.sin(thetasOther[i-1]) + l[i]/2 * np.sin(thetasOther[i]) )

        rs = []
        for i in range(len(x1)) :
            row = []
            for j in range(len(xOther)) :
                row.append( np.sqrt( (x1[i]-xOther[j])**2 + (y1[i]-yOther[j])**2 ) )
            rs.append(row)
        """

        #print("rValues", rs)
        #exit()
        """"""

        # addition terms in thetadot_i due to alignment forces
        # this needs to be fixed
        thetasOther = qOther[2:]
        alignAdjustments = []
        for i in range( len(thetas) ) :
            adjustment = 0
            for j in range( len(thetasOther) ) :
                if rs[i][j] < Rthresh :    # if the cells are within Rthresh of each other
                    #if realCall : print("making an adjustment", z, i, j, rs[i][j], Rthresh)
                    adjustment -= 3 * np.sin( 2 * (thetas[i]-thetasOther[j]) / tauTheta )   # add contribution
            alignAdjustments.append( adjustment )

        sol1 = [ sols[0], sols[1] ]
        for i in range( 2, numRods+2 ) :
            sol1.append( sols[i] + alignAdjustments[i-2] )
        solutions = np.append( solutions, sol1 )

        #if realCall : print("checking thetaDots", sols[2:numRods+2])

        # array of the constraint force valse
        #lambdaX = sols[numRods+2 : 2*numRods+1]
        #lambdaY = sols[2*numRods+1:]

    # append constraint force values to global array of force values
    #   only append when call is 'real' since solver makes additional calls
    """if realCall == True :
        temp = []
        for i in range( len(lambdaX) ) :
            temp.append( [ lambdaX[i], lambdaY[i] ] )
        lambdaVals.append(temp)
    realCall = False    # set to false so 'non-real' calls are not included"""


    # This code can be used to print out all xdot_i and ydot_i if needed
    #   for debugging or other purposes

    """xdots = [sols[0]]
    ydots = [sols[1]]
    thetaDots = sols[2:numRods+2]

    for i in range( 1 , numRods ) :

        val = 0.5 * (lp[i-1] * np.cos(thetas[i-1]) - l[i-1] * thetaDots[i-1] * np.sin(thetas[i-1]) + lp[i] * np.cos(thetas[i]) - l[i] * thetaDots[i] * np.sin(thetas[i]))
        xdots.append(xdots[-1] + val)

        val = 0.5 * (lp[i-1] * np.sin(thetas[i-1]) + l[i-1] * thetaDots[i-1] * np.cos(thetas[i-1]) + lp[i] * np.sin(thetas[i]) + l[i] * thetaDots[i] * np.cos(thetas[i]))
        ydots.append(ydots[-1] + val)

    print("xdot =", xdots)
    print("ydot =", ydots)"""


    #return sols[:numRods+2]
    return solutions

################################################################################
# ODE SOLVER
################################################################################

def simulation(endTime, numRods, angle, mu0, nu0, epsilon0, kb0) :
    """ simulation C. Perfringen bacteria """

    global lambdaVals
    global realCall
    global lastAddTime

    t = 0

    # determine the name of the python file, remove '.py' and add '.pkl'
    paramString = "_mu" + str(mu0) + "_nu" + "{:.2e}".format(nu0) + "_ep" + str(epsilon0) + "_kb" + str(kb0) + "_angle" + str(angle) + "_endTime" + str(t + endTime)
    outputFile = "data.dir/" + os.path.basename(__file__)[:-3] + paramString + ".pkl"
        # "data.dir/" puts it in the data.dir directory

    # determine initial condition ----------------------------------------------

    """angle *= np.pi/180      # convert angle from degrees to radians

    q0 = [0,0,-angle/2]

    step = angle / (numRods-1)
    for i in range(3, numRods+2) :
        q0.append(q0[-1] + step)"""

    numFilaments = 2
    #q1 = [0, 0, 0, np.pi/8, np.pi/4]
    #q2 = [0, -2.5, 0, np.pi/8, np.pi/4]
    #q1 = [0, 0, 0]
    #q2 = [0, -2.5, np.pi/3]
    #q1 = [0, 0, 0, np.pi/6]
    #q2 = [3, -3, -np.pi/8, -np.pi/16]
    q1 = [0, 0, 0, np.pi/6, np.pi/4, np.pi/3]
    q2 = [3, -3, -np.pi/8, -np.pi/16, np.pi/10, np.pi/6]
    q0 = q1 + q2

    numCells = [len(q1)-2, len(q2)-2]

    # constants ----------------------------------------------------------------

    growthRate = 0.1                # rate of cell growth (micrometers/minutes)

    l0 =  5                         # initial length of the rigid rods
    Rthresh = l0                    # cutoff distance for cell alignment potential
    tauTheta = 60                   # time scale for cell alignment
    #totalLength0 = numRods * l0     # initial total length of filament
    #totalLength0 = (len(q0)-2) * l0
    #totalLengths = [numCells[0]*l0, numCells[1]*l0]
    #totalLength0Other = (len(qOther)-2) * l0

    # integration --------------------------------------------------------------

    sol = []                        # array for the data
    sol.append([t, q1, q2])

    #solOther = []                        # array for the data
    #solOther.append([t, *qOther])

    addTimeInterval = l0 / growthRate


    while t < endTime :

        print(t < endTime)

        if endTime <= (t + addTimeInterval) :
            nextStopTime = endTime
            addNodesFlag = False
        else:
            nextStopTime = t + addTimeInterval
            addNodesFlag = True

        print("Adding Nodes?", addNodesFlag)
        if addNodesFlag : print("ADDING NODES AT t =", t + addTimeInterval)

        backend = 'vode'
        #backend = 'dopri5'
        # backend = 'dop853'
        solver = ode(dqdt).set_integrator(backend)
        #solverOther = ode(dqdt).set_integrator(backend)

        print("q0 =", q0)
        #solver.set_initial_value(q0, t).set_f_params(l0, growthRate, kb0, mu0, nu0, epsilon0, totalLength0)
        solver.set_initial_value(q0, t).set_f_params(l0, growthRate, kb0, mu0, nu0, epsilon0, numCells, Rthresh, tauTheta)
        #solverOther.set_initial_value(qOther, t).set_f_params(q0, l0_other, l0, growthRate, kb0, mu0, nu0, epsilon0, totalLength0)


        #solver.integrate(nextStopTime)

        print("StopTime", endTime)
        while solver.successful() and solver.t < nextStopTime :

            realCall = True

            solver.integrate(endTime, step=True)
            f1 = solver.y[:numCells[0]+2]
            f2 = solver.y[numCells[0]+2:]
            #print("f1", f1)
            #print("f2", f2)
            sol.append([solver.t, f1, f2])

            #solverOther.integrate(endTime, step=True)
            #solOther.append([solverOther.t, *solver.y])

            #print([solver.t, *solver.y])
            print("Time =", solver.t)

        t = sol[-1][0]

        addNodesFlag = False      # uncomment/comment to turn off/on cell divsion feature
        if addNodesFlag :

            lastAddTime = t

            print("BEGIN ADDING NODES")

            q = sol[-1][1:]

            lOld = l0 #length(t, l0, growthRate)
            print("length before addition", length(t, lOld, growthRate))
            lNew = lOld / 2
            totalLength = (len(q0) - 2) * lNew

            muOld = dragCoefficient(t, mu0, lNew, lOld)
            nuOld = dragCoefficient(t, nu0, lNew, lOld)
            epsilonOld = rotDragCoefficient(t, epsilon0, lNew, lOld)

            mu0 = muOld / 2
            nu0 = nuOld / 2
            epsilon0 = epsilonOld / 8

            q0 = [ q[0] - lNew * np.cos(q[2]), q[1] - lNew * np.sin(q[2])]
            for i in range( 2 , len(q) ) :
                q0.append(q[i])
                q0.append(q[i])

            #l0 = lNew
            print("length after addition", length(t, lNew, growthRate))
            totalLength0 = l0 * len(q0)
            numRods *= 2

            print("DONE ADDING NODES")

        else : q0 = solver.y

    print("Integration Complete")

    t = []
    xs = [[], []]
    ys = [[], []]
    l0 = 5
    #lastNumRods = len(sol[0][3:])
    lastAddTime = 0
    lastTime = 0
    for s in sol :

        #print(s)
        t.append(s[0])

        for z in range(2) :

            f = s[1 + z]

            xVals, yVals = [], []

            #l0 = math.floor(s[0] % addTimeInterval)
            """if lastNumRods < len(s[3:]) :
                lastNumRods = len(s[3:])
                lastAddTime = lastTime
            lastTime = s[0]"""


            l = [length(s[0], l0, growthRate)] * int( len(f[2:]) )

            x0 = f[0]
            y0 = f[1]
            theta0 = f[2]

            xVals.append( x0 - (l[0] * np.cos(theta0))/2 )
            xVals.append( x0 + (l[0] * np.cos(theta0))/2 )

            yVals.append( y0 - (l[0] * np.sin(theta0))/2 )
            yVals.append( y0 + (l[0] * np.sin(theta0))/2 )

            i = 1
            for theta in f[3:] :
                xVals.append( xVals[-1] + l[0] * np.cos(theta) )
                yVals.append( yVals[-1] + l[0] * np.sin(theta) )
                i += 1

            xs[z].append( xVals )
            ys[z].append( yVals )

    data = [t, xs[0], ys[0], xs[1], ys[1]]

    pickle.dump(data, open(outputFile, "wb"))

    plotsV3.configurationPlot(outputFile)           # generate configuration plot
    #plotsV2.constraintForcePlot(outputFile)         # generate constraint force plots

    #os.remove("equations.pkl")  # remove equation file, you don't need it anymore

    return


################################################################################

def main() :

    global lastAddTime

    startTime = time.clock()

    mu0 = 1e-9
    #muVals = 0.000000001
    kbVals = [1e-7]#[1e-8, 5e-8, 1e-7, 5e-7, 1e-6, 5e-6, 1e-5]
    nuVals = [100*mu0]#[100*mu0, 1000*mu0] # [2*mu0, 5*mu0, 10*mu0,

    endTime = 25            # total simulation time
    numRods = 5             # initial number of rods
    angle = 90              # angle subtended by the initial filament configuration (in degrees)

    #mu0 = 1e-9       # initial parallel drag coefficient
    #kb0 = 10000000          # initial angular spring constant

    for nu0 in nuVals :
        for kb0 in kbVals :

            #nu0 = mu0 * 100        # initial perpendiculat drag coefficient
            epsilon0 = nu0 / 10     # initial angular drag coefficient

            lambdaVals = [None]     # none since the inital lambda values are unknown
            realCall = False        # flag used to determine if a call to dqdt is real
            lastAddTime = 0         # time of the last cell division

            print("NEW CALL:", nu0, kb0)
            simulation(endTime, numRods, angle, mu0, nu0, epsilon0, kb0)

    # Calulating runtime, output in form "xx hrs, xx mins, xx secs"
    totalRuntime = time.clock()-startTime
    mins = round(totalRuntime//60)
    secs = round(totalRuntime % 60)
    if mins >= 60 :
        hrs = round(mins // 60)
        mins = round(mins % 60)
        print( "Runtime =", hrs, "hrs", mins, "minutes", secs, "seconds" )
    else : print( "Runtime =", mins, "minutes", secs, "seconds" )

    print("Sweet Baby Jesus, IT WORKS!")


if __name__ == "__main__" :
    main()

################################################################################
