#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys


plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'figure.autolayout': True})

#outfile = os.path.basename(sys.argv[1])
#filename = sys.argv[1]

psiVals, l0Vals, rVals, kbVals, muVals, aVals, bVals = [], [], [], [], [], [], []
critStressVals, critLengthVals = [], []

variedParam = sys.argv[1].lower()
while variedParam not in ['loc', 'angle', 'l0', 'r', 'kb', 'mu', 'a', 'b'] :
    print("Not a valid varied parameter string.")
    print("Enter 'exit' to quit.")
    print("Enter one of the following: loc, angle, l0, 'r', kb, mu, a, b")
    variedParam = input().lower()
    if variedParam == 'exit' : exit()

for filename in sys.argv[2:] :

    # import data
    data = pickle.load(open(filename, "rb"))

    # key parameters
    psi, l0, r, kb, mu, a, b, numRods, angle, pert_coefficient = data[0]

    #kb, mu, a, b, r, numRods, angle, pert_coefficient, l0 = data[0]
    #psi = kb / (mu * l0**3 * r)

    # other useful values
    growthRate = l0 * r

    # append the parameter values to their respective arrays
    psiVals.append(psi)
    l0Vals.append(l0)
    rVals.append(r)
    kbVals.append(kb)
    muVals.append(mu)
    aVals.append(a)
    bVals.append(b)

    # organzied the other data components -- calculated physical quantities
    lengths = np.array(data[1])
    xdotVals = np.array(data[2])
    ydotVals = np.array(data[3])
    thetadotVals = np.array(data[4])
    lambdaXVals = np.array(data[5])
    lambdaYVals = np.array(data[6])

    # convert the pertubation coefficient (aka q) to the proper index
    index = [int(pert_coefficient * length / l0 - 1) for length in lengths]

    # compute deltaThetaDot for the perturbed linkages
    deltaThetadots = [ thetadotVals[i][index[i]+1]-thetadotVals[i][index[i]] for i in range(len(thetadotVals)) ]

    stresses_dim = [300 * np.sqrt ( lambdaXVals[i][index[i]]**2 + lambdaYVals[i][index[i]]**2 ) for i in range(len(lambdaXVals)) ]

    # compute the Nondimensionalized stress in the perturbed linkages
    stresses = [ np.sqrt ( lambdaXVals[i][index[i]]**2 + lambdaYVals[i][index[i]]**2 ) / (mu * l0**2 * r) for i in range(len(lambdaXVals)) ]

    # determine the critical stresses via linear regression of the stresses and the deltaThetaDots
    m, yint = np.polyfit(stresses, deltaThetadots, 1)
    critStress = -yint/m
    critStressVals.append(critStress)

    #m, yint= np.polyfit(lengths, deltaThetadots, 1)
    #critLength = -yint/m
    #critLengthVals.append(critLength)

    critLengthVals.append( np.sqrt( 8 * critStress * l0**2 ) )

    #deltaThetadotVecs.append( np.array(deltaThetadots) )
    #stressVecs.append( np.array(stresses) )

    #plt.clf()

    title_dict = {
        'loc' : r"$\Delta\dot{\theta}$ Vs. Stress (Various Locations $q$)",
        'angle' : r"$\Delta\dot{\theta}$ Vs. Stress (Various Angles $\Delta\theta$)",
        'l0' : r"$\Delta\dot{\theta}$ Vs. Stress (Various $l_0$)",
        'r' : r"$\Delta\dot{\theta}$ Vs. Stress (Various $r$)",
        'kb' : r"$\Delta\dot{\theta}$ Vs. Stress (Various $k_b$)",
        'mu': r"$\Delta\dot{\theta}$ Vs. Stress (Various $\mu$)",
        'a' : r"$\Delta\dot{\theta}$ Vs. Stress (Various $a$)",
        'b' : r"$\Delta\dot{\theta}$ Vs. Stress (Various $b$)"
    }

    label_dict = {
        'loc' : fr'$q$ = {pert_coefficient}',
        'angle' : fr'$\Delta\theta$ = {angle}',
        'l0' : fr'$l_0$ = {l0}',
        'r' : fr'$r$ = {r}',
        'kb' : fr'$k_b$ = {kb}',
        'mu': fr'$\mu$ = {mu}',
        'a' : fr'$a$ = {a}',
        'b' : fr'$b$ = {b}'
    }

    #plt.title(title_dict[variedParam])
    #plt.xlabel(r"Stress (pN)") #(kg $\mu$m/min$^2$)")
    plt.xlabel(r"$F^* = F / \mu l_0^2 r$") #(kg $\mu$m/min$^2$)")
    #plt.ylabel(r"Perturbation Angle Rate of Change $\Delta \dot{\theta}$")
    plt.ylabel(r"$\frac{d\Delta\theta}{dt^*} = \frac{1}{r}\frac{d\Delta\theta}{dt}$", size=22)
    #plt.plot(stresses_dim, deltaThetadots, '-o', label=label_dict[variedParam])
    plt.plot(stresses, (1/r) * np.array(deltaThetadots), '-o', label=label_dict[variedParam])
    #plt.hlines(0, -1e100, 1e100 , linestyles='dashed')
    plt.legend(loc="upper left", prop={'size': 12})


    #plt.show()
    #plt.clf()

dir_dict = {
    'loc' : 'varied_loc.dir',
    'angle' : 'varied_angle.dir',
    'l0' : 'varied_l0.dir',
    'r' : 'varied_r.dir',
    'kb' : 'varied_kb.dir',
    'mu': 'varied_mu.dir',
    'a' : 'varied_a.dir',
    'b' : 'varied_b.dir'
}

param_array_dict = {
    'psi' : psiVals,
    'l0' : l0Vals,
    'r' : rVals,
    'kb' : kbVals,
    'mu': muVals,
    'a' : aVals,
    'b' : bVals
}

plt.savefig(f"plots.dir/{dir_dict[variedParam]}/stressVsDeltaThetadot.pdf")
#exit()
#psiVals.sort()
#critStressVals.sort()

#critLengthVals.sort()

m, b = np.polyfit(psiVals, critStressVals, 1)
fCrit = lambda psi : m * psi + b
lCrit = lambda psi : l0 * np.sqrt( 8 * (m * psi + b) )

plt.clf()
#plt.title(r"Critical Stress Varies with $\Psi$")
plt.xlabel(r"$\Psi = \frac{k_b}{\mu l_0^3 r}$")
plt.ylabel(r"Scaled Critical Stress $F^{*}_{crit} = F_{crit} / \mu l_0^3 r$") #(kg $\mu$m/min$^2$)")
#plt.ylabel(r"Critical Stress (pN)") #(kg $\mu$m/min$^2$)")
#plt.plot(np.linspace(1,10**5,2), fCrit(np.linspace(1,10**5,2)), 'tab:blue', lw=3, label=f"Slope = {round(m,2)}")
plt.plot(np.linspace(1e-3,1e5, 10000000), fCrit(np.linspace(1e-3,1e5, 10000000)), 'k', lw=3, label=r"$F^{*}_{crit}(\Psi) = $" + rf"{round(m,2)}$\Psi$ + {round(b,2)}")
plt.loglog(psiVals, critStressVals, 'o', lw=3, ms=12, markeredgewidth=3, fillstyle='none')
#plt.plot(np.array(psiVals), fCrit(np.array(psiVals)), 'rx', lw=3, ms=10, label=r"$F_{crit}(\Psi) = $" + rf"{round(m,2)}$\Psi$ + {round(b,2)}")
plt.legend(loc="lower right", fontsize=14)
plt.savefig(f"plots.dir/{dir_dict[variedParam]}/critStressVsPsi_loglog.pdf")


plt.clf()
#plt.title(r"Critical Stress Varies with $\Psi$")
plt.xlabel(r"$\Psi$", fontsize=40)
plt.ylabel(r"$F^{*}_{crit}$", fontsize=40) #(kg $\mu$m/min$^2$)")
#plt.ylabel(r"Critical Stress (pN)") #(kg $\mu$m/min$^2$)")
#plt.plot(np.linspace(1,10**5,2), fCrit(np.linspace(1,10**5,2)), 'tab:blue', lw=3, label=f"Slope = {round(m,2)}")
plt.plot(np.linspace(0, 100, 1000), fCrit(np.linspace(0, 100, 1000)), 'k', lw=6)
#plt.loglog(psiVals, critStressVals, 'o', lw=3, ms=12, markeredgewidth=3, fillstyle='none')
#plt.plot(np.array(psiVals), fCrit(np.array(psiVals)), 'rx', lw=3, ms=10, label=r"$F_{crit}(\Psi) = $" + rf"{round(m,2)}$\Psi$ + {round(b,2)}")
#plt.legend(loc="upper left")

plt.xticks(fontsize=25)
plt.yticks(fontsize=25)

plt.savefig(f"plots.dir/{dir_dict[variedParam]}/critStressVsPsi_linlin.pdf", transparent=True)


"""plt.clf()
#plt.title(r"Critical Breaking Length Vs. $\Psi$")
plt.xlabel(r"$\Psi$ $\left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"$L_{crit}$ ($\mu$m)") #(kg $\mu$m/min$^2$)")
#plt.plot(psiVals, critLengthVals, 'o', lw=3, ms=10, markeredgewidth=3, fillstyle='none')
#plt.plot(psiVals, lCrit(np.array(psiVals)), 'o', ms=10)
plt.plot(np.linspace(1e-3,1e5, 10000000), lCrit(np.linspace(1e-3,1e5, 10000000)), 'tab:blue', lw=5,  label=r"$L_{crit}$ predicted from $F^*_{crit}(\Psi)$")
plt.xscale("log")
plt.yscale("log")
#plt.ticklabel_format(axis='x',style='sci', scilimits=[-6,-6])
plt.legend(loc="upper left")
plt.savefig(f"plots.dir/{dir_dict[variedParam]}/critLengthVsPsi.pdf")"""


lCrit_dict = {
    'l0' : lambda l0 : l0 * np.sqrt( 8 * (m * (kb / (mu * l0**3 * r)) + b ) ),
    'kb' : lambda kb : l0 * np.sqrt( 8 * (m * (kb / (mu * l0**3 * r)) + b ) ),
    'mu' : lambda mu : l0 * np.sqrt( 8 * (m * (kb / (mu * l0**3 * r)) + b ) ),
    'r' : lambda r : l0 * np.sqrt( 8 * (m * (kb / (mu * l0**3 * r)) + b ) )
}

lCrit = lCrit_dict[variedParam]

plt.clf()
plt.xlabel(fr"{variedParam}")
plt.ylabel(r"Nondimensionalized $F^{*}_{crit}$") #(kg $\mu$m/min$^2$)")
plt.plot(param_array_dict[variedParam], critStressVals, 'o', lw=3, ms=12, markeredgewidth=3, fillstyle='none')
plt.savefig(f"plots.dir/{dir_dict[variedParam]}/critStressVs{variedParam}.pdf")

lCrit_small = lambda l0 : l0 * np.sqrt( 8 * m * (kb / (mu * l0**3 * r)) )
lCrit_big = lambda l0 : l0 * np.sqrt( 8 * b )

plt.clf()
plt.xlabel(f"{variedParam}")
plt.ylabel(r"$L_{crit}$ ($\mu$m)") #(kg $\mu$m/min$^2$)")
plt.plot(param_array_dict[variedParam], critLengthVals, 'o', lw=3, ms=10, markeredgewidth=3, fillstyle='none')
plt.plot(np.linspace(5,50,1000), lCrit(np.linspace(5,50,1000)), '-r', ms=10)
plt.plot(np.linspace(5,15,1000), lCrit_small(np.linspace(5,15,1000)), '-g')
plt.plot(np.linspace(30,50,1000), lCrit_big(np.linspace(30,50,1000)), '-m')
plt.savefig(f"plots.dir/{dir_dict[variedParam]}/critLengthVs{variedParam}.pdf")


#plt.show()

m, b = np.polyfit(psiVals, critStressVals, 1)
print(f'C = {m} * Psi + {b}')

print(f'N when Psi = 0: {np.sqrt(8*b)}')
#print(f'N when Psi = 0: {np.sqrt(8*b / (mu * l0 * growthRate))}')

#m, b = np.polyfit(psiVals, critLengthVals, 1)
#print(f'L = {m} * Psi + {b}')
