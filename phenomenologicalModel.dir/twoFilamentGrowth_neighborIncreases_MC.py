#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 08/23/2018

import numpy as np
import scipy as sp
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt


# Parameters ##############################################################################

np.random.seed(7)

mcSteps = 100

totalTime = 60  # mins
dt = 0.01   # timestep (minutes)

frac= 0.3
rateWithout = 0.2               # average growth rate of cell without a neighbor
rateWith = rateWithout * 1.6    # average growth rate of cell with a neighbor
avgDivLen = 10                  # average length at which a cell divides


# Monte Carlo Simulation ##################################################################

F1_count = 0    # number of times filament 1 is longer
#F2_count = 0    # number of times filament 2 is longer

# Initialization ---------------------------------------------------------------------

# FILAMENT #1
l1_init = []
for i in range(7) :
    # initial length of a each cell with some random fluctuations
    l1_init.append( np.random.normal(3*avgDivLen/4, frac * 3*avgDivLen/4) )
L1_init = [sum(l1_init)]

# FILAMENT #2
l2_init = []
for i in range(11) :
    # initial length of a each cell with some random fluctuations
    l2_init.append( np.random.normal(avgDivLen/2, frac * avgDivLen/2) )
L2_init = [sum(l2_init)]

# Simulation ##############################################################################

for n in range( mcSteps ) :

    l1 = l1_init.copy()
    L1 = L1_init.copy()

    l2 = l2_init.copy()
    L2 = L2_init.copy()

    t = 0
    tVec = [t]
    while t < totalTime :

        # UPDATE FIRST FILAMENT

        l1Original = l1.copy()
        for i in range(len(l1)) :

            # determine a growth rate with some random fluctuations
            if (L1[-1] < L2[-1]) or ( sum(l1Original[:i]) < L2[-1] ) :
                growthRate = np.random.normal(rateWith, frac * rateWith)
            else : growthRate = np.random.normal(rateWithout, frac * rateWithout)

            l1[i] += growthRate * dt    # update growth

            # handle cell divsion if necessary
            if l1[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
                newLength = l1[i] / 2       # determine the length of the daughter cells
                l1[i] = newLength
                l1.insert(i+1, newLength)   # inset new cell

        L1.append(sum(l1))      # determine new total length


        # UPDATE SECOND FILAMENT

        l2Original = l2.copy()
        for i in range(len(l2)) :

            # determine a growth rate with some random fluctuations
            if (L2[-1] < L1[-1]) or ( sum(l2Original[:i]) < L1[-1] )  :
                growthRate = np.random.normal(rateWith, frac * rateWith)
            else : growthRate = np.random.normal(rateWithout, frac * rateWithout)

            l2[i] += growthRate * dt    # update growth

            # handle cell divsion if necessary
            if l2[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
                newLength = l2[i] / 2       # determine the length of the daughter cells
                l2[i] = newLength
                l2.insert(i+1, newLength)   # inset new cell

        L2.append(sum(l2))      # determine new total length

        t += dt
        #print(t)
        tVec.append(t)

    tVec = np.array(tVec)
    L1 = np.array(L1)
    L2 = np.array(L2)
    #LDiff = L1 - L2
    finalDiff = L1[-1] - L2[-1]

    if finalDiff > 0 : F1_count += 1
    print("Run", n+1, "is complete")

F2_count = mcSteps - F1_count
#eq_count = mcSteps - (F1_count + F2_count)

print("\n")
if (L1_init[0] - L2_init[0]) > 0 :
    print("Filament 1 was longer initially by", L1_init[0] - L2_init[0])
else : print("Filament 2 was longer initially by", L2_init[0] - L1_init[0])
print("\nMonte Carlo Results:")
print("Filament 1 was longer", F1_count, "times")
print("Filament 2 was longer", F2_count, "times")
#print("The filam", F1_count, "times")

# Plotting ###############################################################################
"""
plt.figure(1)
plt.plot(tVec, LDiff, markersize=0.5)
plt.figure(2)
plt.plot(tVec, L1, tVec, L2, 'r', markersize=0.5)
plt.show()
"""
