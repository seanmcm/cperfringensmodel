#!/bin/bash

# slurm submission for simulation
# Virginia Tech's ARC Cascade Cluster

# RESOURCES #
#SBATCH --nodes=1
#SBATCH --ntasks=8
#SBATCH --mem=16G

# WALLTIME #
# t format d-hr:min:sec
#SBATCH -t 3-00:00:00

# QUEUE #
#SBATCH -p normal_q

# ALLOCATION #
#SBATCH -A CPMot

# MODULES #
module purge

# modules need to load ffmpeg
module load gcc/5.2.0
module load yasm/1.3
module load x264/1.0
module load fdk-aac/1.0
module load lame/3.99.5
module load ffmpeg/2.5.4

module load Anaconda

cd $SLURM_SUBMIT_DIR

# BEGIN SCRIPT HERE
python breakingSimulations_parallel.py

exit;
