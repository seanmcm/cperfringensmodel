print(f"Phenomenologicial Simulation: Psi = {}, Run No. {}")

print("______________________________________________________________________________")
print(f"Step Begin Time = {}\n")

    print(f"\tNext Division Time: {}")
    print(f"\tNext Break Time: {}\n")

    print(f"\tIntermediate Steps:")
    print(f"\tTime = {}, \tDistance = {}, \tDiff = {}\n")

    print(f"\tBREAK EVENT, t = {}, filament = {}, link = {}\n")

        print(f"\t\tLength at breaks = {}\n")

        print(f"\t\tBefore the break:")
            print(f"\t\t\tCritical Forces = {}\n")

        print(f"\t\tAfter the break:")
            print(f"\t\t\tCritical Forces = {}\n")

    if sync: print(f"\tDIVISION EVENT, t = {}")
    else: print(f"\tDIVISION EVENT, t = {}, filament = {}, cell = {}\n")

        print(f"\t\tBefore the division:")
            print(f"\t\t\tCritical Forces = {}\n")

        print(f"\t\tAfter the division:")
            print(f"\t\t\tCritical Forces = {}\n")

        print(f"\t\tNew kb value = {}\n")

print(f"Step End Time = {}")
