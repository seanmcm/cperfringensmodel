#!/bin/bash

# slurm submission for simulation
# Virginia Tech's ARC Tinkercliffs Cluster
# 9/28/2021

# RESOURCES #
#SBATCH --nodes=1
#SBATCH --ntasks=8
#SBATCH --mem=16G

# WALLTIME #
# t format d-hr:min:sec
#SBATCH -t 0-05:00:00

# QUEUE #
#SBATCH -p normal_q

# ALLOCATION #
#SBATCH -A CPMot

# MODULES #
#module purge

# modules need to load ffmpeg
#module load gcc/10.2.0
#module load yasm/1.3
#module load x264/1.0
#module load fdk-aac/1.0
#module load lame/3.99.5
#module load ffmpeg/2.5.4

module load Anaconda3

cd $SLURM_SUBMIT_DIR

#RTOLS=(1e-3 1e-4 1e-5 1e-6 1e-7 1e-8 1e-9 1e-10 1e-11 1e-12)
RTOLS=(1e-2 1e-1 1 10 100)
for rtol in "${RTOLS[@]}"
do
    date
    echo "relative tolerance =" ${rtol}
    python exponentialGrowth.py ${rtol}
date
done

exit;
