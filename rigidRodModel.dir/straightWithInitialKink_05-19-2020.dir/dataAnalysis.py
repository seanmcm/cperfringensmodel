#!/usr/local/env python

import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import stats
from scipy.optimize import least_squares
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
#from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys


def growthDynamics(t, q, lt, kb, mu, nu, epsilon, growthRate) :
    """ updates the qdots for the filament based only on growth dynamics
            l (array) : lengths of the cells in the Filaments
            lp (array) : growth rates for each cell
            kb (float) : angular spring constant
            mu (float) : parallel drag coefficient
            nu (float) : perpendiular drag coefficient
            epsilion (float) : rotational drag coefficient """

    l = [lt] * ( len(q) - 2 )
    lp = [ growthRate ] * ( len(q) - 2 )

    x0 = q[0]
    y0 = q[1]
    thetas = q[2:]

    numRods = len(thetas)
    numNodes = numRods + 1

    #totalLength = sum(l)

    #--------------------------------------------------------

    xEqns, yEqns, thetaEqns = [], [], []

    # x_i Euler-Lagrange equations --------------------------------------------------------

    # initialize x_0 E-L equation and x E_L equation array
    xEqn0 = np.zeros(3*numRods)
    xEqn0[0] = ( mu * np.cos(thetas[0])**2 + nu * np.sin(thetas[0])**2 ) * l[0]
    xEqn0[1] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    if numRods != 1 : xEqn0[numRods + 2] = -1
    xEqns.append(xEqn0)

    # initialize y_0 E-L equation and y E_L equation array
    yEqn0 = np.zeros(3*numRods)
    yEqn0[0] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    yEqn0[1] = ( mu * np.sin(thetas[0])**2 + nu * np.cos(thetas[0])**2 ) * l[0]
    if numRods != 1 : yEqn0[2*numRods + 1] = -1
    yEqns.append(yEqn0)

    # initialize theta_0 E-L equation and theta E_L equation array
    thetaEqn0 = np.zeros(3*numRods)
    thetaEqn0[2] = epsilon * l[0]**3
    if numRods != 1 :
        thetaEqn0[numRods + 2] = 0.5 * l[0] * np.sin(thetas[0])
        thetaEqn0[2*numRods + 1] = -0.5 * l[0] * np.cos(thetas[0])
    thetaEqns.append(thetaEqn0)

    #generate the 2 through N-1 x, y, and theta E-L equations
    for i in range(1, numRods) :

        # x_i E_L equations -----------------------------------------------------------

        xEqni = np.zeros(3*numRods)

        xEqni[0] = ( mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[i]

        xEqni[1] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * np.cos(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[0] * l[i] * np.sin(thetas[0]) )

        for j in range(1, i) :
            xEqni[j+2] = (mu - nu) * l[j] * l[i] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[j] * l[i] * np.sin(thetas[j])

        xEqni[i+2] = -0.5 * nu * l[i]**2 * ( np.sin(thetas[i]) * np.cos(thetas[i])**2 + np.sin(thetas[i])**3 )

        xEqni[i + numRods + 1] = 1

        if i != (numRods-1) :
            xEqni[i + numRods + 2] = -1

        xEqns.append(xEqni)


        # y_i E_L equations -----------------------------------------------------------

        yEqni = np.zeros(3*numRods)

        yEqni[0] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        yEqni[1] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[i]

        yEqni[2] = 0.5 * ( (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[0] * l[i] * np.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * np.sin(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) )

        for j in range(1, i) :
            yEqni[j+2] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[j] * l[i] * np.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])

        yEqni[i+2] = 0.5 * nu * l[i]**2 * (np.cos(thetas[i]) * np.sin(thetas[i])**2 + np.cos(thetas[i])**3)

        yEqni[i + (2*numRods)] = 1

        if i != (numRods-1) :
            yEqni[i + (2*numRods) + 1] = -1

        yEqns.append(yEqni)


        # theta_i E_L equations -----------------------------------------------------------

        thetaEqni = np.zeros(3*numRods)

        thetaEqni[i+2] = epsilon * l[i]**3

        thetaEqni[i + numRods + 1] = 0.5 * l[i] * np.sin(thetas[i])

        thetaEqni[i + (2*numRods)] = -0.5 * l[i] * np.cos(thetas[i])

        if i != (numRods-1) :
            thetaEqni[i + numRods + 2] = 0.5 * l[i] * np.sin(thetas[i])
            thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * np.cos(thetas[i])

        thetaEqns.append(thetaEqni)


        # stress calculations --------------------------------------------------------------
        #self.stressX.append( 0.5 * ( l[i-1]*np.cos(thetas[i-1]) - l[i]*np.sin(thetas[i]) ) )
        #self.stressY.append( 0.5 * ( -l[i-1]*np.sin(thetas[i-1]) + l[i]*np.cos(thetas[i]) ) )

    # combine all E-L equation coeffcients into a single array
    eqns = np.array( xEqns + yEqns + thetaEqns )

    # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
    #   are standard arrays
    #print(eqns)
    #input("stopza")

    if numRods == 1 : xVals, yVals, thetaVals = [0.0], [0.0], [0.0]
    else : xVals, yVals, thetaVals = [0.0], [0.0], [-kb * (thetas[0]-thetas[1])]

    for i in range(1, numRods) :

        xVal = -(mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i]))
        for j in range(1,i) :
            xVal -= (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] * lp[j] * np.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        xVals.append(xVal)

        yVal = -(mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i]))
        for j in range(1,i) :
            yVal -= (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] * lp[j] * np.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        yVals.append(yVal)

        if i == (numRods-1) :
            thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
        else :
            thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
        thetaVals.append(thetaVal)

    vals = np.array( xVals + yVals + thetaVals )


    numRods = len(q)-2

    sols = np.linalg.solve(eqns, vals)

    return sols

# Regression and Plotting Fuctions #################################################################

# Test Case where [kb_mu_ratio = 100, growthRate = 0.1, angle = 10] --------------------------------

def testCase(rawData) :
    """ Note: rawdata[77] is [kb_mu_ratio = 100, growthRate = 0.1, angle = 10] """

    print(rawdata[77][0])
    data = np.array(rawdata[77][1])
    x, y = np.log(data[:,0]), np.log(data[:,1])
    linfit = stats.linregress(x,y)
    print(linfit)


# nonlinear regression of raw data and linear regression of log-log data ---------------------------

def lin_regressions(rawdata) :

    # array where each entry is [params, fit]
    linFits = []        # linear fits of the log-log data

    for rawset in rawdata :

        params = rawset[0]
        params[0] = round( params[0])   # k_b
        params[1] = round( params[1])   # mu
        params[2] = round( params[1], 2 )   # growth rate
        params[3] = round( params[2] * (180 / np.pi) )  # convert the angle back to degrees from radians

        # Linear fits of log-log data
        data = np.array(rawset[1])
        x, y = np.log(data[:,0]), np.log(data[:,1])
        linfit = stats.linregress(x,y)
        linFits.append([params, linfit])

    return linFits


def nonlin_regressions(rawset) :
    """ see https://scipy-cookbook.readthedocs.io/items/robust_regression.html
        for details on nonlinear regression in scipy """

    # array where each entry is [params, fit]
    #nonLinFits = []     # nonlinear power law fits of the raw data

    #for rawset in rawdata :

    """params = rawset[0]
    params[0] = round( params[0])   # k_b
    params[1] = round( params[1])   # mu
    params[2] = round( params[2], 2 )   # growth rate
    params[3] = round( params[3] * (180 / np.pi) )  # convert the angle back to degrees from radians"""

    def powerLaw(x, A, B) : return A * x**B
    def fun(p, x, y) : return (p[0] * x**p[1]) - y

    # nonlinear fit using the robust leasr squares methods (see link in comment above)
    data = np.array(rawset)
    x, y = data[:,0], data[:,1]

    x0 = np.ones(2)
    res_lsq = least_squares(fun, x0, args=(x, y))
    res_robust = least_squares(fun, x0, loss='soft_l1', f_scale=0.1, args=(x,y))
    #nonLinFits.append([params, res_robust])
    y_robust = powerLaw(x, *res_robust.x)

    return y_robust



# Plotting ----------------------------------------------------------------------------------------

def plots(rawdata) :

    # run linear regressions
    linFits = lin_regressions(rawdata)

    lengthData = []

    for linfit in linFits :

        params = linfit[0]
        slope, intercept, rval, pval, stderr = linfit[1]

        # solve for the length when t_break = 0.1 min
        length = np.exp( (np.log(0.1) - intercept) / slope )
        lengthData.append([params, length])


    #kb_mu_ratios = [1, 15, 30, 45, 60, 75, 90, 100, 125, 150, 175, 200]  # argument 1
    #kb_mu_ratios = [ kb_mu * 1e-9 for kb_mu in kb_mu_ratios]
    #rates = [0.05, 0.10, 0.20, 0.30]                                       # argument 2
    #angles = [5, 10, 15, 20, 30]                                      # argument 3

    kbs = [1e-2]        # argument 0
    mus = [1]           # argument 1
    rates = [0.10]      # argument 2
    angles = [20]       # argument 3

    indicies = [0, 1, 2, 3, 4, 5, 6, 7, 8]

    for index in indicies :

        basedirpath = "plots.dir/index" + str(index) + ".dir/"
        currentData = list(filter( lambda x : (x[0][4] == index), lengthData) )

        """var_kb, var_mu, var_rate, var_angle = {}, {}, {}, {}

        for angle in angles :
            for rate in rates :
                temp = list(filter( lambda x : (x[0][2] == rate and x[0][3] == angle), currentData) )
                for i in range(len(temp)) : temp[i] = [temp[i][0][0], temp[i][1]]
                var_kb_mu_ratio[str(rate) + ' ' + str(angle)] = np.array(temp)

        for angle in angles :
            for kb in kbs :
                temp = list(filter( lambda x : (x[0][0] == kb and x[0][3] == angle), currentData) )
                for i in range(len(temp)) : temp[i] = [temp[i][0][1], temp[i][1]]
                var_rate[str(kb_mu_ratio) + ' ' + str(angle)] = np.array(temp)

        for rate in rates :
            for kb in kbs :
                temp = list(filter( lambda x : (x[0][0] == kb and x[0][2] == rate), currentData) )
                for i in range(len(temp)) : temp[i] = [temp[i][0][2], temp[i][1]]
                var_angle[str(kb_mu_ratio) + ' ' + str(rate)] = np.array(temp)

        #j = 1

        # Breaking length vs kb/mu
        dirpath = basedirpath + "var_kb.dir"
        print(dirpath)
        os.mkdir(dirpath)
        for key in var_kb_mu_ratio.keys() :

            rate, angle = key.split()[0], key.split()[1]
            data = var_kb_mu_ratio[key]

            #plt.figure(j)
            plt.title(r"Various $k_b$ ($\lambda$ = " + rate + r", $\theta$ = " + angle + ")" )
            plt.xlabel(r"$k_b$")
            plt.ylabel(r"Breaking Length ($\mu$m)")
            plt.plot(data[:,0], data[:,1])
            #plt.legend([r"$x_{"+str(i)+"}$" for i in range(len(x)) ] , loc='upper left')
            plt.savefig(dirpath + "/rate" + rate + "_angle" + angle + ".pdf", format='pdf')
            #j += 1
            plt.clf()


        # Breaking length vs growthRate
        dirpath = basedirpath + "var_rate.dir"
        print(dirpath)
        os.mkdir(dirpath)
        for key in var_rate.keys() :

            kb_mu_ratio, angle = key.split()[0], key.split()[1]
            data = var_rate[key]

            #plt.figure(j)
            plt.title(r"Various Rate ($k_b$ = " + kb_mu_ratio + r", $\theta$ = " + angle + ")" )
            plt.xlabel(r"Rate ($\mu m/s$)")
            plt.ylabel(r"Breaking Length ($\mu$m)")
            plt.plot(data[:,0], data[:,1])
            #plt.legend([r"$x_{"+str(i)+"}$" for i in range(len(x)) ] , loc='upper left')
            plt.savefig(dirpath + "/kb" + kb_mu_ratio + "_angle" + angle + ".pdf", format='pdf')
            #j += 1
            plt.clf()


        # Breaking length vs angle
        dirpath = basedirpath + "var_angle.dir"
        print(dirpath)
        os.mkdir(dirpath)
        for key in var_angle.keys() :

            kb_mu_ratio, rate = key.split()[0], key.split()[1]
            data = var_angle[key]

            #plt.figure(j)
            plt.title(r"Various Angle ($k_b$ = " + kb_mu_ratio + r", $\lambda$ = " + rate + ")" )
            plt.xlabel(r"Angle (Degrees)")
            plt.ylabel(r"Breaking Length ($\mu$m)")
            plt.plot(data[:,0], data[:,1])
            #plt.legend([r"$x_{"+str(i)+"}$" for i in range(len(x)) ] , loc='upper left')
            plt.savefig(dirpath + "/kb" + kb_mu_ratio + "_rate" + rate + ".pdf", format='pdf')
            #j += 1
            plt.clf() """

    print("Done!")

####################################################################################################

def growthRateAnalysis() :

    # run nonlinear and linear regressions
    nonLinFits, linFits = regressions(rawdata)

    lengthData = []

    for linfit in linFits :

        params = linfit[0]
        slope, intercept, rval, pval, stderr = linfit[1]

        # solve for the length when t_break = 0.1 min
        length = np.exp( (np.log(0.1) - intercept) / slope )
        lengthData.append([params, length])


    kb_mu_ratios = [1, 15, 30, 45, 60, 75, 90, 100, 125, 150, 175, 200]  # argument 1
    kb_mu_ratios = [ kb_mu * 1e-9 for kb_mu in kb_mu_ratios]
    rates = [0.05, 0.10, 0.20, 0.30]                                       # argument 2
    angles = [5, 10, 15, 20, 30]                                      # argument 3

    var_kb_mu_ratio, var_rate, var_angle = {}, {}, {}

    for angle in angles :
        for rate in rates :
            temp = list(filter( lambda x : (x[0][1] == rate and x[0][2] == angle), lengthData) )
            for i in range(len(temp)) : temp[i] = [temp[i][0][0], temp[i][1]]
            var_kb_mu_ratio[str(rate) + ' ' + str(angle)] = np.array(temp)

    for angle in angles :
        for kb_mu_ratio in kb_mu_ratios :
            temp = list(filter( lambda x : (x[0][0] == kb_mu_ratio and x[0][2] == angle), lengthData) )
            for i in range(len(temp)) : temp[i] = [temp[i][0][1], temp[i][1]]
            var_rate[str(kb_mu_ratio) + ' ' + str(angle)] = np.array(temp)

    for rate in rates :
        for kb_mu_ratio in kb_mu_ratios :
            temp = list(filter( lambda x : (x[0][0] == kb_mu_ratio and x[0][1] == rate), lengthData) )
            for i in range(len(temp)) : temp[i] = [temp[i][0][2], temp[i][1]]
            var_angle[str(kb_mu_ratio) + ' ' + str(rate)] = np.array(temp)


####################################################################################################

def finalLengthChecks(rawdata) :

    #indicies = [0, 1, 2, 3, 4, 5, 6, 7, 8]
    indicies = [round(0.01 * z, 2) for z in range(1,51)]
    for index in indicies :


        dirpath = "plots.dir/lengthChecks.dir"
        try : os.mkdir(dirpath)
        except : pass

        l0 = 5

        currentData = list(filter( lambda x : (x[0][4] == index), rawdata) )
        for set in currentData :

            params, data_0 = set[0], set[1]
            print(params)
            kb, mu, growthRate, angle = params[0], params[1], params[2], params[3]*(180 / np.pi)
            T = l0/growthRate * np.log(2)   # time of one cell cycle

            data = []
            for d in data_0 :

                N, t = d[0], d[1]   # initial number of cells, time for a break to occur
                L0 = N * l0
                if t > T :
                    t = t%T  # determine the time since the last cell division event
                    N = N * 2**(int(t//T))
                Lb = N * l0 * np.exp( (growthRate/l0) * t )  # length of the filament when breaking

                data.append([L0, Lb])
            data = np.array(data)

            regress_data = nonlin_regressions(data)

            plt.title(r"$k_b$ = " + str(kb) + r", $\lambda$ = " + str(round(growthRate, 2)) + r", $\theta$ = " + str(round(angle)) )
            plt.xlabel(r"Initial Length ($\mu$m)")
            plt.ylabel(r"Breaking Length ($\mu$m)")
            plt.plot(data[:,0], data[:,1], "o", label='Simulation')
            plt.plot(data[:,0], regress_data, label='LSQ Fit')
            plt.legend()
            #plt.legend([r"$x_{"+str(i)+"}$" for i in range(len(x)) ] , loc='upper left')
            plt.savefig(dirpath + "/index" + str(index) + "_kb" + str(kb) + "_mu" + str(mu) + "_rate" + str(round(growthRate, 2)) + "_angle" + str(round(angle)) + ".pdf", format='pdf')
            plt.clf()


####################################################################################################

def breakingRateChecks(rawdata) :

    #indicies = [0, 1, 2, 3, 4, 5, 6, 7, 8]
    indicies = [round(0.01 * z, 2) for z in range(1,51)]
    #indicies = [0.01]
    for index in indicies :

        dirpath = "plots.dir/breakingRates.dir"
        try : os.mkdir(dirpath)
        except : pass

        dirpath2 = "plots.dir/breakRateVsStress.dir"
        try : os.mkdir(dirpath2)
        except : pass

        l0 = 5

        currentData = list(filter( lambda x : (round(x[0][4],2) == index), rawdata) )
        for set in currentData :

            params, data_0, qVecs = set[0], np.array(set[1])[:,0], np.array(set[1])[:,1]
            print(params)
            kb, mu, growthRate, angle = params[0], params[1], params[2], params[3]*(180 / np.pi)
            T = l0/growthRate * np.log(2)   # time of one cell cycle

            stresses = []
            for i in range( len(qVecs) ) :

                qVec = qVecs[i]
                numRods = len(qVec)-2

                t = data_0[i][1]
                lt = numRods * l0 * np.exp( (growthRate/l0) * t )  # length of a cell when breaking
                lp = numRods * growthRate * np.exp( (growthRate/l0) * t )

                nu = 100*mu
                epsilon = nu/10

                # compute qdots and Lagrange multipliers
                sols = growthDynamics(t, qVec, lt, kb, mu, nu, epsilon, lp)
                # organize qdots
                #qdots = sols[: numRods+2]
                # organize Lagrange multipliers
                lambdaXs = sols[numRods+2 : 2*numRods+1]
                lambdaYs = sols[2*numRods+1 :]

                firstPertRod = int(round(index * numRods)) - 1
                stress = np.sqrt( lambdaXs[firstPertRod]**2 + lambdaYs[firstPertRod]**2 )
                stresses.append(stress)
            stresses = np.array(stresses)

            data = []
            for d in data_0 :

                L0, t = d[0], d[1]   # initial number of cells, time for a break to occur
                #L0 = N * l0
                """if t > T :
                    t = t%T  # determine the time since the last cell division event
                    N = N * 2**(int(t//T))
                Lb = N * l0 * np.exp( (growthRate/l0) * t )  # length of the filament when breaking"""

                data.append([L0, 1/t])
            data = np.array(data)

            regress_data = nonlin_regressions(data)

            # PLOT BREAKING RATE AS FUNCTION OF INITIAL LENGTH
            plt.title(r"$q$ = " + str(index) + r", $k_b$ = " + str(kb) + r", $\mu$ = " + str(mu) + r", $\lambda$ = " + str(round(growthRate, 2)) + r", $\theta$ = " + str(round(angle)) + r"$^\circ$" )
            plt.xlabel(r"Initial Length ($\mu$m)")
            plt.ylabel(r"Breaking Rate (1/min)")
            plt.plot(data[:,0], data[:,1], "o", label='Simulation')
            plt.plot(data[:,0], regress_data, label='LSQ Fit')
            plt.legend()
            #plt.legend([r"$x_{"+str(i)+"}$" for i in range(len(x)) ] , loc='upper left')
            plt.savefig(dirpath + "/index" + str(index) + "_kb" + str(kb) + "_mu" + str(mu) + "_rate" + str(round(growthRate, 2)) + "_angle" + str(round(angle)) + ".pdf", format='pdf')
            plt.clf()

            L_inits = np.sqrt( 2 * stresses * l0 * np.exp(-2 * growthRate / (data[:,1] * l0) ) / (mu * index * (1-index) * growthRate) )
            stresses = 0.5 * mu * index * (1-index) * data[:,0] * growthRate * len(data[:,0])
            #print(L_inits)

            # PLOT BREAKING RATE AS FUNCTION OF STRESS
            plt.title(r"$q$ = " + str(index) + r", $k_b$ = " + str(kb) + r", $\mu$ = " + str(mu) + r", $\lambda$ = " + str(round(growthRate, 2)) + r", $\theta$ = " + str(round(angle)) + r"$^\circ$" )
            plt.xlabel(r"Stress (kg$\mu$m/min$^2$)")
            plt.ylabel(r"Breaking Rate (1/min)")
            plt.plot(stresses, data[:,1], "o", label='Simulation')
            #plt.plot(L_inits, data[:,1], "o", label='Simulation')
            plt.legend()
            #plt.legend([r"$x_{"+str(i)+"}$" for i in range(len(x)) ] , loc='upper left')
            plt.savefig(dirpath2 + "/index" + str(index) + "_kb" + str(kb) + "_mu" + str(mu) + "_rate" + str(round(growthRate, 2)) + "_angle" + str(round(angle)) + ".pdf", format='pdf')
            plt.clf()


####################################################################################################

rawdata = pickle.load(open("data.dir/data.pkl", "rb"))
#rawdata2 = pickle.load(open("data_kb1e-7_mu1e-9.pkl", "rb"))
#rawdata = [ [ rawdata1[i][0], rawdata1[i][1] + rawdata2[i][1] ] for i in range( len(rawdata1) ) ]

# each entry in rawdata is an array, the first item is the param set, the second is the data points

#testCase(rawdata)
#plots(rawdata)
#finalLengthChecks(rawdata)
breakingRateChecks(rawdata)
#growthRateAnalysis(rawdata)
