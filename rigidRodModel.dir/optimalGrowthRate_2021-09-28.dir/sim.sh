#!/bin/bash

# slurm submission for simulation
# Virginia Tech's ARC Tinkercliffs Cluster
# 07/19/2021

# RESOURCES #
#SBATCH --nodes=1
#SBATCH --ntasks=64
#SBATCH --mem=64G

# WALLTIME #
# t format d-hr:min:sec
#SBATCH -t 6-00:00:00

# QUEUE #
#SBATCH -p normal_q

# ALLOCATION #
#SBATCH -A CPMot

# MODULES #
module load Anaconda3

cd $SLURM_SUBMIT_DIR

date
echo "BEGINNING..."
python growthRateSimulationReorganized.py 400 5 $1 $2 file
echo "DONE!"
date
exit;
