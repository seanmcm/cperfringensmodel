from sympy import *
import sympy as sym

xc1, yc1, xc2, yc2 = sym.symbols('xc1 yc1 xc2 yc2')
theta1, theta2 = sym.symbols('theta1 theta2')
n1x, n1y, n2x, n2y = sym.symbols('n1x n1y n2x n2y')
l1, l2 = sym.symbols('l1 l2')
V0, r0 = sym.symbols('V0, r0')
n, w = sym.symbols('n, w')

x1 = xc1 + 0.5 * n1x * l1 * sym.cos(theta1)
y1 = yc1 + 0.5 * n1y * l1 * sym.sin(theta1)

x2 = xc2 + 0.5 * n2x * l2 * sym.cos(theta2)
y2 = yc2 + 0.5 * n2y * l2 * sym.sin(theta2)

r = sym.sqrt( (x2 - x1)**2 + (y2 - y1)**2 )

V = V0 * sym.exp( -(r - w/2)/r0 )       # exponential function potential
#V = V0 * (r0 / (r - w/2))**n            # LJ potential (replusive only)


print(diff(V,xc1), '\n')
print(diff(V,yc1), '\n')
print(diff(V,theta1), '\n')
print(diff(V,theta2), '\n')



"""
# Exponential potential forces

dVdxc1 = -V0*n*(r0/(-w/2 + r))**n*(0.5*l1*n1x*cos(theta1) - 0.5*l2*n2x*cos(theta2) + xc1 - xc2)/((-w/2 + r)*r)

dVdyc1 = -V0*n*(r0/(-w/2 + r))**n*(0.5*l1*n1y*sin(theta1) - 0.5*l2*n2y*sin(theta2) + yc1 - yc2)/((-w/2 + r)*r)

dVdtheta1 = -V0*n*(r0/(-w/2 + r))**n*(0.5*l1*n1x*(-0.5*l1*n1x*cos(theta1) + 0.5*l2*n2x*cos(theta2) - xc1 + xc2)*sin(theta1) - 0.5*l1*n1y*(-0.5*l1*n1y*sin(theta1) + 0.5*l2*n2y*sin(theta2) - yc1 + yc2)*cos(theta1))/((-w/2 + r)*r)

----------------------------------------------------------------------------------------------------

dVdxc2 = V0*n*(r0/(-w/2 + r))**n*(0.5*l1*n1x*cos(theta1) - 0.5*l2*n2x*cos(theta2) + xc1 - xc2)/((-w/2 + r)*r)

dVdyc2 = V0*n*(r0/(-w/2 + r))**n*(0.5*l1*n1y*sin(theta1) - 0.5*l2*n2y*sin(theta2) + yc1 - yc2)/((-w/2 + r)*r)

dVdtheta2 = -V0*n*(r0/(-w/2 + r))**n*(-0.5*l2*n2x*(-0.5*l1*n1x*cos(theta1) + 0.5*l2*n2x*cos(theta2) - xc1 + xc2)*sin(theta2) + 0.5*l2*n2y*(-0.5*l1*n1y*sin(theta1) + 0.5*l2*n2y*sin(theta2) - yc1 + yc2)*cos(theta2))/((-w/2 + r)*r)

####################################################################################################

# LJ replusive terms
dVdxc1 = V0*n*(r0/r)**n*(-0.5*l1*n1x*np.cos(theta1) + 0.5*l2*n2x*np.cos(theta2) - xc1 + xc2)/r**2
dVdxc1 = -V0*n*(r0/(- w/2 + r))**n*(0.5*l1*n1x*cos(theta1) - 0.5*l2*n2x*cos(theta2) + xc1 - xc2)/((-w/2 + r)*r)

dVdyc1 = V0*n*(r0/r)**n*(-0.5*l1*n1y*np.sin(theta1) + 0.5*l2*n2y*np.sin(theta2) - yc1 + yc2)/r**2
dVdyc1 = -V0*n*(r0/(- w/2 + r))**n*(0.5*l1*n1y*sin(theta1) - 0.5*l2*n2y*sin(theta2) + yc1 - yc2)/((-w/2 + r)*r)


dVdtheta1 = V0*n*(r0/r)**n*(-0.5*l1*n1x*(-0.5*l1*n1x*np.cos(theta1) + 0.5*l2*n2x*np.cos(theta2) - xc1 + xc2)*np.sin(theta1) + 0.5*l1*n1y*(-0.5*l1*n1y*np.sin(theta1) + 0.5*l2*n2y*np.sin(theta2) - yc1 + yc2)*np.cos(theta1))/r**2
dVdtheta1 = -V0*n*(r0/(-w/2 + r))**n*(0.5*l1*n1x*(-0.5*l1*n1x*cos(theta1) + 0.5*l2*n2x*cos(theta2) - xc1 + xc2)*sin(theta1) - 0.5*l1*n1y*(-0.5*l1*n1y*sin(theta1) + 0.5*l2*n2y*sin(theta2) - yc1 + yc2)*cos(theta1))/((-w/2 + r)*r)

----------------------------------------------------------------------------------------------------

dVdxc2 = V0*n*(r0/r)**n*(0.5*l1*n1x*np.cos(theta1) - 0.5*l2*n2x*np.cos(theta2) + xc1 - xc2)/r**2
dVdxc2 = V0*n*(r0/(- w/2 + r))**n*(0.5*l1*n1x*cos(theta1) - 0.5*l2*n2x*cos(theta2) + xc1 - xc2)/((-w/2 + r)*r)


dVdyc2 = V0*n*(r0/r)**n*(0.5*l1*n1y*np.sin(theta1) - 0.5*l2*n2y*np.sin(theta2) + yc1 - yc2)/r**2
dVdyc2 = V0*n*(r0/(- w/2 + r))**n*(0.5*l1*n1y*sin(theta1) - 0.5*l2*n2y*sin(theta2) + yc1 - yc2)/((-w/2 + r)*r)

dVdtheta2 = V0*n*(r0/r)**n*(0.5*l2*n2x*(-0.5*l1*n1x*np.cos(theta1) + 0.5*l2*n2x*np.cos(theta2) - xc1 + xc2)*np.sin(theta2) - 0.5*l2*n2y*(-0.5*l1*n1y*np.sin(theta1) + 0.5*l2*n2y*np.sin(theta2) - yc1 + yc2)*np.cos(theta2))/r**2
dvdtheta2 = -V0*n*(r0/(-w/2 + r))**n*(-0.5*l2*n2x*(-0.5*l1*n1x*cos(theta1) + 0.5*l2*n2x*cos(theta2) - xc1 + xc2)*sin(theta2) + 0.5*l2*n2y*(-0.5*l1*n1y*sin(theta1) + 0.5*l2*n2y*sin(theta2) - yc1 + yc2)*cos(theta2))/((-w/2 + r)*r) """
