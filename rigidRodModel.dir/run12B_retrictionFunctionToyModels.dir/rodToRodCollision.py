#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 08/26/2019

import pdb
import numpy as np
from sympy.physics.vector import *
import sympy as sym
from sympy import Point, Polygon, pi
from sympy.geometry import Segment
from scipy.integrate import ode
from scipy.integrate import odeint
import scipy as sp
from mpmath import *
import random
from random import randrange
import math
import time
import csv
import os
import pickle
#import plotsV4
#import plotsV4_withSites
#from FilamentV9 import *

p1_old, p2_old, p3_old, p4_old = None, None, None, None
oldVals = None


def Ra(sPerp, rt, rc) :
    """ returns the activation function
            sPerp : perpendicular distance to the constraint (float)
            rt : transition coefficient (float)
            rc : stiffness parameter (float) """
    sPerp = float( sym.N( sPerp ) )
    x = np.tanh( rt * (sPerp - rc) )
    return 0.5 * ( 1 - x )

def dRdq(p1, seg2, x1, y1, theta1, v0x1, v0y1, omega1, x2, y2, theta2, v0x2, v0y2, omega2, rt, rc, rf) :

    p12 = seg2.projection(p1)
    d12 = p1.distance(p12)      # perpendicular (or shortest) distance
    unit12x = (p12.x - p1.x) / d12
    unit12y = (p12.y - p1.y) / d12
    v12x = v0x1 + omega1 * (p1.x - x1) - v0x2 - omega2 * (p12.x - x2)
    v12y = v0y1 + omega1 * (p1.y - y1) - v0y2 - omega2 * (p12.y - y2)
    v12perp = v12x * unit12x + v12y * unit12y


    dR12dx1 = Ra(d12, rt, rc) * 0.5 * rf * (p12.x - p1.x) / d12
    dR12dy1 = Ra(d12, rt, rc) * 0.5 * rf * (p12.y - p1.y) / d12
    dR12dtheta1 = Ra(d12, rt, rc) * 0.5 * rf * ( (p1.x - x1 ) * (p12.x - p1.x) + (p1.y - y1) * (p12.y - p1.y) ) / d12

    dR12dx2 = -dR12dx1
    dR12dy2 = -dR12dy1
    dR12dtheta2 = -Ra(d12, rt, rc) * 0.5 * rf * ( (p12.x - x2 ) * (p12.x - p1.x) + (p12.y - y2) * (p12.y - p1.y) ) / d12

    return dR12dx1, dR12dy1, dR12dtheta1, dR12dx2, dR12dy2, dR12dtheta2


def dRdqV2(p, segOther, x1, y1, theta1, v0x1, v0y1, omega1, x2, y2, theta2, v0x2, v0y2, omega2, rt, rc, rf) :

    pOther = segOther.projection(p)
    d = p.distance(pOther)      # perpendicular (or shortest) distance
    unitx = (pOther.x - p.x) / d
    unity = (pOther.y - p.y) / d
    vx = v0x1 + omega1 * (p.x - x1) - v0x2 - omega2 * (pOther.x - x2)
    vy = v0y1 + omega1 * (p.y - y1) - v0y2 - omega2 * (pOther.y - y2)
    vperp = vx * unitx + vy * unity


    dRdx1 = Ra(d, rt, rc) * 0.5 * rf * (pOther.x - p.x) / d
    dRdy1 = Ra(d, rt, rc) * 0.5 * rf * (pOther.y - p.y) / d
    dRdtheta1 = Ra(d, rt, rc) * 0.5 * rf * ( (p.x - x1 ) * (pOther.x - p.x) + (p.y - y1) * (pOther.y - p.y) ) / d

    dRdx2 = -dRdx1
    dRdy2 = -dRdy1
    dRdtheta2 = -Ra(d, rt, rc) * 0.5 * rf * ( (pOther.x - x2 ) * (pOther.x - p.x) + (pOther.y - y2) * (pOther.y - p.y) ) / d

    return dRdx1, dRdy1, dRdtheta1, dRdx2, dRdy2, dRdtheta2



def dRdqV3(p, segOther, x1, y1, theta1, x2, y2, theta2, rt, rc, rf, pcheck, val) :

    pOther = segOther.projection(p)
    d = val * sym.N( p.distance(pOther) )     # perpendicular (or shortest) distance

    actFunc = Ra(d, rt, rc)

    dRdx1 = actFunc * sym.N(0.5 * rf * (pOther.x - p.x)) / d
    dRdy1 = actFunc * sym.N(0.5 * rf * (pOther.y - p.y)) / d
    dRdtheta1 = actFunc * sym.N( 0.5 * rf * ( -(p.y - y1) * (pOther.x - p.x) + (p.x - x1) * (pOther.y - p.y) ) ) / d

    dRdx2 = -dRdx1
    dRdy2 = -dRdy1
    dRdtheta2 = -actFunc * sym.N( 0.5 * rf * ( -(pOther.y - y2) * (pOther.x - p.x) + (pOther.x - x2) * (pOther.y - p.y) ) ) / d

    #dRdx1, dRdy1, dRdtheta1, dRdx2, dRdy2, dRdtheta2 = map( sym.N, (dRdx1, dRdy1, dRdtheta1, dRdx2, dRdy2, dRdtheta2) )

    if pcheck :
        #print( actFunc * sym.N(0.5 * rf * (pOther.x - p.x)) / d )
        #print(sym.N(0.5 * rf * (pOther.x - p.x)), '\n')

        print( 'd =', d )
        print( 'Ra =', actFunc, '\n' )
        print( 'dRqdx1 =', sym.N(0.5 * rf * (pOther.x - p.x)) / d )
        print( 'dRpdy1 =', sym.N(0.5 * rf * (pOther.y - p.y)) / d )
        print( 'dRpdtheta1 =', sym.N( 0.5 * rf * ( -(p.y - y1) * (pOther.x - p.x) + (p.x - x1) * (pOther.y - p.y) ) ) / d, '\n' )
        print( 'dRqdx2 =', -sym.N(0.5 * rf * (pOther.x - p.x)) / d )
        print( 'dRpdy2 =', -sym.N(0.5 * rf * (pOther.y - p.y)) / d )
        print( 'dRpdtheta2 =', -sym.N( 0.5 * rf * ( -(pOther.y - y2) * (pOther.x - p.x) + (pOther.x - x2) * (pOther.y - p.y) ) ), '\n' )

    return dRdx1, dRdy1, dRdtheta1, dRdx2, dRdy2, dRdtheta2



def rodToRodElastic(q, t, m1, m2, l1, l2, rf, rd, rc, rt, alpha, beta, gamma) :

    global oldVals
    global p1_old, p2_old, p3_old, p4_old

    print("t =", t, '---------------------------------------------------\n')
    x1, y1, theta1, v0x1, v0y1, omega1, x2, y2, theta2, v0x2, v0y2, omega2 = q

    # endpoints in rod 1
    p1 = Point( x1 - 0.5 * l1 * np.cos(theta1), y1 - 0.5 * l1 * np.sin(theta1) )
    p2 = Point( x1 + 0.5 * l1 * np.cos(theta1), y1 + 0.5 * l1 * np.sin(theta1) )
    seg1  = Segment(p1, p2)    # line segment along rod 1

    # endpoints in rod 2
    p3 = Point( x2 - 0.5 * l2 * np.cos(theta2), y2 - 0.5 * l2 * np.sin(theta2) )
    p4 = Point( x2 + 0.5 * l2 * np.cos(theta2), y2 + 0.5 * l2 * np.sin(theta2) )
    seg2 = Segment(p3, p4)     # line segment along rod 2

    p12, p22 = seg2.projection(p1), seg2.projection(p2)
    p31, p41 = seg1.projection(p3), seg1.projection(p4)


    if oldVals != None :
        # first time through vals isn't initalized
        vals = oldVals
    else :
        # initialize vals since this is the first time through
        # note this only works if there is no initial intersections
        vals = {'12':1.0, '22':1.0, '31':1.0, '41':1.0}


    # first time through p1_old, etc are not initialized
    if p1_old != None :
        # displacement vector of the four cell enpoints
        seg_p1 = Segment(p1_old, p1)
        seg_p2 = Segment(p2_old, p2)
        seg_p3 = Segment(p3_old, p3)
        seg_p4 = Segment(p4_old, p4)

        signFlips = [ seg_p1.intersection(seg2) != [], seg_p2.intersection(seg2) != [], seg_p3.intersection(seg1) != [], seg_p4.intersection(seg1) != [] ]

        # if a point crosses another cell -- flip the sign of the distance
        if signFlips[0] : vals['12'] = -1.0 * oldVals['12']
        if signFlips[1] : vals['22'] = -1.0 * oldVals['22']
        if signFlips[2] : vals['31'] = -1.0 * oldVals['31']
        if signFlips[3] : vals['41'] = -1.0 * oldVals['41']

        print("sign flips?:", signFlips)
        #if True in signFlips : input("Press any key to continue")

    p1_old, p2_old, p3_old, p4_old = p1, p2, p3, p4

    print('Vals:', vals, '\n' )
    oldVals = vals

    print("p1_old test:", p1_old)

    """intersection_list = seg1.intersection(seg2)
    print("intersections", intersection_list)
    if intersection_list != [] :

        pt = intersection_list[0]

        d12 = float( sym.N( pt.distance(p1) + pt.distance(p12) ) )
        d22 = float( sym.N( pt.distance(p2) + pt.distance(p22) ) )
        d31 = float( sym.N( pt.distance(p3) + pt.distance(p31) ) )
        d41 = float( sym.N( pt.distance(p4) + pt.distance(p41) ) )

        m = min( [d12, d22, d31, d41] )
        dict1 = {d12:"12", d22:"22", d31:"31", d41:"41"}
        vals[dict1[m]] = -1.0"""

    # p1 to segment 2
    dR12dx1, dR12dy1, dR12dtheta1, dR12dx2, dR12dy2, dR12dtheta2 = dRdqV3(p1, seg2, x1, y1, theta1, x2, y2, theta2, rt, rc, rf, False, vals["12"])

    # p2 to segment 2
    dR22dx1, dR22dy1, dR22dtheta1, dR22dx2, dR22dy2, dR22dtheta2 = dRdqV3(p2, seg2, x1, y1, theta1, x2, y2, theta2, rt, rc, rf, True, vals["22"])

    # p3 to segment 1
    dR31dx2, dR31dy2, dR31dtheta2, dR31dx1, dR31dy1, dR31dtheta1 = dRdqV3(p3, seg1, x2, y2, theta2, x1, y1, theta1, rt, rc, rf, False, vals["31"])

    # p4 to segment 1
    dR41dx2, dR41dy2, dR41dtheta2, dR41dx1, dR41dy1, dR41dtheta1 = dRdqV3(p4, seg1, x2, y2, theta2, x1, y1, theta1, rt, rc, rf, False, vals["41"])

    """
    # p1 to segment 2
    p12 = seg2.projection(p1)
    d12 = p1.distance(p12)      # perpendicular (or shortest) distance
    unit12x = (p12.x - p1.x) / d12
    unit12y = (p12.y - p1.y) / d12
    v12x = v0x1 + omega1 * (p1.x - x1) - v0x2 - omega2 * (p12.x - x2)
    v12y = v0y1 + omega1 * (p1.y - y1) - v0y2 - omega2 * (p12.y - y2)
    v12perp = v12x * unit12x + v12y * unit12y

    dR12dx1z = Ra(d12, rt, rc) * 0.5 * rf * (p12.x - p1.x) / d12
    dR12dy1z = Ra(d12, rt, rc) * 0.5 * rf * (p12.y - p1.y) / d12
    #dR12dtheta1z = Ra(d12, rt, rc) * 0.5 * rf * ( (p1.x - x1 ) * (p12.x - p1.x) + (p1.y - y1) * (p12.y - p1.y) ) / d12
    dR12dtheta1z = Ra(d12, rt, rc) * 0.5 * rf * ( (p1.y - y1 ) * (p12.x - p1.x) + (p1.x - x1) * (p12.y - p1.y) ) / d12
    dR12dx2z = -dR12dx1z
    dR12dy2z = -dR12dy1z
    #dR12dtheta2z = -Ra(d12, rt, rc) * 0.5 * rf * ( (p12.x - x2 ) * (p12.x - p1.x) + (p12.y - y2) * (p12.y - p1.y) ) / d12
    dR12dtheta2z = -Ra(d12, rt, rc) * 0.5 * rf * ( (p12.y - y2) * (p12.x - p1.x) + (p12.x - x2) * (p12.y - p1.y) ) / d12

    # p2 to segment 2
    p22 = seg2.projection(p2)
    d22 = p2.distance(p22)      # perpendicular (or shortest) distance
    unit22x = (p22.x - p2.x) / d22
    unit22y = (p22.y - p2.y) / d22
    v22x = v0x1 + omega1 * (p2.x - x1) - v0x2 - omega2 * (p22.x - x2)
    #v22y = v0y1 + omega1 * (p2.y - y1) - v0y2 - omega2 * (p22.y - y2)
    v22y = v0y1 + omega1 * (p2.x - x1) - v0y2 - omega2 * (p22.x - x2)
    v22perp = v22x * unit22x + v22y * unit22y

    dR22dx1z = Ra(d22, rt, rc) * 0.5 * rf * (p22.x - p2.x) / d22
    dR22dy1z = Ra(d22, rt, rc) * 0.5 * rf * (p22.y - p2.y) / d22
    #dR22dtheta1z = Ra(d22, rt, rc) * 0.5 * rf * ( (p2.x - x1 ) * (p22.x - p2.x) + (p2.y - y1) * (p22.y - p2.y) ) / d22
    dR22dtheta1z = Ra(d22, rt, rc) * 0.5 * rf * ( (p2.y - y1 ) * (p22.x - p2.x) + (p2.x - x1) * (p22.y - p2.y) ) / d22
    dR22dx2z = -dR22dx1z
    dR22dy2z = -dR22dy1z
    #dR22dtheta2z = -Ra(d22, rt, rc) * 0.5 * rf * ( (p22.x - x2 ) * (p22.x - p2.x) + (p22.y - y2) * (p22.y - p2.y) ) / d22
    dR22dtheta2z = -Ra(d22, rt, rc) * 0.5 * rf * ( (p22.y - y2) * (p22.x - p2.x) + (p22.x - x2) * (p22.y - p2.y) ) / d22


    # p3 to segment 1
    p31 = seg1.projection(p3)
    d31 = p3.distance(p31)      # perpendicular (or shortest) distance
    unit31x = (p31.x - p3.x) / d31
    unit31y = (p31.y - p3.y) / d31
    v31x = v0x1 + omega1 * (p3.x - x1) - v0x2 - omega2 * (p31.x - x2)
    v31y = v0y1 + omega1 * (p3.y - y1) - v0y2 - omega2 * (p31.y - y2)
    v31perp = v31x * unit31x + v31y * unit31y

    dR31dx1z = -Ra(d31, rt, rc) * 0.5 * rf * (p31.x - p3.x) / d31
    dR31dy1z = -Ra(d31, rt, rc) * 0.5 * rf * (p31.y - p3.y) / d31
    #dR31dtheta1z = Ra(d31, rt, rc) * 0.5 * rf * ( (p3.x - x1 ) * (p31.x - p3.x) + (p3.y - y1) * (p31.y - p3.y) ) / d31
    dR31dtheta1z = -Ra(d31, rt, rc) * 0.5 * rf * ( (p31.y - y1) * (p31.x - p3.x) + (p31.x - x1) * (p31.y - p3.y) ) / d31
    dR31dx2z = -dR31dx1z
    dR31dy2z = -dR31dy1z
    #dR31dtheta2z = -Ra(d31, rt, rc) * 0.5 * rf * ( (p31.x - x2 ) * (p31.x - p3.x) + (p31.y - y2) * (p31.y - p3.y) ) / d31
    dR31dtheta2z = Ra(d31, rt, rc) * 0.5 * rf * ( (p3.y - y2) * (p31.x - p3.x) + (p3.x - x2) * (p31.y - p3.y) ) / d31

    # p4 to segment 1
    p41 = seg1.projection(p4)
    d41 = p4.distance(p41)      # perpendicular (or shortest) distance
    unit41x = (p41.x - p4.x) / d41
    unit41y = (p41.y - p4.y) / d41
    v41x = v0x1 + omega1 * (p4.x - x1) - v0x2 - omega2 * (p41.x - x2)
    v41y = v0y1 + omega1 * (p4.y - y1) - v0y2 - omega2 * (p41.y - y2)
    v41perp = v41x * unit41x + v41y * unit41y

    dR41dx1z = -Ra(d41, rt, rc) * 0.5 * rf * (p41.x - p4.x) / d41
    dR41dy1z = -Ra(d41, rt, rc) * 0.5 * rf * (p41.y - p4.y) / d41
    #dR41dtheta1z = Ra(d41, rt, rc) * 0.5 * rf * ( (p4.x - x1 ) * (p41.x - p4.x) + (p4.y - y1) * (p41.y - p4.y) ) / d41
    dR41dtheta1z = -Ra(d41, rt, rc) * 0.5 * rf * ( (p41.y - y1) * (p41.x - p4.x) + (p41.x - x1) * (p41.y - p4.y) ) / d41
    dR41dx2z = -dR41dx1z
    dR41dy2z = -dR41dy1z
    #dR41dtheta2z = -Ra(d41, rt, rc) * 0.5 * rf * ( (p41.x - x2 ) * (p41.x - p4.x) + (p41.y - y2) * (p41.y - p4.y) ) / d41
    dR41dtheta2z = Ra(d41, rt, rc) * 0.5 * rf * ( (p4.y - y2) * (p41.x - p4.x) + (p4.x - x2) * (p41.y - p4.y) ) / d41

    print(dR12dx1==dR12dx1z, dR12dx2==dR12dx2z, dR12dtheta1==dR12dtheta1z, dR12dx2==dR12dx2z, dR12dy2==dR12dy2z, dR12dtheta2==dR12dtheta2z)

    print(dR22dx1==dR22dx1z, dR22dx2==dR22dx2z, dR22dtheta1==dR22dtheta1z, dR22dx2==dR22dx2z, dR22dy2==dR22dy2z, dR22dtheta2==dR22dtheta2z)

    print(sym.simplify(dR31dx1)==sym.simplify(dR31dx1z), sym.simplify(dR31dx2)==sym.simplify(dR31dx2z), dR31dtheta1==dR31dtheta1z, sym.simplify(dR31dx2)==sym.simplify(dR31dx2z), sym.simplify(dR31dy2)==sym.simplify(dR31dy2z), sym.simplify(dR31dtheta2)==sym.simplify(dR31dtheta2z))

    print(sym.simplify(dR41dx1)==sym.simplify(dR41dx1z), sym.simplify(dR41dx2)==sym.simplify(dR41dx2z), dR41dtheta1==dR41dtheta1z, sym.simplify(dR41dx2)==sym.simplify(dR41dx2z), sym.simplify(dR41dy2)==sym.simplify(dR41dy2z), dR41dtheta2==dR41dtheta2z)
    """

    """
    #print(dR31dx1, dR31dx1z)
    #print(dR31dtheta1, dR31dtheta1z)
    #print(dR31dtheta2, dR31dtheta2z)

    print(dR41dx1)
    print(dR41dx1z, '\n')
    print(dR41dy1)
    print(dR41dy1z, '\n')
    #print(dR41dtheta1)
    #print(dR41dtheta1z, '\n')
    print(dR41dx2)
    print(dR41dx2z, '\n')
    print(dR41dy2)
    print(dR41dy2z, '\n')
    #print(dR41dtheta2)
    #print(dR41dtheta2z)"""


    """print(sym.N(dR31dx1), sym.N(dR31dx1z))
    print(sym.N(dR31dtheta1), sym.N(dR31dtheta1z))
    print(sym.N(dR31dtheta2), sym.N(dR31dtheta2z))

    print(sym.N(dR41dx1), sym.N(dR41dx1z))
    print(sym.N(dR41dtheta1), sym.N(dR41dtheta1z))
    print(sym.N(dR41dtheta2), sym.N(dR41dtheta2z))"""



    v0x1dot = -(alpha/m1 * v0x1) + dR12dx1 + dR22dx1 + dR31dx1 + dR41dx1
    v0y1dot = -(beta/m1 * v0y1) + dR12dy1 + dR22dy1 + dR31dy1 + dR41dy1
    omega1dot = -(12*gamma/(m1*l1**2) * v0x1) + dR12dtheta1 + dR22dtheta1 + dR31dtheta1 + dR41dtheta1

    v0x2dot = -(alpha/m2 * v0x2) + dR12dx2 + dR22dx2 + dR31dx2 + dR41dx2
    v0y2dot = -(beta/m2 * v0y2) + dR12dy2 + dR22dy2 + dR31dy2 + dR41dy2
    omega2dot = -(12*gamma/(m2*l2**2) * v0x2) + dR12dtheta2 + dR22dtheta2 + dR31dtheta2 + dR41dtheta2

    #print(x1, x2, theta1, v0x1, v0y1, omega1, x2, y2, theta2, v0x2, v0y2, omega2)
    dqdt = [v0x1, v0y1, omega1, v0x1dot, v0y1dot, omega1dot, v0x2, v0y2, omega2, v0x2dot, v0y2dot, omega2dot]
    return dqdt



def main() :

    s0 = 0.1 #1.0
    sc = 0.06 #0.6
    ri = 0.99
    rd = 4.0
    rf = 40.0
    rt = 2 * np.arctanh(ri) / (s0 - sc)
    rc = (s0 + sc) / 2

    l1 = 5
    l2 = 5
    m1 = 0.1
    m2 = 0.1
    grav = 9.8

    alpha = 0.0
    beta = 0.0
    gamma = 0.0

    t = np.linspace(0, 1.1, 101)
    print(t)

    # ROD-TO-ROD COLLISION TOY MODEL
    # q format : x1, y1, theta1, v0x1, v0y1, omega1, x2, y2, theta2, v0x2, v0y2, omega2
    q0 = [0, 0, np.pi/6, 5, 0, 0, 7, 2, np.pi/3, 0, 0, 0]
    print(odeint)
    sol = sp.integrate.odeint( rodToRodElastic, q0, t, args=(m1, m2, l1, l2, rf, rd, rc, rt, alpha, beta, gamma))

    import matplotlib.pyplot as plt
    from matplotlib.animation import FuncAnimation

    plt.plot(t, sol[:, 0], 'b', label='$x1(t)$')
    plt.plot(t, sol[:, 1], 'b', label='$y1(t)$')
    plt.plot(t, sol[:, 2], 'g', label='$theta1(t)$')
    plt.plot(t, sol[:, 3], 'g', label='$v_x1(t)$')
    plt.plot(t, sol[:, 4], 'r', label='$v_y1(t)$')
    plt.plot(t, sol[:, 5], 'y', label='$omega(t)$')

    plt.legend(loc='best')
    plt.xlabel('t')
    plt.grid()

    plt.savefig("plots.dir/rodToRodCollisionDynamics.eps", format='eps')
    #plt.show()


    fig, ax = plt.subplots(figsize=(4,6))
    ax.set(xlim=(-5,5), ylim=(-2, 15))
    ax.set_aspect('equal')

    x1, y1, theta1 = sol[:,0], sol[:,1], sol[:,2]
    x2, y2, theta2 = sol[:,6], sol[:,7], sol[:,8]

    xmin = min(sol[:,0] + sol[:,6])
    xmax = max(sol[:,0] + sol[:,6])

    ymin = min(sol[:,1] + sol[:,7])
    ymax = max(sol[:,1] + sol[:,7])


    scat = ax.plot([x1[0] - 0.5 * l1 * np.cos(theta1[0]), x1[0] + 0.5 * l1 * np.cos(theta1[0])], [y1[0] - 0.5 * l1 * np.sin(theta1[0]), y1[0] + 0.5 * l1 * np.sin(theta1[0])], '-o')

    ax.plot([x2[0] - 0.5 * l2 * np.cos(theta2[0]), x2[0] + 0.5 * l2 * np.cos(theta2[0])], [y2[0] - 0.5 * l2 * np.sin(theta2[0]), y2[0] + 0.5 * l2 * np.sin(theta2[0])], '-o')

    #ax.plot([-100, 100], [0,0] , '-ro')

    def animate(i):
        #line.set_ydata(F[i, :])
        p1x = x1[i] - 0.5 * l1 * np.cos(theta1[i])
        p1y = y1[i] - 0.5 * l1 * np.sin(theta1[i])

        p2x = x1[i] + 0.5 * l1 * np.cos(theta1[i])
        p2y = y1[i] + 0.5 * l1 * np.sin(theta1[i])


        p3x = x2[i] - 0.5 * l2 * np.cos(theta2[i])
        p3y = y2[i] - 0.5 * l2 * np.sin(theta2[i])

        p4x = x2[i] + 0.5 * l2 * np.cos(theta2[i])
        p4y = y2[i] + 0.5 * l2 * np.sin(theta2[i])


        ax.clear()
        ax.plot(0, 'r')
        ax.set(xlim=(-xmin-5,xmax+5), ylim=(-ymin-5, ymax+5))
        ax.set_aspect('equal')
        #ax.plot([-100, 100], [0,0] , '-ro')
        ax.plot([p1x, p2x], [p1y, p2y], '-o')
        ax.plot([p3x, p4x], [p3y, p4y], '-o')

    anim = FuncAnimation(fig, animate, interval=100, frames=len(t)-1)
    anim.save("plots.dir/rodToRodCollisionAnimation.mp4")

    plt.grid()
    plt.draw()
    #plt.show()


main()
