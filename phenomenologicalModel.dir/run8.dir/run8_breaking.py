#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 09/10/2018

from timeit import default_timer as timer
import numpy as np
import scipy as sp
from scipy import optimize
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle

from main import *


 # Functions ######################################################################

def rateFunc(Favg, rateMax, fStall, fTrans) :
    return rateMax / ( 1 + sp.exp( 7 * (2*Favg - fStall - fTrans) / (2 * (fStall - fTrans))) )

def forceFunc(i, mu, l, rateVals, q) :
    return 0.5 * mu * sum(l) * sum(rateVals) * q[i] * (1 - q[i])

def forceSolver(vars, *args) :

    l, rateMax, mu, fTrans, fStall, q, scale = args
    L = sum(l)

    mu *= 1e9 * scale
    fTrans *= 1e9
    fStall *= 1e9

    rateVals = vars[:len(vars)//2]
    forces = vars[len(vars)//2:]

    eqns = []

    for i in range( len(vars)//2 ) :

        if i == len(l)-1 :
            eqns.append( rateVals[i] * ( 1 + sp.exp( 7 * (forces[i]-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax )
        else :
            eqns.append( rateVals[i] * ( 1 + sp.exp( 7 * (forces[i+1]+forces[i]-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax )

    for i in range( len(vars)//2 ) :
        eqns.append( forces[i] - 0.5 * mu * sum(l) * sum(rateVals) * q[i] * (1 - q[i]) )

    eqns = tuple(eqns)

    #print("Equations?", eqns)

    return eqns

#def forceDependentGrowth(mu, L, LPrime, q) :
def forceDependentGrowth(l, rateWithout, mu, vars0) :
    """ determines the constraint force for the point
        located at l = q L """

    plotting = False
    scale = 1

    rateMax = 0.2
    mu = 1e-9
    fTrans = 5e-9
    fStall = 1e-7

    q = [0.5]
    for i in range( len(l) ) :
        q.append( 0.5 * ( sum(l[:i+1]) / sum(l) + 1 ) )

    params = l, rateMax, mu, fTrans, fStall, q, scale

    #vars0 = ( [0.1] * int(len(l)) ) + ( [10**-8] * int(len(l)) )
    vars0 = tuple(vars0)

    sols = sp.optimize.fsolve( forceSolver, vars0, args=params )
    #sols = sp.optimize.newton_krylov( lambda vars:forceSolver(vars, *params), vars0 )

    rateSols = sols[: len(sols)//2]
    forceSols = sols[len(sols)//2 :]

    avgForces = []
    for i in range( len(forceSols) ) :
        try : avgForces.append( (forceSols[i] + forceSols[i+1]) / 2 )
        except : avgForces.append( forceSols[i] / 2 )

    print("rates", rateSols)
    print("forces", forceSols)
    print("lengths", l)
    #print(len(rateSols), len(avgForces))
    #print("rateEquations", [rateSols[i]-rateFunc(avgForces[i], rateMax, fStall*1e9, fTrans*1e9) for i in range(len(rateSols))] )
    #print("forceEquations", [forceSols[i]-forceFunc(i, mu*1e9, l, rateSols, q) for i in range(len(forceSols))] )
    print("equationVals", forceSolver(sols, *params))

    if plotting :
        plt.scatter(avgForces, rateSols)
        X = np.linspace(0, 150, 250, endpoint=True)
        V = rateFunc(X/1e9, rateMax, fStall, fTrans)
        plt.plot(X,V)
        plt.show()

    #return rateSols
    return sols


def simulation(sets, totalTime, mcSteps, option, random, seedNo) :

    start = timer()

    dt, totalTime, frac, mu, avgDivLen, rateWithout, rateWith, option, random = initialization(totalTime, option, random)

    forceThreshold = 1e-7       # force value where breaking occurs

    np.random.seed(seedNo)

    filaments = pickle.load(open("filaments100.pkl", "rb"))

    margins, probs = [], []

    #sets = 100
    for N in range(sets) :

        l1_init = np.random.choice(filaments)
        l2_init = np.random.choice(filaments)
        #l1_init = [6.3, 5.4, 7.1, 4.9, 6.4]
        #l2_init = [6.3, 5.4, 7.1, 4.9, 6.4]

        tVals, L1Vals, L2Vals, LDiff = [0], [sum(l1_init)], [sum(l2_init)], [sum(l1_init)-sum(l2_init)]

        count = 0   # number of monte carlo steps with final L1 > L2

        for n in range(mcSteps) :

            ls = [l1_init.copy(), l2_init.copy()]
            margin = sum(ls[0]) - sum(ls[1])

            t = 0
            while t < totalTime :

                count = 1
                for l in ls :

                    if t == 0 :
                        vars0 = [ ( [0.1] * int(len(l)) ) + ( [10**-8] * int(len(l)) ) ] * 2

                    print("\n Filament", count, "-----------------------------------")
                    #sols = forceDependentGrowth(l, rateWithout, mu, vars0[count-1])
                    #rates = sols[: len(sols)//2]
                    #forceSols = sols[len(sols)//2 :]
                    #vars0[count-1] = sols

                    #total_L = sum(l[i])
                    rates = []
                    for i in range( len(l) ) :

                        rate = 0.2

                        #growthRate = getGrowthRate(rates[i], frac, random)
                        growthRate = getGrowthRate(rate, frac, random)
                        rates.append(growthRate)
                        l[i] += growthRate * dt

                        if random :
                            threshold = np.random.normal(avgDivLen, 0.1  * avgDivLen)
                            if l[i] >= threshold :
                                print("DIVISION:", l[i], threshold)
                                print("old vars", vars0[count-1])
                                #input("PRESS ENTER TO CONTINUE")
                                newLength = l[i] / 2       # determine the length of the daughter cells
                                l[i] = newLength
                                print("l before", l)
                                l.insert(i+1, newLength)   # inset new cell
                                print("l after", l)
                                vars0[count-1] = np.insert(vars0[count-1], i, vars0[count-1][i])
                                vars0[count-1] = np.insert(vars0[count-1], len(vars0[count-1])//2 + i + 1, vars0[count-1][len(vars0[count-1])//2 + i + 1])
                                print("new vars", vars0[count-1])
                        else:
                            if l[i] >= avgDivLen :
                                newLength = l[i] / 2       # determine the length of the daughter cells
                                l[i] = newLength
                                l.insert(i+1, newLength)   # inset new cell
                                vars0[count-1] = np.insert(vars0[count-1], i, vars0[i])
                                vars0[count-1] = np.insert(vars0, len(vars0[count-1])//2 + i + 1, vars0[len(vars0[count-1])//2 + i + 1])


                    L_tot = sum(l)
                    L_tot_Prime = sum(rates)
                    forces = []
                    for i in range( len(l) + 1 ) :
                        q = 0.5 * ( (sum(l[:i]) / L_tot) + 1 )
                        force_val = getForce(mu, L_tot, L_tot_Prime, q)
                        forces.append(force_val)
                    print("Forces:", forces)

                    # add breaking dynamics here
                    for i in range(len(forces)) :
                        if forces[i] > forceThreshold :
                            print("BREAKING!")
                            print("before:", l)
                            l = l[:i]
                            print("after:", l)
                        break

                    count += 1

                print("\n\nTime", t, "#######################################")
                t += dt

                tVals.append(t)

                L1Vals.append(sum(ls[0]))
                L2Vals.append(sum(ls[1]))
                LDiff.append(sum(ls[0])-sum(ls[1]))

            if sum(ls[0]) > sum(ls[1]) : count += 1
            print("Set", N+1,", MC step", n+1, "complete")

        margins.append(margin)
        probs.append(count/mcSteps)
        print("Set", N+1, "complete")

    print("\nDone")
    print("margins", margins)
    print("probabilities", probs)

    end = timer()
    print("RUNTIME:", end-start)

    #pickle.dump( [margins, probs], open("run5_outcomes_forces_" + str(sets) + ".pkl", "wb") )

    """plt.figure(1)
    plt.plot(tVals, LDiff, markersize=0.5)
    plt.figure(2)
    plt.plot(tVals, L1Vals, tVals, L2Vals, 'r', markersize=0.5)
    plt.show()"""

#simulation(set, totalTime, mcSteps, option, random, seedNo)
simulation(1, 5, 1, 0, True, 7)
#imulation(25, 30, 30, 0, True, 7)
#simulation(15, 40, 30, 0, True, 7)
#simulation(50, 60, 100, 0, True, 7)
#simulation(100, 60, 100, 0, True, 7)
