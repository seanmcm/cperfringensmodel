#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import glob
import pickle
from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys
from matplotlib.ticker import ScalarFormatter

plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'figure.autolayout': True})

#outfile = os.path.basename(sys.argv[1])
#filename = sys.argv[1]

variedParam = sys.argv[1].lower()
while variedParam not in ['loc', 'angle', 'l0', 'r', 'kb', 'mu', 'a', 'b', 'psi'] :
    print("Not a valid varied parameter string.")
    print("Enter 'exit' to quit.")
    print("Enter one of the following: loc, angle, l0, 'r', kb, mu, a, b, psi")
    variedParam = input().lower()
    if variedParam == 'exit' : exit()

dirs = glob.glob(f"data.dir/varied_psi-{variedParam}.dir/{variedParam}*.dir")
print(dirs)

paramVals, slopes, intercepts = [], [], []

for dir in dirs :

    psiVals, l0Vals, rVals, kbVals, muVals, aVals, bVals = [], [], [], [], [], [], []
    critStressVals, critLengthVals = [], []

    for filename in glob.glob(dir+"/*.pkl") :

        # import data
        data = pickle.load(open(filename, "rb"))

        # key parameters
        psi, l0, r, kb, mu, a, b, numRods, angle, pert_coefficient = data[0]

        #kb, mu, a, b, r, numRods, angle, pert_coefficient, l0 = data[0]
        #psi = kb / (mu * l0**3 * r)

        # other useful values
        growthRate = l0 * r

        # append the parameter values to their respective arrays
        psiVals.append(psi)
        l0Vals.append(l0)
        rVals.append(r)
        kbVals.append(kb)
        muVals.append(mu)
        aVals.append(a)
        bVals.append(b)

        # organzied the other data components -- calculated physical quantities
        lengths = np.array(data[1])
        xdotVals = np.array(data[2])
        ydotVals = np.array(data[3])
        thetadotVals = np.array(data[4])
        lambdaXVals = np.array(data[5])
        lambdaYVals = np.array(data[6])

        # convert the pertubation coefficient (aka q) to the proper index
        index = [int(pert_coefficient * length / l0 - 1) for length in lengths]

        # compute deltaThetaDot for the perturbed linkages
        deltaThetadots = [ thetadotVals[i][index[i]+1]-thetadotVals[i][index[i]] for i in range(len(thetadotVals)) ]

        stresses_dim = [300 * np.sqrt ( lambdaXVals[i][index[i]]**2 + lambdaYVals[i][index[i]]**2 ) for i in range(len(lambdaXVals)) ]

        # compute the Nondimensionalized stress in the perturbed linkages
        stresses = [ np.sqrt ( lambdaXVals[i][index[i]]**2 + lambdaYVals[i][index[i]]**2 ) / (mu * l0**2 * r) for i in range(len(lambdaXVals)) ]

        # determine the critical stresses via linear regression of the stresses and the deltaThetaDots
        m, yint = np.polyfit(stresses, deltaThetadots, 1)
        critStress = -yint/m
        critStressVals.append(critStress)

        m, yint = np.polyfit(lengths, deltaThetadots, 1)
        critLength = -yint/m
        critLengthVals.append(critLength)


        #deltaThetadotVecs.append( np.array(deltaThetadots) )
        #stressVecs.append( np.array(stresses) )

        #plt.clf()

        title_dict = {
            'loc' : r"$\Delta\dot{\theta}$ Vs. Stress (Various Locations $q$)",
            'angle' : r"$\Delta\dot{\theta}$ Vs. Stress (Various Angles $\Delta\theta$)",
            'l0' : r"$\Delta\dot{\theta}$ Vs. Stress (Various $l_0$)",
            'r' : r"$\Delta\dot{\theta}$ Vs. Stress (Various $r$)",
            'kb' : r"$\Delta\dot{\theta}$ Vs. Stress (Various $k_b$)",
            'mu': r"$\Delta\dot{\theta}$ Vs. Stress (Various $\mu$)",
            'a' : r"$\Delta\dot{\theta}$ Vs. Stress (Various $a$)",
            'b' : r"$\Delta\dot{\theta}$ Vs. Stress (Various $b$)",
            'psi' : r"$\Delta\dot{\theta}$ Vs. Stress (Various $\Psi$)"
        }

        label_dict = {
            'loc' : fr'$q$ = {pert_coefficient}',
            'angle' : fr'$\Delta\theta$ = {angle}',
            'l0' : fr'$l_0$ = {l0}',
            'r' : fr'$r$ = {r}',
            'kb' : fr'$k_b$ = {kb}',
            'mu': fr'$\mu$ = {mu}',
            'a' : fr'$a$ = {a}',
            'b' : fr'$b$ = {b}',
            'psi' : fr'$\Psi$ = {psi}'
        }

        plt.title(title_dict[variedParam])
        #plt.xlabel(r"Stress (pN)") #(kg $\mu$m/min$^2$)")
        plt.xlabel(r"Non-dimensionalized Stress") #(kg $\mu$m/min$^2$)")
        plt.ylabel(r"Perturbation Angle Rate of Change")
        #plt.plot(stresses_dim, deltaThetadots, '-o', label=label_dict[variedParam])
        plt.plot(stresses, deltaThetadots, '-o', label=label_dict[variedParam])
        #plt.hlines(0, -1e100, 1e100 , linestyles='dashed')
        plt.legend(loc="upper left", prop={'size': 12})


        """plt.title(r"$\Delta\dot{\theta}$ Vs. Stress (Various Locations $q$)")
        plt.xlabel(r"Stress (pN)") #(kg $\mu$m/min$^2$)")
        plt.ylabel(r"Perturbation Angle Rate of Change")
        plt.plot(stresses, deltaThetadots, '-o', label=fr'q = {pert_coefficient}')
        #plt.hlines(0, -1e100, 1e100 , linestyles='dashed')
        plt.legend(loc="upper left", prop={'size': 12})"""

        """
        plt.title(r"$\Delta\dot{\theta}$ Vs. Stress (Various Angles $\Delta\theta$)")
        plt.xlabel(r"Stress (pN)") #(kg $\mu$m/min$^2$)")
        plt.ylabel(r"Perturbation Angle Rate of Change")
        plt.plot(stresses, deltaThetadots, '-o', label=fr'$\Delta\theta$ = {angle}$^\circ$')
        #plt.hlines(0, -1e100, 1e100 , linestyles='dashed')
        plt.legend(loc="upper left", prop={'size': 11})
        """

        """
        plt.title(r"$\Delta\dot{\theta}$ Vs. Stress (Various $\Psi$)")
        plt.xlabel(r"Stress (pN)") #(kg $\mu$m/min$^2$)")
        plt.ylabel(r"Perturbation Angle Rate of Change")
        plt.plot(stresses, deltaThetadots, '-o', label=fr'$\Delta\theta$ = {angle}$^\circ$')
        #plt.hlines(0, -1e100, 1e100 , linestyles='dashed')
        plt.legend(loc="upper left", prop={'size': 11})
        """


        """lengthVecs.append(np.array(data[1]))
        xdotVecs.append(np.array(data[2]))
        ydotVecs.append(np.array(data[3]))
        thetadotVecs.append(np.array(data[4]))
        lambdaXVecs.append(np.array(data[5]))
        lambdaYVecs.append(np.array(data[6]))"""




        #plt.show()
        #plt.clf()

    dir_dict = {
        'loc' : 'varied_loc.dir',
        'angle' : 'varied_angle.dir',
        'l0' : 'varied_l0.dir',
        'r' : 'varied_r.dir',
        'kb' : 'varied_kb.dir',
        'mu': 'varied_mu.dir',
        'a' : 'varied_a.dir',
        'b' : 'varied_b.dir'
    }

    #plt.savefig(f"plots.dir/{dir_dict[variedParam]}/stressVsDeltaThetadot.pdf")
    #exit()
    psiVals.sort()
    critStressVals.sort()
    critLengthVals.sort()

    slope, intercept = np.polyfit(psiVals, critStressVals, 1)
    fCrit = lambda psi : slope * psi + intercept
    lCrit = lambda psi : l0 * np.sqrt( 8 * (slope * psi + intercept) )

    slopes.append(slope)
    intercepts.append(intercept)
    exec(f"paramVals.append({variedParam})")


    m, b = np.polyfit(psiVals, critStressVals, 1)
    print(f'C = {m} * Psi + {b}')

    #print(f'N when Psi = 0: {np.sqrt(8*b)}')
    #print(f'N when Psi = 0: {np.sqrt(8*b / (mu * l0 * growthRate))}')

    #m, b = np.polyfit(psiVals, critLengthVals, 1)
    #print(f'L = {m} * Psi + {b}')

plt.clf()
plt.title(r"$F^*_{crit}$"+ f" Function Dependence on {variedParam}")
plt.xlabel(rf"{variedParam}") #(kg $\mu$m/min$^2$)")
plt.ylabel(r"Slope $m$")
plt.plot(paramVals, slopes, 'o')
plt.savefig(f"plots.dir/varied_psi-{variedParam}.dir/slope-vs-{variedParam}.pdf")

plt.clf()
plt.title(r"$F^*_{crit}$"+ f" Function Dependence on {variedParam}")
plt.xlabel(rf"{variedParam}") #(kg $\mu$m/min$^2$)")
plt.ylabel(r"Intercept $z$")
plt.plot(paramVals, intercepts, 'o')
plt.savefig(f"plots.dir/varied_psi-{variedParam}.dir/intercept-vs-{variedParam}.pdf")



plt.clf()
fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_title(r"$F_{crit}^*$"+ f" Function Dependence on {variedParam}")
ax1.set_xlabel(rf"{variedParam}") #(kg $\mu$m/min$^2$)")
ax1.set_ylabel(r"Slope $m$", color=color)
ax1.plot(paramVals, slopes, 'o', ms=12, color=color)
ax1.tick_params(axis='y', labelcolor=color)

y_formatter = ScalarFormatter(useOffset=False)
ax1.yaxis.set_major_formatter(y_formatter)


ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel(r"Intercept $z$", color=color)
ax2.plot(paramVals, intercepts, '^', ms=12, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped


#plt.ticklabel_format(axis='y',style='sci', scilimits=(-4,-4))
plt.savefig(f"plots.dir/varied_psi-{variedParam}.dir/m-z-vs-{variedParam}.pdf")
plt.clf()

"""

#param_key = sys.argv[-1]    # string representing the parameter that is varied
plot_titles = {
    "kb" : "Varied Angular Spring Constant",
    "mu" : "Varied Parallel Drag Per Unit Length",
    "growthRate" : "Varied Growth Rate",
    "angle" : "Varied Pertubation Angle",
    "q" : "Varied Location"
}

#plt.title( plot_titles[param_key] )
plt.title("")

plt.axhline(y=0, color='black', linestyle='-')

for i in range(1, len(sys.argv)-1) :

    outfile = os.path.basename(sys.argv[i])
    filename = sys.argv[i]

    data = pickle.load(open(filename, "rb"))

    kb, mu, growthRate, numRods, angle, pert_coefficient = data[0]
    data = np.array(data[1:])

    paramstrs = {
        "kb" : r"$k_b$ = " + str(kb),
        "mu" : r"$\mu$ = " + str(mu),
        "growthRate" : r"$G_r$ = " + str(growthRate),
        "angle" : r"$\theta$ = " + str(angle),
        "q" : r"$q$ = " + str(pert_coefficient)
    }
    paramstr = paramstrs[param_key]

    l0 = 5
    lengths, thetadotDiffs = data[:,0], data[:,1]
    stress = 0.5 * mu * lengths**2 * (growthRate/l0) * pert_coefficient * (1-pert_coefficient)

    lengths, stressDiffs = data[:,0], data[:,1]

    plt.xlabel(r"Length ($\mu$m)")
    plt.ylabel("Stress Difference")
    plt.plot(stress, thetadotDiffs, '-o', label=paramstr)

plt.legend(loc="upper right")
plt.savefig('plots.dir/' + outfile[:-4] + '.pdf', format='pdf')

"""
