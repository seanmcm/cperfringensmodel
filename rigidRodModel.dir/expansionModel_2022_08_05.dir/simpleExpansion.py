import numpy as np
import scipy as sp
from numpy import random
import matplotlib
import matplotlib.pyplot as plt
import csv

class Chain :

    def __init__(self, t, Lcrit, x0, y0, theta, L0=None) :
        """ create a new chain """

        self.t0 = t
        self.Lcrit = Lcrit
        self.x0 = x0
        self.y0 = y0
        self.theta = theta

        """if L0 == None :
            self.L0 = Lcrit / 2
            self.break_time = np.log(2) / 0.02

        else :
            self.L0 = L0
            self.break_time = np.log(Lcrit/L0) / 0.02"""

        self.L0 = Lcrit / 2
        self.break_time = np.log(2) / 0.02

        #self.tips = [x0 - (self.L0/2) * np.cos(theta), x0 + (self.L0/2) * np.cos(theta)]

    def getTips(self, t) :

        x0, theta = self.x0, self.theta
        L = self.getLength(t)

        return ( x0 - ((L/2) * np.cos(theta)), x0 + ((L/2) * np.cos(theta)) )

    def getLength(self, t) :
        """ get the length of the chain at time t """
        r = 0.02
        L0 = self.L0

        return L0 * np.exp( r * (t-self.t0) )

    def chain_break(self) :
        """ implement chain chain breakage at the center """

        x0, y0, theta = self.x0, self.y0, self.theta
        Lcrit = self.Lcrit

        x1 = x0 - (Lcrit/4) * np.cos(theta)
        y1 = y0 - (Lcrit/4) * np.sin(theta)

        x2 = x0 + (Lcrit/4) * np.cos(theta)
        y2 = y0 + (Lcrit/4) * np.sin(theta)

        # noiseless case
        #deltaTheta1 = 0
        #deltaTheta2 = 0

        # offset from the previous chain
        deltaTheta1 = np.random.uniform(theta, 20*np.pi/180)
        deltaTheta2 = np.random.uniform(theta, 20*np.pi/180)

        # offset from the horizontal
        #deltaTheta1 = np.random.uniform(0, 2*np.pi/180)
        #deltaTheta2 = np.random.uniform(0, 2*np.pi/180)

        theta1, theta2 = theta + deltaTheta1, theta + deltaTheta2

        return Chain(t, Lcrit, x1, y1, theta1), Chain(t, Lcrit, x2, y2, theta2)


np.random.seed(7)

# parameters
l0 = 5          # cell length
r = 0.02        # cell growth rate
cycles = 6      # number of cell cycles
mc_steps = 1

# simulation time range
t_span = np.linspace(0, cycles * np.log(2) / r, endpoint=True, num=cycles*1000)

# parameter values
psiVals = [1, 2, 5, 10, 20, 50, 100]
aVals = [1, 2, 5, 10, 20, 50, 100]

# initialize data matrices
expRateTheory = np.zeros( (len(psiVals), len(aVals)) )
expRateMeans = np.zeros( (len(psiVals), len(aVals)) )
expRateStdevs = np.zeros( (len(psiVals), len(aVals)) )

# interate over all possible parameter combinations
for i in range( len(psiVals) ) :
    for j in range( len(aVals) ) :

        psi, a = psiVals[i], aVals[j]

        # import the predicted critical kinking length
        kp = []
        with open("fitParams.csv") as f :
            csv_reader = csv.reader(f, delimiter=',')
            for row in csv_reader : kp.append(row)
        kp = kp[1:]     # remove the header row
        temp = list(filter(lambda x : ( round(float(x[0])) == round(a) ), kp))
        m, z = float(temp[0][2]), float(temp[0][1])

        # compute the breaking length based on psi and a
        Lcrit = l0 * np.sqrt( 8 * (m * psi + z) )
        L0 = Lcrit/2

        # compute the theoretical expansion rate for a perfectly straight system
        rate_theory = r * Lcrit / ( 2 * np.log(2) )
        theory = lambda t : rate_theory * t + L0

        expRates = []
        for m in range(mc_steps) :

            expDist = np.zeros( len(t_span) )

            chains = [ Chain(t=t_span[0], Lcrit=Lcrit, x0=0, y0=0, theta=0) ]
            expDist[0] = chains[0].L0

            tbreak = np.log(2) / r
            next_break = t_span[0] + tbreak

            for n in range( len(t_span) ) :

                t = t_span[n]

                left_tips, right_tips = [], []
                for chain in chains:
                    left, right = chain.getTips(t)
                    left_tips.append(left)
                    right_tips.append(right)

                D = max(right_tips) - min(left_tips)
                expDist[n] = D

                if t > next_break :
                    #print(f"Break at t = {t}")

                    newChains = []
                    for chain in chains :
                        chain1, chain2 = chain.chain_break()
                        newChains.append(chain1)
                        newChains.append(chain2)
                    chains = newChains

                    next_break += tbreak

            exp_diff = expDist[-1] - expDist[0]

            # these pairs of numbers should be the same in the noiseless case
            #print(exp_diff / t_span[-1], rate_theory)
            #print(expDist[-1], theory(t_span[-1]))

            #plt.plot(t_span, expDist)
            #plt.plot(t_span, theory(t_span))
            #plt.show()

            expRates.append( exp_diff / t_span[-1] )

        expRates = np.array( expRates )             # convert to numpy array
        expRateMeans[i,j] = np.mean( expRates )     # compute mean; append to matrix
        expRateStdevs[i,j] = np.std( expRates )     # compute the standard derviations
        expRateTheory[i,j] = rate_theory            # save the theoretical straight expansion rate

        print(f"Done Psi = {psi}, a = {a}")

total_pts = len(psiVals) * len(aVals)
x = np.reshape(expRateTheory, total_pts)
y = np.reshape(expRateMeans, total_pts)
yerr = np.reshape(expRateStdevs, total_pts)

#print(x,y)

plt.title(r"$\Delta\theta$ = 20, offset from previous chain")
plt.xlabel("Straight case expansion rate")
plt.ylabel("Simulation expansion rate")
plt.errorbar(x=x, y=y, yerr=yerr, fmt='o')
plt.plot(x,x, '-')
plt.savefig("out.dir/plot.pdf")
