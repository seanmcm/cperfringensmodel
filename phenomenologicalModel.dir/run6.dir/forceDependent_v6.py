#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 10/16/2018

from timeit import default_timer as timer
import numpy as np
import scipy as sp
from scipy import optimize
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle

from main import *


def rateFunc(Favg, rateMax, fStall, fTrans) :
    return rateMax / ( 1 + sp.exp( 7 * (2*Favg - fStall - fTrans) / (2 * (fStall - fTrans))) )

def forceFunc(i, mu, l, rateVals, q) :
    return 0.5 * mu * sum(l) * sum(rateVals) * q[i] * (1 - q[i])

def forceSolver(vars, *args) :

    l, rateMax, mu, fTrans, fStall, q, scale = args
    L = sum(l)

    mu *= 1e9 * scale
    fTrans *= 1e9
    fStall *= 1e9

    rateVals = vars[:len(vars)//2]
    forces = vars[len(vars)//2:]

    eqns = []

    for i in range( len(vars)//2 ) :

        if i == len(l)-1 :
            eqns.append( rateVals[i] * ( 1 + sp.exp( 7 * (forces[i]-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax )
        else :
            eqns.append( rateVals[i] * ( 1 + sp.exp( 7 * (forces[i+1]+forces[i]-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax )

    for i in range( len(vars)//2 ) :
        eqns.append( forces[i] - 0.5 * mu * sum(l) * sum(rateVals) * q[i] * (1 - q[i]) )

    eqns = tuple(eqns)

    #print("Equations?", eqns)

    return eqns

#def forceDependentGrowth(mu, L, LPrime, q) :
def forceDependentGrowth(l, rateWithout, mu, vars0) :
    """ determines the constraint force for the point
        located at l = q L """

    plotting = False
    scale = 1

    rateMax = 0.2
    mu = 1e-9
    fTrans = 5e-9
    fStall = 1e-7

    q = [0.5]
    for i in range( len(l) ) :
        q.append( 0.5 * ( sum(l[:i+1]) / sum(l) + 1 ) )

    params = l, rateMax, mu, fTrans, fStall, q, scale

    vars0 = tuple(vars0)

    #sols = sp.optimize.fsolve( forceSolver, vars0, args=params )
    sols = sp.optimize.fsolve( lambda vars:forceSolver(vars, *params) , vars0 )

    rateSols = sols[: len(sols)//2]
    forceSols = sols[len(sols)//2 :]

    avgForces = []
    for i in range( len(forceSols) ) :
        try : avgForces.append( (forceSols[i] + forceSols[i+1]) / 2 )
        except : avgForces.append( forceSols[i] / 2 )

    """print("rates", rateSols)
    print("forces", forceSols)
    print("lengths", l)
    print("functionVals", forceSolver(sols, *params))"""

    if plotting :
        plt.scatter(avgForces, rateSols)
        X = np.linspace(0, 150, 250, endpoint=True)
        V = rateFunc(X/1e9, rateMax, fStall, fTrans)
        plt.plot(X,V)
        plt.show()

    #return rateSols
    return sols


def simulation(sets, totalTime, mcSteps, option, random, seedNo) :

    start = timer()

    dt, totalTime, frac, mu, avgDivLen, rateWithout, rateWith, option, random = initialization(totalTime, option, random)

    np.random.seed(seedNo)

    filaments = pickle.load(open("filaments100.pkl", "rb"))

    margins, probs = [], []

    for N in range(sets) :

        l1_init = np.random.choice(filaments)
        l2_init = np.random.choice(filaments)

        print("l1_init", l1_init)
        print("l2_init", l2_init)

        tVals, L1Vals, L2Vals, LDiff = [0], [sum(l1_init)], [sum(l2_init)], [sum(l1_init)-sum(l2_init)]

        count = 0   # number of monte carlo steps with final L1 > L2

        for n in range(mcSteps) :

            ls = [l1_init.copy(), l2_init.copy()]
            margin = sum(ls[0]) - sum(ls[1])

            vars0 = [( [0.1] * int(len(x)) ) + ( [10**-8] * int(len(x)) ) for x in ls]

            t = 0
            while t < totalTime :

                fil = 1
                for l in ls :

                    j = fil-1

                    """if t == 0 :
                        print(len(l), l)
                        vars0 = [ ( [0.1] * int(len(l)) ) + ( [10**-8] * int(len(l)) ) ] * 2
                        print(vars0)"""

                    #print("\n Filament", fil, "-----------------------------------")
                    sols = forceDependentGrowth(l, rateWithout, mu, vars0[j])
                    rates = sols[: len(sols)//2]
                    #forceSols = sols[len(sols)//2 :]
                    vars0[j] = sols

                    for i in range( len(l) ) :

                        growthRate = getGrowthRate(rates[i], frac, random)
                        l[i] += growthRate * dt

                        if random :
                            threshold = np.random.normal(avgDivLen, 0.1  * avgDivLen)
                            if l[i] >= threshold :
                                #print("DIVISION:", l[i], threshold)
                                #print("old vars", vars0[j])
                                #input("PRESS ENTER TO CONTINUE")
                                newLength = l[i] / 2       # determine the length of the daughter cells
                                l[i] = newLength
                                #print("l before", l)
                                l.insert(i+1, newLength)   # inset new cell
                                #print("l after", l)
                                vars0[j] = np.insert(vars0[j], i, vars0[j][i])
                                vars0[j] = np.insert(vars0[j], len(vars0[j])//2 + i + 1, vars0[j][len(vars0[j])//2 + i + 1])
                                #print("new vars", vars0[j])
                        else:
                            if l[i] >= avgDivLen :
                                newLength = l[i] / 2       # determine the length of the daughter cells
                                l[i] = newLength
                                l.insert(i+1, newLength)   # inset new cell
                                vars0[j] = np.insert(vars0[j], i, vars0[i])
                                vars0[j] = np.insert(vars0[j], len(vars0[j])//2 + i + 1, vars0[len(vars0[j])//2 + i + 1])

                    fil += 1

                #print("\n\nTime", t, "#######################################")
                print("t =", t)
                t += dt

                tVals.append(t)

                L1Vals.append(sum(ls[0]))
                L2Vals.append(sum(ls[1]))
                LDiff.append(sum(ls[0])-sum(ls[1]))

            if sum(ls[0]) > sum(ls[1]) : count += 1
            print("Set", N+1,", MC step", n+1, "complete")

        margins.append(margin)
        probs.append(count/mcSteps)
        print("Set", N+1, "complete")

    print("\nDone")
    print("margins", margins)
    print("probabilities", probs)

    end = timer()
    print("RUNTIME:", end-start)

    #pickle.dump( [margins, probs], open("run6_outcomes_forces_" + str(sets) + ".pkl", "wb") )

    """plt.figure(1)
    plt.plot(tVals, LDiff, markersize=0.5)
    plt.figure(2)
    plt.plot(tVals, L1Vals, tVals, L2Vals, 'r', markersize=0.5)
    plt.show()"""

#simulation(set, totalTime, mcSteps, option, random, seedNo)
simulation(1, 30, 10, 0, True, 7)
#simulation(20, 30, 25, 0, True, 7)
#imulation(25, 30, 30, 0, True, 7)
#simulation(15, 40, 30, 0, True, 7)
#simulation(50, 60, 100, 0, True, 7)
#simulation(100, 60, 100, 0, True, 7)
