#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys


plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'figure.autolayout': True})

dir_dict = {
    'loc' : 'varied_loc.dir',
    'angle' : 'varied_angle.dir',
    'l0' : 'varied_l0.dir',
    'r' : 'varied_r.dir',
    'kb' : 'varied_kb.dir',
    'mu': 'varied_mu.dir',
    'a' : 'varied_a.dir',
    'b' : 'varied_b.dir'
}

#outfile = os.path.basename(sys.argv[1])
#filename = sys.argv[1]

psiVals, l0Vals, rVals, kbVals, muVals, aVals, bVals = [], [], [], [], [], [], []
critStressVals, critLengthVals = [], []

variedParam = sys.argv[1].lower()
while variedParam not in ['loc', 'angle', 'l0', 'r', 'kb', 'mu', 'a', 'b'] :
    print("Not a valid varied parameter string.")
    print("Enter 'exit' to quit.")
    print("Enter one of the following: loc, angle, l0, 'r', kb, mu, a, b")
    variedParam = input().lower()
    if variedParam == 'exit' : exit()

for filename in sys.argv[2:] :

    # import data
    data = pickle.load(open(filename, "rb"))

    # key parameters
    psi, l0, r, kb, mu, a, b, numRods, angle, pert_coefficient = data[0]

    #kb, mu, a, b, r, numRods, angle, pert_coefficient, l0 = data[0]
    #psi = kb / (mu * l0**3 * r)

    # other useful values
    growthRate = l0 * r

    # append the parameter values to their respective arrays
    psiVals.append(psi)
    l0Vals.append(l0)
    rVals.append(r)
    kbVals.append(kb)
    muVals.append(mu)
    aVals.append(a)
    bVals.append(b)

    # organzied the other data components -- calculated physical quantities
    lengths = np.array(data[1])
    xdotVals = np.array(data[2])
    ydotVals = np.array(data[3])
    thetadotVals = np.array(data[4])
    lambdaXVals = np.array(data[5])
    lambdaYVals = np.array(data[6])

    numRods = lengths / l0

    # convert the pertubation coefficient (aka q) to the proper index
    index = [int(pert_coefficient * length / l0 - 1) for length in lengths]

    # compute deltaThetaDot for the perturbed linkages
    deltaThetadots = [ thetadotVals[i][index[i]+1]-thetadotVals[i][index[i]] for i in range(len(thetadotVals)) ]

    stresses_dim = [300 * np.sqrt ( lambdaXVals[i][index[i]]**2 + lambdaYVals[i][index[i]]**2 ) for i in range(len(lambdaXVals)) ]

    # compute the Nondimensionalized stress in the perturbed linkages
    stresses = [ np.sqrt ( lambdaXVals[i][index[i]]**2 + lambdaYVals[i][index[i]]**2 ) / (mu * l0**2 * r) for i in range(len(lambdaXVals)) ]

    # determine the critical stresses via linear regression of the stresses and the deltaThetaDots
    m, yint = np.polyfit(stresses, deltaThetadots, 1)
    critStress = -yint/m
    critStressVals.append(critStress)

    #m, yint= np.polyfit(lengths, deltaThetadots, 1)
    #critLength = -yint/m
    #critLengthVals.append(critLength)

    critLengthVals.append( np.sqrt( 8 * critStress * l0**2 ) )

    #deltaThetadotVecs.append( np.array(deltaThetadots) )
    #stressVecs.append( np.array(stresses) )

    #plt.clf()

    label_dict = {
        'loc' : fr'$q$ = {pert_coefficient}',
        'angle' : fr'$\Delta\theta$ = {angle}',
        'l0' : fr'$l_0$ = {l0}',
        'r' : fr'$r$ = {r}',
        'kb' : fr'$k_b$ = {kb}',
        'mu': fr'$\mu$ = {mu}',
        'a' : fr'$a$ = {a}',
        'b' : fr'$b$ = {b}'
    }

    #plt.title(title_dict[variedParam])
    #plt.xlabel(r"Stress (pN)") #(kg $\mu$m/min$^2$)")
    plt.xlabel(r"Number of cells $N$")
    #plt.ylabel(r"Perturbation Angle Rate of Change $\Delta \dot{\theta}$")
    plt.ylabel(r"$\frac{d\Delta\theta}{dt^*} = \frac{1}{r}\frac{d\Delta\theta}{dt}$", size=22)
    #plt.plot(stresses_dim, deltaThetadots, '-o', label=label_dict[variedParam])
    plt.plot(numRods, (1/r) * np.array(deltaThetadots), '-o', label=label_dict[variedParam])
    #plt.hlines(0, -1e100, 1e100 , linestyles='dashed')
    if variedParam == 'angle' or 'l0': plt.xticks(range(26, 42, 2))
    plt.legend(loc="upper left", prop={'size': 9})

plt.savefig(f"plots.dir/{dir_dict[variedParam]}/numRodsVsDeltaThetadot.pdf")
