#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 08/26/2019

import pdb
import numpy as np
from sympy.physics.vector import *
import sympy as sym
from sympy import Point, Polygon, pi
from sympy.geometry import Segment
from scipy.integrate import ode
from scipy.integrate import odeint
import scipy as sp
from mpmath import *
import random
from random import randrange
import math
import time
import csv
import os
import pickle
#import plotsV4
#import plotsV4_withSites
#from FilamentV9 import *

p1_old, p2_old, p3_old, p4_old = None, None, None, None
oldVals = None


def Ra(sPerp, rt, rc) :
    """ returns the activation function
            sPerp : perpendicular distance to the constraint (float)
            rt : transition coefficient (float)
            rc : stiffness parameter (float) """
    sPerp = float( sym.N( sPerp ) )
    x = np.tanh( rt * (sPerp - rc) )
    return 0.5 * ( 1 - x )



def dRdqV3(p, segOther, x1, y1, theta1, x2, y2, theta2, rt, rc, rf, pcheck, val, signFlip) :

    pOther = segOther.projection(p)
    d = val * sym.N( p.distance(pOther) )     # perpendicular (or shortest) distance

    actFunc = Ra(d, rt, rc)

    print( "Sign Flip:", signFlip, "\t Ra:", actFunc,  "\t d:", d, "\t Shifted d:", d-rc )

    dRdx1 = actFunc * sym.N(0.5 * rf * (pOther.x - p.x)) / d
    dRdy1 = actFunc * sym.N(0.5 * rf * (pOther.y - p.y)) / d
    dRdtheta1 = actFunc * sym.N( 0.5 * rf * ( -(p.y - y1) * (pOther.x - p.x) + (p.x - x1) * (pOther.y - p.y) ) ) / d

    dRdx2 = -dRdx1
    dRdy2 = -dRdy1
    dRdtheta2 = -actFunc * sym.N( 0.5 * rf * ( -(pOther.y - y2) * (pOther.x - p.x) + (pOther.x - x2) * (pOther.y - p.y) ) ) / d

    #dRdx1, dRdy1, dRdtheta1, dRdx2, dRdy2, dRdtheta2 = map( sym.N, (dRdx1, dRdy1, dRdtheta1, dRdx2, dRdy2, dRdtheta2) )

    if pcheck :
        #print( actFunc * sym.N(0.5 * rf * (pOther.x - p.x)) / d )
        #print(sym.N(0.5 * rf * (pOther.x - p.x)), '\n')

        print( 'd =', d )
        print( 'Ra =', actFunc, '\n' )
        print( 'dRqdx1 =', sym.N(0.5 * rf * (pOther.x - p.x)) / d )
        print( 'dRpdy1 =', sym.N(0.5 * rf * (pOther.y - p.y)) / d )
        print( 'dRpdtheta1 =', sym.N( 0.5 * rf * ( -(p.y - y1) * (pOther.x - p.x) + (p.x - x1) * (pOther.y - p.y) ) ) / d, '\n' )
        print( 'dRqdx2 =', -sym.N(0.5 * rf * (pOther.x - p.x)) / d )
        print( 'dRpdy2 =', -sym.N(0.5 * rf * (pOther.y - p.y)) / d )
        print( 'dRpdtheta2 =', -sym.N( 0.5 * rf * ( -(pOther.y - y2) * (pOther.x - p.x) + (pOther.x - x2) * (pOther.y - p.y) ) ), '\n' )

    return dRdx1, dRdy1, dRdtheta1, dRdx2, dRdy2, dRdtheta2



def rodToRodElastic(q, t, m1, m2, l1, l2, rf, rd, rc, rt, alpha, beta, gamma) :

    global oldVals
    global p1_old, p2_old, p3_old, p4_old

    print("\nt =", t, '---------------------------------------------------\n')
    x1, y1, theta1, v0x1, v0y1, omega1, x2, y2, theta2, v0x2, v0y2, omega2 = q

    # endpoints in rod 1
    p1 = Point( x1 - 0.5 * l1 * np.cos(theta1), y1 - 0.5 * l1 * np.sin(theta1) )
    p2 = Point( x1 + 0.5 * l1 * np.cos(theta1), y1 + 0.5 * l1 * np.sin(theta1) )
    seg1  = Segment(p1, p2)    # line segment along rod 1

    # endpoints in rod 2
    p3 = Point( x2 - 0.5 * l2 * np.cos(theta2), y2 - 0.5 * l2 * np.sin(theta2) )
    p4 = Point( x2 + 0.5 * l2 * np.cos(theta2), y2 + 0.5 * l2 * np.sin(theta2) )
    seg2 = Segment(p3, p4)     # line segment along rod 2

    p12, p22 = seg2.projection(p1), seg2.projection(p2)
    p31, p41 = seg1.projection(p3), seg1.projection(p4)

    signFlips = [False]*4

    if oldVals != None :
        # first time through vals isn't initalized
        vals = oldVals
    else :
        # initialize vals since this is the first time through
        # note this only works if there is no initial intersections
        vals = {'12':1.0, '22':1.0, '31':1.0, '41':1.0}


    # first time through p1_old, etc are not initialized
    if p1_old != None :
        # displacement vector of the four cell enpoints
        seg_p1 = Segment(p1_old, p1)
        seg_p2 = Segment(p2_old, p2)
        seg_p3 = Segment(p3_old, p3)
        seg_p4 = Segment(p4_old, p4)

        signFlips = [ seg_p1.intersection(seg2) != [], seg_p2.intersection(seg2) != [], seg_p3.intersection(seg1) != [], seg_p4.intersection(seg1) != [] ]

        # if a point crosses another cell -- flip the sign of the distance
        if signFlips[0] : vals['12'] = -1.0 * oldVals['12']
        if signFlips[1] : vals['22'] = -1.0 * oldVals['22']
        if signFlips[2] : vals['31'] = -1.0 * oldVals['31']
        if signFlips[3] : vals['41'] = -1.0 * oldVals['41']

        print("sign flips?:", signFlips)
        #if True in signFlips : input("Press any key to continue")

    p1_old, p2_old, p3_old, p4_old = p1, p2, p3, p4

    print('Vals:', vals, '\n' )
    oldVals = vals


    # p1 to segment 2
    dR12dx1, dR12dy1, dR12dtheta1, dR12dx2, dR12dy2, dR12dtheta2 = dRdqV3(p1, seg2, x1, y1, theta1, x2, y2, theta2, rt, rc, rf, False, vals["12"], signFlips[0])

    # p2 to segment 2
    dR22dx1, dR22dy1, dR22dtheta1, dR22dx2, dR22dy2, dR22dtheta2 = dRdqV3(p2, seg2, x1, y1, theta1, x2, y2, theta2, rt, rc, rf, False, vals["22"], signFlips[1])

    # p3 to segment 1
    dR31dx2, dR31dy2, dR31dtheta2, dR31dx1, dR31dy1, dR31dtheta1 = dRdqV3(p3, seg1, x2, y2, theta2, x1, y1, theta1, rt, rc, rf, False, vals["31"], signFlips[2])

    # p4 to segment 1
    dR41dx2, dR41dy2, dR41dtheta2, dR41dx1, dR41dy1, dR41dtheta1 = dRdqV3(p4, seg1, x2, y2, theta2, x1, y1, theta1, rt, rc, rf, False, vals["41"], signFlips[3])



    v0x1dot = -(alpha/m1 * v0x1) + (dR12dx1 + dR22dx1 + dR31dx1 + dR41dx1) / m1
    v0y1dot = -(beta/m1 * v0y1) + (dR12dy1 + dR22dy1 + dR31dy1 + dR41dy1) / m1
    omega1dot = -(12*gamma/(m1*l1**2) * omega1) + (12/(m1*l1**2)) * (dR12dtheta1 + dR22dtheta1 + dR31dtheta1 + dR41dtheta1)

    v0x2dot = -(alpha/m2 * v0x2) + (dR12dx2 + dR22dx2 + dR31dx2 + dR41dx2) / m2
    v0y2dot = -(beta/m2 * v0y2) + (dR12dy2 + dR22dy2 + dR31dy2 + dR41dy2) / m2
    omega2dot = -(12*gamma/(m2*l2**2) * omega2) + (12/(m2*l2**2)) * (dR12dtheta2 + dR22dtheta2 + dR31dtheta2 + dR41dtheta2)

    #print(x1, x2, theta1, v0x1, v0y1, omega1, x2, y2, theta2, v0x2, v0y2, omega2)
    dqdt = np.array([v0x1, v0y1, omega1, v0x1dot, v0y1dot, omega1dot, v0x2, v0y2, omega2, v0x2dot, v0y2dot, omega2dot])
    return dqdt



def main() :

    s0 = 0.1 #1.0
    sc = 0.06 #0.6
    ri = 0.99
    rd = 4.0
    rf = 40.0
    rt = 2 * np.arctanh(ri) / (s0 - sc)
    rc = (s0 + sc) / 2

    l1 = 5
    l2 = 5
    m1 = 0.1
    m2 = 0.1
    grav = 9.8

    alpha = 0.0
    beta = 0.0
    gamma = 0.0

    #t = np.linspace(0, 1.1, 101)
    #print(t)

    # ROD-TO-ROD COLLISION TOY MODEL
    # q format :
    #              x1,  y1,  theta1,  v0x1, v0y1, omega1, x2,  y2,  theta2,  v0x2, v0y2, omega2
    q0 = np.array([0.0, 0.0, np.pi/6, 15.0,  0.0,  0.0,    7.0, 2.0, np.pi/3, 0.0,  0.0,  0.0])
    #print(odeint)

    # if using ode_int
    #sol = sp.integrate.odeint( rodToRodElastic, q0, t, args=(m1, m2, l1, l2, rf, rd, rc, rt, alpha, beta, gamma))

    # if using solve_ivp
    t_end = 2.0
    num_pts = int(t_end) * 100
    t_span = np.array([0.0, t_end])
    t = np.linspace(0.0, t_end, num_pts+1)
    func = lambda t1, q : rodToRodElastic(q, t1, m1, m2, l1, l2, rf, rd, rc, rt, alpha, beta, gamma)
    sol = sp.integrate.solve_ivp( func, t_span, q0, t_eval=t )

    import matplotlib.pyplot as plt
    from matplotlib.animation import FuncAnimation


    # plotting for odeint --------------------------------------------------------------------

    """
    print("\nPlotting...")

    paramStr = "_rt"+str(rt) + "_rc"+str(rc) + "_alpha"+str(alpha) + "_beta"+str(beta) + "_gamma"+str(gamma)

    plt.plot(t, sol[:, 0], 'b', label='$x1(t)$')
    plt.plot(t, sol[:, 1], 'b', label='$y1(t)$')
    plt.plot(t, sol[:, 2], 'g', label='$theta1(t)$')
    plt.plot(t, sol[:, 3], 'g', label='$v_x1(t)$')
    plt.plot(t, sol[:, 4], 'r', label='$v_y1(t)$')
    plt.plot(t, sol[:, 5], 'y', label='$omega(t)$')

    plt.legend(loc='best')
    plt.xlabel('t')
    plt.grid()

    plt.savefig("plots.dir/paramTesting.dir/rodToRod" + paramStr + ".eps", format='eps')
    #plt.show()

    fig, ax = plt.subplots(figsize=(4,6))
    ax.set(xlim=(-5,5), ylim=(-2, 15))
    ax.set_aspect('equal')

    x1, y1, theta1 = sol[:,0], sol[:,1], sol[:,2]
    x2, y2, theta2 = sol[:,6], sol[:,7], sol[:,8]

    xmin = min(sol[:,0] + sol[:,6])
    xmax = max(sol[:,0] + sol[:,6])

    ymin = min(sol[:,1] + sol[:,7])
    ymax = max(sol[:,1] + sol[:,7])


    scat = ax.plot([x1[0] - 0.5 * l1 * np.cos(theta1[0]), x1[0] + 0.5 * l1 * np.cos(theta1[0])], [y1[0] - 0.5 * l1 * np.sin(theta1[0]), y1[0] + 0.5 * l1 * np.sin(theta1[0])], '-o')

    ax.plot([x2[0] - 0.5 * l2 * np.cos(theta2[0]), x2[0] + 0.5 * l2 * np.cos(theta2[0])], [y2[0] - 0.5 * l2 * np.sin(theta2[0]), y2[0] + 0.5 * l2 * np.sin(theta2[0])], '-o')

    #ax.plot([-100, 100], [0,0] , '-ro')

    def animate(i):
        #line.set_ydata(F[i, :])
        p1x = x1[i] - 0.5 * l1 * np.cos(theta1[i])
        p1y = y1[i] - 0.5 * l1 * np.sin(theta1[i])

        p2x = x1[i] + 0.5 * l1 * np.cos(theta1[i])
        p2y = y1[i] + 0.5 * l1 * np.sin(theta1[i])


        p3x = x2[i] - 0.5 * l2 * np.cos(theta2[i])
        p3y = y2[i] - 0.5 * l2 * np.sin(theta2[i])

        p4x = x2[i] + 0.5 * l2 * np.cos(theta2[i])
        p4y = y2[i] + 0.5 * l2 * np.sin(theta2[i])


        ax.clear()
        ax.plot(0, 'r')
        ax.set(xlim=(-xmin-5,xmax+5), ylim=(-ymin-5, ymax+5))
        ax.set_aspect('equal')
        #ax.plot([-100, 100], [0,0] , '-ro')
        ax.plot([p1x, p2x], [p1y, p2y], '-o')
        ax.plot([p3x, p4x], [p3y, p4y], '-o')

    anim = FuncAnimation(fig, animate, interval=100, frames=len(t)-1)
    anim.save("plots.dir/paramTesting.dir/rodToRod" + paramStr + ".mp4")

    plt.grid()
    plt.draw()
    #plt.show()

    print("Done!") """

    # plotting for solve_ivp ------------------------------------------------------------------

    t = sol.t
    trajs = sol.y

    print("\nPlotting...")

    paramStr = "_rt"+str(rt) + "_rc"+str(rc) + "_alpha"+str(alpha) + "_beta"+str(beta) + "_gamma"+str(gamma)

    x1, y1, theta1 = trajs[0], trajs[1], trajs[2]
    vx1, vy1, omega1 = trajs[3], trajs[4], trajs[5]

    x2, y2, theta2 = trajs[6], trajs[7], trajs[8]
    vx2, vy2, omega2 = trajs[9], trajs[10], trajs[11]


    # CELL #1 PLOT

    plt.figure(1)
    plt.plot(t, x1, 'b', label='$x1(t)$')
    plt.plot(t, y1, 'c', label='$y1(t)$')
    plt.plot(t, theta1, 'g', label='$theta1(t)$')
    plt.plot(t, vx1, 'r', label='$v_x1(t)$')
    plt.plot(t, vy1, 'm', label='$v_y1(t)$')
    plt.plot(t, omega1, 'y', label='$omega(t)$')

    plt.legend(loc='best')
    plt.title("Cell #1 (Blue in animation)")
    plt.xlabel('t')
    plt.grid()

    plt.savefig("plots.dir/paramTesting2.dir/rod1" + paramStr + ".eps", format='eps')

    # CELL #2 Plot

    plt.figure(2)
    plt.plot(t, x2, 'b', label='$x1(t)$')
    plt.plot(t, y2, 'c', label='$y1(t)$')
    plt.plot(t, theta2, 'g', label='$theta1(t)$')
    plt.plot(t, vx2, 'r', label='$v_x1(t)$')
    plt.plot(t, vy2, 'm', label='$v_y1(t)$')
    plt.plot(t, omega2, 'y', label='$omega(t)$')

    plt.legend(loc='best')
    plt.title("Cell #2 (Orange in animation)")
    plt.xlabel('t')
    plt.grid()

    plt.savefig("plots.dir/paramTesting2.dir/rod2" + paramStr + ".eps", format='eps')
    #plt.show()


    # DYNMAICS ANIMATION

    fig, ax = plt.subplots(figsize=(4,6))
    ax.set(xlim=(-5,5), ylim=(-2, 15))
    ax.set_aspect('equal')


    xmin, xmax = min(x1 + x2), max(x1 + x2)
    ymin, ymax = min(y1 + y2), max(y1 + y2)

    scat = ax.plot([x1[0] - 0.5 * l1 * np.cos(theta1[0]), x1[0] + 0.5 * l1 * np.cos(theta1[0])], [y1[0] - 0.5 * l1 * np.sin(theta1[0]), y1[0] + 0.5 * l1 * np.sin(theta1[0])], '-o')

    ax.plot([x2[0] - 0.5 * l2 * np.cos(theta2[0]), x2[0] + 0.5 * l2 * np.cos(theta2[0])], [y2[0] - 0.5 * l2 * np.sin(theta2[0]), y2[0] + 0.5 * l2 * np.sin(theta2[0])], '-o')

    #ax.plot([-100, 100], [0,0] , '-ro')

    def animate(i):
        #line.set_ydata(F[i, :])
        p1x = x1[i] - 0.5 * l1 * np.cos(theta1[i])
        p1y = y1[i] - 0.5 * l1 * np.sin(theta1[i])

        p2x = x1[i] + 0.5 * l1 * np.cos(theta1[i])
        p2y = y1[i] + 0.5 * l1 * np.sin(theta1[i])


        p3x = x2[i] - 0.5 * l2 * np.cos(theta2[i])
        p3y = y2[i] - 0.5 * l2 * np.sin(theta2[i])

        p4x = x2[i] + 0.5 * l2 * np.cos(theta2[i])
        p4y = y2[i] + 0.5 * l2 * np.sin(theta2[i])


        ax.clear()
        #ax.plot(0, 'r')
        ax.set(xlim=(-xmin-5,xmax+5), ylim=(-ymin-5, ymax+5))
        ax.set_aspect('equal')
        #ax.plot([-100, 100], [0,0] , '-ro')
        ax.plot([p1x, p2x], [p1y, p2y], '-o')
        ax.plot([p3x, p4x], [p3y, p4y], '-o')

    anim = FuncAnimation(fig, animate, interval=100, frames=len(t)-1)
    anim.save("plots.dir/paramTesting2.dir/rodToRod" + paramStr + ".mp4")

    plt.grid()
    plt.draw()
    #plt.show()

    print("Done!")


main()
