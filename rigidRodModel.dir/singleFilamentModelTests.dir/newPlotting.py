import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys


####################################################################################################
# Filament Dynamics Functions
####################################################################################################

def length(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    return l0 + growthRate * ( t - lastAddTime )

#---------------------------------------------------------------------------------------------------

def growthDynamics(t, q, lt, kb, mu, nu, epsilon, growthRate) :
    """ updates the qdots for the filament based only on growth dynamics
            l (array) : lengths of the cells in the Filaments
            lp (array) : growth rates for each cell
            kb (float) : angular spring constant
            mu (float) : parallel drag coefficient
            nu (float) : perpendiular drag coefficient
            epsilion (float) : rotational drag coefficient """

    l = [lt] * ( len(q) - 2 )
    lp = [ growthRate ] * ( len(q) - 2 )

    x0 = q[0]
    y0 = q[1]
    thetas = q[2:]

    numRods = len(thetas)
    numNodes = numRods + 1

    #totalLength = sum(l)

    #--------------------------------------------------------

    xEqns, yEqns, thetaEqns = [], [], []

    # x_i Euler-Lagrange equations --------------------------------------------------------

    # initialize x_0 E-L equation and x E_L equation array
    xEqn0 = np.zeros(3*numRods)
    xEqn0[0] = ( mu * np.cos(thetas[0])**2 + nu * np.sin(thetas[0])**2 ) * l[0]
    xEqn0[1] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    if numRods != 1 : xEqn0[numRods + 2] = -1
    xEqns.append(xEqn0)

    # initialize y_0 E-L equation and y E_L equation array
    yEqn0 = np.zeros(3*numRods)
    yEqn0[0] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    yEqn0[1] = ( mu * np.sin(thetas[0])**2 + nu * np.cos(thetas[0])**2 ) * l[0]
    if numRods != 1 : yEqn0[2*numRods + 1] = -1
    yEqns.append(yEqn0)

    # initialize theta_0 E-L equation and theta E_L equation array
    thetaEqn0 = np.zeros(3*numRods)
    thetaEqn0[2] = epsilon * l[0]**3
    if numRods != 1 :
        thetaEqn0[numRods + 2] = 0.5 * l[0] * np.sin(thetas[0])
        thetaEqn0[2*numRods + 1] = -0.5 * l[0] * np.cos(thetas[0])
    thetaEqns.append(thetaEqn0)

    #generate the 2 through N-1 x, y, and theta E-L equations
    for i in range(1, numRods) :

        # x_i E_L equations -----------------------------------------------------------

        xEqni = np.zeros(3*numRods)

        xEqni[0] = ( mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[i]

        xEqni[1] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * np.cos(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[0] * l[i] * np.sin(thetas[0]) )

        for j in range(1, i) :
            xEqni[j+2] = (mu - nu) * l[j] * l[i] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[j] * l[i] * np.sin(thetas[j])

        xEqni[i+2] = -0.5 * nu * l[i]**2 * ( np.sin(thetas[i]) * np.cos(thetas[i])**2 + np.sin(thetas[i])**3 )

        xEqni[i + numRods + 1] = 1

        if i != (numRods-1) :
            xEqni[i + numRods + 2] = -1

        xEqns.append(xEqni)


        # y_i E_L equations -----------------------------------------------------------

        yEqni = np.zeros(3*numRods)

        yEqni[0] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        yEqni[1] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[i]

        yEqni[2] = 0.5 * ( (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[0] * l[i] * np.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * np.sin(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) )

        for j in range(1, i) :
            yEqni[j+2] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[j] * l[i] * np.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])

        yEqni[i+2] = 0.5 * nu * l[i]**2 * (np.cos(thetas[i]) * np.sin(thetas[i])**2 + np.cos(thetas[i])**3)

        yEqni[i + (2*numRods)] = 1

        if i != (numRods-1) :
            yEqni[i + (2*numRods) + 1] = -1

        yEqns.append(yEqni)


        # theta_i E_L equations -----------------------------------------------------------

        thetaEqni = np.zeros(3*numRods)

        thetaEqni[i+2] = epsilon * l[i]**3

        thetaEqni[i + numRods + 1] = 0.5 * l[i] * np.sin(thetas[i])

        thetaEqni[i + (2*numRods)] = -0.5 * l[i] * np.cos(thetas[i])

        if i != (numRods-1) :
            thetaEqni[i + numRods + 2] = 0.5 * l[i] * np.sin(thetas[i])
            thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * np.cos(thetas[i])

        thetaEqns.append(thetaEqni)


        # stress calculations --------------------------------------------------------------
        #self.stressX.append( 0.5 * ( l[i-1]*np.cos(thetas[i-1]) - l[i]*np.sin(thetas[i]) ) )
        #self.stressY.append( 0.5 * ( -l[i-1]*np.sin(thetas[i-1]) + l[i]*np.cos(thetas[i]) ) )

    # combine all E-L equation coeffcients into a single array
    eqns = np.array( xEqns + yEqns + thetaEqns )

    # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
    #   are standard arrays
    #print(eqns)
    #input("stopza")

    if numRods == 1 : xVals, yVals, thetaVals = [0.0], [0.0], [0.0]
    else : xVals, yVals, thetaVals = [0.0], [0.0], [-kb * (thetas[0]-thetas[1])]

    for i in range(1, numRods) :

        xVal = -(mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i]))
        for j in range(1,i) :
            xVal -= (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] * lp[j] * np.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        xVals.append(xVal)

        yVal = -(mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i]))
        for j in range(1,i) :
            yVal -= (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] * lp[j] * np.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        yVals.append(yVal)

        if i == (numRods-1) :
            thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
        else :
            thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
        thetaVals.append(thetaVal)

    vals = np.array( xVals + yVals + thetaVals )


    numRods = len(q)-2

    sols = np.linalg.solve(eqns, vals)

    return sols


####################################################################################################
# Plotting Functions
####################################################################################################


def configurationPlot(t, nodeX, nodeY, minX, maxX, minY, maxY) :

    print("\tCONFIGURATION PLOT...")

    #plt.figure(1)
    minAx = round( min( [minX, minY] ) - 5 )
    maxAx = round( max( [maxX, maxY] ) + 5 )
    #axesRange = [round(minAx-5), round(maxAx+5), round(minAx-5), round(maxAx+5)]

    fig = plt.figure(1)
    l1, = plt.plot([], [], '-bo')       # filament 1 nodes / cells
    #l2, = plt.plot([], [], '-ro')       # filament 2 nodes / cells
    #l3, = plt.plot([], [], 'go')        # filament 1 binding sites
    #l4, = plt.plot([], [], 'go')        # filament 2 binding sites

    plt.xlim(minAx, maxAx)
    plt.ylim(minAx, maxAx)

    framesPerSec = 15
    #framesPerSec = round(len(data)/data[-1][0]*10)
    print("fps =", framesPerSec)

    # sets up video output stuff
    FFMpegWriter = manimation.writers['ffmpeg']
    metadata = dict(title='Movie Test', artist='Matplotlib',
                    comment='Movie support!')
    writer = FFMpegWriter(fps=framesPerSec, metadata=metadata)

    # remember to only use on .csv files in the data.dir directory
    # Splicing [8:-4] removes the "data.dir" at the front and ".csv" at the end
    #outfile = "plots.dir" + str(sys.argv[1])[8:-4] + ".mp4"
    #outfile = "plots.dir" + filename[8:-4] + ".mp4"
    outfile = "plots.dir/configuration.mp4"
    print(outfile)
    count = 0

    with writer.saving(fig, outfile, 100):
        """for d in data :

            #if count % 20 == 0 :  # indent the 3 lines below and uncomment this line
            plt.title("Filament Configuration at t = " + str(round(d[0],2)) )
            l1.set_data(d[1], d[2])
            #l2.set_data(d[3], d[4])

            writer.grab_frame()

            count+=1
            #plt.savefig('plots.dir/snapshots.dir/rigid-rod'+ str(round(d[0],0))+'.svg', format='svg')"""

        for i in range(0, len(t), 100) :
            print(i)
            plt.title("Filament Configuration at t = " + str(round(t[i],2)) )
            l1.set_data(nodeX[i], nodeY[i])

            writer.grab_frame()

            # use the next line to save indivdual frames if desired
            #plt.savefig('plots.dir/snapshots.dir/rigid-rod'+ str(round(d[0],0))+'.svg', format='svg')

            count+=1


    plt.gcf().clear()

#---------------------------------------------------------------------------------------------------

def qdotsPlot(t, qdot, lastAddtime) :

    qdot = np.array(qdot)
    qdot = [ qdot[:,i] for i in range( len(qdot[0] ) ) ]
    # still needs fixing

#---------------------------------------------------------------------------------------------------

def xydotsPlot(t, q, qdot, lastAddtime) :

    print("\tX and Y DOT PLOTS...")

    for qdots in qdot :

        numRods = len(qdot)-2

        xdots, ydots = [qdots[0]], [qdots[1]]

        for i in range(numRods-1) :

            # relevant orientation angles, lengths, and growth rates
            theta1, theta2 = q[i+2], q[i+3]
            thetadot1, thetadot2 = qdots[i+2], qdots[i+3]
            l1, l2 = lt[i], lt[i+1]
            lp1, lp2 = lp[i], lp[i+1]

            # compute the next xdot
            xdot1 = self.xdots[i]
            xdot2 = xdot1 + 0.5 * ( lp1 * np.cos(theta1) - l1 * thetadot1 * np.sin(theta1) + lp2 * np.cos(theta2) - l2 * thetadot2 * np.sin(theta2) )
            self.xdots.append( xdot2 )

            # compute the next ydot
            ydot1 = self.ydots[i]
            ydot2 = ydot1 + 0.5 * ( lp1 * np.sin(theta1) + l1 * thetadot1 * np.cos(theta1) + lp2 * np.sin(theta2) + l2 * thetadot2 * np.cos(theta2) )
            self.ydots.append( ydot2 )



#---------------------------------------------------------------------------------------------------



def stressPlot(t, stressX, stressY) :

    print("\tSTRESS PLOTS...")

    divTimes = []
    for i in range( 1, len(stressX) ) :
        if len(stressX[i-1]) != len(stressX[i]) :
            divTimes.append(i)
    print(divTimes)

    fx, fy = [], []
    for i in range( len(stressX[-1]) ) :
        fx.append( [None] * len(t) )
        fy.append( [None] * len(t) )

    #for i in range( len(divTimes)-1, -1, -1 ) :
    for i in range( len(stressX[-1]) ) :
        #print("Node:", i)
        j = len(divTimes)-1
        divTime = divTimes[j]
        #for j in range( len(divTimes)-1, -1, -1 ) :
        current = i
        for k in range( len(t)-1, -1, -1 ) :
            if (k < divTime) and (j != 0) :
                current = (current-1) / 2
                if current % 1 == 0 : current = int(current)
                j -= 1
                divTime = divTimes[j]
            #print("node:", i, "\tcurrent", current, "\ttime index:", k, "\ttime:", t[k], "\tdivTime:", divTime)
            try :
                fx[i][k] = stressX[k][current]
                fy[i][k] = stressY[k][current]
            except :
                fx[i][k] = None
                fy[i][k] = None


    """t2, t3 = t[divTimes[0]:], t[divTimes[1]:]
    #print(t2, t3)

    fx1, fy1 = stressX[:divTimes[0]], stressY[:divTimes[0]]
    fx2, fy2 = stressX[divTimes[0]:divTimes[1]], stressY[divTimes[0]:divTimes[1]]
    fx3, fy3 = stressX[divTimes[1]:], stressY[divTimes[1]:]

    fx1, fy1, fx2, fy2, fx3, fy3 = map(np.array, (fx1, fy1, fx2, fy2, fx3, fy3))

    print(len(fx[0]), fx3[:,0])

    #testNode = np.concatenate( (fx1[:,0], fx2[:,1], fx3[:,3]) )
    #print(len(testNode))

    node0 = np.sqrt( fx3[:,0]**2 + fy3[:,0]**2 )
    node1 = np.sqrt( np.concatenate( (fx2[:,0], fx3[:,1]) )**2 + np.concatenate( (fy2[:,0], fy3[:,1]) )**2 )
    node2 = np.sqrt( fx3[:,2]**2 + fy3[:,2]**2 )
    node3 = np.sqrt( np.concatenate( (fx1[:,0], fx2[:,1], fx3[:,3]) )**2 + np.concatenate( (fy1[:,0], fy2[:,1], fy3[:,3]) )**2 )
    node4 = np.sqrt( fx3[:,4]**2 + fy3[:,4]**2 )
    node5 = np.sqrt( np.concatenate( (fx2[:,2], fx3[:,5]) )**2 + np.concatenate( (fy2[:,2], fy3[:,5]) )**2 )
    node6 = np.sqrt( fx3[:,6]**2 + fy3[:,6]**2 )
    node7 = np.sqrt( np.concatenate( (fx1[:,1], fx2[:,3], fx3[:,7]) )**2 + np.concatenate( (fy1[:,1], fy2[:,3], fy3[:,7]) )**2 )
    node8 = np.sqrt( fx3[:,8]**2 + fy3[:,8]**2 )
    node9 = np.sqrt( np.concatenate( (fx2[:,4], fx3[:,9]) )**2 + np.concatenate( (fy2[:,4], fy3[:,9]) )**2 )
    node10 = np.sqrt( fx3[:,10]**2 + fy3[:,10]**2)

    plt.figure(1)
    plt.title("Stress in Cell-to-Cell Connections")
    plt.xlabel("Time (min)")
    plt.ylabel("Stress Magnitude")

    plt.plot(t3, node0)
    plt.plot(t2, node1)
    plt.plot(t3, node2)
    plt.plot(t, node3)
    plt.plot(t3, node4)
    plt.plot(t2, node5)
    plt.plot(t3, node6)
    plt.plot(t, node7)
    plt.plot(t3, node8)
    plt.plot(t2, node9)
    plt.plot(t3, node10)

    plt.legend([ "Node" + str(i) for i in range(1,12) ] , loc='upper left')
    plt.savefig('plots.dir/stressMethod1.eps', format='eps')
    #plt.show()
    """



    #plt.figure(2)
    plt.title("Stress in Cell-to-Cell Connections")
    plt.xlabel("Time (min)")
    plt.ylabel("Stress Magnitude")

    for i in range( len(fx) ) :
        fmag = []
        for j in range( len(fx[i]) ) :
            try : fmag.append( np.sqrt( fx[i][j]**2 + fy[i][j]**2 ) )
            except : fmag.append(None)
        plt.plot(t, fmag)

    plt.legend([ "Node" + str(i) for i in range(1, len(fx)+1) ] , loc='upper left')

    #plt.show()
    plt.savefig('plots.dir/stresss_simulation.pdf', format='pdf')
    print("HEY WE GOT HERE!")

#---------------------------------------------------------------------------------------------------

def lengthPlot(t, l0, growthRate, lastAddTime) :

    l = []
    i = 0
    last = lastAddTime[i]
    for time in t :
        if i < len(lastAddTime) :
            if (time > lastAddTime[i+1]) :
                i+=1
                last = lastAddTime[i]
        l.append( length(time, l0, growthRate, last) )
    plt.plot(t, l)
    plt.savefig('plots.dir/lengths.eps', format='eps')


####################################################################################################
# Data Organization
####################################################################################################

print("LOADING DATA...")
sol = pickle.load(open("data.dir/run10_straightExample_epsilonLJ0.0_rm1.5_Lc5.0_endTime120.0.pkl", "rb"))
l0, growthRate, kb, mu, nu, epsilon = sol[0]
sol = sol[1:]

print("ORGANIZING DATA...")

minX, maxX, minY, maxY = 1E100, 0, 1E100, 0
qdot, lambdaX, lambdaY, nodeX, nodeY = [], [], [], [], []
lastAddTime = [sol[0].t[0]]
ltVec, lpVec = [], []
for s in sol :

    q = [ s.y[:,i] for i in range(len(s.y[0])) ]
    numRods = len(q[0]) - 2

    lt = [ [ length(time, l0, growthRate, lastAddTime[-1]) ] * numRods for time in s.t ]
    ltVec.append(lt)
    lp = [ [growthRate] * numRods for time in s.t ]
    lpVec.append(lp)

    # interating over time
    for i in range(len(q)) :

        numRods = len(q[i])-2
        lt = length(s.t[i], l0, growthRate, lastAddTime[-1])

        # compute qdots and Lagrange multipliers
        sols = growthDynamics(s.t[i], q[i], lt, kb, mu, nu, epsilon, growthRate)

        # organize qdots
        qdots = sols[: numRods+2]
        qdot.append(qdots)

        # organize Lagrange multipliers
        lambdaXs = sols[numRods+2 : 2*numRods+1]
        lambdaX.append(lambdaXs)
        lambdaYs = sols[2*numRods+1 :]
        lambdaY.append(lambdaYs)

        # calculate node positions
        thetas = q[i][2:]
        xNodes = [ q[i][0] - lt/2 * np.cos( thetas[0] ) ]
        yNodes = [ q[i][1] - lt/2 * np.sin( thetas[0] ) ]
        for i in range( len(q[i]) - 2 ) :
            xNodes.append( xNodes[-1] + lt * np.cos(thetas[i]) )
            yNodes.append( yNodes[-1] + lt * np.sin(thetas[i]) )
            if min(xNodes) < minX : minX = min(xNodes)
            if max(xNodes) > maxX : maxX = max(xNodes)
            if min(yNodes) < minY : minY = min(yNodes)
            if max(yNodes) > maxY : maxY = max(yNodes)
        nodeX.append(xNodes)
        nodeY.append(yNodes)

    lastAddTime.append(s.t[-1])
t = np.concatenate( [s.t for s in sol], axis=None )


print("PLOTTING...")

#configurationPlot(t, nodeX, nodeY, minX, maxX, minY, maxY)
stressPlot(t, lambdaX, lambdaY)
#qdotsPlot(t, qdot, lastAddTime)
#lengthPlot(t, l0, growthRate, lastAddTime)





"""
# t array
t = np.concatenate( [s.t for s in sol], axis=None )

# solution arrays
y = np.array( [ np.concatenate([s.y[i] for s in sol], axis=None) for i in range(len(sol[0].y)) ] )

# q arrays
q = [ y[:,i] for i in range(len(y)) ]
"""
