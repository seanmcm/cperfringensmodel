#!/bin/bash

echo "RUNNING"

KBS=(1e-7)
#KBS=(1e-9 2.5e-9 5e-9 7.5e-9 1e-8 2.5e-8 5e-8 7.5e-8 1e-7)

MUS=(1e-9)
#MUS=(1e-12 1e-11 1e-10 1e-9 1e-8 1e-7 1e-6)
#MUS=(7e-10 8e-10 9e-10 1e-9 2e-9 3e-9 4e-9)

RATES=(0.1)
#RATES=(0.0333 0.05 0.1 0.2 0.3)

ANGLES=(2)
#ANGLES=(1 1.5 2 2.5 3 3.5 4 4.5 5)

QS=(0.5)
#QS=(0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5)

for q in "${QS[@]}"
do
    echo "NEW Q (LOCATION)!"
    for angle in "${ANGLES[@]}"
    do
        echo "NEW ANGLE!"
        for kb in "${KBS[@]}"
        do
            echo "NEW KB!"
            for mu in "${MUS[@]}"
            do
                echo "NEW MU!"
                for rate in "${RATES[@]}"
                do
                    echo "NEW RATE!"
                    echo "q =" $q
                    echo "angle =" $angle
                    echo "rate =" $rate
                    echo "mu =" $mu
                    echo "kb =" $kb
                    flag="true"
                    numrods=20
                    maxnumrods=40
                    #python -c "import pickle; pickle.dump([], open('data.dir/tempData.pkl', 'wb'))"
                    #mv data.dir/tempData.pkl data.dir/data_kb-mu-ratio${kb_mu_ratio}_growthRate${rate}_angle${angle}.pkl
                    while [ $numrods -le $maxnumrods ]
                    do
                        echo $numrods
                        python criticalForceCalculations.py $kb $mu $rate $numrods $angle $q
                        numrods=$(( numrods + 2 ))

                    done
                    duration=$SECONDS
                    echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
                done
            done
        done
    done
done

#python criticalForcePlotting.py data*.pkl
mv data*.pkl data.dir

echo "SIMULATIONS COMPLETE!"
