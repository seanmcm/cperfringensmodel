import numpy as np
from scipy.integrate import solve_ivp

# %% Define derivative function
def f(t, y, c):
    dydt = [1]  # list of derivatives as functions of t, y, and c
    return dydt

# %% Define time spans, initial values, and constants
tspan = np.linspace(0, 9, 10)  # start, finish, numpoints
yinit = [0]             # list of initial values
c = [ ]                 # list of constants

#%% Solve differential equation
sol = solve_ivp(lambda t, y: f(t, y, c), [tspan[0], tspan[-1]], yinit, t_eval=tspan)
solver = 
