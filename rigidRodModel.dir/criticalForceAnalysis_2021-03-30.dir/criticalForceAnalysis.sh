 #!/bin/bash

echo "RUNNING"

QS=(0.5)
#QS=(0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.450 0.50)

ANGLES=(2)
#ANGLES=(1 1.5 2 2.5 3 3.5 4 4.5 5)

L0S=(5)
#L0S=(5 10 15 20 25 30 35 40 45 50)

RATES=(0.02)
#RATES=(0.02 0.06 0.1 0.14 0.18 0.22 0.26 0.3 0.34 0.38 0.42 0.46 0.5)

#KBS=(2.5e-4) #0.001)
#KBS=(0 2.5e-4 5e-4 7.5e-5 10e-5 12.5e-5 15e-5 17.5e-5 20e-5)
#KBS=(2.5e-8 2.5e-7 2.5e-6 2.5e-5 2.5e-4 2.5e-3 2.5e-2 0.25 2.5)
#KBS=()
#KBS=(0)

# for various values of psi use psi = (1 2 3 4 5 6 7 8 9 10):
KBS=(2.5e-5 5e-5 7.5e-5 1e-4 1.25e-5 1.5e-4 1.75e-4 2e-4 2.25e-4 2.5e-4)

MUS=(1e-5)
#MUS=(0e-6 5e-6 10e-6 15e-6 20e-6 25e-6 30e-5 35e-6 40e-6 45e-6 50e-6)

AS=(2)
#AS=(1 2 3 4 5 6 7 8 9 10)

# note this is 1/b -- NOT b
#BS=(12)
BS=(12 6 4 3 2.4 2 1.7143 1.5 1.3333 1.2)

for q in "${QS[@]}"
do
    for angle in "${ANGLES[@]}"
    do
        for l in "${L0S[@]}"
        do
            for rate in "${RATES[@]}"
            do
                for kb in "${KBS[@]}"
                do
                    for mu in "${MUS[@]}"
                    do
                        for a in "${AS[@]}"
                        do
                            for b in "${BS[@]}"
                            do
                                echo "NEW PARAMETER SET"
                                echo "q =" $q
                                echo "angle =" $angle
                                echo "l =" $l
                                echo "rate =" $rate
                                echo "kb =" $kb
                                echo "mu =" $mu
                                echo "a =" $a
                                echo "1/b = $b"
                                flag="true"
                                # use N = 20-40 for varied angle
                                # use N = 30-50 for varied location
                                numrods=20
                                maxnumrods=50
                                #python -c "import pickle; pickle.dump([], open('data.dir/tempData.pkl', 'wb'))"
                                #mv data.dir/tempData.pkl data.dir/data_kb-mu-ratio${kb_mu_ratio}_growthRate${rate}_angle${angle}.pkl
                                while [ $numrods -le $maxnumrods ]
                                do
                                    echo $numrods
                                    #python criticalForceCalculations.py $kb $mu $a $rate $numrods $angle $q $l
                                    python criticalForceCalculations.py $q $angle $numrods $l $rate $kb $mu $a $b
                                    numrods=$(( numrods + 2 ))
                                done
                                duration=$SECONDS
                                echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
                            done
                        done
                    done
                done
            done
        done
    done
done

#python criticalForcePlotting.py data*.pkl
mv data*.pkl data.dir

echo "SIMULATIONS COMPLETE!"
