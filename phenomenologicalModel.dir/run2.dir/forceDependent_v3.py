#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 09/10/2018

import numpy as np
import scipy as sp
from scipy import optimize
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle

from main import *


def forceSolver(vars, *args) :

    l, rateMax, mu, fStall, fTrans, q = args

    rates = vars[:len(vars)//2]
    forces = vars[len(vars)//2:]

    eqns = []

    for i in range( int(len(vars)/2 ) ) :

        try:
            eqns.append( rates[i] - ( rateMax / ( 1 + np.exp( rateMax / (fStall - fTrans) * (np.abs(forces[i]) + np.abs(forces[i+1]) - fStall - fTrans) / 2 ) ) ) )
        except :
            if i == len(l)-1 :
                eqns.append( rates[i] - ( rateMax / ( 1 + np.exp( rateMax / (fStall - fTrans) * (np.abs(forces[i]) - fStall - fTrans) / 2 ) ) ) )
            else : print("Index out of range", len(forces), "i =", i)

        eqns.append( forces[i] - ( 0.5 * mu * sum(l) * sum(rates) * q[i] * (1-q[i]) ) )

    #print("IT WORKED! (?)")
    return(eqns)


#def forceDependentGrowth(mu, L, LPrime, q) :
def forceDependentGrowth(l, rateWithout, mu) :
    """ determines the constraint force for the point
        located at l = q L """

    q = [0] * len(l)
    for i in range( 1 , len(l) ) :
        q[i] = 0.5 * ( sum(l[:i+1]) / sum(l) + 1 )

    fTrans = 5e-9
    fStall = 10^-7

    params = l, rateWithout, mu, fStall, fTrans, q

    vars0 = [0] * int(len(l) * 2)
    sols = sp.optimize.fsolve( forceSolver, vars0, args=params )


    """if force < fTrans :
        return rateWithout
    elif force < fStall :
        return ( -rateWithout / (fStall - fTrans) ) * (force - fTrans) + rateWithout
    else :
        return 0"""

    return sols[: int(len(sols)/2) + 1]



def simulation(sets, totalTime, mcSteps, option, random, seedNo) :

    dt, totalTime, frac, mu, avgDivLen, rateWithout, rateWith, option, random = initialization(totalTime, option, random)

    np.random.seed(seedNo)

    filaments = pickle.load(open("filaments100.pkl", "rb"))

    margins, probs = [], []

    #sets = 100
    for N in range(sets) :

        l1_init = np.random.choice(filaments)
        l2_init = np.random.choice(filaments)

        tVals, L1Vals, L2Vals = [0], [sum(l1_init)], [sum(l2_init)]

        count = 0   # number of monte carlo steps with final L1 > L2

        for n in range(mcSteps) :

            ls = [l1_init.copy(), l2_init.copy()]
            margin = sum(ls[0]) - sum(ls[1])

            t = 0
            while t < totalTime :

                for l in ls :

                    rates = forceDependentGrowth(l, rateWithout, mu)

                    for i in range( len(l) ) :

                        growthRate = getGrowthRate(rates[i], frac, random)
                        l[i] += growthRate * dt

                        if random :
                            if l[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
                                newLength = l[i] / 2       # determine the length of the daughter cells
                                l[i] = newLength
                                l.insert(i+1, newLength)   # inset new cell
                        else:
                            if l[i] >= avgDivLen :
                                newLength = l[i] / 2       # determine the length of the daughter cells
                                l[i] = newLength
                                l.insert(i+1, newLength)   # inset new cell
                print(t)
                t += dt

                tVals.append(t)
                L1Vals.append(sum(ls[0]))
                L2Vals.append(sum(ls[1]))

            if sum(ls[0]) > sum(ls[1]) : count += 1
            print("Set", N+1,", MC step", n+1, "complete")

        margins.append(margin)
        probs.append(count/mcSteps)
        print("Set", N+1, "complete")

    print("\nDone")
    print("margins", margins)
    print("probabilities", probs)

    pickle.dump( [margins, probs], open("run2_outcomes_forces_" + str(sets) + ".pkl", "wb") )

    """plt.figure(1)
    #plt.plot(tVec, LDiff, markersize=0.5)
    #plt.figure(2)
    plt.plot(tVals, L1Vals, tVals, L2Vals, 'r', markersize=0.5)
    plt.show()"""

#simulation(set, totalTime, mcSteps, option, random, seedNo)
simulation(15, 40, 30, 0, True, 7)
#simulation(50, 60, 100, 0, True, 7)
#simulation(100, 60, 100, 0, True, 7)
