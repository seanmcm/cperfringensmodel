Rigid Rod Model for Clostridium Perfrigens
Run 10: Lateral Interaction Implementation


V1: a rough version where nothing to important was implemented

V2: a complete overhaul of the simluation -- introduced a
    class for Filaments, Binding Sites, and Bonds
    all the major dynamics are handled in these classes;
    the main function basically organizes the calls
    This was the first attempt at lateral interaction
    implementation by converting torques and forces to
    qdots but it did not work

V3: the same framework used in V2 except now lateral interactions
    are implemented using Lagrangian mechanics

V4: introduced randomness in the binding sites and introduce cell division
    FilamentV4.py is the best working version of the Filament related classes

V4_paramTest: parameter testing with the working V4 model
