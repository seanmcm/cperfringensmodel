#!/usr/local/env python

import pdb
import numpy as np
from sympy.physics.vector import *
import sympy as sym
from sympy import Point, Polygon, pi
from sympy.geometry import Segment
from scipy.integrate import ode
from scipy.integrate import odeint
from scipy.integrate import solve_ivp
import scipy as sp
from mpmath import *
import random
from random import randrange
import math
import time
import csv
import os
import pickle


# simulation to test event based implementation

def f(x) : return x * (1-x)


def event(t, y) :
    z, u = y[0], y[1]
    fz, fu = f(z), f(u)
    return (fz * fu) - ( ( np.sign(fz) - 1 ) * ( np.sign(fu) - 1 ) )


def ode(t, y, dy) : return dy


def main() :

    pairs = [ # z crossing 0; positive
              [[-0.5, 0.5], [0.1, 0], True, "0 < u < 1"],
              [[-0.5, 1.5], [0.1, 0], False, "u > 1"],
              [[-0.5, -0.5], [0.1, 0], False, "u < 1"],

              # z crossing 0; negative
              [[0.5, 0.5], [-0.1, 0], False, "0 < u < 1"],
              [[0.5, 1.5], [-0.1, 0], False, "u > 1"],
              [[0.5, -0.5], [-0.1, 0], False, "u < 1"],

              # z crossing 1; positive
              [[0.5, 0.5], [0.1, 0], False, "0 < u < 0"],
              [[0.5, 1.5], [0.1, 0], False, "u > 1"],
              [[0.5, -0.5], [0.1, 0], False, "u < 1"],

              # z crossing 1; negative
              [[1.5, 0.5], [-0.1, 0], True, "0 < u < 0"],
              [[1.5, 1.5], [-0.1, 0], False, "u > 1"],
              [[1.5, -0.5], [-0.1, 0], False, "u < 1"],


              # u crossing 0; positive
              [[0.5, -0.5], [0, 0.1], True, "0 < z < 1"],
              [[1.5, -0.5], [0, 0.1], False, "z > 1"],
              [[-0.5, -0.5], [0, 0.1], False, "z < 1"],

              # u crossing 0; negative
              [[0.5, 0.5], [0, -0.1], False, "0 < z < 1"],
              [[1.5, 0.5], [0, -0.1], False, "z > 1"],
              [[-0.5, 0.5], [0, -0.1], False, "z < 1"],

              # u crossing 1; positive
              [[0.5, 0.5], [0, 0.1], False, "0 < z < 1"],
              [[1.5, 0.5], [0, 0.1], False, "z > 1"],
              [[-0.5, 0.5], [0, 0.1], False, "z < 1"],

              # u crossing 1; negative
              [[0.5, 1.5], [0, -0.1], True, "0 < z < 1"],
              [[1.5, 1.5], [0, -0.1], False, "z > 1"],
              [[-0.5, 1.5], [0, -0.1], False, "z < 1"] ]


    for pair in pairs :
        y0, dy, result, code = pair[0], pair[1], pair[2], pair[3]
        #print("y0 =", y0)
        #print("dy =", dy)
        t_span = [0, 10.0]
        func = lambda t, y : ode(t, y, dy)
        event.terminal = True
        event.direction = 1
        sol = solve_ivp(func, t_span, y0, events = event)

        print( (len(sol.t_events[0]) != 0) == result, "\t"+code )

        #print("t_events", sol.t_events)
        #print("times", sol.t)
        #print("y", sol.y, "\n")
        #input()

def test(y0, dy, result) :
    t_span = [0, 10.0]
    func = lambda t, y : ode(t, y, dy)
    event.terminal = True
    event.direction = 1
    sol = solve_ivp(func, t_span, y0, events = event)
    return (len(sol.t_events[0]) != 0) == result

def main2() :
    # initial conditions and rates of change


    # z crossing 0; positive
    y0, dy = [-0.5, 0.5], [0.1, 0]        # 0 < u < 1
    print(test(y0, dy, True))
    y0, dy = [-0.5, 1.5], [0.1, 0]        # u > 1
    print(test(y0, dy, False))
    y0, dy = [-0.5, -0.5], [0.1, 0]       # u < 1
    print(test(y0, dy, False))
    print("\n")

    # z crossing 0; negative
    y0, dy = [0.5, 0.5], [-0.1, 0]        # 0 < u < 1
    print(test(y0, dy, False))
    y0, dy = [0.5, 1.5], [-0.1, 0]        # u > 1
    print(test(y0, dy, False))
    y0, dy = [0.5, -0.5], [-0.1, 0]       # u < 1
    print(test(y0, dy, False))
    print("\n")

    # z crossing 1; positive
    y0, dy = [0.5, 0.5], [0.1, 0]         # 0 < u < 0
    print(test(y0, dy, False))
    y0, dy = [0.5, 1.5], [0.1, 0]         # u > 1
    print(test(y0, dy, False))
    y0, dy = [0.5, -0.5], [0.1, 0]        # u < 1
    print(test(y0, dy, False))
    print("\n")

    # z crossing 1; negative
    y0, dy = [1.5, 0.5], [-0.1, 0]        # 0 < u < 0
    print(test(y0, dy, True))
    y0, dy = [1.5, 1.5], [-0.1, 0]        # u > 1
    print(test(y0, dy, False))
    y0, dy = [1.5, -0.5], [-0.1, 0]       # u < 1
    print(test(y0, dy, False))
    print("\n")


    # u crossing 0; positive
    y0, dy = [0.5, -0.5], [0, 0.1]        # 0 < z < 1
    print(test(y0, dy, True))
    y0, dy = [1.5, -0.5], [0, 0.1]        # z > 1
    print(test(y0, dy, False))
    y0, dy = [-0.5, -0.5], [0, 0.1]       # z < 1
    print(test(y0, dy, False))
    print("\n")

    # u crossing 0; negative
    y0, dy = [0.5, 0.5], [0, -0.1]        # 0 < z < 1
    print(test(y0, dy, False))
    y0, dy = [1.5, 0.5], [0, -0.1]        # z > 1
    print(test(y0, dy, False))
    y0, dy = [-0.5, 0.5], [0, -0.1]       # z < 1
    print(test(y0, dy, False))
    print("\n")

    # u crossing 1; positive
    y0, dy = [0.5, 0.5], [0, 0.1]        # 0 < z < 1
    print(test(y0, dy, False))
    y0, dy = [1.5, 0.5], [0, 0.1]        # z > 1
    print(test(y0, dy, False))
    y0, dy = [-0.5, 0.5], [0, 0.1]       # z < 1
    print(test(y0, dy, False))
    print("\n")

    # u crossing 1; negative (should STOP)
    y0, dy = [0.5, 1.5], [0, -0.1]        # 0 < z < 1
    print(test(y0, dy, True))
    y0, dy = [1.5, 1.5], [0, -0.1]        # z > 1
    print(test(y0, dy, False))
    y0, dy = [-0.5, 1.5], [0, -0.1]       # z < 1
    print(test(y0, dy, False))
    print("\n")

main()
#main2()
