from multiprocessing import Process
from timeit import default_timer as timer

def func1(args):
  n, s = args
  print('func1: starting')
  for i in range(n):
    if i % 10000 == 0 : print(s, i)
  print('func1: finishing')

def func2(args):
  n, s = args
  print('func2: starting')
  for i in range(n):
    if i % 1000000 == 0 : print(s, i)
  print('func2: finishing')

if __name__ == '__main__':

  f1 = lambda n : func1(n)
  f2 = lambda n : func1(n)

  args1 = (10000000, "f1")
  args2 = (1000000, "f2")

  tstart = timer()
  p1 = Process( target=f1, args=(args1,) )
  p1.start()
  p2 = Process( target=f2, args=(args2,) )
  p2.start()
  p1.join()
  p2.join()
  print( "runtime:", timer()-tstart )

"""import multiprocessing
import time
import random

def hello(n):
    time.sleep(random.randint(1,3))
    print("[{0}] Hello!".format(n))

processes = [ ]
for i in range(10):
    t = multiprocessing.Process(target=hello, args=(i,))
    processes.append(t)
    t.start()

for one_process in processes:
    one_process.join()

print("Done!")"""
