#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 09/10/2018

import numpy as np
import scipy as sp
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle

from main import *


def simulation(sets, totalTime, mcSteps, option, random, seedNo) :

    dt, totalTime, frac, mu, avgDivLen, rateWithout, rateWith, option, random = initialization(totalTime, option, random)

    np.random.seed(seedNo)

    filaments = pickle.load(open("filaments100.pkl", "rb"))

    margins = []
    probs = []

    #sets = 100
    for N in range(sets) :

        l1_init = np.random.choice(filaments)
        l2_init = np.random.choice(filaments)

        count = 0   # number of monte carlo steps with final L1 > L2

        for n in range(mcSteps) :

            ls = [l1_init.copy(), l2_init.copy()]
            margin = sum(ls[0]) - sum(ls[1])

            t = 0
            while t < totalTime :

                for l in ls :

                    for i in range( len(l) ) :

                        growthRate = getGrowthRate(rateWithout, frac, random)
                        l[i] += growthRate * dt

                        if random :
                            if l[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
                                newLength = l[i] / 2       # determine the length of the daughter cells
                                l[i] = newLength
                                l.insert(i+1, newLength)   # inset new cell
                        else:
                            if l[i] >= avgDivLen :
                                newLength = l[i] / 2       # determine the length of the daughter cells
                                l[i] = newLength
                                l.insert(i+1, newLength)   # inset new cell

                t += dt

            if sum(ls[0]) > sum(ls[1]) : count += 1
            print("Set", N+1,", MC step", n+1, "complete")

        margins.append(margin)
        probs.append(count/mcSteps)
        print("Set", N+1, "complete")

    print("\nDone")
    print("margins", margins)
    print("probabilities", probs)

    pickle.dump( [margins, probs], open("run2_outcomes_" + str(sets) + ".pkl", "wb") )

#simulation(set, totalTime, mcSteps, option, random, seedNo)
simulation(10, 60, 100, 0, True, 7)
simulation(50, 60, 100, 0, True, 7)
simulation(100, 60, 100, 0, True, 7)
