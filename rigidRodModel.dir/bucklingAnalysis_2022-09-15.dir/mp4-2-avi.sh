#!/bin/bash

for file in "$@"
do
    ffmpeg -i $file -an -vcodec rawvideo -y "${file%.*}.avi"
done
