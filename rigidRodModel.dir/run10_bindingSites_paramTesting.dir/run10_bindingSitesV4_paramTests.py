#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 03/26/2019

import pdb
import numpy as np
from scipy.integrate import ode
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
import plotsV4
from FilamentV4 import *

################################################################################
# SIMULATION NOTES
#   TIME-SCALE: minutes
#   LENGTH-SCALE: micrometers
################################################################################

################################################################################
# FUNCTIONS
################################################################################

################################################################################
# FUNCTIONS
################################################################################

def length(t, l0, growthRate) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    global lastAddTime
    return l0 + growthRate * ( t - lastAddTime )

    # Something might be funky with the growth rate

def springConstant(t, k0, length, l0) :
    """ returns the linear spring constant at time t
            t: time
            k0: the initial linear spring constant (value at t = 0)
            length: the current equilibrium length
            l0: the initial spring equilibrium length (value at t = 0) """
    return k0 * l0 / length

def dragCoefficient(t, gamma0, l, l0) :
    """ returns the drag coefficient as time t
            t: time
            gamma0: the initial drag coefficient (value at time t =0 )
            totalLength: total length of the filament at time t
            totalLength0: total length of the filament at time t = 0 """
    return gamma0 * l0 / l

def rotDragCoefficient(t, gamma0, l, l0) :
    """ returns the drag coefficient as time t
            t: time
            gamma0: the initial drag coefficient (value at time t =0 )
            totalLength: total length of the filament at time t
            totalLength0: total length of the filament at time t = 0 """
    return gamma0 * (l0 / l)**3

################################################################################
# ODE FUNCTION
################################################################################

def dqdt(t, q, growthRate, kb0, mu0, nu0, epsilon0, width, kon, koff0, ks, Lmax, Lc, Fc) :
    """ the ODE function
        returns an array of the qDots of the N+2 generalized coordinate at time
        t to be used in solving dq/dt = qDot
            t: time
            q: array of the form [x0, y0 , theta0, theta1, ... , thetaN] """

    global lambdaVals
    global realCall
    global filaments
    global dt

    #filaments = myFilamentList.fs
    #print("look here!", myFilaments)
    #filaments = myFilaments[0]
    #print("LOOK HERE", filaments)

    #oldFilaments = []
    tick = time.time()
    for filament in filaments :

        #oldFilaments.append( filament.copy() )
        numRods = len(filament.q) - 2
        l0 = filament.l0

        # set parameters
        filament.lt = np.array( [length(t, l0, growthRate)] * numRods )
        filament.lp = np.array( [growthRate] * numRods )
        kb = kb0
        mu = dragCoefficient(t, mu0, filament.lt[0], l0)
        nu = dragCoefficient(t, nu0, filament.lt[0], l0)
        epsilon = rotDragCoefficient(t, epsilon0, filament.lt[0], l0)

        # update qdots based on single filament growth dynamics
        #if realCall == True : print("Filament =", filament, "Length", filament.lt)
        filament.growthDynamics(kb, mu, nu, epsilon)
        #growth_qdots = np.concatenate((growth_qdots, filament.qdots), axis=None)


    #print("before lateral", filaments[0], filaments[0].qdots)

    for i in range( len(filaments) ) :
        for j in range( len(filaments) ) :

            if i == j : pass
            else :
                # update qdots based on lateral interactions
                filaments[i].lateralDynamics(filaments[j], width, kon, koff0, ks, Lmax, Lc, Fc, dt )

    # solve for the qdots in each filament
    for filament in filaments :
        filament.solve()

    #print("after lateral", filaments[0], filaments[0].qdots)

    qdots = np.array([])
    for filament in filaments :
        qdots = np.concatenate((qdots, filament.qdots), axis=None)


    #lateral_qdots = np.array( lateral_qdots )

    #qdots = growth_qdots #+ lateral_qdots
    realCall = False
    tock = time.time()
    #print("One interation time", tock-tick)
    #print("timeStep", dt)
    #print("check bindingSites0", [len(filaments[0].bindingSites[i]) for i in range(len(filaments[0].bindingSites))] )
    #print("check bindingSites1", [len(filaments[1].bindingSites[i]) for i in range(len(filaments[1].bindingSites))] )
    return qdots


################################################################################
# ODE SOLVER
################################################################################

def main(endTime, numRods, growthRate, angle, mu0, nu0, epsilon0, kb0, width, kon, kOff0, ks, Lmax, Lc, Fc) :
    """ simulation C. Perfringen bacteria """

    global lambdaVals
    global realCall
    global lastAddTime
    global filaments
    global dt

    t = 0

    # determine the name of the python file, remove '.py' and add '.pkl'
    #paramString = "_mu" + str(mu0) + "_nu" + "{:.2e}".format(nu0) + "_ep" + str(int(epsilon0)) + "_kb" + str(kb0) + "_angle" + str(angle) + "_endTime" + str(t + endTime)
    paramString = "_ks" + str(ks) + "_kon" + str(kon) + "_kOff" + str(kOff0) + "_endTime" + str(t + endTime)
    outputFile = "data.dir/" + os.path.basename(__file__)[:-3] + paramString + ".pkl"
        # "data.dir/" puts it in the data.dir directory

    # constants ----------------------------------------------------------------

    l0 =  5                         # initial length of the rigid rods
    #totalLength0 = numRods * l0     # initial total length of filament
    #totalLength0 = (len(q0)-2) * l0
    #totalLength0Other = (len(qOther)-2) * l0

    # integration --------------------------------------------------------------

    q0 = q1 + q2
    q0 = np.array(q0)

    sol = []                        # array for the data
    sol.append( [t, [filaments[0].nodePos, filaments[1].nodePos] ] )

    #solOther = []                        # array for the data
    #solOther.append([t, *qOther])

    addTimeInterval = l0 / growthRate


    while t < endTime :

        print(t < endTime)

        if endTime <= (t + addTimeInterval) :
            nextStopTime = endTime
            addNodesFlag = False
        else:
            nextStopTime = t + addTimeInterval
            addNodesFlag = True

        print("Adding Nodes?", addNodesFlag)
        if addNodesFlag : print("ADDING NODES AT t =", t + addTimeInterval)

        params = growthRate, kb0, mu0, nu0, epsilon0, width, kon, kOff0, ks, Lmax, Lc, Fc

        backend = 'vode'
        #backend = 'dopri5'
        # backend = 'dop853'
        solver = ode(dqdt).set_integrator(backend)
        #solver = ode( lambda time,vars:dqdt(time,vars,*params) ).set_integrator(backend)
        #solverOther = ode(dqdt).set_integrator(backend)

        #print("q0 =", q0)
        #solver.set_initial_value(q0, t).set_f_params(l0, growthRate, kb0, mu0, nu0, epsilon0, totalLength0)
        solver.set_initial_value(q0, t).set_f_params(growthRate, kb0, mu0, nu0, epsilon0, width, kon, kOff0, ks, Lmax, Lc, Fc)
        #solver.set_initial_value(q0, t)
        #solverOther.set_initial_value(qOther, t).set_f_params(q0, l0_other, l0, growthRate, kb0, mu0, nu0, epsilon0, totalLength0)


        #solver.integrate(nextStopTime)

        print("StopTime", nextStopTime)
        #while ( solver.successful() and solverOther.successful() ) and solver.t < nextStopTime :
        while solver.successful() and solver.t < nextStopTime :

            realCall = True

            solver.integrate(endTime, step=True)
            dt = solver.t - t

            # update each filament's attributes based on the new q solutions -----------------------
            # store the node positions in an array and save these positions

            solutions = []

            qLengths = [ len( filament.q ) for filament in filaments ]
            for i in range( len(qLengths) ) :
                #print("Updating Filament", filaments[i] )
                #lt = length(t, filaments[i].l0, growthRate)
                if i == 0 :
                    qNew = solver.y[ : qLengths[0] ]
                else :
                    qNew = solver.y[ qLengths[i-1] : qLengths[i-1] + qLengths[i] ]
                filaments[i].qUpdate( qNew )
                filaments[i].updateNodesPos()
                filaments[i].updateCom()
                filaments[i].updateBindingSites(dt)
                solutions.append(filaments[i].nodePos)

            sol.append( [solver.t, solutions] )

            #---------------------------------------------------------------------------------------

            #print([solver.t, *solver.y])
            print("Time =", solver.t, "\t dt =", dt)

            #t = sol[-1][0]
            t = solver.t

        #addNodesFlag = False      # uncomment to turn off cell divsion feature
        if addNodesFlag :

            lastAddTime = t

            print("BEGIN ADDING NODES")

            for filament in filaments :
                filament.cellDivision()

            lOld = l0 #length(t, l0, growthRate)
            print("length before addition", length(t, lOld, growthRate))
            lNew = lOld / 2
            #totalLength = (len(q0) - 2) * lNew

            muOld = dragCoefficient(t, mu0, lNew, lOld)
            nuOld = dragCoefficient(t, nu0, lNew, lOld)
            epsilonOld = rotDragCoefficient(t, epsilon0, lNew, lOld)

            mu0 = muOld / 2
            nu0 = nuOld / 2
            epsilon0 = epsilonOld / 8

            """q0 = [ q[0] - lNew * np.cos(q[2]), q[1] - lNew * np.sin(q[2])]
            for i in range( 2 , len(q) ) :
                q0.append(q[i])
                q0.append(q[i])"""

            q0 = np.concatenate( (filaments[0].q, filaments[1].q) , axis=None )

            #l0 = lNew
            print("length after addition", length(t, lNew, growthRate))
            #totalLength0 = l0 * len(q0)
            #numRods *= 2

            print("DONE ADDING NODES")

    print("Integration Complete")

    """t, x, y = [], [], []
    l0 = 5
    lastNumRods = len(sol[0][3:])
    lastAddTime = 0
    lastTime = 0
    for s in sol :

        t.append(s[0])

        xVals, yVals = [], []

        #l0 = math.floor(s[0] % addTimeInterval)
        if lastNumRods < len(s[3:]) :
            lastNumRods = len(s[3:])
            lastAddTime = lastTime
        lastTime = s[0]


        l = [length(s[0], l0, growthRate)] * int( len(s[3:]) )

        x0 = s[1]
        y0 = s[2]
        theta0 = s[3]

        xVals.append( x0 - (l[0] * np.cos(theta0))/2 )
        xVals.append( x0 + (l[0] * np.cos(theta0))/2 )

        yVals.append( y0 - (l[0] * np.sin(theta0))/2 )
        yVals.append( y0 + (l[0] * np.sin(theta0))/2 )

        i = 1
        for theta in s[4:] :
            xVals.append( xVals[-1] + l[i] * np.cos(theta) )
            yVals.append( yVals[-1] + l[i] * np.sin(theta) )
            i += 1

        x.append( xVals )
        y.append( yVals )

    data = [t, x, y, lambdaVals]"""

    #pickle.dump(data, open(outputFile, "wb"))
    pickle.dump(sol, open(outputFile, "wb"))

    plotsV4.configurationPlot(outputFile)           # generate configuration plot
    #plotsV4.constraintForcePlot(outputFile)         # generate constraint force plots

    #os.remove("equations.pkl")  # remove equation file, you don't need it anymore

    return


################################################################################


startTime = time.clock()

dt = 0

mu0 = 1e-9
#muVals = 0.000000001
kb0 = 1e-7#[1e-8, 5e-8, 1e-7, 5e-7, 1e-6, 5e-6, 1e-5]
nu0 = 100*mu0#[100*mu0, 1000*mu0] # [2*mu0, 5*mu0, 10*mu0,

growthRate = 0.2        # rate of cell growth (micrometers/minutes)

endTime = 27            # total simulation time
numRods = 5             # initial number of rods
angle = 90              # angle subtended by the initial filament configuration (in degrees)

#mu0 = 1e-9             # initial parallel drag coefficient
#kb0 = 10000000         # initial angular spring constant

#for nu0 in nuVals :
    #for kb0 in kbVals :

#nu0 = mu0 * 100        # initial perpendiculat drag coefficient
epsilon0 = nu0 / 10     # initial angular drag coefficient

width = 0.5             # cell width (micrometers)
kon = 33             # binding site association rate
kOff0 = 5               # dissociation rate for bonded sites
ks = 1e-8               # spring constant between two bonded sites
Lmax = 2 * width        # maximum length at which a bond can form
Lc = 2.5 * width        # characteristic distance between two bonded sites
Fc = ks * (Lc - width)  # characteristic force between binding sites

lambdaVals = [None]     # none since the inital lambda values are unknown
realCall = False        # flag used to determine if a call to dqdt is real
lastAddTime = 0         # time of the last cell division

#q1 = [0, 0, 0, np.pi/8, np.pi/4]
#q2 = [0, -2.5, 0, np.pi/8, np.pi/4]
#q2 = [0, -3, 0, np.pi/6, np.pi/4, np.pi/3]
#q2 = [3, -3, -np.pi/8, -np.pi/16, np.pi/10, np.pi/6]

# similar curvature
#q1 = [0, 0, 0, np.pi/6, np.pi/4, np.pi/3]
#q2 = [0.5, -1, 0, np.pi/6, np.pi/4, np.pi/3, np.pi/2.5]

#straight configuration
q1 = [0, 0, 0, 0, 0, 0, 0]
q2 = [-1, -3, 0, 0, 0, 0, 0, 0]

filaments = [Filament(q1, 5, 9, growthRate), Filament(q2, 5, 9, growthRate)]

print("NEW CALL:", nu0, kb0)
main(endTime, numRods, growthRate, angle, mu0, nu0, epsilon0, kb0, width, kon, kOff0, ks, Lmax, Lc, Fc)

# Calulating runtime, output in form "xx hrs, xx mins, xx secs"
totalRuntime = time.clock()-startTime
mins = round(totalRuntime//60)
secs = round(totalRuntime % 60)
if mins >= 60 :
    hrs = round(mins // 60)
    mins = round(mins % 60)
    print( "Runtime =", hrs, "hrs", mins, "minutes", secs, "seconds" )
else : print( "Runtime =", mins, "minutes", secs, "seconds" )

print("Sweet Baby Jesus, IT WORKS!")


################################################################################
