#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 05/09/2019

import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import odeint
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
#import plotsV4
#import plotsV4_withSites
#from FilamentV9 import *


def fallingRod(q, t, m, l, rf, rd, rc, rt, yWall, alpha, beta, gamma, grav) :

    x0, y0, theta, v0x, v0y, omega = q

    x1 = x0 + l/2 * np.cos(theta)
    y1 = y0 + l/2 * np.sin(theta)

    x2 = x0 - l/2 * np.cos(theta)
    y2 = y0 - l/2 * np.sin(theta)

    print("x1, y1:", x1, y1, "\t x2, y2:", x2, y2)

    print("distance", np.sqrt( (x2 - x1)**2 + (y2 - y1)**2 ) )


    v0xdot = -alpha * v0x

    #v0ydot = -grav -(beta/m) * v0y + ( (0.25/m) * (1-np.tanh(rd * ( v0y + omega * (x-x0)))) * ( 1- np.tanh(rt * (y-yWall-rc))) )
    v0ydot = -grav -(beta/m) * v0y + ( (0.25/m) * rf * (1-np.tanh(rd * ( v0y + omega * (x1-x0)))) * ( 1- np.tanh(rt * (y1-yWall-rc))) ) + ( (0.25/m) * rf * (1-np.tanh(rd * ( v0y + omega * (x2-x0)))) * ( 1- np.tanh(rt * (y2-yWall-rc))) )

    #omegadot = -(12*gamma/(m * l**2)) * omega + ( (12/(m * l**2)) * (0.25/m) * (x-x0) * (1-np.tanh(rd * ( v0y + omega * (x-x0)))) * ( 1- np.tanh(rt * (y-yWall-rc))) )
    omegadot = -(12*gamma/(m * l**2)) * omega + ( (12/(m * l**2)) * 0.25 * rf *(x1-x0) * (1-np.tanh(rd * ( v0y + omega * (x1-x0)))) * ( 1- np.tanh(rt * (y1-yWall-rc))) ) + ( (12/(m * l**2)) * 0.25 * rf * (x2-x0) * (1-np.tanh(rd * ( v0y + omega * (x2-x0)))) * ( 1- np.tanh(rt * (y2-yWall-rc))) )

    dqdt = [v0x, v0y, omega, v0xdot, v0ydot, omegadot]

    return dqdt



def fallingRodElastic(q, t, m, l, rf, rd, rc, rt, yWall, alpha, beta, gamma, grav) :

    x0, y0, theta, v0x, v0y, omega = q

    x1 = x0 + l/2 * np.cos(theta)
    y1 = y0 + l/2 * np.sin(theta)

    x2 = x0 - l/2 * np.cos(theta)
    y2 = y0 - l/2 * np.sin(theta)

    #print("x1, y1:", x1, y1, "\t x2, y2:", x2, y2)

    #print("distance", np.sqrt( (x2 - x1)**2 + (y2 - y1)**2 ) )


    v0xdot = -alpha * v0x

    #v0ydot = -grav -(beta/m) * v0y + ( (0.25/m) * (1-np.tanh(rd * ( v0y + omega * (x-x0)))) * ( 1- np.tanh(rt * (y-yWall-rc))) )
    v0ydot = -grav -(beta/m) * v0y + ( (0.25/m) * rf * ( 1- np.tanh(rt * (y1-yWall-rc))) ) + ( (0.25/m) * rf * ( 1- np.tanh(rt * (y2-yWall-rc))) )

    #omegadot = -(12*gamma/(m * l**2)) * omega + ( (12/(m * l**2)) * (0.25/m) * (x-x0) * (1-np.tanh(rd * ( v0y + omega * (x-x0)))) * ( 1- np.tanh(rt * (y-yWall-rc))) )
    omegadot = -(12*gamma/(m * l**2)) * omega + ( (3/(m * l**2))  * rf *(x1-x0) * ( 1- np.tanh(rt * (y1-yWall-rc))) ) + ( (3/(m * l**2)) * rf * (x2-x0) * ( 1- np.tanh(rt * (y2-yWall-rc))) )

    dqdt = [v0x, v0y, omega, v0xdot, v0ydot, omegadot]

    return dqdt


def fallingRodPlastic(q, t, m, l, rf, rd, rc, rt, yWall, alpha, beta, gamma, grav) :

    x0, y0, theta, v0x, v0y, omega = q

    x1 = x0 + l/2 * np.cos(theta)
    y1 = y0 + l/2 * np.sin(theta)

    x2 = x0 - l/2 * np.cos(theta)
    y2 = y0 - l/2 * np.sin(theta)

    v0xdot = -alpha * v0x

    if ( v0y + omega * (x1-x0) ) < 0 :

        if ( v0y + omega * (x2-x0) ) < 0 :
            # both points moving down

            v0ydot = -grav -(beta/m) * v0y + ( (0.5/m) * rf * ( 1- np.tanh(rt * (y1-yWall-rc))) ) + ( (0.5/m) * rf * ( 1- np.tanh(rt * (y2-yWall-rc))) )

            omegadot = -(12*gamma/(m * l**2)) * omega + ( (6/(m * l**2))  * rf *(x1-x0) * ( 1- np.tanh(rt * (y1-yWall-rc))) ) + ( (6/(m * l**2)) * rf * (x2-x0) * ( 1- np.tanh(rt * (y2-yWall-rc))) )

        else :
            # only point 1 moving down

            v0ydot = -grav -(beta/m) * v0y + ( (0.5/m) * rf * ( 1- np.tanh(rt * (y1-yWall-rc))) )

            omegadot = -(12*gamma/(m * l**2)) * omega + ( (6/(m * l**2))  * rf *(x1-x0) * ( 1- np.tanh(rt * (y1-yWall-rc))) )


    elif ( v0y + omega * (x2-x0) ) < 0 :
        # only point 2 moving down

        v0ydot = -grav -(beta/m) * v0y + ( (0.5/m) * rf * ( 1- np.tanh(rt * (y2-yWall-rc))) )

        omegadot = -(12*gamma/(m * l**2)) * omega + ( (6/(m * l**2)) * rf * (x2-x0) * ( 1- np.tanh(rt * (y2-yWall-rc))) )

    else :
        # no points moving down
        v0ydot = -grav -(beta/m) * v0y
        omegadot = -(12*gamma/(m * l**2)) * omega

    dqdt = [v0x, v0y, omega, v0xdot, v0ydot, omegadot]

    return dqdt




def main() :

    s0 = 0.01 #1.0
    sc = 0.006 #0.6
    ri = 0.99
    rd = 4.0
    rf = 40.0
    rt = 2 * np.arctanh(ri) / (s0 - sc)
    rc = (s0 + sc) / 2

    xWall = 0.0

    l = 5
    m = 0.1
    grav = 9.8

    alpha = 0.5
    beta = 0.5
    gamma = 1

    t = np.linspace(0, 8, 201)

    # FALLING ROD TOY MODEL
    q0 = [0, 10, np.pi/6, 0, -5, 0]

    limit = ''
    while True not in [limit.lower() == 'intermediate', limit.lower() == "elastic", limit.lower() == 'plastic'] :
        limit = input("Enter a limit of 'Intermediate', 'Elastic', or 'Plastic' or 'exit to quit' : ")
        if limit.lower() == 'exit': exit()

    if limit.lower() == "elastic" :
        label = "Elastic"
        sol = odeint( fallingRodElastic, q0, t, args=(m, l, rf, rd, rc, rt, xWall, alpha, beta, gamma, grav))

    elif limit.lower() == "plastic" :
        label = "Plastic"
        sol = odeint( fallingRodPlastic, q0, t, args=(m, l, rf, rd, rc, rt, xWall, alpha, beta, gamma, grav))

    else :
        label = 'Intermediate'
        sol = odeint( fallingRod, q0, t, args=(m, l, rf, rd, rc, rt, xWall, alpha, beta, gamma, grav))

    print("Simulation complete.")
    print("Plotting...")

    import matplotlib.pyplot as plt
    from matplotlib.animation import FuncAnimation

    #plt.plot(t, sol[:, 0], 'b', label='$x(t)$')
    plt.plot(t, sol[:, 1], 'b', label='$y(t)$')
    plt.plot(t, sol[:, 2], 'g', label='$theta(t)$')
    #plt.plot(t, sol[:, 3], 'g', label='$v_x(t)$')
    plt.plot(t, sol[:, 4], 'r', label='$v_y(t)$')
    plt.plot(t, sol[:, 5], 'y', label='$omega(t)$')

    plt.legend(loc='best')
    plt.xlabel('t')
    plt.title("Falling Rod Dynamics ("+label+" Limit)")
    plt.grid()

    plt.savefig("plots.dir/rodAndWallCollision"+label+".eps", format='eps')
    #plt.show()


    fig, ax = plt.subplots(figsize=(4,6))
    ax.set(xlim=(-5,5), ylim=(-2, 15))
    ax.set_aspect('equal')

    x = sol[:,0]
    y = sol[:,1]
    theta = sol[:,2]


    scat = ax.plot([x[0] - 0.5 * l * np.cos(theta[0]), x[0] + 0.5 * l * np.cos(theta[0])], [y[0] - 0.5 * l * np.sin(theta[0]), y[0] + 0.5 * l * np.sin(theta[0])], '-o')
    ax.plot([-100, 100], [0,0] , '-ro')

    def animate(i):
        #line.set_ydata(F[i, :])
        x1 = x[i] - 0.5 * l * np.cos(theta[i])
        x2 = x[i] + 0.5 * l * np.cos(theta[i])

        y1 = y[i] - 0.5 * l * np.sin(theta[i])
        y2 = y[i] + 0.5 * l * np.sin(theta[i])

        ax.clear()
        ax.plot(0, 'r')
        ax.set(xlim=(-5,5), ylim=(-2, 15))
        ax.set_aspect('equal')
        ax.plot([-100, 100], [0,0] , '-ro')
        ax.plot([x1, x2], [y1, y2], '-o')

    anim = FuncAnimation(fig, animate, interval=100, frames=len(t)-1)
    anim.save("plots.dir/rodAndWallCollision"+label+".mp4")

    plt.grid()
    plt.draw()
    #plt.show()
    print("Complete!")


main()
