#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 09/07/2018

import numpy as np
import scipy as sp
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt
import pickle

from main import *


def generateFilaments(n) :
    """ generates n filaments and outputs them to a pickle file """

    np.random.seed(7)

    # used to define constants and stuff
    # totalTime = 175 min, option = 0, random = True
    dt, totalTime, frac, mu, avgDivLen, rateWithout, rateWith, option, random = initialization(60, 0, True)

    print(rateWithout)

    filaments = []
    for i in range(n) : filaments.append([avgDivLen/2])
    #filaments = [[avgDivLen/2]] * n    # initialize n arrays of cell lengths
    print(filaments)

    t = 0
    while t < totalTime :

        for l in filaments :

            for i in range( len(l) ) :
                growthRate = getGrowthRate(rateWithout, frac, random)
                l[i] += growthRate * dt

                if random :
                    if l[i] >= np.random.normal(avgDivLen, 0.1  * avgDivLen) :
                        newLength = l[i] / 2       # determine the length of the daughter cells
                        l[i] = newLength
                        l.insert(i+1, newLength)   # inset new cell
                else:
                    if l[i] >= avgDivLen :
                        newLength = l[i] / 2       # determine the length of the daughter cells
                        l[i] = newLength
                        l.insert(i+1, newLength)   # inset new cell

        print(t)
        #print(t, [len(l) for l in filaments])
        t += dt

    print([sum(l) for l in filaments])
    print([len(l) for l in filaments])
    #for l in filaments : print(l)

    pickle.dump(filaments, open("filaments" + str(n) + ".pkl", "wb"))

generateFilaments(100)
