#!/bin/bash

angle=20
while [ $angle -le 80 ]
do
    echo $angle
    python exponentialGrowth.py ${angle}
    python newPlotting.py
    mkdir plots.dir/angle${angle}_broken.dir
    mv plots.dir/*.pdf plots.dir/angle${angle}_broken.dir
    mv plots.dir/*.mp4 plots.dir/angle${angle}_broken.dir
    (( angle= angle+20 ))
done
