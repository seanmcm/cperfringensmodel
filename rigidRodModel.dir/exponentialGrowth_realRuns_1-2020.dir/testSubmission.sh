#!/bin/bash

# test slurm submission for simulation
# 1/13/2020

# RESOURCES #
#SBATCH --nodes=1

# WALLTIME #
#SBATCH -t 00:10:00

# QUEUE #
#SBATCH -p dev_q

# ALLOCATION #
#SBATCH -A CPMot

# MODULES #
module purge

# modules need to load ffmpeg
module load gcc/5.2.0
module load yasm/1.3
module load x264/1.0
module load fdk-aac/1.0
module load lame/3.99.5
module load ffmpeg/2.5.4

module load Anaconda


cd $SLURM_SUBMIT_DIR
python exponentialGrowth.py 20 1e-7
python newPlotting.py

exit;

