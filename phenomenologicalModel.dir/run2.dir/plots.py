#!/usr/local/env python

# S.G McMahon
# C. Perfringens two filament growth model
# 08/30/2018

import numpy as np
import scipy as sp
import matplotlib
import random as rand
import matplotlib as mpl
import matplotlib.pyplot as plt

margins = [-5.0, -2.5, -1.25, 0, 1.25, 2.5, 5]

force = [14, 36, 34, 47, 60, 63, 75]

neighborPromote = [59, 81, 82, 84, 100, 100, 100]

neighborSuppress = [18, 36, 46, 53, 65, 72, 85]

standard = [75, 84, 89, 87, 99, 96, 96]


plt.plot(margins, standard, '-ro', margins, neighborSuppress, '-go', margins, neighborPromote, '-yo', margins, force, '-bo')
plt.show()
