#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import optimize
from scipy.interpolate import interp1d
from scipy import interpolate
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys
import pandas as pd
import seaborn as sns


####################################################################################################
# Classes
####################################################################################################

class ComputeCurvature:
    def __init__(self):
        """ Initialize some variables """
        self.xc = 0  # X-coordinate of circle center
        self.yc = 0  # Y-coordinate of circle center
        self.r = 0   # Radius of the circle
        self.xx = np.array([])  # Data points
        self.yy = np.array([])  # Data points

    def calc_r(self, xc, yc):
        """ calculate the distance of each 2D points from the center (xc, yc) """
        return np.sqrt((self.xx-xc)**2 + (self.yy-yc)**2)

    def f(self, c):
        """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
        ri = self.calc_r(*c)
        return ri - ri.mean()

    def df(self, c):
        """ Jacobian of f_2b
        The axis corresponding to derivatives must be coherent with the col_deriv option of leastsq"""
        xc, yc = c
        df_dc = np.empty((len(c), self.xx.size))

        ri = self.calc_r(xc, yc)
        df_dc[0] = (xc - self.xx)/ri                   # dR/dxc
        df_dc[1] = (yc - self.yy)/ri                   # dR/dyc
        df_dc = df_dc - df_dc.mean(axis=1)[:, np.newaxis]
        return df_dc

    def fit(self, xx, yy):
        self.xx = xx
        self.yy = yy
        center_estimate = np.r_[np.mean(xx), np.mean(yy)]
        center = optimize.leastsq(self.f, center_estimate, Dfun=self.df, col_deriv=True)[0]

        self.xc, self.yc = center
        ri = self.calc_r(*center)
        self.r = ri.mean()

        return 1 / self.r  # Return the curvature

    def circleFit(self, xx, yy):
        self.xx = xx
        self.yy = yy
        center_estimate = np.r_[np.mean(xx), np.mean(yy)]
        center = optimize.leastsq(self.f, center_estimate, Dfun=self.df, col_deriv=True)[0]

        self.xc, self.yc = center
        ri = self.calc_r(*center)
        self.r = ri.mean()

        return self.r, self.xc, self.yc   # return the properties of the best fit circle


####################################################################################################
# Filament Dynamics Functions
####################################################################################################

def length(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return l0 * np.exp( (growthRate/l0) * (t - lastAddTime) )


def growthRateFunc(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return growthRate * np.exp( (growthRate/l0) * (t - lastAddTime) )

#---------------------------------------------------------------------------------------------------

def growthDynamics(t, q, lt, kb, mu, nu, epsilon, growthRate) :
    """ updates the qdots for the filament based only on growth dynamics
            l (array) : lengths of the cells in the Filaments
            lp (array) : growth rates for each cell
            kb (float) : angular spring constant
            mu (float) : parallel drag coefficient
            nu (float) : perpendiular drag coefficient
            epsilion (float) : rotational drag coefficient """

    l = [lt] * ( len(q) - 2 )
    lp = [ growthRate ] * ( len(q) - 2 )

    x0 = q[0]
    y0 = q[1]
    thetas = q[2:]

    numRods = len(thetas)
    numNodes = numRods + 1

    #totalLength = sum(l)

    #--------------------------------------------------------

    xEqns, yEqns, thetaEqns = [], [], []

    # x_i Euler-Lagrange equations --------------------------------------------------------

    # initialize x_0 E-L equation and x E_L equation array
    xEqn0 = np.zeros(3*numRods)
    xEqn0[0] = ( mu * np.cos(thetas[0])**2 + nu * np.sin(thetas[0])**2 ) * l[0]
    xEqn0[1] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    if numRods != 1 : xEqn0[numRods + 2] = -1
    xEqns.append(xEqn0)

    # initialize y_0 E-L equation and y E_L equation array
    yEqn0 = np.zeros(3*numRods)
    yEqn0[0] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    yEqn0[1] = ( mu * np.sin(thetas[0])**2 + nu * np.cos(thetas[0])**2 ) * l[0]
    if numRods != 1 : yEqn0[2*numRods + 1] = -1
    yEqns.append(yEqn0)

    # initialize theta_0 E-L equation and theta E_L equation array
    thetaEqn0 = np.zeros(3*numRods)
    thetaEqn0[2] = epsilon * l[0]**3
    if numRods != 1 :
        thetaEqn0[numRods + 2] = 0.5 * l[0] * np.sin(thetas[0])
        thetaEqn0[2*numRods + 1] = -0.5 * l[0] * np.cos(thetas[0])
    thetaEqns.append(thetaEqn0)

    #generate the 2 through N-1 x, y, and theta E-L equations
    for i in range(1, numRods) :

        # x_i E_L equations -----------------------------------------------------------

        xEqni = np.zeros(3*numRods)

        xEqni[0] = ( mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[i]

        xEqni[1] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * np.cos(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[0] * l[i] * np.sin(thetas[0]) )

        for j in range(1, i) :
            xEqni[j+2] = (mu - nu) * l[j] * l[i] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[j] * l[i] * np.sin(thetas[j])

        xEqni[i+2] = -0.5 * nu * l[i]**2 * ( np.sin(thetas[i]) * np.cos(thetas[i])**2 + np.sin(thetas[i])**3 )

        xEqni[i + numRods + 1] = 1

        if i != (numRods-1) :
            xEqni[i + numRods + 2] = -1

        xEqns.append(xEqni)


        # y_i E_L equations -----------------------------------------------------------

        yEqni = np.zeros(3*numRods)

        yEqni[0] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        yEqni[1] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[i]

        yEqni[2] = 0.5 * ( (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[0] * l[i] * np.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * np.sin(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) )

        for j in range(1, i) :
            yEqni[j+2] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[j] * l[i] * np.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])

        yEqni[i+2] = 0.5 * nu * l[i]**2 * (np.cos(thetas[i]) * np.sin(thetas[i])**2 + np.cos(thetas[i])**3)

        yEqni[i + (2*numRods)] = 1

        if i != (numRods-1) :
            yEqni[i + (2*numRods) + 1] = -1

        yEqns.append(yEqni)


        # theta_i E_L equations -----------------------------------------------------------

        thetaEqni = np.zeros(3*numRods)

        thetaEqni[i+2] = epsilon * l[i]**3

        thetaEqni[i + numRods + 1] = 0.5 * l[i] * np.sin(thetas[i])

        thetaEqni[i + (2*numRods)] = -0.5 * l[i] * np.cos(thetas[i])

        if i != (numRods-1) :
            thetaEqni[i + numRods + 2] = 0.5 * l[i] * np.sin(thetas[i])
            thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * np.cos(thetas[i])

        thetaEqns.append(thetaEqni)


        # stress calculations --------------------------------------------------------------
        #self.stressX.append( 0.5 * ( l[i-1]*np.cos(thetas[i-1]) - l[i]*np.sin(thetas[i]) ) )
        #self.stressY.append( 0.5 * ( -l[i-1]*np.sin(thetas[i-1]) + l[i]*np.cos(thetas[i]) ) )

    # combine all E-L equation coeffcients into a single array
    eqns = np.array( xEqns + yEqns + thetaEqns )

    # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
    #   are standard arrays
    #print(eqns)
    #input("stopza")

    if numRods == 1 : xVals, yVals, thetaVals = [0.0], [0.0], [0.0]
    else : xVals, yVals, thetaVals = [0.0], [0.0], [-kb * (thetas[0]-thetas[1])]

    for i in range(1, numRods) :

        xVal = -(mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i]))
        for j in range(1,i) :
            xVal -= (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] * lp[j] * np.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        xVals.append(xVal)

        yVal = -(mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i]))
        for j in range(1,i) :
            yVal -= (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] * lp[j] * np.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        yVals.append(yVal)

        if i == (numRods-1) :
            thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
        else :
            thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
        thetaVals.append(thetaVal)

    vals = np.array( xVals + yVals + thetaVals )


    numRods = len(q)-2

    sols = np.linalg.solve(eqns, vals)

    return sols


####################################################################################################
# Analysis and plotting
####################################################################################################

def subplot_index(psi, a) :
    """ Calculates the indicies of subplot corresponding to (psi, a)

        Inputs: psi -- float -- the value of the parameter psi = kb / (mu l0^3 r)
                a   -- float -- the value of the parameter a = nu / mu

        returns row, column (both integers) """

    # assumes psi range is [1, 2, 5, 10, 20, 50, 100]
    dict = {
        1 : 6,
        2 : 5,
        5 : 4,
        10 : 3,
        20 : 2,
        50 : 1,
        100 : 0
    }
    row = dict[psi]

    # assumes a range is [1, 2, 5, 10, 20, 50, 100]
    dict = {
        1 : 0,
        2 : 1,
        5 : 2,
        10 : 3,
        20 : 4,
        50 : 5,
        100 : 6
    }
    col = dict[a]

    return row, col

#---------------------------------------------------------------------------------------------------

def stressOrganization(simulationSets) :

    newSets = []

    for s in simulationSets :

        # grab the parameter values and subplot indicies
        params, plotParams, d = s
        psi, a, b, numRods_init, angle = params
        row, col = subplot_index(psi, a)

        qVecs = list(d["q"])

        stressXs, stressYs = [], []
        stressMags = []
        stressMeans = []
        stressParallels = []
        for i in range(0, len(d["t"]), 1) :

            # isolate theta values and compute deltaTheta values
            thetas = qVecs[i][2:]
            deltaThetas = [thetas[i] - thetas[i-1] for i in range(1, len(thetas))]

            # isloate the stress data sets
            stressX, stressY = list(d["Fx"][i]), list(d["Fy"][i])

            # compute the stress parallel to the current node
            stressParallel = [ abs( stressX[j] * np.cos( deltaThetas[j]/2 ) + stressY[j] * np.sin(deltaThetas[j]/2) ) for j in range(len(stressX)) ]

            stressParallels.append(stressParallel)

            # add in stress at tips (fx, fy = 0)
            stressX = np.insert(stressX, [0, len(stressX)], [0,0])
            stressY = np.insert(stressY, [0, len(stressY)], [0,0])

            stressXs.append(stressX)
            stressYs.append(stressY)

            # compute the magnitudes of the stresses
            stress = [ np.sqrt(stressX[j]**2 + stressY[j]**2) for j in range(len(stressX)) ]

            stressMags.append(stress)
            stressMeans.append( np.mean(stress) )

        d['Fx'] = stressXs
        d['Fy'] = stressYs
        d["Fmag"] = stressMags
        d["Favg"] = stressMeans
        d["Fparallel"] = stressParallels

        newSets.append( (params, plotParams, d) )

    return newSets

#---------------------------------------------------------------------------------------------------

def configurationPlot(simulationSets) :

    print("\tCONFIGURATION PLOTS...")

    for s in simulationSets :
        #s = simulationSets[0]

        params, plotParams, d = s

        t = list(d["t"])

        psi, a, b, numRods_init, angle = params

        minX, maxX, minY, maxY = plotParams
        minAx = round( min( [minX, minY] ) - 5 )
        maxAx = round( max( [maxX, maxY] ) + 5 )

        nodeX, nodeY = d["xNode"], d["yNode"]

        comp_curv = ComputeCurvature()
        curvatures = []

        #plt.figure(1)
        minAx = round( min( [minX, minY] ) - 5 )
        maxAx = round( max( [maxX, maxY] ) + 5 )
        #axesRange = [round(minAx-5), round(maxAx+5), round(minAx-5), round(maxAx+5)]

        fig = plt.figure(1)
        l1, = plt.plot([], [], '-bo')       # filament 1 nodes / cells
        l2, = plt.plot([], [], '-r')       # filament 2 nodes / cells
        #l3, = plt.plot([], [], 'go')        # filament 1 binding sites
        #l4, = plt.plot([], [], 'go')        # filament 2 binding sites

        plt.axis('scaled')

        plt.xlabel(r"$x$ coordinate ($\mathrm{\mu}$m)")
        plt.ylabel(r"$y$ coordinate ($\mathrm{\mu}$m)")

        plt.xlim(minAx, maxAx)
        plt.ylim(minAx, maxAx)

        plt.xticks( plt.yticks()[0][1:-1] )

        framesPerSec = 10
        #framesPerSec = round(len(data)/data[-1][0]*10)
        #print("fps =", framesPerSec)

        # sets up video output stuff
        FFMpegWriter = manimation.writers['ffmpeg']
        metadata = dict(title='Movie Test', artist='Matplotlib',
                        comment='Movie support!')
        writer = FFMpegWriter(fps=framesPerSec, metadata=metadata)

        # remember to only use on .csv files in the data.dir directory
        # Splicing [8:-4] removes the "data.dir" at the front and ".csv" at the end
        #outfile = "plots.dir" + str(sys.argv[1])[8:-4] + ".mp4"
        #outfile = "plots.dir" + filename[8:-4] + ".mp4"
        outfile = f"{output}/configurations/configuration_psi{round(psi,0)}_a{round(a,0)}_N{numRods_init}.mp4"
        #print(outfile)
        count = 0

        with writer.saving(fig, outfile, 100):
        # third arugment is dpi --> controls resolution and size of the plot
            """for d in data :

                #if count % 20 == 0 :  # indent the 3 lines below and uncomment this line
                plt.title("Filament Configuration at t = " + str(round(d[0],2)) )
                l1.set_data(d[1], d[2])
                #l2.set_data(d[3], d[4])

                writer.grab_frame()

                count+=1
                #plt.savefig('plots.dir/snapshots.dir/rigid-rod'+ str(round(d[0],0))+'.svg', format='svg')"""

            for i in range(0, len(t), 1) :
                print(i)
                plt.title(rf"Chain Configuration at t = {str(round(t[i]))} min ($\Psi$ = {psi})")
                l1.set_data(nodeX[i], nodeY[i])

                # compute curvature
                numMiddle = 7   # number of nodes in the "middle"; always use even numCells so this should be odd
                numNodes = len(nodeX[i])
                index = numMiddle + ( (numNodes - numMiddle) // 2 )
                x = np.array( nodeX[i][-index:index] )
                y = np.array( nodeY[i][-index:index] )
                radius, xc, yc = comp_curv.circleFit(x, y)

                theta_circ = np.linspace(0, 2*np.pi, 150)
                x_circ = xc + radius * np.cos(theta_circ)
                y_circ = yc + radius * np.sin(theta_circ)

                #l2.set_data( x_circ, y_circ )
                #l2.set_data( plt.Circle( xy=(xc,yc), radius=radius, fill=False ) )

                #chain_curve = interp1d(nodeX[i], nodeY[i], kind='cubic')
                #xnew = np.linspace(min(nodeX[i]), max(nodeX[i]), num=100, endpoint=True)
                #l2.set_data( xnew, chain_curve(xnew) )

                numRods = len(nodeX)-1
                #zeta = [ i * numRods // 2 for in range(-numRods//2, numRods//2+1) ]
                #zeta = np.arange( -1, 1 + (2/numRods), 2/numRods )
                tck, u = interpolate.splprep( [nodeX[i], nodeY[i]], s=0 )
                unew = np.linspace(0, 1, num=1000, endpoint=True)
                out = interpolate.splev(unew, tck, ext=0)
                l2.set_data( out[0], out[1] )

                # METHOD 1: Use built in spline derivative
                dout = interpolate.splev(unew, tck, der=1)
                kappa_der1 = np.sqrt(dout[0]**2 + dout[1]**2) - np.sqrt(dout[0][0]**2 + dout[1][0]**2)

                ddout = interpolate.splev(unew, tck, der=2)
                kappa_der2 = np.sqrt(ddout[0]**2 + ddout[1]**2) - np.sqrt(ddout[0][0]**2 + ddout[1][0]**2)

                # METHOD 2: compute directly by differentiation
                # Curvature computation source:
                # https://www.delftstack.com/howto/numpy/curvature-formula-numpy/

                # first derivatives
                dx = np.gradient(out[0])
                dy = np.gradient(out[1])

                # second derivatives
                ddx = np.gradient(dx)
                ddy = np.gradient(dy)

                # curvature computation
                kappa_comp = (ddx * dy - dx * ddy) / (dx * dx + dy * dy)**1.5

                """vel = np.array( [ [dx[i], dy[i]] for i in range(dx.size) ] )
                speed = np.sqrt(dx * dx + dy * dy)
                tangent = np.array([1/speed] * 2).transpose() * vel"""

                curvatures.append((kappa_der1, kappa_der2, kappa_comp))

                writer.grab_frame()

                # use the next line to save indivdual frames if desired
                #if count == 0 or int(round(t[i],0)) == 90 :
                #    plt.savefig('plots.dir/configuration' + str(round(t[i],0))+'.pdf', format='pdf')

                count+=1


        plt.gcf().clear()


        def align_zeros(axes):
            """ https://stackoverflow.com/questions/55646777/matplotlib-aligning-two-y-axis-around-zero """

            ylims_current = {}   #  Current ylims
            ylims_mod     = {}   #  Modified ylims
            deltas        = {}   #  ymax - ymin for ylims_current
            ratios        = {}   #  ratio of the zero point within deltas

            for ax in axes:
                ylims_current[ax] = list(ax.get_ylim())
                                # Need to convert a tuple to a list to manipulate elements.
                deltas[ax]        = ylims_current[ax][1] - ylims_current[ax][0]
                ratios[ax]        = -ylims_current[ax][0]/deltas[ax]

            for ax in axes:      # Loop through all axes to ensure each ax fits in others.
                ylims_mod[ax]     = [np.nan,np.nan]   # Construct a blank list
                ylims_mod[ax][1]  = max(deltas[ax] * (1-np.array(list(ratios.values()))))
                                # Choose the max value among (delta for ax)*(1-ratios),
                                # and apply it to ymax for ax
                ylims_mod[ax][0]  = min(-deltas[ax] * np.array(list(ratios.values())))
                                # Do the same for ymin
                ax.set_ylim(tuple(ylims_mod[ax]))

        plt.rcParams['axes.grid'] = False

        for i in range( len(curvatures) ) :

            kappa_der1, kappa_der2, kappa_comp = curvatures[i]

            fig, ax = plt.subplots()
            plt.title(f"Curvature Profile at t = {round(t[i],0)} mins")

            zeta = np.linspace(0, 1, num=len(kappa_comp), endpoint=True)
            ax.plot(zeta, kappa_comp, '-r', label='Formula')
            ax.hlines(y=0, xmin=min(zeta), xmax=max(zeta), colors='k')

            #ax2 = ax.twinx()
            #zeta = np.linspace(0, 1, num=len(kappa_der2), endpoint=True)
            #ax2.plot(zeta, kappa_der2, '-b', label='2nd derivative')

            #align_zeros([ax, ax2])

            #plt.legend(loc='best')

            plt.xlabel(r"Location $q$")
            plt.ylabel(r"Curvature $\kappa$")

            plt.savefig(f'{output}/curvatureProfiles/psi{str(round(psi))}_N{numRods_init}.dir/time{str(round(t[i],0))}.png', format='png')

            plt.gcf().clear()
            plt.close()

    print("\t\t Done!")


#---------------------------------------------------------------------------------------------------


def configurationPlotGrid(simulationSets) :

    print("\tCONFIGURATION PLOT GRID...")

    comp_curv = ComputeCurvature()

    # initialize the subplot
    fig, axs = plt.subplots(7,7, sharex=False, sharey=False, figsize=(24,18))

    count = 0   # counter to track how many frames are complete

    t = list(simulationSets[0][2]["t"])
    for i in range(0, len(t), 1) :
        print(i)
        fig.suptitle(rf"Chain Configurations at t = {str(round(t[i]))} min", fontsize=48)

        cols = ['{}'.format(col) for col in [1, 2, 5, 10, 20, 50, 100]]
        rows = ['{}'.format(row) for row in [100, 50, 20, 10, 5, 2, 1]]

        for s in simulationSets :

            params, plotParams, d = s

            psi, a, b, numRods_init, angle = params

            minX, maxX, minY, maxY = plotParams
            minAx = round( min( [minX, minY] ) - 5 )
            maxAx = round( max( [maxX, maxY] ) + 5 )

            nodeX, nodeY = d["xNode"], d["yNode"]

            row, col = subplot_index(psi, a)
            ax = axs[row, col]
            ax.clear()
            ax.axis('scaled')
            #ax.set_xlabel(r"$x$ ($\mathrm{\mu}$m)", fontsize=20)
            #ax.set_ylabel(r"$y$ ($\mathrm{\mu}$m)", fontsize=20)

            ax.set_xlim(minAx, maxAx)
            ax.set_ylim(minAx, maxAx)

            ax.set_xticks( ax.get_yticks()[1:-1] )
            #ax.tick_params(axis='both', which='major', labelsize=20)

            # compute curvature and circle data
            numMiddle = 7   # number of nodes in the "middle"; always use even numCells so this should be odd
            numNodes = len(nodeX[i])
            index = numMiddle + ( (numNodes - numMiddle) // 2 )
            x = np.array( nodeX[i][-index:index] )
            y = np.array( nodeY[i][-index:index] )
            radius, xc, yc = comp_curv.circleFit(x, y)

            theta_circ = np.linspace(0, 2*np.pi, 150)
            x_circ = xc + radius * np.cos(theta_circ)
            y_circ = yc + radius * np.sin(theta_circ)

            ax.plot(nodeX[i], nodeY[i], '-bo')      # plot the configuration
            ax.plot(x_circ, y_circ, '-r')           # plot the fitted circle

        #plt.text(x=-3, y=5, s=r"$a$", fontsize=48)
        #plt.text(x=0, y=0, s=r"$a$", fontsize=48)

        pad = 5 # in points

        for ax, col in zip(axs[-1], cols):
            ax.annotate(rf"$a$ = {col}", xy=(0.5, -0.5), xytext=(0, pad),
                        xycoords='axes fraction', textcoords='offset points',
                        size=32, ha='center', va='baseline')

        for ax, row in zip(axs[:,0], rows):
            ax.annotate(rf"$\Psi$ = {row}", xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                        xycoords=ax.yaxis.label, textcoords='offset points',
                        size=32, ha='right', va='center')

        """axs[0,0].annotate(r"$a$", xy=(0.5, -0.5), xytext=(0, pad),
                    xycoords='axes fraction', textcoords='offset points',
                    size=48, ha='center', va='baseline')
        axs[0,0].annotate(r"$\Psi$", xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size=48, ha='right', va='center')"""

        plt.savefig(f'{output}/configurationGrid/time{str(round(t[i],0))}.png', format='png')

        count+=1


    plt.gcf().clear()

    print("\t\t Done!")


#---------------------------------------------------------------------------------------------------

def curvatureProfile(simulationSet) :

    print("\tDELTA THETA PROFILE GRID...")

    comp_curv = ComputeCurvature()

    # initialize the subplot
    fig, axs = plt.subplots(7,7, sharex=False, sharey=False, figsize=(24,18))

    count = 0   # counter to track how many frames are complete

    for s in simulationSets:
        params, plotParams, d = s
        d["avgCurvature"] = np.zeros( len(d["t"]) )

    t = list(simulationSets[0][2]["t"])
    for i in range(0, len(t), 1) :
        print(i)
        fig.suptitle(rf"Curvature profiles at t = {str(round(t[i]))} min", fontsize=48)

        for s in simulationSets :

            params, plotParams, d = s

            thetas = list(d["q"])[i][2:]
            deltaThetas = [ thetas[i+1] - thetas[i] for i in range(len(thetas)-1) ]

            psi, a, b, numRods_init, angle = params

            minX, maxX, minY, maxY = plotParams
            minAx = round( min( [minX, minY] ) - 5 )
            maxAx = round( max( [maxX, maxY] ) + 5 )

            nodeX, nodeY = d["xNode"], d["yNode"]

            row, col = subplot_index(psi, a)
            ax = axs[row, col]
            ax.clear()
            #ax.axis('scaled')

            #ax.set_xticks( ax.get_yticks()[1:-1] )
            #ax.tick_params(axis='both', which='major', labelsize=20)

            q = [ j / (len(nodeX[i])-1) for j in range(1, len(nodeX[i])-1) ]
            curvatureVsQ = []
            for j in range(1, len(nodeX[i])-1) :
                x = np.array( nodeX[i][j-1:j+2] )
                y = np.array( nodeY[i][j-1:j+2] )
                kappa = comp_curv.fit(x,y)
                curvatureVsQ.append( np.sign(deltaThetas[j-1]) * kappa )

            ax.plot(q, curvatureVsQ, '-o')
            #ax.set_ylim(-0.3, 0.3)

            d["avgCurvature"][i] =  np.mean( np.abs( np.array(curvatureVsQ) ) )

        plt.savefig(f'{output}/curvatureProfiles/time{str(round(t[i],0))}.png', format='png')

        count+=1

    plt.gcf().clear()

    for i in range(0, len(t), 1) :
        print(i)

        dfs = [ s[2] for s in simulationSets ]
        # re-organize data frames by timestep
        seriesList = [ dfs[j].loc[i] for j in range(len(dfs)) ]
        t_df = pd.DataFrame(seriesList)
        #t_dfs.append(t_df)

        # get the current time
        time = list(t_df["t"])[0]

        fig, ax = plt.subplots(figsize=(12,9))

        matrix = t_df.pivot(index="psi", columns="a", values="avgCurvature")

        plt.title(r"Average curvature" + f"   t = {round(time)}", fontsize=18)
        plt.xlabel(r"$a$", fontsize=16)
        plt.ylabel(r"$\Psi$", fontsize=16)
        #lim = max([abs(minCurveRate), abs(maxCurveRate)])
        sns.heatmap(matrix, annot=matrix, cmap='RdYlGn', linewidth=0.30, ax=ax, vmin=0, vmax=0.15)
        ax.invert_yaxis()
        plt.savefig(f"{output}/avgCurvature/time{round(time,0)}.png")


    # initialize the subplot
    fig, axs = plt.subplots(7,7, sharex=False, sharey=False, figsize=(24,18))
    fig.suptitle(rf"Average curvature vs. time", fontsize=48)
    for s in simulationSets :

        params, plotParams, d = s
        psi, a, b, numRods_init, angle = params

        row, col = subplot_index(psi, a)
        ax = axs[row, col]

        ax.plot(d["t"], d["avgCurvature"], '-', lw=6)
        ax.set_ylim(0, 0.15)

    plt.savefig(f'{output}/avgCurvatureVsTime.png', format='png')
    plt.gcf().clear()

    print("\t\t Done!")

#---------------------------------------------------------------------------------------------------

def curvatureAnalysis(simulationSets) :

    # Data Analysis:
    #   - compute the curvature of the middle of the chain as a function of time and
    #   - compute deltaTheta and deltaThetaDot as a function of time for the center linkage

    #comp_curv = ComputeCurvature()

    minCurveDiff, maxCurveDiff = 1E100000, -1E100000
    minCurveRate, maxCurveRate = 1E100000, -1E100000
    minDeltaTheta, maxDeltaTheta = 1E100000, -1E100000
    minDeltaThetaDot, maxDeltaThetaDot = 1E100000, -1E100000

    newDfs = []
    for s in simulationSets :

        params, plotParams, d = s

        curvatures = list(d["curvature"])
        curvatureDiffs = list(d["curvatureDiff"])
        curvatureRates = list(d["curvatureRate"])
        deltaThetas = list(d["deltaTheta"])
        deltThetaDots = list(d["deltaThetaDot"])

        if min(curvatureDiffs) < minCurveDiff : minCurveDiff = min(curvatureDiffs)
        if max(curvatureDiffs) > maxCurveDiff : maxCurveDiff = max(curvatureDiffs)

        if min(curvatureRates[1:]) < minCurveRate : minCurveRate = min(curvatureRates[1:])
        if max(curvatureRates[1:]) > maxCurveRate : maxCurveRate = max(curvatureRates[1:])

        if min(deltaThetas) < minDeltaTheta : minDeltaTheta = min(deltaThetas)
        if max(deltaThetas) > maxDeltaTheta : maxDeltaTheta = max(deltaThetas)

        if min(deltaThetaDots) < minDeltaThetaDot : minDeltaThetaDot = min(deltaThetaDots)
        if max(deltaThetaDots) > maxDeltaThetaDot : maxDeltaThetaDot = max(deltaThetaDots)

        newDfs.append(d)

    # curvature vs time plots:

    psi_vals = [df["psi"][0] for df in newDfs]
    a_vals = [df["a"][0] for df in newDfs]
    time_arrays = [list(df["t"]) for df in newDfs]
    curvature_arrays = [list(df["curvature"]) for df in newDfs]

    d = {
        "psi" : psi_vals,
        "a" : a_vals,
        "t" : time_arrays,
        "curvature" : curvature_arrays
    }
    df_curvature = pd.DataFrame(d)


    fig, axs = plt.subplots(7,7, sharex=True, sharey=True, figsize=(24,18))
    fig.suptitle("Curvature as a function of time", fontsize=48)
    for i in range( len(df_curvature) ):

        x = df_curvature["t"][i]
        y = df_curvature["curvature"][i]
        psi_val = df_curvature["psi"][i]
        a_val = df_curvature["a"][i]
        row, col = subplot_index(psi_val, a_val)
        ax = axs[row, col]

        ax.hlines(y=y[0], xmin=x[0], xmax=x[-1], colors="red", linestyle="dashed", lw=6)
        ax.plot(x, y, lw=6)
        #ax.set_title(rf"$\Psi$ = {psi_val}, $a$ = {a_val}")
        #ax.set_xticks([0, 20, 40, 60, 80])
        #ax.set_yticks([0, 0.1, 0.2])
        ax.tick_params(axis='both', which='major', labelsize=20)

    plt.savefig(f"{output}/curvatureVsTime.pdf")


    # Create heatmaps

    #t_dfs = []

    for i in range(len(newDfs[0]["t"]) ) :

        #if (i == 0) or (i == 1) or (i % 10 == 0) :

        print(i)

        # re-organize data frames by timestep
        seriesList = [ newDfs[j].loc[i] for j in range(len(newDfs)) ]
        t_df = pd.DataFrame(seriesList)
        #t_dfs.append(t_df)

        # get the current time
        time = list(t_df["t"])[0]

        # --------------------------------------------------------------------------------------
        # curvature  heatmap

        fig, ax = plt.subplots(figsize=(12,9))

        matrix = t_df.pivot(index="psi", columns="a", values="curvatureDiff")

        plt.title(rf"Percent change from initial curvature $\kappa_0$   t = {round(time)}", fontsize=18)
        plt.xlabel(r"$a$", fontsize=16)
        plt.ylabel(r"$\Psi$", fontsize=16)
        #ax.set_xticks([])
        #ax.set_yticks([])
        lim = max([abs(minCurveDiff), abs(maxCurveDiff)])
        sns.heatmap(matrix, annot=matrix, cmap='RdYlGn', linewidth=0.30, ax=ax, vmin=-lim, vmax=lim)
        ax.invert_yaxis()
        plt.savefig(f"{output}/curvature/time{round(time,0)}.png")

        # --------------------------------------------------------------------------------------
        # curvature rate heatmap

        if i != 0 :

            fig, ax = plt.subplots(figsize=(12,9))

            matrix = t_df.pivot(index="psi", columns="a", values="curvatureRate")

            plt.title(r"Curvature Rate of Change $\dot{\kappa}" + f"   t = {round(time)}", fontsize=18)
            plt.xlabel(r"$a$", fontsize=16)
            plt.ylabel(r"$\Psi$", fontsize=16)
            #ax.set_xticks([])
            #ax.set_yticks([])
            lim = max([abs(minCurveRate), abs(maxCurveRate)])
            sns.heatmap(matrix, annot=matrix, cmap='RdYlGn', linewidth=0.30, ax=ax, vmin=-lim, vmax=lim)
            ax.invert_yaxis()
            plt.savefig(f"{output}/curvatureRate/time{round(time,0)}.png")


        # --------------------------------------------------------------------------------------
        # deltaTheta  heatmap

        fig, ax = plt.subplots(figsize=(12,9))

        matrix = t_df.pivot(index="psi", columns="a", values="deltaTheta")

        plt.title(rf"$\Delta\theta$    t = {round(time)}", fontsize=18)
        plt.xlabel(r"$a$", fontsize=16)
        plt.ylabel(r"$\Psi$", fontsize=16)
        #ax.set_xticks([])
        #ax.set_yticks([])
        lim = max([abs(minDeltaTheta), abs(maxDeltaTheta)])
        sns.heatmap(matrix, annot=matrix, cmap='RdYlGn', linewidth=0.30, ax=ax, vmin=-lim, vmax=lim)
        ax.invert_yaxis()
        plt.savefig(f"{output}/deltaTheta/time{round(time,0)}.png")

        # --------------------------------------------------------------------------------------
        # deltaThetaDot  heatmap

        fig, ax = plt.subplots(figsize=(12,9))

        matrix = t_df.pivot(index="psi", columns="a", values="deltaThetaDot")

        plt.title(r"$\Delta\dot{\theta}$" + f"    t = {round(time)}", fontsize=18)
        plt.xlabel(r"$a$", fontsize=16)
        plt.ylabel(r"$\Psi$", fontsize=16)
        #ax.set_xticks([])
        #ax.set_yticks([])
        lim = max([abs(minDeltaThetaDot), abs(maxDeltaThetaDot)])
        sns.heatmap(matrix, annot=matrix, cmap='RdYlGn', linewidth=0.30, ax=ax, vmin=-lim, vmax=lim)
        ax.invert_yaxis()
        plt.savefig(f"{output}/deltaThetaDot/time{round(time,0)}.png")

#---------------------------------------------------------------------------------------------------

def stressAnalysis(simulationSets) :

    """### COMPUTE STRESS MAGNITUDES AND MEAN STRESSES ###

    for s in simulationSets :

        # grab the parameter values and subplot indicies
        params, plotParams, d = s
        psi, a, b, numRods_init, angle = params
        row, col = subplot_index(psi, a)

        qVecs = list(d["q"])

        stressXs = d['Fx']
        stressYs = d['Fy']
        stressMags = d["Fmag"]
        stressMeans = d["Favg"]
        stressParallels = d["Fparallel"]"""

    ### STRESS PROFILES FOR X, Y, AND MAGNITUDE ###

    # initialize the subplots
    fig_mag, axs_mag = plt.subplots(7,7, sharex=True, sharey=True, figsize=(24,18))
    fig_x, axs_x = plt.subplots(7,7, sharex=True, sharey=True, figsize=(24,18))
    fig_y, axs_y = plt.subplots(7,7, sharex=True, sharey=True, figsize=(24,18))

    count = 0   # counter to track how many frames are complete

    # time vector is the same for all simulations so only initialize it once
    t = list(simulationSets[0][2]["t"])

    for i in range(0, len(t), 1) :

        print(i)

        fig_mag.suptitle(rf"Stress magnitude profiles at t = {str(round(t[i]))} min", fontsize=48)
        fig_x.suptitle(rf"Stress $x$-component profiles at t = {str(round(t[i]))} min", fontsize=48)
        fig_y.suptitle(rf"Stress $y$-component profiles at t = {str(round(t[i]))} min", fontsize=48)

        for s in simulationSets :

            # grab the parameter values and subplot indicies
            params, plotParams, d = s
            psi, a, b, numRods_init, angle = params
            row, col = subplot_index(psi, a)

            stressX, stressY, stress = d["Fx"][i], d["Fy"][i], d["Fmag"][i]
            q = [ j / (len(stress)-1) for j in range(len(stress)) ]

            # plot the stress magnitude
            ax = axs_mag[row, col]
            ax.clear()
            ax.plot(q, stress, '-o')

            # plot the stress x-component
            ax = axs_x[row, col]
            ax.clear()
            ax.plot(q, stressX, '-o')

            # plot the stress y-component
            ax = axs_y[row, col]
            ax.clear()
            ax.plot(q, stressY, '-o')

        # save the figures
        fig_mag.savefig(f'{output}/stressMag/time{str(round(t[i],0))}.png', format='png')
        fig_x.savefig(f'{output}/stressX/time{str(round(t[i],0))}.png', format='png')
        fig_y.savefig(f'{output}/stressY/time{str(round(t[i],0))}.png', format='png')

        count+=1

    plt.gcf().clear()


    ### AVERAGE STRESS VS TIME ###

    # initialize the subplots
    fig, axs = plt.subplots(7,7, sharex=True, sharey=True, figsize=(24,18))
    fig.suptitle("Mean stress as a function of time", fontsize=48)

    for s in simulationSets :

        # grab the parameter values and subplot indicies
        params, plotParams, d = s
        psi, a, b, numRods_init, angle = params
        row, col = subplot_index(psi, a)

        # plot the stress magnitude
        ax = axs[row, col]
        print(f"TEST Favg = {d['Favg']}")
        ax.plot(d['t'], d['Favg'], '-', lw=6)

    fig.savefig(f'{output}/avgStressVsTime.pdf')
    plt.gcf().clear()


#---------------------------------------------------------------------------------------------------

def breakingStressAnalysis(simulationSets) :

    ### COMPUTE STRESS MAGNITUDES AND MEAN STRESSES ###

    fig, axs = plt.subplots(7,7, sharex=True, sharey=True, figsize=(24,18))
    fig.suptitle(r"Maximum parallel stress as percent of critical value", fontsize=48)

    fig2, axs2 = plt.subplots(7,7, sharex=True, sharey=False, figsize=(24,18))
    fig2.suptitle(r"Maximum parallel stress vs. time", fontsize=48)

    psiVals, aVals, breakTimes, critStresses = [], [], [], []
    # interate over all simulation sets

    for s in simulationSets :

        # grab the parameter values and subplot indicies
        params, plotParams, d = s
        psi, a, b, numRods_init, angle = params
        row, col = subplot_index(psi, a)

        qVecs = list(d["q"])

        stressParallels = d["Fparallel"]

        ### HEATMAP OF FIRST PASSAGE TIME TO CRITICAL STRESS ###

        # read the critical force fit parameters into a DataFrame
        df = pd.read_csv("fitParams.csv")
        Fcrit = lambda psi, a : df['slope'][df['a']==a] * psi + df['intercept'][df['a']==a]

        # grab the parameter values
        params, plotParams, d = s
        psi, a, b, numRods_init, angle = params
        row, col = subplot_index(psi, a)

        Fcrit_nd = Fcrit(psi, a).values[0]
        dim_factor = (5/278) * (5**2) * (0.02)
        Fcrit_dim = abs(dim_factor * Fcrit_nd)

        #print(f"\n Psi = {psi}, a = {a}, Fcrit = {Fcrit_dim}")

        t = list(d["t"])
        qVecs = d["q"]

        # interate over all generalized coordinate vectors until
        # a deltaTheta exceeds the breaking angle
        critStressPercents = []
        breakTime = 170
        for time, stressParallel in zip(t, stressParallels) :

            #print(f"\n\t Time: {time}, Percent: {max(stressParallel) / Fcrit_dim * 100}%")
            critStressPercents.append(abs(max(stressParallel)) / Fcrit_dim * 100)

            # check if any stressParallel values exceed the critical stress
            if max(stressParallel) > Fcrit_dim :
                breakTime = time
                break

        critStressPercents +=  [None] * ( len(t) - len(critStressPercents) )

        ax = axs[row, col]
        ax.plot(t, critStressPercents, '-', lw=6)

        #ax.set_xticks([0, 60, 90, 0])
        #ax.set_yticks([0, 33, 67, 100])
        ax.tick_params(axis='both', which='major', labelsize=20)


        maxStress = [max(stressParallel) for stressParallel in stressParallels]
        ax2 = axs2[row, col]
        ax2.plot(t, maxStress, '-', lw=6)
        ax2.axhline(y=Fcrit_dim, color='r', linestyle='-', lw=6)

        psiVals.append(psi)
        aVals.append(a)
        breakTimes.append(breakTime)
        critStresses.append(Fcrit_dim)

    cols = ['{}'.format(col) for col in [1, 2, 5, 10, 20, 50, 100]]
    rows = ['{}'.format(row) for row in [100, 50, 20, 10, 5, 2, 1]]
    pad = 5 # in points

    for ax, col in zip(axs[-1], cols):
        ax.annotate(rf"$a$ = {col}", xy=(0.5, -1), xytext=(0, pad),
                    xycoords='axes fraction', textcoords='offset points',
                    size=32, ha='center', va='baseline')

    for ax, row in zip(axs[:,0], rows):
        ax.annotate(rf"$\Psi$ = {row}", xy=(-0.5, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size=32, ha='right', va='center')

    for ax, col in zip(axs2[-1], cols):
        ax.annotate(rf"$a$ = {col}", xy=(0.5, -1), xytext=(0, pad),
                    xycoords='axes fraction', textcoords='offset points',
                    size=32, ha='center', va='baseline')

    for ax, row in zip(axs2[:,0], rows):
        ax.annotate(rf"$\Psi$ = {row}", xy=(-0.5, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size=32, ha='right', va='center')

    fig.savefig(f"{output}/maxStressPercentVsTime.pdf")
    fig2.savefig(f"{output}/maxStressVsTime.pdf")

    # organize the data into a DataFrame
    df = pd.DataFrame({
        "psi" : psiVals,
        "a" : aVals,
        "breakFPT" : breakTimes,
        "critStress" : critStresses
    })
    matrix = df.pivot(index="psi", columns="a", values="breakFPT")

    # plots the FPTs in psi vs. a heatmap
    fig, ax = plt.subplots(figsize=(12,9))
    ax.set_title(r"First passage times to any $F_\parallel > F_{crit}$", fontsize=24)
    #plt.xlabel(r"$a$", fontsize=16)
    #plt.ylabel(r"$\Psi$", fontsize=16)
    sns.heatmap(matrix, annot=matrix, cmap='RdYlGn', linewidth=0.30, ax=ax, vmin=0, vmax=170, cbar_kws={'label': 'First passage time (mins)'})
    ax.invert_yaxis()
    fig.savefig(f"{output}/critStressFPT.pdf")

    return df

#---------------------------------------------------------------------------------------------------

def breakingAnalysis(simulationSets) :

    # interate over various breaking angles associated with mechanical snapping (values in degrees)

    fig, axs = plt.subplots(7,7, sharex=True, sharey=True, figsize=(24,18))
    fig.suptitle(r"First passage time to any $\Delta\theta > \Delta\theta_c$", fontsize=48)

    fig2, axs2 = plt.subplots(7,7, sharex=True, sharey=False, figsize=(24,18))
    fig2.suptitle(r"Maximum stress at FPT to $\Delta\theta > \Delta\theta_c$", fontsize=48)

    # interate over all simulation sets
    for s in simulationSets :

        # grab the parameter values and subplot indicies
        params, plotParams, d = s
        psi, a, b, numRods_init, angle = params
        row, col = subplot_index(psi, a)

        t = d["t"]
        qVecs = list(d["q"])

        stressParallels = d["Fparallel"]

        breakTimes = []
        breakStresses = []
        breakAngles = range(5, 180)
        for breakAngle in breakAngles :

            # interate over all generalized coordinate vectors until
            # a deltaTheta exceeds the breaking angle
            breakTime = 170
            breakStress = None
            for time, qVec, stressParallel in zip(t, qVecs, stressParallels) :

                thetas = qVec[2:]   # isolate theta values

                # compute deltaTheta values
                deltaThetas = [thetas[i] - thetas[i-1] for i in range(1, len(thetas))]

                # check if any deltaTheta values exceed the breaking angle
                breakAngle_rads = breakAngle * np.pi / 180  # convert break angle to radians
                if True in [ deltaTheta > breakAngle_rads for deltaTheta in deltaThetas] :
                    breakTime = time
                    breakStress = max(stressParallel)
                    break

            breakTimes.append(breakTime)
            breakStresses.append(breakStress)

        # plot FPTs vs breaking angle thresholds
        ax = axs[row, col]
        ax.plot(breakAngles, breakTimes, '-', lw=6)
        ax.set_xticks([0, 60, 120, 180])
        ax.set_yticks([0, 60, 120, 180])
        ax.tick_params(axis='both', which='major', labelsize=20)

        # plot stresss at FPTs vs breaking angle thresholds
        ax = axs2[row, col]
        ax.plot(breakAngles, breakStresses, '-', lw=6)
        ax.set_xticks([0, 60, 120, 180])
        ax.tick_params(axis='both', which='major', labelsize=20)


    cols = ['{}'.format(col) for col in [1, 2, 5, 10, 20, 50, 100]]
    rows = ['{}'.format(row) for row in [100, 50, 20, 10, 5, 2, 1]]
    pad = 5 # in points

    for ax, col in zip(axs[-1], cols):
        ax.annotate(rf"$a$ = {col}", xy=(0.5, -1), xytext=(0, pad),
                    xycoords='axes fraction', textcoords='offset points',
                    size=32, ha='center', va='baseline')

    for ax, row in zip(axs[:,0], rows):
        ax.annotate(rf"$\Psi$ = {row}", xy=(-0.5, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size=32, ha='right', va='center')

    for ax, col in zip(axs2[-1], cols):
        ax.annotate(rf"$a$ = {col}", xy=(0.5, -1), xytext=(0, pad),
                    xycoords='axes fraction', textcoords='offset points',
                    size=32, ha='center', va='baseline')

    for ax, row in zip(axs2[:,0], rows):
        ax.annotate(rf"$\Psi$ = {row}", xy=(-0.5, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size=32, ha='right', va='center')


    fig.savefig(f"{output}/breakFPT_vs_breakAngle.pdf")
    fig2.savefig(f"{output}/breakStress_vs_breakAngle.pdf")


    dfs = []

    breakAngles = range(5, 180, 1)
    for breakAngle in breakAngles :

        psiVals, aVals, breakTimes, breakStresses  = [], [], [], []
        # interate over all simulation sets
        for s in simulationSets :

            # grab the parameter values
            params, plotParams, d = s
            psi, a, b, numRods_init, angle = params
            row, col = subplot_index(psi, a)

            t = d["t"]
            qVecs = d["q"]

            stressParallels = d["Fparallel"]

            # interate over all generalized coordinate vectors until
            # a deltaTheta exceeds the breaking angle
            breakTime = 170
            for time, qVec, stressParallel in zip(t, qVecs, stressParallels) :

                thetas = qVec[2:]   # isolate theta values

                # compute deltaTheta values
                deltaThetas = [thetas[i] - thetas[i-1] for i in range(1, len(thetas))]

                # check if any deltaTheta values exceed the breaking angle
                breakAngle_rads = breakAngle * np.pi / 180  # convert break angle to radians
                if True in [ deltaTheta > breakAngle_rads for deltaTheta in deltaThetas] :
                    breakTime = time
                    breakStress = max(stressParallel)
                    break

            psiVals.append(psi)
            aVals.append(a)
            breakTimes.append(breakTime)
            breakStresses.append(breakStress)

        # organize the data into a DataFrame
        df = pd.DataFrame({
            "psi" : psiVals,
            "a" : aVals,
            "breakFPT" : breakTimes,
            "breakStress" : breakStresses
        })
        matrix = df.pivot(index="psi", columns="a", values="breakFPT")

        dfs.append(df)

        # plots the FPTs in psi vs. a heatmap
        fig_heat, ax_heat = plt.subplots(figsize=(12,9))
        ax_heat.set_title(rf"First passage times to any $\Delta\theta > {breakAngle}^\circ$", fontsize=24)
        #plt.xlabel(r"$a$", fontsize=16)
        #plt.ylabel(r"$\Psi$", fontsize=16)
        sns.heatmap(matrix, annot=matrix, cmap='RdYlGn', linewidth=0.30, ax=ax_heat, vmin=0, vmax=170, cbar_kws={'label': 'First passage time (mins)'})
        ax_heat.invert_yaxis()
        fig_heat.savefig(f"{output}/breakingAngleFPT/angle{breakAngle}.pdf")
        plt.close()

    return dfs, breakAngles


#---------------------------------------------------------------------------------------------------


def stressVsBending(stress_df, bending_dfs, breakAngles) :
    """ compare the FPT to breaks due to stress and bending
        compare the stress at bending breaks to the analytic critical stress """

    fig = plt.figure()
    plt.figure().clear()
    plt.close()
    plt.cla()
    plt.clf()

    for bending_df, breakAngle in zip(bending_dfs, breakAngles) :

        stressFPT = list(stress_df["breakFPT"])
        bendingFPT = list(bending_df["breakFPT"])

        plt.clf()
        plt.title(rf"FPT to breaks ($\Delta\theta_c$ = {breakAngle}$^\circ$)")
        plt.scatter(bendingFPT, stressFPT)
        plt.xlabel("FPT to bending breaks (mins)")
        plt.ylabel("FPT to stress breaks (mins)")
        plt.xticks([0, 15, 30, 45, 60, 75, 90])
        plt.yticks([0, 15, 30, 45, 60, 75, 90])
        plt.axes('square')
        plt.savefig(f"{output}/stressVsBending/fptComp_deltaTheta{breakAngle}.pdf")


        critStress = list(stress_df["critStress"])
        bendingStress = list(bending_df["breakStress"])

        plt.clf()
        plt.title(rf"Breaking stresses ($\Delta\theta_c$ = {breakAngle}$^\circ$)")
        plt.scatter(critStress, bendingStress)
        plt.xlabel("Critical stress")
        plt.ylabel(rf"Breaking stress for $\Delta\theta$ > $\Delta\theta_c$")
        plt.axes('square')
        plt.savefig(f"{output}/stressVsBending/stressComp_deltaTheta{breakAngle}.pdf")


####################################################################################################
# Data Organization
####################################################################################################

# formats figures with larger fonts
#plt.rcParams.update({'font.size': 16})
#plt.rcParams.update({'figure.autolayout': True})
#plt.rcParams['text.usetex'] = True

comp_curv = ComputeCurvature()

print("LOADING DATA...")
count = 1

output = sys.argv[1]

simulationSets = []
for file in sys.argv[2:] :

    try :
        filename = str(file)
    except IndexError :
        print("If loading package, continue, otherwise\nERROR: No input file given -> try again")
        exit()

    #sol = pickle.load(open("data.dir/exponentialGrowth_epsilonLJ0.0_rm1.5_Lc5.0_endTime180.0.pkl", "rb"))
    sol = pickle.load(open(filename, "rb"))
    l0, growthRate, kb, mu, nu, epsilon, numRods_init, angle, rtol, atol = sol[0]
    psi = int(kb / (mu * l0**2 * growthRate))
    a = int(nu/mu)
    b = epsilon/mu
    sol = sol[1:]

    #print( sum( [ sum( [ len(sol[i].y[j]) for j in range( len(sol[i].y) ) ] ) for i in range( len(sol) ) ] ) )
    #print( len(sol[0].y[0]) )
    #print( len(sol[0].t) )
    # use these three lines to coarsen data if desired
    n = 100     # take every nth output
    for s in sol :
        s.t = s.t[0::n]
        s.y = np.array( [ s.y[i][0::n] for i in range(len(s.y)) ] )
    #print( sum( [ sum( [ len(sol[i].y[j]) for j in range( len(sol[i].y) ) ] ) for i in range( len(sol) ) ] ) )
    #print( len(sol[0].y[0]) )
    #print( len(sol[0].t) )



    print(f"{count} ORGANIZING DATA ... Psi = {psi} \t a = {a}")
    count += 1

    t = np.concatenate( [s.t for s in sol], axis=None )

    minX, maxX, minY, maxY = 1E100, 0, 1E100, 0
    qs, qdot, lambdaX, lambdaY, nodeX, nodeY = [], [], [], [], [], []
    lastAddTime = [sol[0].t[0]]
    ltVec, lpVec = [], []

    qVec = []
    for i in range( len(sol[-1].y) ) : qVec.append( [None] * len(t) )

    k = 0   # time index

    for s in sol :

        #print("starting a new sol")

        q = [ s.y[:,i] for i in range(len(s.y[0])) ]
        numRods = len(q[0]) - 2

        lt = [ [ length(time, l0, growthRate, lastAddTime[-1]) ] * numRods for time in s.t ]
        ltVec += lt
        lp = [ [ growthRateFunc(time, l0, growthRate, lastAddTime[-1]) ] * numRods for time in s.t ]
        lpVec += lp

        # interating over time
        for i in range(len(q)) :

            numRods = len(q[i])-2
            lt = length(s.t[i], l0, growthRate, lastAddTime[-1])
            lp = growthRateFunc(s.t[i], l0, growthRate, lastAddTime[-1])

            # organize q
            for j in range( len(s.y) ) :
                qVec[j][k] = s.y[j][i]
            k += 1

            # compute qdots and Lagrange multipliers
            sols = growthDynamics(s.t[i], q[i], lt, kb, mu, nu, epsilon, lp)

            # organize q arrays
            qs.append(q[i])

            # organize qdots
            qdots = sols[: numRods+2]
            qdot.append(qdots)

            # organize Lagrange multipliers
            lambdaXs = sols[numRods+2 : 2*numRods+1]
            lambdaX.append(lambdaXs)
            lambdaYs = sols[2*numRods+1 :]
            lambdaY.append(lambdaYs)

            # calculate node positions
            thetas = q[i][2:]
            xNodes = [ q[i][0] - lt/2 * np.cos( thetas[0] ) ]
            yNodes = [ q[i][1] - lt/2 * np.sin( thetas[0] ) ]
            for i in range( len(q[i]) - 2 ) :
                xNodes.append( xNodes[-1] + lt * np.cos(thetas[i]) )
                yNodes.append( yNodes[-1] + lt * np.sin(thetas[i]) )
                if min(xNodes) < minX : minX = min(xNodes)
                if max(xNodes) > maxX : maxX = max(xNodes)
                if min(yNodes) < minY : minY = min(yNodes)
                if max(yNodes) > maxY : maxY = max(yNodes)
            nodeX.append(xNodes)
            nodeY.append(yNodes)

        lastAddTime.append(s.t[-1])

    d = {
        "t" : t,
        "psi" : [psi] * len(t),
        "a" : [a] * len(t),
        "q" : qs,
        "qdot" : qdot,
        "Fx" : lambdaX,
        "Fy" : lambdaY,
        "xNode" : nodeX,
        "yNode" : nodeY,
        "lt" : ltVec,
        "ldot" : lpVec
    }

    curvatures, deltaThetas, deltaThetaDots = [], [], []

    for i in range( len(d["t"]) ) :

        numRods = len(d["q"][i])-2
        numNodes = numRods - 1

        # compute curvature
        numMiddle = 7   # number of nodes in the "middle"; always use even numCells so this should be odd
        index = numMiddle + ( (numNodes - numMiddle) // 2 )
        x = np.array( d["xNode"][i][-index:index] )
        y = np.array( d["yNode"][i][-index:index] )
        curvature = comp_curv.fit(x, y)
        curvatures.append(curvature)

        # compute deltaTheta
        thetas = d["q"][i][2:]
        deltaTheta = thetas[numRods//2] - thetas[numRods//2-1]
        deltaThetas.append(deltaTheta * 180 / np.pi)

        # compute deltaThetaDot
        thetaDots = d["qdot"][i][2:]
        deltaThetaDot = thetaDots[numRods//2] - thetaDots[numRods//2-1]
        deltaThetaDots.append(deltaThetaDot * 180 / np.pi)

    # compute the difference of the curvature form the initial value at each times
    curvatureDiffs = list( 100 * (np.array(curvatures) - np.array( [curvatures[0]] * len(curvatures) ) ) / curvatures[0] )

    # compute the rate of change of the curvatures as a function of time
    curvatureRates = [None] + [(curvatures[i+1] - curvatures[i]) / (d["t"][i+1] - d["t"][i]) for i in range( len(d["t"])-1 )]

    d["curvature"] = curvatures
    d["curvatureDiff"] = curvatureDiffs
    d["curvatureRate"] = curvatureRates
    d["deltaTheta"] = deltaThetas
    d["deltaThetaDot"] = deltaThetaDots

    #configurationPlot(t, nodeX, nodeY, minX, maxX, minY, maxY, psi, a, b, numRods_init, angle)

    params = ( psi, a, b, numRods_init, angle )
    plotParams = ( minX, maxX, minY, maxY )
    simulationSets.append( (params, plotParams, pd.DataFrame(d)) )

# Organize the stress data -- compute mean stress, magnitudes, and parallel stresses
# Add these data to the existing data frames
simulationSets = stressOrganization(simulationSets)

configurationPlot(simulationSets)

#configurationPlotGrid(simulationSets)
#curvatureAnalysis(simulationSets)
#curvatureProfile(simulationSets)
#stressAnalysis(simulationSets)
#stress_df = breakingStressAnalysis(simulationSets)
#bending_dfs, breakAngles = breakingAnalysis(simulationSets)

#f = open("stressVsBending.pkl", 'wb')
#pickle.dump((stress_df, bending_dfs, breakAngles), f)
#f.close()

#stressVsBending(stress_df, bending_dfs, breakAngles)
