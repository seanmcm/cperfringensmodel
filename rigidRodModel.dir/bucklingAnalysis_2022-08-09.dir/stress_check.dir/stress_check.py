#import pdb
import numpy as np
import scipy as sp
from scipy import signal
from scipy import integrate
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import optimize
from scipy.interpolate import interp1d
from scipy import interpolate
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys
import pandas as pd
import seaborn as sns


def length(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return l0 * np.exp( (growthRate/l0) * (t - lastAddTime) )


def growthRateFunc(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return growthRate * np.exp( (growthRate/l0) * (t - lastAddTime) )

#---------------------------------------------------------------------------------------------------

def growthDynamics(t, q, lt, kb, mu, nu, epsilon, growthRate) :
    """ updates the qdots for the filament based only on growth dynamics
            l (array) : lengths of the cells in the Filaments
            lp (array) : growth rates for each cell
            kb (float) : angular spring constant
            mu (float) : parallel drag coefficient
            nu (float) : perpendiular drag coefficient
            epsilion (float) : rotational drag coefficient """

    l = [lt] * ( len(q) - 2 )
    lp = [ growthRate ] * ( len(q) - 2 )

    x0 = q[0]
    y0 = q[1]
    thetas = q[2:]

    numRods = len(thetas)
    numNodes = numRods + 1

    #totalLength = sum(l)

    #--------------------------------------------------------

    xEqns, yEqns, thetaEqns = [], [], []

    # x_i Euler-Lagrange equations --------------------------------------------------------

    # initialize x_0 E-L equation and x E_L equation array
    xEqn0 = np.zeros(3*numRods)
    xEqn0[0] = ( mu * np.cos(thetas[0])**2 + nu * np.sin(thetas[0])**2 ) * l[0]
    xEqn0[1] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    if numRods != 1 : xEqn0[numRods + 2] = -1
    xEqns.append(xEqn0)

    # initialize y_0 E-L equation and y E_L equation array
    yEqn0 = np.zeros(3*numRods)
    yEqn0[0] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    yEqn0[1] = ( mu * np.sin(thetas[0])**2 + nu * np.cos(thetas[0])**2 ) * l[0]
    if numRods != 1 : yEqn0[2*numRods + 1] = -1
    yEqns.append(yEqn0)

    # initialize theta_0 E-L equation and theta E_L equation array
    thetaEqn0 = np.zeros(3*numRods)
    thetaEqn0[2] = epsilon * l[0]**3
    if numRods != 1 :
        thetaEqn0[numRods + 2] = 0.5 * l[0] * np.sin(thetas[0])
        thetaEqn0[2*numRods + 1] = -0.5 * l[0] * np.cos(thetas[0])
    thetaEqns.append(thetaEqn0)

    #generate the 2 through N-1 x, y, and theta E-L equations
    for i in range(1, numRods) :

        # x_i E_L equations -----------------------------------------------------------

        xEqni = np.zeros(3*numRods)

        xEqni[0] = ( mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[i]

        xEqni[1] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * np.cos(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[0] * l[i] * np.sin(thetas[0]) )

        for j in range(1, i) :
            xEqni[j+2] = (mu - nu) * l[j] * l[i] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[j] * l[i] * np.sin(thetas[j])

        xEqni[i+2] = -0.5 * nu * l[i]**2 * ( np.sin(thetas[i]) * np.cos(thetas[i])**2 + np.sin(thetas[i])**3 )

        xEqni[i + numRods + 1] = 1

        if i != (numRods-1) :
            xEqni[i + numRods + 2] = -1

        xEqns.append(xEqni)


        # y_i E_L equations -----------------------------------------------------------

        yEqni = np.zeros(3*numRods)

        yEqni[0] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        yEqni[1] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[i]

        yEqni[2] = 0.5 * ( (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[0] * l[i] * np.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * np.sin(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) )

        for j in range(1, i) :
            yEqni[j+2] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[j] * l[i] * np.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])

        yEqni[i+2] = 0.5 * nu * l[i]**2 * (np.cos(thetas[i]) * np.sin(thetas[i])**2 + np.cos(thetas[i])**3)

        yEqni[i + (2*numRods)] = 1

        if i != (numRods-1) :
            yEqni[i + (2*numRods) + 1] = -1

        yEqns.append(yEqni)


        # theta_i E_L equations -----------------------------------------------------------

        thetaEqni = np.zeros(3*numRods)

        thetaEqni[i+2] = epsilon * l[i]**3

        thetaEqni[i + numRods + 1] = 0.5 * l[i] * np.sin(thetas[i])

        thetaEqni[i + (2*numRods)] = -0.5 * l[i] * np.cos(thetas[i])

        if i != (numRods-1) :
            thetaEqni[i + numRods + 2] = 0.5 * l[i] * np.sin(thetas[i])
            thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * np.cos(thetas[i])

        thetaEqns.append(thetaEqni)


        # stress calculations --------------------------------------------------------------
        #self.stressX.append( 0.5 * ( l[i-1]*np.cos(thetas[i-1]) - l[i]*np.sin(thetas[i]) ) )
        #self.stressY.append( 0.5 * ( -l[i-1]*np.sin(thetas[i-1]) + l[i]*np.cos(thetas[i]) ) )

    # combine all E-L equation coeffcients into a single array
    eqns = np.array( xEqns + yEqns + thetaEqns )

    # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
    #   are standard arrays
    #print(eqns)
    #input("stopza")

    if numRods == 1 : xVals, yVals, thetaVals = [0.0], [0.0], [0.0]
    else : xVals, yVals, thetaVals = [0.0], [0.0], [-kb * (thetas[0]-thetas[1])]

    for i in range(1, numRods) :

        xVal = -(mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i]))
        for j in range(1,i) :
            xVal -= (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] * lp[j] * np.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        xVals.append(xVal)

        yVal = -(mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i]))
        for j in range(1,i) :
            yVal -= (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] * lp[j] * np.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        yVals.append(yVal)

        if i == (numRods-1) :
            thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
        else :
            thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
        thetaVals.append(thetaVal)

    vals = np.array( xVals + yVals + thetaVals )


    numRods = len(q)-2

    sols = np.linalg.solve(eqns, vals)

    return sols



#---------------------------------------------------------------------------------------------------
# Test the perfectly straight case

psi = 100
a = 2

l0 = 5 # microns
r = 0.02 # 1/min
mu = 5/278
kb = psi * mu * l0**3 * r
nu = a * mu
epsilon = nu / 12

q = [0.0, 0.0, 0.0, 0.0]
numRods = len(q) - 2

lt = l0     #length(s.t[i], l0, growthRate, lastAddTime[-1])
lp = l0*r   #growthRateFunc(s.t[i], l0, growthRate, lastAddTime[-1])

# compute qdots and Lagrange multipliers
sol = growthDynamics(0, q, lt, kb, mu, nu, epsilon, lp)

Fx = (mu * l0**2 * r) * numRods**2 / 8

# organize Lagrange multipliers
lambdaXs = np.array(sol[numRods+2 : 2*numRods+1])
lambdaYs = np.array(sol[2*numRods+1 :])

print(f"Check if the straight case works: {np.abs(lambdaXs[0]) / Fx == 1}\n")

#---------------------------------------------------------------------------------------------------
# Tilted case before cell divsion

psi = 100
a = 2

l0 = 5 # microns
r = 0.02 # 1/min
mu = 5/278
kb = psi * mu * l0**3 * r
nu = a * mu
epsilon = nu / 12

q = [0.0, 0.0, -np.pi/12, np.pi/12]
numRods = len(q) - 2

lt = l0     #length(s.t[i], l0, growthRate, lastAddTime[-1])
lp = l0*r   #growthRateFunc(s.t[i], l0, growthRate, lastAddTime[-1])

# compute qdots and Lagrange multipliers
sol = growthDynamics(np.log(2)/r, q, lt, kb, mu, nu, epsilon, lp)

# organize Lagrange multipliers
lambdaXs = np.array(sol[numRods+2 : 2*numRods+1])
lambdaYs = np.array(sol[2*numRods+1 :])

print(f"Tilted, before:\n\t Fx: {lambdaXs} \n\t Fy: {lambdaYs}")

#---------------------------------------------------------------------------------------------------
# Tilted case after cell division

psi = 100
a = 2

l0 = 5 # microns
r = 0.02 # 1/min
mu = 5/278
kb = psi * mu * l0**3 * r
nu = a * mu
epsilon = nu / 12

q = [0.0, 0.0, -np.pi/12, -np.pi/12, np.pi/12, np.pi/12]
numRods = len(q) - 2

lt = l0     #length(s.t[i], l0, growthRate, lastAddTime[-1])
lp = l0*r   #growthRateFunc(s.t[i], l0, growthRate, lastAddTime[-1])

# compute qdots and Lagrange multipliers
sol = growthDynamics(0, q, lt, kb, mu, nu, epsilon, lp)

# organize Lagrange multipliers
lambdaXs = np.array(sol[numRods+2 : 2*numRods+1])
lambdaYs = np.array(sol[2*numRods+1 :])

print(f"Tilted, after:\n\t Fx: {lambdaXs} \n\t Fy: {lambdaYs}\n")


#---------------------------------------------------------------------------------------------------
# Zig-zag before cell division

psi = 100
a = 2

l0 = 5 # microns
r = 0.02 # 1/min
mu = 5/278
kb = psi * mu * l0**3 * r
nu = a * mu
epsilon = nu / 12

q = [0.0, 0.0, -np.pi/12, np.pi/12, -np.pi/12, np.pi/12, -np.pi/12, np.pi/12]
numRods = len(q) - 2

lt = l0     #length(s.t[i], l0, growthRate, lastAddTime[-1])
lp = l0*r   #growthRateFunc(s.t[i], l0, growthRate, lastAddTime[-1])

# compute qdots and Lagrange multipliers
sol = growthDynamics(np.log(2)/r, q, lt, kb, mu, nu, epsilon, lp)

# organize Lagrange multipliers
lambdaXs = np.array(sol[numRods+2 : 2*numRods+1])
lambdaYs = np.array(sol[2*numRods+1 :])

print(f"Zig-zag, before:\n\t Fx: {lambdaXs} \n\t Fy: {lambdaYs}")

#---------------------------------------------------------------------------------------------------
# Zig-zag after cell division

psi = 100
a = 2

l0 = 5 # microns
r = 0.02 # 1/min
mu = 5/278
kb = psi * mu * l0**3 * r
nu = a * mu
epsilon = nu / 12

q = [0.0, 0.0, -np.pi/12, -np.pi/12, np.pi/12, np.pi/12, -np.pi/12, -np.pi/12, np.pi/12, np.pi/12, -np.pi/12, -np.pi/12, np.pi/12, np.pi/12]
numRods = len(q) - 2

lt = l0     #length(s.t[i], l0, growthRate, lastAddTime[-1])
lp = l0*r   #growthRateFunc(s.t[i], l0, growthRate, lastAddTime[-1])

# compute qdots and Lagrange multipliers
sol = growthDynamics(0, q, lt, kb, mu, nu, epsilon, lp)

# organize Lagrange multipliers
lambdaXs = np.array(sol[numRods+2 : 2*numRods+1])
lambdaYs = np.array(sol[2*numRods+1 :])

print(f"Zig-zag, after:\n\t Fx: {lambdaXs} \n\t Fy: {lambdaYs}\n")

#---------------------------------------------------------------------------------------------------
# psi = 100, a = 1 simulation data, before division

psi = 100
a = 1

l0 = 5 # microns
r = 0.02 # 1/min
growthRate = l0 * r
mu = 5/278
kb = psi * mu * l0**3 * r
nu = a * mu
epsilon = nu / 12

q = [-6.85004824e+01,  2.76740380e+00, -3.27645173e-01, -3.27140510e-01,
 -2.69169631e-01, -5.65271609e-02,  3.17200988e-01,  6.45020277e-01,
  6.31014803e-01,  1.66006369e-01, -5.47656060e-01, -1.04685297e+00,
 -1.00741904e+00, -4.19237060e-01,  4.19237060e-01,  1.00741904e+00,
  1.04685297e+00,  5.47656060e-01, -1.66006369e-01, -6.31014803e-01,
 -6.45020277e-01, -3.17200988e-01,  5.65271609e-02,  2.69169631e-01,
  3.27140510e-01,  3.27645173e-01]
numRods = len(q) - 2

t = 68.65735902799051
lastAddTime = 34.657359027997266

lt = length(t, l0, growthRate, lastAddTime)
lp = growthRateFunc(t, l0, growthRate, lastAddTime)

# compute qdots and Lagrange multipliers
sol = growthDynamics(t, q, lt, kb, mu, nu, epsilon, lp)

# organize Lagrange multipliers
lambdaXs = np.array(sol[numRods+2 : 2*numRods+1]) / (mu * l0**2 * r)
lambdaYs = np.array(sol[2*numRods+1 :]) / (mu * l0**2 * r)

print(f"Simulation, before:\n\t Fx: {lambdaXs} \n\t Fy: {lambdaYs}")


#---------------------------------------------------------------------------------------------------
# psi = 100, a = 1 simulation data, after division

psi = 100
a = 1

l0 = 5 # microns
r = 0.02 # 1/min
growthRate = l0 * r
mu = 5/278
kb = psi * mu * l0**3 * r
nu = a * mu
epsilon = nu / 12

q = [-7.13291329e+01,  3.74182457e+00, -3.42135298e-01, -3.42135298e-01,
 -3.40696068e-01, -3.40696068e-01, -2.77462725e-01, -2.77462725e-01,
 -5.35540214e-02, -5.35540214e-02,  3.33507617e-01,  3.33507617e-01,
  6.65596661e-01,  6.65596661e-01,  6.42245253e-01,  6.42245253e-01,
  1.60043798e-01,  1.60043798e-01, -5.67657488e-01, -5.67657488e-01,
 -1.06960685e+00, -1.06960685e+00, -1.02430064e+00, -1.02430064e+00,
 -4.25502825e-01, -4.25502825e-01,  4.25502825e-01,  4.25502825e-01,
  1.02430064e+00,  1.02430064e+00,  1.06960685e+00,  1.06960685e+00,
  5.67657488e-01,  5.67657488e-01, -1.60043798e-01, -1.60043798e-01,
 -6.42245253e-01, -6.42245253e-01, -6.65596661e-01, -6.65596661e-01,
 -3.33507617e-01, -3.33507617e-01,  5.35540214e-02,  5.35540214e-02,
  2.77462725e-01,  2.77462725e-01,  3.40696068e-01,  3.40696068e-01,
  3.42135298e-01,  3.42135298e-01]
numRods = len(q) - 2

t = 69.31471805599453
lastAddTime = 69.31471805599453


lt = length(t, l0, growthRate, lastAddTime)
lp = growthRateFunc(t, l0, growthRate, lastAddTime)

# compute qdots and Lagrange multipliers
sol = growthDynamics(t, q, lt, kb, mu, nu, epsilon, lp)

# organize Lagrange multipliers
lambdaXs = np.array(sol[numRods+2 : 2*numRods+1]) / (mu * l0**2 * r)
lambdaYs = np.array(sol[2*numRods+1 :]) / (mu * l0**2 * r)

print(f"Simulation, after:\n\t Fx: {lambdaXs} \n\t Fy: {lambdaYs}")
