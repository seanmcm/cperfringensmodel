#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys


#outfile = os.path.basename(sys.argv[1])
filename = sys.argv[1]

psiVals = []
critStressVals = []

#deltaThetadotVecs = []
#stressVecs = []

"""lengthVecs = []
xdotVecs = []
ydotVecs = []
thetadotVecs = []
lambdaXVecs = []
lambdaYVecs = []"""

for filename in sys.argv[1:] :

    data = pickle.load(open(filename, "rb"))

    l0 = 5
    kb, mu, growthRate, numRods, angle, pert_coefficient = data[0]
    psi = kb / (mu * l0**2 * growthRate)

    lengths = np.array(data[1])
    xdotVals = np.array(data[2])
    ydotVals = np.array(data[3])
    thetadotVals = np.array(data[4])
    lambdaXVals = np.array(data[5])
    lambdaYVals = np.array(data[6])

    index = [int(pert_coefficient * length / l0 - 1) for length in lengths]

    deltaThetadots = [ thetadotVals[i][index[i]+1]-thetadotVals[i][index[i]] for i in range(len(thetadotVals)) ]

    stresses = [ np.sqrt ( lambdaXVals[i][index[i]]**2 + lambdaYVals[i][index[i]]**2 ) / (mu * l0 * growthRate) for i in range(len(lambdaXVals)) ]

    m, b = np.polyfit(stresses, deltaThetadots, 1)
    critStress = -b/m

    psiVals.append(psi)
    critStressVals.append(critStress)

    #deltaThetadotVecs.append( np.array(deltaThetadots) )
    #stressVecs.append( np.array(stresses) )


    """lengthVecs.append(np.array(data[1]))
    xdotVecs.append(np.array(data[2]))
    ydotVecs.append(np.array(data[3]))
    thetadotVecs.append(np.array(data[4]))
    lambdaXVecs.append(np.array(data[5]))
    lambdaYVecs.append(np.array(data[6]))"""


plt.title(r"Critical Stress Vs. $\Psi$")
plt.xlabel(r"$\Psi$ $\left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Critical Stress (Nondimensionalized)") #(kg $\mu$m/min$^2$)")
plt.plot(psiVals, critStressVals, '-o')
#plt.legend(loc="upper right")

#plt.show()

m, b = np.polyfit(psiVals, critStressVals, 1)
print("C =", m, "* Psi +", b)

plt.savefig("plots.dir/critStressVsPsi.pdf")

"""

#param_key = sys.argv[-1]    # string representing the parameter that is varied
plot_titles = {
    "kb" : "Varied Angular Spring Constant",
    "mu" : "Varied Parallel Drag Per Unit Length",
    "growthRate" : "Varied Growth Rate",
    "angle" : "Varied Pertubation Angle",
    "q" : "Varied Location"
}

#plt.title( plot_titles[param_key] )
plt.title("")

plt.axhline(y=0, color='black', linestyle='-')

for i in range(1, len(sys.argv)-1) :

    outfile = os.path.basename(sys.argv[i])
    filename = sys.argv[i]

    data = pickle.load(open(filename, "rb"))

    kb, mu, growthRate, numRods, angle, pert_coefficient = data[0]
    data = np.array(data[1:])

    paramstrs = {
        "kb" : r"$k_b$ = " + str(kb),
        "mu" : r"$\mu$ = " + str(mu),
        "growthRate" : r"$G_r$ = " + str(growthRate),
        "angle" : r"$\theta$ = " + str(angle),
        "q" : r"$q$ = " + str(pert_coefficient)
    }
    paramstr = paramstrs[param_key]

    l0 = 5
    lengths, thetadotDiffs = data[:,0], data[:,1]
    stress = 0.5 * mu * lengths**2 * (growthRate/l0) * pert_coefficient * (1-pert_coefficient)

    lengths, stressDiffs = data[:,0], data[:,1]

    plt.xlabel(r"Length ($\mu$m)")
    plt.ylabel("Stress Difference")
    plt.plot(stress, thetadotDiffs, '-o', label=paramstr)

plt.legend(loc="upper right")
plt.savefig('plots.dir/' + outfile[:-4] + '.pdf', format='pdf')

"""
