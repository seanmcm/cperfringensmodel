from scipy.optimize import fsolve
import scipy as sp
import math

def equations(p) :
    x, y = p
    eqn = (x+y**2-4, math.exp(x) + x*y - 3)
    print(eqn)
    return eqn

def test2(vars, *args) :

    l, rateMax, mu, fStall, fTrans, q = args

    eqn1 = vars[0] * ( 1 + sp.exp( 7 * (vars[3]-vars[2]-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax
    eqn2 = vars[1] * ( 1 + sp.exp( 7 * (vars[3]-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax
    eqn3 = vars[2] - 0.5 * mu * sum(l) * (vars[0] + vars[1]) * q[0]
    eqn4 = vars[3] - 0.5 * mu * sum(l) * (vars[0] + vars[1]) * q[1]

    eqns = (eqn1, eqn2, eqn3, eqn4)

    print("Equations?", eqns)

    return eqns

def test3(vars, *args) :

    l, rateMax, mu, fStall, fTrans, q = args

    rateVals = vars[:len(vars)//2]
    forces = vars[len(vars)//2:]
    eqns = []

    for i in range( len(vars)//2 ) :

        if i == len(l)-1 :
            eqns.append( rateVals[i] * ( 1 + sp.exp( 7 * (forces[i]-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax )
        else :
            eqns.append( rateVals[i] * ( 1 + sp.exp( 7 * (forces[i+1]-forces[i]-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax )

    for i in range( len(vars)//2 ) :
        eqns.append( forces[i] - 0.5 * mu * sum(l) * sum(rateVals) * q[i] )

    eqns = tuple(eqns)

    print("Equations?", eqns)

    return eqns

vars0 = (0.1, 0.1, 10**-8, 10**-8)
#tuple(vars0)
#x, y =  fsolve(equations, (1, 1))

#l1 = 6.116396580878517
#l1 = 5.93827
l = (6.12, 5.97)
rateMax = 0.2
mu = 10**-9
fStall = 10**-7
fTrans = 5e-9
#q = [0.5, l[0]/sum(l), 1]

q = []
for i in range( len(l) ) :
    #q[i] = 0.5 * ( sum(l[:i+1]) / sum(l) + 1 )
    q.append( 0.5 * ( sum(l[:i+1]) / sum(l) + 1 ) )

args = (l, rateMax, mu, fStall, fTrans, q)

#rate1, rate2, F0, F1 = fsolve(test2, vars0, args)

print("lengths", l)
print("guess", vars0)
print("params", [rateMax, mu, fStall, fTrans, q] )
sol1 = fsolve(test2, vars0, args)
print("sol1", sol1, "\n")

print("lengths", l)
print("guess", vars0)
print("params", [rateMax, mu, fStall, fTrans, q] )
sol2 = fsolve(test3, vars0, args)
print("sol2", sol2, "\n")

"""res1 = rate1 * ( 1 + sp.exp( 7 * (F0-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax
res2 = rate2 * ( 1 + sp.exp( 7 * (F1-F0-fStall -fTrans) / (2 * (fStall - fTrans))) ) - rateMax
res3 = F0 - 0.5 * mu * sum(l) * (rate1 + rate2) * q[0]
res4 = F1 - 0.5 * mu * sum(l) * (rate1 + rate2) * q[1]"""

#print("sols", [rate1, rate2, F0, F1])
#print("check", (res1, res2, res3, res4) )
