#import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys


#outfile = os.path.basename(sys.argv[1])
filename = sys.argv[1]

data = pickle.load(open(filename, "rb"))

l0 = 5
kb, mu, growthRate, numRods, angle, pert_coefficient = data[0]
psi = kb / (mu * l0**2 * growthRate)

lengths = np.array(data[1])
xdotVals = np.array(data[2])
ydotVals = np.array(data[3])
thetadotVals = np.array(data[4])
lambdaXVals = np.array(data[5])
lambdaYVals = np.array(data[6])

plotCounter = 1

plt.figure(plotCounter)
plt.title(r"$\dot{x}_0$ and $\dot{y}_0$ vs filament length")
plt.xlabel(r"Length ($\mu$m)")
plt.ylabel(r"$\dot{x}_0$ and $\dot{y}_0$ ($\mu$m/min)")
xdotTheory = np.array( [-growthRate * (l/l0/2 - 0.5) for l in lengths] )
plt.plot(lengths, xdotVals - xdotTheory, '-o', label=r"$\dot{x}_0$")
plt.plot(lengths, ydotVals, '-o', label=r"$\dot{y}_0$")
plt.legend(loc="upper right")

plotCounter += 1

plt.figure(plotCounter)
plt.title(r"$\dot{\theta}_i$ vs filament length")
plt.xlabel(r"Length ($\mu$m)")
plt.ylabel(r"$\dot{\theta}_i$ (1/min)")
plt.plot(lengths, thetadotVals[:,0], '-o', label=r"$\dot{\theta}_{0}$")
plt.plot(lengths, thetadotVals[:,1], '-o', label=r"$\dot{\theta}_{0.1}$")
plt.plot(lengths, thetadotVals[:,2], '-o', label=r"$\dot{\theta}_{0.2}$")
plt.plot(lengths, thetadotVals[:,3], '-o', label=r"$\dot{\theta}_{0.3}$")
plt.plot(lengths, thetadotVals[:,4], '-o', label=r"$\dot{\theta}_{0.4}$")
plt.plot(lengths, thetadotVals[:,5], '-o', label=r"$\dot{\theta}_{0.5}$")
plt.legend(loc="upper right")

plotCounter += 1

coeffs = [0.1, 0.2, 0.3, 0.4, 0.5]
lambdaXTheory = [ np.array( [-0.5 * mu * l**2 * (growthRate/l0) * q * (1-q) for l in lengths] ) for q in coeffs ]

plt.figure(plotCounter)
plt.title(r"$\lambda^x_i$ vs filament length")
plt.xlabel(r"Length ($\mu$m)")
plt.ylabel(r"$\lambda^x_i$ (kg $\mu$m/min$^2$)")
plt.plot(lengths, lambdaXVals[:,0], '-o', label=r"$\lambda^x_{0}$")
plt.plot(lengths, lambdaXVals[:,1] - lambdaXTheory[0], '-o', label=r"$\lambda^x_{0.1}$")
plt.plot(lengths, lambdaXVals[:,2] - lambdaXTheory[1], '-o', label=r"$\lambda^x_{0.2}$")
plt.plot(lengths, lambdaXVals[:,3] - lambdaXTheory[2], '-o', label=r"$\lambda^x_{0.3}$")
plt.plot(lengths, lambdaXVals[:,4] - lambdaXTheory[3], '-o', label=r"$\lambda^x_{0.4}$")
plt.plot(lengths, lambdaXVals[:,5] - lambdaXTheory[4], '-o', label=r"$\lambda^x_{0.5}$")

"""plt.plot(lengths, lambdaXVals[:,0], '-o', label=r"$\lambda^x_{0}$")
plt.plot(lengths, lambdaXVals[:,1] , '-o', label=r"$\lambda^x_{0.1}$")
plt.plot(lengths, lambdaXVals[:,2] , '-o', label=r"$\lambda^x_{0.2}$")
plt.plot(lengths, lambdaXVals[:,3], '-o', label=r"$\lambda^x_{0.3}$")
plt.plot(lengths, lambdaXVals[:,4] , '-o', label=r"$\lambda^x_{0.4}$")
plt.plot(lengths, lambdaXVals[:,5] , '-o', label=r"$\lambda^x_{0.5}$")"""

plt.legend(loc="upper right")

plotCounter += 1

plt.figure(plotCounter)
plt.title(r"$\lambda^y_i$ vs filament length")
plt.xlabel(r"Length ($\mu$m)")
plt.ylabel(r"$\lambda^y_i$ (kg $\mu$m/min$^2$)")
plt.plot(lengths, lambdaYVals[:,0], '-o', label=r"$\lambda^y_{0}$")
plt.plot(lengths, lambdaYVals[:,1], '-o', label=r"$\lambda^y_{0.1}$")
plt.plot(lengths, lambdaYVals[:,2], '-o', label=r"$\lambda^y_{0.2}$")
plt.plot(lengths, lambdaYVals[:,3], '-o', label=r"$\lambda^y_{0.3}$")
plt.plot(lengths, lambdaYVals[:,4], '-o', label=r"$\lambda^y_{0.4}$")
plt.plot(lengths, lambdaYVals[:,5], '-o', label=r"$\lambda^y_{0.5}$")
plt.legend(loc="upper right")

plotCounter += 1

plt.figure(plotCounter)
plt.title(r"$|\lambda|_i$ vs filament length")
plt.xlabel(r"Length ($\mu$m)")
plt.ylabel(r"$\lambda^y_i$ (kg $\mu$m/min$^2$)")
plt.plot(lengths, np.sqrt(lambdaXVals[:,0]**2 + lambdaYVals[:,0]**2), '-o', label=r"$\lambda^y_{0}$")
plt.plot(lengths, np.sqrt(lambdaXVals[:,1]**2 + lambdaYVals[:,1]**2) - np.abs(lambdaXTheory[0]), '-o', label=r"$\lambda^y_{0.1}$")
plt.plot(lengths, np.sqrt(lambdaXVals[:,2]**2 + lambdaYVals[:,2]**2) - np.abs(lambdaXTheory[1]), '-o', label=r"$\lambda^y_{0.2}$")
plt.plot(lengths, np.sqrt(lambdaXVals[:,3]**2 + lambdaYVals[:,3]**2) - np.abs(lambdaXTheory[2]), '-o', label=r"$\lambda^y_{0.3}$")
plt.plot(lengths, np.sqrt(lambdaXVals[:,4]**2 + lambdaYVals[:,4]**2) - np.abs(lambdaXTheory[3]), '-o', label=r"$\lambda^y_{0.4}$")
plt.plot(lengths, np.sqrt(lambdaXVals[:,5]**2 + lambdaYVals[:,5]**2) - np.abs(lambdaXTheory[4]), '-o', label=r"$\lambda^y_{0.5}$")
plt.legend(loc="upper right")



plt.show()



"""

#param_key = sys.argv[-1]    # string representing the parameter that is varied
plot_titles = {
    "kb" : "Varied Angular Spring Constant",
    "mu" : "Varied Parallel Drag Per Unit Length",
    "growthRate" : "Varied Growth Rate",
    "angle" : "Varied Pertubation Angle",
    "q" : "Varied Location"
}

#plt.title( plot_titles[param_key] )
plt.title("")

plt.axhline(y=0, color='black', linestyle='-')

for i in range(1, len(sys.argv)-1) :

    outfile = os.path.basename(sys.argv[i])
    filename = sys.argv[i]

    data = pickle.load(open(filename, "rb"))

    kb, mu, growthRate, numRods, angle, pert_coefficient = data[0]
    data = np.array(data[1:])

    paramstrs = {
        "kb" : r"$k_b$ = " + str(kb),
        "mu" : r"$\mu$ = " + str(mu),
        "growthRate" : r"$G_r$ = " + str(growthRate),
        "angle" : r"$\theta$ = " + str(angle),
        "q" : r"$q$ = " + str(pert_coefficient)
    }
    paramstr = paramstrs[param_key]

    l0 = 5
    lengths, thetadotDiffs = data[:,0], data[:,1]
    stress = 0.5 * mu * lengths**2 * (growthRate/l0) * pert_coefficient * (1-pert_coefficient)

    lengths, stressDiffs = data[:,0], data[:,1]

    plt.xlabel(r"Length ($\mu$m)")
    plt.ylabel("Stress Difference")
    plt.plot(stress, thetadotDiffs, '-o', label=paramstr)

plt.legend(loc="upper right")
plt.savefig('plots.dir/' + outfile[:-4] + '.pdf', format='pdf')

"""
