#!/usr/local/env python

import numpy as np
import scipy as sp
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy.optimize import fsolve
from scipy.stats import gamma
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
from random import randrange
import statistics as stat
import math
import time
import csv
import os
import pickle
import matplotlib
#matplotlib.use("Agg")      # COMMENT THIS FOR PLOTS FOR plt.show(), UNCOMMENT FOR ANIMATIONS
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys

####################################################################################################

class filament(object) :

    #-----------------------------------------------------------------------------------------------

    def __init__(self, t, numRods, l0, com, growthRate, mu, kb, Fcrit, kbDist, sync) :
        """ creates a filament object
                numRods : number of rods in the filament
                l0 : initial length of the cells
                com : center of mass of the filaments
                growthRate : initial growth rate of the
                Lb : length at which a filament breaks
                mu : drag per unit lenght parallel to the axis of the rod
                kb : angular spring constant
                Fcrit : breaking force as a function of psi"""

        self.sync = sync

        self.numRods = numRods
        self.l0 = l0
        self.l0s = [l0] * numRods
        self.growthRate = growthRate
        self.T = l0/growthRate * np.log(2)  # cell cycle time
        l = l0 * np.exp( (growthRate/l0) * (t % self.T) )   # cell length
        self.L = numRods * l    # filament length
        self.com = com
        #self.Lb = Lb
        self.mu = mu
        self.kb = kb
        self.Fcrit = Fcrit

        self.kbDist = kbDist

        kbs =  [ self.kbDist(self.kb) for i in range(self.numRods-1) ]
        self.fc = [self.Fcrit(kbs[i]/(self.mu * self.l0**2 * self.growthRate)) * (self.mu * self.l0 * self.growthRate) for i in range(len(kbs)) ]
        self.broken = [False] * (self.numRods-1)

        self.lastDivTime = t
        self.lastDivTimes = [t] * numRods

        divLens = np.random.normal(2*self.l0, 0.075*2*self.l0, numRods)
        print(divLens)
        self.nextDivTimes = [ self.lastDivTimes[i] + ( (self.l0/self.growthRate) * np.log(divLens[i]/self.l0s[i]) ) for i in range(numRods) ]
        print(self.nextDivTimes)
        #self.nextDivTimes = list(np.random.normal(t+self.T, 0.1*self.T, numRods))

        self.updateStress(t)
        self.updateTips()

    #-----------------------------------------------------------------------------------------------

    def updateLength(self, t) :
        """ updates the length of the filament
                t : the current time """
        #l = self.l0 * np.exp( (self.growthRate/self.l0) * (t % self.T) )

        if self.sync :
            l = self.l0 * np.exp( (self.growthRate/self.l0) * (t - self.lastDivTime) )
            self.L = self.numRods * l
        else :
            r = self.growthRate / self.l0
            self.L = sum( [ self.l0s[i] * np.exp( r * (t-self.lastDivTimes[i]) ) for i in range(self.numRods)] )

        #if self.L > self.Lb : self.broken = True

    #-----------------------------------------------------------------------------------------------

    def cellDivision(self, t, index=None) :
        """ handles cell division """

        # synchronous cell division (all cells divide at the same time)
        if index == None :

            # double the number of cells in the chain
            self.numRods = int(self.numRods * 2)
            self.broken = [False] * (self.numRods-1)

            # copy old Fcrit vals and add in Fcrit vals for the new nodes
            oldFc = self.fc
            newFc = []
            for i in range( len(oldFc) ) :
                newFc.append( self.Fcrit( kbDist(self.kb) / (self.mu * self.l0**2 * self.growthRate) ) * (self.mu * self.l0 * self.growthRate) )
                newFc.append(oldFc[i])
                newKbs.append(newKb)
            newFc.append( self.Fcrit( kbDist(self.kb) / (self.mu * self.l0**2 * self.growthRate) ) * (self.mu * self.l0 * self.growthRate) )

            meanFc = self.Fcrit(self.kb / (self.mu * self.l0**2 * self.growthRate) ) * (self.mu * self.l0 * self.growthRate)
            print(f"\t\tMean Fc = {meanFc}")
            print(f"\t\tNew Fc values = {newFc}")
            print(f"\t\tPercent Difference = {[100 * (newFc[i] - meanFc)/meanFc for i in range(len(newFc))]}\n")

            self.fc = newFc


        # asynchronous cell division (each cell divides individually)
        else :

            # increase the number of the cells in the chain by one
            self.numRods = int(self.numRods) + 1
            self.broken = [False] * (self.numRods-1)

            # Add in Fcrit vals for the new nodes
            newFc = self.Fcrit( self.kbDist(self.kb) / (self.mu * self.l0**2 * self.growthRate) ) * (self.mu * self.l0 * self.growthRate)
            self.fc.insert( index, newFc )

            meanFc = self.Fcrit(self.kb / (self.mu * self.l0**2 * self.growthRate) ) * (self.mu * self.l0 * self.growthRate)
            print(f"\t\tMean Fc = {meanFc}")
            print(f"\t\tNew Fc value = {newFc}\n")
            print(f"\t\tPercent Differences = {100 * (newFc - meanFc)/meanFc}%\n")


            r = self.growthRate/self.l0

            # determine the lengths of the two daughter cells
            newLength = ( self.l0s[index] * np.exp( r * (t - self.lastDivTimes[index])) ) / 2
            self.l0s[index] = newLength
            self.l0s.insert( index, newLength )

            # update time of last division for the two daughter cells
            self.lastDivTimes[index] = t
            self.lastDivTimes.insert( index, t )

            # determine the lengths (and times) at which the daughter cells will divide
            divLen1 = abs(np.random.normal(2*self.l0, 0.075*2*self.l0))
            divLen2 = abs(np.random.normal(2*self.l0, 0.075*2*self.l0))

            #divLen1 = sp.stats.gamma.rvs(100, scale=kb/100)

            divTime1 = t + ( (self.l0/self.growthRate) * np.log(divLen1/newLength) )
            divTime2 = t + ( (self.l0/self.growthRate) * np.log(divLen2/newLength) )
            self.nextDivTimes[index] = divTime1
            self.nextDivTimes.insert( index, divTime2 )

            # left over from time based division rather than length based
            #self.nextDivTimes[index] = np.random.normal(t+self.T, 0.1*self.T)
            #self.nextDivTimes.insert( index, np.random.normal(t+self.T, 0.1*self.T) )

    #-----------------------------------------------------------------------------------------------

    def chainBreak(self, t, break_index) :

        #breakLens.append(f.L)

        if sum(self.broken) > 1 :
            #print("Fcrits", f.fc)
            #print("stress", f.stress)
            #print("F-Fc:", np.array(f.stress) - np.array(f.fc) )
            #print("Flags:", f.broken)
            input("UH OH -- More than one breaking event occured")

        """oldFc = self.fc
        oldLastDivTime = self.lastDivTime

        new_com_1 = self.com - self.L/2 * (1 - ( (break_index+1) / self.numRods ) )
        f1 = filament(t, break_index+1, self.l0, new_com_1, self.growthRate, self.mu, self.kb, self.Fcrit, self.kbDist, self.sync)
        f1.fc = oldFc[:break_index]
        f1.lastDivTime = oldLastDivTime
        #newFilaments.append(f1)

        new_com_2 = self.com - self.L/2 * (1 - ( (self.numRods + break_index +1 ) / self.numRods) )
        f2 = filament(t, self.numRods-break_index-1, self.l0, new_com_2, self.growthRate, self.mu, self.kb, self.Fcrit, self.kbDist, self.sync)
        f2.fc = oldFc[break_index+1:]
        f2.lastDivTime = oldLastDivTime
        #newFilaments.append(f2)"""


        oldFc = self.fc
        oldl0s = self.l0s
        if self.sync : oldLastDivTime = self.lastDivTime
        else :
            oldLastDivTimes = self.lastDivTimes
            oldNextDivTimes = self.nextDivTimes


        # calculate the center of masses of the two chains left after breakage
        if self.sync :

            # these equations only work if all cells have the same length
            new_com_1 = self.com - ( self.L/2 * (1 - ( (break_index+1) / self.numRods ) ) )
            new_com_2 = self.com - ( self.L/2 * (1 - ( (self.numRods + break_index +1 ) / self.numRods) ) )

        else :

            """ Visualization of equations below (Example: six cell chain)

               cell 0     cell 1       cell 2    cell 3    cell 4    cell 5
              =======o==============o=========o==========o=======o============
              |                     x                                        |
             tip1             broken linkage                                tip2
              |<---------L1-------->|<------------------L2------------------>|
              new_com_1 = tip1 + L1/2      new_com_2 = tip1 + L1 + L2/2
            """

            tip1, tip2 = self.tip_locations[0], self.tip_locations[1]
            r = self.growthRate / self.l0

            L1 = sum( [ self.l0s[i] * np.exp( r * (t-self.lastDivTimes[i]) ) for i in range(break_index+1)] )

            L2 = sum( [ self.l0s[i] * np.exp( r * (t-self.lastDivTimes[i]) ) for i in range(break_index+1, self.numRods)] )

            new_com_1 = tip1 + (L1 / 2)
            new_com_2 = tip1 + L1 + (L2 / 2)

        f1 = filament(t, break_index+1, self.l0, new_com_1, self.growthRate, self.mu, self.kb, self.Fcrit, self.kbDist, self.sync)
        f1.fc = oldFc[:break_index]
        f1.l0s = oldl0s[:break_index+1]
        if self.sync : f1.lastDivTime = oldLastDivTime
        else :
            f1.lastDivTimes = oldLastDivTimes[:break_index+1]
            f1.nextDivTimes = oldNextDivTimes[:break_index+1]

        #newFilaments.append(f1)

        #print(f'\tAfter Break:, Fc: {f1.fc}, l0s: {f1.l0s}, lastDivTimes: {f1.lastDivTimes}, nextDivTimes: {f1.nextDivTimes}' )

        f2 = filament(t, self.numRods-break_index-1, self.l0, new_com_2, self.growthRate, self.mu, self.kb, self.Fcrit, self.kbDist, self.sync)
        f2.fc = oldFc[break_index+1:]
        f2.l0s = oldl0s[break_index+1:]
        if self.sync : f2.lastDivTime = oldLastDivTime
        else :
            f2.lastDivTimes = oldLastDivTimes[break_index+1:]
            f2.nextDivTimes = oldNextDivTimes[break_index+1:]

        #newFilaments.append(f2)

        return f1, f2

    #-----------------------------------------------------------------------------------------------

    def updateTips(self) :
        """ calculate the location of the end tips of the filaments """
        self.tip_locations = [ self.com - (self.L / 2), self.com + (self.L / 2) ]

    #-----------------------------------------------------------------------------------------------

    def updateStress(self, t) :

        stress = []

        if self.sync :

            for i in range(1, self.numRods ) :
                q = i / self.numRods
                #s = 0.5 * self.mu * q * (1-q) * self.L * self.numRods * self.growthRate
                s = 0.5 * self.mu * q * (1-q) * self.L**2 * self.growthRate / self.l0

                #s = 0.5 * self.mu * q * (1-q) * ( self.numRods * self.l0 * np.exp( (self.growthRate/self.l0) * (t % self.T) ) )**2 * self.growthRate / self.l0

                #s = 0.5 * q * (1-q) * self.numRods**2
                stress.append(s)
            self.stress = stress


        else :

            # define growth rate r
            r = self.growthRate / self.l0

            # compute the total length and total growth rate of the chain
            L = sum([ self.l0s[j] * np.exp(r*(t-self.lastDivTimes[j])) for j in range(self.numRods) ])
            Lprime = sum([ self.l0s[j] * r * np.exp(r*(t-self.lastDivTimes[j])) for j in range(self.numRods) ])

            for i in range(1, self.numRods ) :
                #q = i / self.numRods
                q = sum(self.l0s[:i]) / sum(self.l0s)
                #s = 0.5 * self.mu * q * (1-q) * self.L * self.numRods * self.growthRate
                #s = 0.5 * self.mu * q * (1-q) * self.L**2 * self.growthRate / self.l0

                s = 0.5 * self.mu * q * (1-q) * L * Lprime

                #s = 0.5 * self.mu * q * (1-q) * ( self.numRods * self.l0 * np.exp( (self.growthRate/self.l0) * (t % self.T) ) )**2 * self.growthRate / self.l0

                #s = 0.5 * q * (1-q) * self.numRods**2
                stress.append(s)

            self.stress = stress

    #-----------------------------------------------------------------------------------------------

    def update(self, t, dt) :
        """ updates the attributes of the filament """

        self.updateLength(t)
        self.updateTips()   # updateTips must be called AFTER updateLength
        self.updateStress(t)

        if len(self.broken) != self.numRods-1 :
            input("Error: number of broken flags does not equal number of nodes")


####################################################################################################

# helper functions ---------------------------------------------------------------------------------

def getDivisionTimes(t, filaments, nextDivTime, sync) :
    """ sync is true is division is synchronous; false is division is asynchronous """

    if sync : divEventTimes = [nextDivTime]

    else :

        divEventTimes = []
        for f in filaments : divEventTimes += f.nextDivTimes

    return divEventTimes


def getBreakTimes(t, filaments, lastDivTime, sync) :
    """ sync is true is division is synchronous; false is division is asynchronous """

    breakEventTimes = []

    if sync :

        # compute the times of the next break events
        for f in filaments :
            for i in range(len(f.stress)) :

                q = (i+1) / f.numRods   # cefficient q used to compute stress

                # compute the time the stress in each linkage will exceed its breaking stress
                nextBreakTime = (f.l0 / (2 * f.growthRate)) * np.log( 2 * f.fc[i] / ( f.growthRate * f.mu * q * (1-q) * f.numRods**2 * f.l0 ) )

                # insure that the time of the next break is in the future not the past
                # if the break time is in the past, make it occur at the current time step
                if (nextBreakTime + lastDivTime) < t : breakEventTimes.append(t)
                else : breakEventTimes.append(nextBreakTime + lastDivTime)

    else :

        for f in filaments :
            for i in range(len(f.stress)) :
                #q = (i+1) / f.numRods
                q = sum(f.l0s[:i+1]) / sum(f.l0s)

                if len(f.fc) != 0 :

                    r = f.growthRate / f.l0

                    breakTimeFunc = lambda t : f.fc[0] - 0.5 * f.mu * q * (1-q) * sum( [ f.l0s[i] * np.exp(r*(t-f.lastDivTimes[i])) for i in range(f.numRods) ] ) * sum( [ f.l0s[i] * r * np.exp(r*(t-f.lastDivTimes[i])) for i in range(f.numRods) ] )

                    nextBreakTime = fsolve(breakTimeFunc, t + (f.growthRate/f.l0))[0]

                    # insure that the time of the next break is in the future not the past
                    # if the break time is in the past, make it occur at the current time step
                    if nextBreakTime < t : breakEventTimes.append(t)
                    else : breakEventTimes.append(nextBreakTime)

    return breakEventTimes

def criticalForceHistograms(t, filaments) :

    psi = filaments[0].kb / (filaments[0].mu * filaments[0].l0**2 * filaments[0].growthRate)
    psi = round(psi, 0)

    #print([f.fc for f in filaments])
    #input('stop')

    allFc = []
    for f in filaments : allFc += f.fc

    plt.clf()
    n, bins, patches = plt.hist(x=allFc, bins='auto', color='#0504aa', alpha=0.7, rwidth=0.85)

    plt.xlabel('Critical Force Value')
    plt.ylabel('Frequency')
    plt.title('Distribution of Critical Breaking Forces')

    plt.savefig(f"plots.dir/criticalForceHistograms.dir/critForceDist_psi{round(t,2)}.pdf")

# main simulation calls ----------------------------------------------------------------------------

def main(sync, endTime, numRods, l0, growthRate, mu, kb, Fcrit, kbDist) :

    # vectors for saving expansion distance at fixed time intervals
    times = np.linspace(0, endTime, 1000)
    time_index = 0
    displacementVec = []
    chainLens = []
    chainTips = []

    # vectors to track cell division times and expansion distance when dividing
    timesDiv = []
    displacementVecDiv = []

    # vectors to track chain brekaing times and expansion distance when breaks occur
    timesBreak = []
    displacementVecBreak = []

    t = 0   # initial time
    T = l0/growthRate * np.log(2)       # cell cycle time
    dt = T / 5000     # timestep (not used anymore)

    # determine time of the next cell divison (only used if division is synchronous)
    nextDivTime = t + T
    nextEventTime = nextDivTime
    lastDivTime = 0.0   # assume simulation starts at the beginning of a new cell cycle

    # track how many breaks have occurred and at what lengths the chains break
    numBreaks = 0
    breakLens = []

    # initialize the array of filaments with a single filament
    filaments = [ filament(t, numRods, l0, 0.0, growthRate, mu, kb, Fcrit, kbDist, sync) ]

    # arrays to track expansnion distance and rate (mostly not used)
    totalLength = [[t, numRods * l0]]   # [t, Lt] where Lt is the length from the two farthest tips
    totalLengthRate = []                # [t, Lt_rate] where Lt_rate is the expansion rate


    # the simulation begins!
    while t < endTime :

        print("______________________________________________________________________________")
        print(f"Step Begin Time = {t}")

        # determine the time(s) of the next cell division event(s)
        divEventTimes = getDivisionTimes(t, filaments, nextDivTime, sync)
        nextDivTime = min(divEventTimes)

        # if asynchronous division, determine which cell in which filament will divide next
        if sync == False :
            index = divEventTimes.index(nextDivTime)
            for i in range( len(filaments) ) :
                if index >= filaments[i].numRods :
                    index -= filaments[i].numRods
                else:
                    f_div_index = i
                    div_index = index
                    break

        # determine the times of the next breakages
        breakEventTimes = getBreakTimes(t, filaments, lastDivTime, sync)
        if len(breakEventTimes) == 0 : nextBreakTime = 1e100
        else :
            nextBreakTime = min(breakEventTimes)
            index = breakEventTimes.index(nextBreakTime)
            for i in range(len(filaments)) :
                if index >= len(filaments[i].stress) :
                    index -= len(filaments[i].stress)
                else :
                    f_break_index = i
                    break_index = index
                    break

        print(f"\tNext Division Time: {nextDivTime}")
        print(f"\tNext Break Time: {nextBreakTime}\n")

        # save the expansion distance at a fixed time intervals up until the next event
        lastDist = None
        while ( times[time_index] < min([nextBreakTime, nextDivTime]) ) :
            for f in filaments: f.update(times[time_index], dt)
            tip_max = max( [ max(f.tip_locations) for f in filaments ] )
            tip_min = min( [ min(f.tip_locations) for f in filaments ] )
            displacementVec.append( tip_max - tip_min )
            chainLens.append([f.L for f in filaments])
            chainTips.append([f.tip_locations for f in filaments])

            #print(f"\tIntermediate Steps:")
            #print(f"\tTime = {times[time_index]}, \tDistance = {tip_max - tip_min}\n")
            #if lastDist != None : print(f"\t\t\tDiff = {(tip_max - tip_min) - lastDist}")

            lastDist = tip_max - tip_min
            time_index += 1
            if time_index > len(times)-1 : break


        # if the next event is a breakage
        if nextBreakTime < nextDivTime :

            t = nextBreakTime

            print(f"\tBREAK EVENT, t = {t}, filament = {f_break_index}, link = {break_index}\n")

            numBreaks += 1

            filaments[f_break_index].broken[break_index] = True

            for f in filaments : f.update(t, dt)

            tip_max = max( [ max(f.tip_locations) for f in filaments ] )
            tip_min = min( [ min(f.tip_locations) for f in filaments ] )

            print(f"\t\tBefore the break:")
            print(f"\t\t\tStresses = {filaments[f_break_index].stress}")
            print(f"\t\t\tCritical Stresses = {filaments[f_break_index].fc}")
            print(f"\t\t\tDistance = {tip_max- tip_min}")
            print('\n')

            newFilaments = []
            for f in filaments :

                #f.update(t, dt)

                # if the filament has reached the breaking length
                if True in f.broken :
                    print(f"\t\tLength at break = {f.L}\n")
                    breakLens.append(f.L)
                    f1, f2 = f.chainBreak(t, break_index)
                    newFilaments.append(f1)
                    newFilaments.append(f2)
                else : newFilaments.append(f)

            filaments = newFilaments

            for f in filaments : f.update(t, dt)

            timesBreak.append(t)
            tip_max = max( [ max(f.tip_locations) for f in filaments ] )
            tip_min = min( [ min(f.tip_locations) for f in filaments ] )
            displacementVecBreak.append( tip_max - tip_min )

            print(f"\t\After the break:")
            print(f"\t\t\tStresses = {filaments[f_break_index].stress}")
            print(f"\t\t\tCritical stress = {filaments[f_break_index].fc}")
            print(f"\t\t\tCritical stress = {filaments[f_break_index+1].fc}")
            print(f"\t\t\tDistance = {tip_max- tip_min}")
            print('\n')

        # cell division
        else :
            t = nextDivTime
            #print(f"\tDIV \tTime = {t}, filament = {f_div_index}, cell = {div_index}")
            #print(f"\tNum Filaments = {len(filaments)}, len f = {filaments[f_div_index].numRods}")
            # synchronous cell divsion

            for f in filaments : f.update(t, dt)

            tip_max = max( [ max(f.tip_locations) for f in filaments ] )
            tip_min = min( [ min(f.tip_locations) for f in filaments ] )

            print(f"\t\tBefore the division:")
            print(f"\t\t\tStresses = {filaments[f_div_index].stress}")
            print(f"\t\t\tCritical stress = {filaments[f_div_index].fc}")
            print(f"\t\t\tDistance = {tip_max- tip_min}")
            print(' ')

            if sync :
                for f in filaments :
                    #f.update(t, dt)
                    f.cellDivision(t)   # check if cells divide
                    f.lastDivTime = t
                    f.update(t, dt)

            # asynchronous cell division
            else :
                #for f in filaments : f.update(t, dt)
                filaments[f_div_index].cellDivision(t, div_index)
                filaments[f_div_index].lastDivTime = t
                for f in filaments : f.update(t, dt)

            lastDivTime = t
            nextDivTime = t + T

            timesDiv.append(t)
            tip_max = max( [ max(f.tip_locations) for f in filaments ] )
            tip_min = min( [ min(f.tip_locations) for f in filaments ] )
            displacementVecDiv.append( tip_max - tip_min )

            print(f"\t\tAfter the division:")
            print(f"\t\t\tStresses = {filaments[f_div_index].stress}")
            print(f"\t\t\tCritical stress = {filaments[f_div_index].fc}")
            print(f"\t\t\tDistance = {tip_max- tip_min}")
            print(' ')

        tip_max = max( [ max(f.tip_locations) for f in filaments ] )
        tip_min = min( [ min(f.tip_locations) for f in filaments ] )
        diff = tip_max - tip_min
        #print(f'\tMax tip = {tip_max}')
        #print(f'\tMin tip = {tip_min}')
        #print(f'\tdisplacment after: {diff}')

        tip_max = max( [ max(f.tip_locations) for f in filaments ] )
        tip_min = min( [ min(f.tip_locations) for f in filaments ] )
        totalLength.append( [t, tip_max - tip_min] )
        Lt_rate =  (totalLength[-1][1] - totalLength[-2][1]) / (totalLength[-1][0] - totalLength[-2][0])
        totalLengthRate.append( [t, Lt_rate] )

        print(f'Step End Time = {t}\n')
        #print("step complete\n")
        #input("step complete")

    #criticalForceHistograms(t, filaments)

    print("Average Rate =", totalLength[-1][1]/endTime, "um/min")
    print("Number of breaks:", numBreaks)
    return totalLength[-1][1]/endTime, numBreaks, breakLens, times, displacementVec, timesDiv, displacementVecDiv, timesBreak, displacementVecBreak, chainLens, chainTips, filaments

    """totalLength = np.array(totalLength)
    plt.figure(1)
    plt.title(r"Tip-to-Tip Expansion Displacement" )
    plt.xlabel(r"Time(min)")
    plt.ylabel(r"Displacement ($\mu$m)")
    plt.plot(totalLength[:,0], totalLength[:,1])

    totalLengthRate = np.array(totalLengthRate)
    plt.figure(2)
    plt.title(r"Tip-to-Tip Expansion Rate" )
    plt.xlabel(r"Time(min)")
    plt.ylabel(r"Rate of Expansion ($\mu$m/min)")
    plt.plot(totalLengthRate[:,0], totalLengthRate[:,1])

    plt.show() """


# formats figures with larger fonts
plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'figure.autolayout': True})

# how to output printed details,
#   "terminal" : output to the terminal
#   "file" : output to a file for each run,
#   "none" : suppress all detail output
outOpt = "file"

# SIMULATION PARAMETERS
np.random.seed(32)
sync = False        # is cell division synchronous?
mc_steps = 10
endTime = 300       # total simulation time

# PHYSICAL PARAMETERS
numRods = 1         # number of initial cells
l0 = 5              # initial cell length
growthRate = 0.1    # intitial  growth rate
mu = 1e-5           # drag coefficient per unit length for parallel drag

# critical breaking stress as function of psi = kb / mu l0^2 growthRate
Fcrit = lambda psi : 3.466036922444842 * psi + 0.43012681408031994

# distribution of kb values assigned to the cell-to-cell linkages
kbDist = lambda kb : abs(np.random.normal(kb, 0.1 * kb))
#kbDist = lambda kb : sp.stats.gamma.rvs(100, scale=kb/100)

# values of psi and corresponding values of kb or mu assuming other paarams held constant
psiVals = [1, 2]#, 3, 4, 5, 6, 7, 8, 9, 10]
#psiVals = [1e-1, 1, 1e1, 1e2, 1e3, 1e4, 1e5]
kbVals = [psiVal * (mu * l0**2 * growthRate) for psiVal in psiVals]
muVals = [1e5 / (psiVal * l0**2 * growthRate) for psiVal in psiVals]

# SIMULATION CALLS
expRateMeans, expRateStDevs = [], []
avgNumBreaks, stDevNumBreaks = [], []
meanBreakLens = []
#for kbVal in kbVals :
for psiVal in psiVals :

    kbVal = psiVal * (mu * l0**2 * growthRate)

    chainData = []

    vals = []
    breakCount = []
    breakLenVals = []
    for i in range(mc_steps) :

        # setup output based on option chosen
        if outOpt == "file" :
            filestr = f"simulationDetails.dir/details_psi{psiVal}-{i}.txt"
            try : os.remove(filestr)
            except : pass
            f = open(filestr, "w")
            sys.stdout = f
        elif outOpt == "none" :
            sys.stdout = open(os.devnull, "w")

        print(f"Phenomenologicial Simulation: Psi = {psiVal}, Run No. {i}")

        expRate, numBreaks, breakLens, times, displacementVec, timesDiv, displacementVecDiv, timesBreak, displacementVecBreak, chainLens, chainTips, filaments = main(sync, endTime, numRods, l0, growthRate, mu, kbVal, Fcrit, kbDist)
        chainData.append([times, chainLens, chainTips])

        if outOpt == "file" : f.close()
        sys.stdout = sys.__stdout__
        vals.append( expRate )
        breakCount.append(numBreaks)
        breakLenVals.append(stat.mean(breakLens))

        plt.clf()
        plt.title(rf"Expansion Distance ($\Psi$ = {psiVal})" )
        plt.xlabel(r"Time (min)")
        plt.ylabel(r"Expansion Distance $D(t)$ ($\mu$m)")
        plt.plot(times, displacementVec, label=r"D(t)", lw=3)
        timeRange = int(500 + 25*(psiVal-1))
        #plt.plot(np.array(times[:timeRange]), numRods * l0 * np.exp(growthRate * np.array(times[:timeRange]) / l0), label="Exp Growth", ls=':', lw=3)
        plt.plot(timesBreak[:-1], displacementVecBreak[:-1], 'o', label="Breaks", alpha=0.75, marker='|', ms=20)
        plt.plot(timesDiv, displacementVecDiv, 'o', label="Divisions", ms=5)
        plt.legend(loc='upper left')
        plt.savefig(f"plots.dir/trajectories.dir/displacementVsTime_psi{psiVal}_{i}.pdf")

        expRateVec = [ (displacementVec[i+1]-displacementVec[i])/(times[i+1]-times[i]) for i in range(len(displacementVec)-1)]

        plt.clf()
        plt.title(rf"Expansion Rate ($\Psi$ = {psiVal})" )
        plt.xlabel(r"Time (min)")
        plt.ylabel(r"Expansion Rate $D'(t)$ ($\mu$m/min)")
        plt.plot(times[1:], expRateVec, lw=3)
        plt.savefig(f"plots.dir/trajectories.dir/expansionRateVsTime_psi{psiVal}_{i}.pdf")

        #if (psiVal == 2 and i == 2) : input("stop here")

        allFc = []
        for f in filaments : allFc += f.fc

        plt.clf()
        n, bins, patches = plt.hist(x=allFc, bins='auto', color='#0504aa', alpha=0.7, rwidth=0.85)

        plt.xlabel('Critical Force Value')
        plt.ylabel('Frequency')
        plt.title('Distribution of Critical Breaking Forces')

        plt.savefig(f"plots.dir/criticalForceHistograms.dir/critForceDist_psi{psiVal}-{i}.pdf")

    pickle.dump(chainData, open(f"plots.dir/chainData.dir/chainData_psi{psiVal}.pkl", "wb"))

    print("Done Psi =", psiVal)
    expRateMeans.append(stat.mean(vals))
    expRateStDevs.append(stat.stdev(vals))
    avgNumBreaks.append(stat.mean(breakCount))
    stDevNumBreaks.append(stat.stdev(breakCount))
    meanBreakLens.append(stat.mean(breakLenVals))


plt.clf()
plt.title(r"Expansion Rate Varies with $\Psi$" )
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Average Expansion Rate ($\mu$m/min)")
#plt.plot(psiVals, expRateMeans)
plt.errorbar(psiVals, expRateMeans, yerr=None, fmt='-o', ecolor='r', lw=3, ms=10)
#plt.xscale('log')
#plt.yscale('log')
#plt.axvline(x=4e5, color='r')
#plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
plt.savefig(f"plots.dir/averageExpansionRate.pdf")

plt.clf()
plt.title(r"Number of Breaks vs. $\Psi$" )
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Number of Breaks")
#plt.plot(psiVals, expRateMeans)
plt.errorbar(psiVals, avgNumBreaks, yerr=None, fmt='-o', ecolor='r', lw=3, ms=10)
#plt.xscale('log')
#plt.yscale('log')
#plt.axvline(x=4e5, color='r')
#plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
plt.savefig("plots.dir/numBreaksVsPsi.pdf")

plt.clf()
plt.title(r"Average Break Length Varies with $\Psi$" )
#plt.xlabel(r"Parallel Drag Coefficient $\mu$ (kg / $\mu$m min)")
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Breaking Length ($\mu$m)")
plt.plot(psiVals, meanBreakLens, '-o', lw=3, ms=10)
#plt.errorbar(psiVals, meanBreakLens, yerr=[], fmt='-o', ecolor='r')
#plt.axvline(x=4e5, color='r')
#plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
plt.savefig("plots.dir/breakLenVsPsi.pdf")
