#!/bin/bash

# slurm submission for simulation
# Virginia Tech's ARC Tinkercliffs Cluster
# 07/19/2021

# RESOURCES #
#SBATCH --nodes=1
#SBATCH --ntasks=64
#SBATCH --mem=64G

# WALLTIME #
# t format d-hr:min:sec
#SBATCH -t 0-01:00:00

# QUEUE #
#SBATCH -p normal_q

# ALLOCATION #
#SBATCH -A CPMot

# MODULES #
module load Anaconda3

cd $SLURM_SUBMIT_DIR
unset DISPLAY

date
echo "BEGINNING..."
echo $1
python dataAnalysis.py $1/plots.dir $1/dataFiles.dir/*
echo "DONE!"
date
exit;
