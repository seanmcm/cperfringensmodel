#!/bin/bash

date
for psi in 5 10 20
do
    #                                a time N
    python exponentialGrowth.py $psi 1 102 8 &
    python exponentialGrowth.py $psi 1 102 16 &
    python exponentialGrowth.py $psi 1 68 32 &
done
wait
date
echo "Complete"
