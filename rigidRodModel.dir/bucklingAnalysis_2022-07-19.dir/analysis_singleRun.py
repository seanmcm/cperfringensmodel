#import pdb
import numpy as np
from scipy import integrate
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import optimize
from scipy.interpolate import interp1d
from scipy import interpolate
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys
import pandas as pd
import seaborn as sns


####################################################################################################
# Classes
####################################################################################################

class ComputeCurvature:
    def __init__(self):
        """ Initialize some variables """
        self.xc = 0  # X-coordinate of circle center
        self.yc = 0  # Y-coordinate of circle center
        self.r = 0   # Radius of the circle
        self.xx = np.array([])  # Data points
        self.yy = np.array([])  # Data points

    def calc_r(self, xc, yc):
        """ calculate the distance of each 2D points from the center (xc, yc) """
        return np.sqrt((self.xx-xc)**2 + (self.yy-yc)**2)

    def f(self, c):
        """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
        ri = self.calc_r(*c)
        return ri - ri.mean()

    def df(self, c):
        """ Jacobian of f_2b
        The axis corresponding to derivatives must be coherent with the col_deriv option of leastsq"""
        xc, yc = c
        df_dc = np.empty((len(c), self.xx.size))

        ri = self.calc_r(xc, yc)
        df_dc[0] = (xc - self.xx)/ri                   # dR/dxc
        df_dc[1] = (yc - self.yy)/ri                   # dR/dyc
        df_dc = df_dc - df_dc.mean(axis=1)[:, np.newaxis]
        return df_dc

    def fit(self, xx, yy):
        self.xx = xx
        self.yy = yy
        center_estimate = np.r_[np.mean(xx), np.mean(yy)]
        center = optimize.leastsq(self.f, center_estimate, Dfun=self.df, col_deriv=True)[0]

        self.xc, self.yc = center
        ri = self.calc_r(*center)
        self.r = ri.mean()

        return 1 / self.r  # Return the curvature

    def circleFit(self, xx, yy):
        self.xx = xx
        self.yy = yy
        center_estimate = np.r_[np.mean(xx), np.mean(yy)]
        center = optimize.leastsq(self.f, center_estimate, Dfun=self.df, col_deriv=True)[0]

        self.xc, self.yc = center
        ri = self.calc_r(*center)
        self.r = ri.mean()

        return self.r, self.xc, self.yc   # return the properties of the best fit circle


####################################################################################################
# Filament Dynamics Functions
####################################################################################################

def length(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return l0 * np.exp( (growthRate/l0) * (t - lastAddTime) )


def growthRateFunc(t, l0, growthRate, lastAddTime) :
    """ returns the rod length at time t
            t: time
            l0: the initial spring equilibrium length (value at t = 0)
            growthRate: the cell growth rate"""

    #return l0 + growthRate * ( t - self.lastAddTime )
    return growthRate * np.exp( (growthRate/l0) * (t - lastAddTime) )

#---------------------------------------------------------------------------------------------------

def growthDynamics(t, q, lt, kb, mu, nu, epsilon, growthRate) :
    """ updates the qdots for the filament based only on growth dynamics
            l (array) : lengths of the cells in the Filaments
            lp (array) : growth rates for each cell
            kb (float) : angular spring constant
            mu (float) : parallel drag coefficient
            nu (float) : perpendiular drag coefficient
            epsilion (float) : rotational drag coefficient """

    l = [lt] * ( len(q) - 2 )
    lp = [ growthRate ] * ( len(q) - 2 )

    x0 = q[0]
    y0 = q[1]
    thetas = q[2:]

    numRods = len(thetas)
    numNodes = numRods + 1

    #totalLength = sum(l)

    #--------------------------------------------------------

    xEqns, yEqns, thetaEqns = [], [], []

    # x_i Euler-Lagrange equations --------------------------------------------------------

    # initialize x_0 E-L equation and x E_L equation array
    xEqn0 = np.zeros(3*numRods)
    xEqn0[0] = ( mu * np.cos(thetas[0])**2 + nu * np.sin(thetas[0])**2 ) * l[0]
    xEqn0[1] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    if numRods != 1 : xEqn0[numRods + 2] = -1
    xEqns.append(xEqn0)

    # initialize y_0 E-L equation and y E_L equation array
    yEqn0 = np.zeros(3*numRods)
    yEqn0[0] = ( mu - nu ) * l[0] * np.cos(thetas[0]) * np.sin(thetas[0])
    yEqn0[1] = ( mu * np.sin(thetas[0])**2 + nu * np.cos(thetas[0])**2 ) * l[0]
    if numRods != 1 : yEqn0[2*numRods + 1] = -1
    yEqns.append(yEqn0)

    # initialize theta_0 E-L equation and theta E_L equation array
    thetaEqn0 = np.zeros(3*numRods)
    thetaEqn0[2] = epsilon * l[0]**3
    if numRods != 1 :
        thetaEqn0[numRods + 2] = 0.5 * l[0] * np.sin(thetas[0])
        thetaEqn0[2*numRods + 1] = -0.5 * l[0] * np.cos(thetas[0])
    thetaEqns.append(thetaEqn0)

    #generate the 2 through N-1 x, y, and theta E-L equations
    for i in range(1, numRods) :

        # x_i E_L equations -----------------------------------------------------------

        xEqni = np.zeros(3*numRods)

        xEqni[0] = ( mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[i]

        xEqni[1] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * np.cos(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2 ) * l[0] * l[i] * np.sin(thetas[0]) )

        for j in range(1, i) :
            xEqni[j+2] = (mu - nu) * l[j] * l[i] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i]) - (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[j] * l[i] * np.sin(thetas[j])

        xEqni[i+2] = -0.5 * nu * l[i]**2 * ( np.sin(thetas[i]) * np.cos(thetas[i])**2 + np.sin(thetas[i])**3 )

        xEqni[i + numRods + 1] = 1

        if i != (numRods-1) :
            xEqni[i + numRods + 2] = -1

        xEqns.append(xEqni)


        # y_i E_L equations -----------------------------------------------------------

        yEqni = np.zeros(3*numRods)

        yEqni[0] = ( mu - nu ) * l[i] * np.cos(thetas[i]) * np.sin(thetas[i])

        yEqni[1] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[i]

        yEqni[2] = 0.5 * ( (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[0] * l[i] * np.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * np.sin(thetas[0]) * np.sin(thetas[i]) * np.cos(thetas[i]) )

        for j in range(1, i) :
            yEqni[j+2] = ( mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2 ) * l[j] * l[i] * np.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])

        yEqni[i+2] = 0.5 * nu * l[i]**2 * (np.cos(thetas[i]) * np.sin(thetas[i])**2 + np.cos(thetas[i])**3)

        yEqni[i + (2*numRods)] = 1

        if i != (numRods-1) :
            yEqni[i + (2*numRods) + 1] = -1

        yEqns.append(yEqni)


        # theta_i E_L equations -----------------------------------------------------------

        thetaEqni = np.zeros(3*numRods)

        thetaEqni[i+2] = epsilon * l[i]**3

        thetaEqni[i + numRods + 1] = 0.5 * l[i] * np.sin(thetas[i])

        thetaEqni[i + (2*numRods)] = -0.5 * l[i] * np.cos(thetas[i])

        if i != (numRods-1) :
            thetaEqni[i + numRods + 2] = 0.5 * l[i] * np.sin(thetas[i])
            thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * np.cos(thetas[i])

        thetaEqns.append(thetaEqni)


        # stress calculations --------------------------------------------------------------
        #self.stressX.append( 0.5 * ( l[i-1]*np.cos(thetas[i-1]) - l[i]*np.sin(thetas[i]) ) )
        #self.stressY.append( 0.5 * ( -l[i-1]*np.sin(thetas[i-1]) + l[i]*np.cos(thetas[i]) ) )

    # combine all E-L equation coeffcients into a single array
    eqns = np.array( xEqns + yEqns + thetaEqns )

    # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
    #   are standard arrays
    #print(eqns)
    #input("stopza")

    if numRods == 1 : xVals, yVals, thetaVals = [0.0], [0.0], [0.0]
    else : xVals, yVals, thetaVals = [0.0], [0.0], [-kb * (thetas[0]-thetas[1])]

    for i in range(1, numRods) :

        xVal = -(mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i]))
        for j in range(1,i) :
            xVal -= (mu * np.cos(thetas[i])**2 + nu * np.sin(thetas[i])**2) * l[i] * lp[j] * np.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.sin(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        xVals.append(xVal)

        yVal = -(mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * np.sin(thetas[0]) + 0.5 * lp[i] * np.sin(thetas[i])) - (mu - nu) * l[i] * np.sin(thetas[i]) * np.cos(thetas[i]) * (0.5 * lp[0] * np.cos(thetas[0]) + 0.5 * lp[i] * np.cos(thetas[i]))
        for j in range(1,i) :
            yVal -= (mu * np.sin(thetas[i])**2 + nu * np.cos(thetas[i])**2) * l[i] * lp[j] * np.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * np.cos(thetas[j]) * np.sin(thetas[i]) * np.cos(thetas[i])
        yVals.append(yVal)

        if i == (numRods-1) :
            thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
        else :
            thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
        thetaVals.append(thetaVal)

    vals = np.array( xVals + yVals + thetaVals )


    numRods = len(q)-2

    sols = np.linalg.solve(eqns, vals)

    return sols


####################################################################################################
# Analysis and plotting
####################################################################################################

def subplot_index(psi, a) :
    """ Calculates the indicies of subplot corresponding to (psi, a)

        Inputs: psi -- float -- the value of the parameter psi = kb / (mu l0^3 r)
                a   -- float -- the value of the parameter a = nu / mu

        returns row, column (both integers) """

    # assumes psi range is [1, 2, 5, 10, 20, 50, 100]
    dict = {
        1 : 0,#6,
        2 : 1,#5,
        5 : 2,#4,
        10 : 3,
        20 : 4,#2,
        50 : 5,#1,
        100 : 6#0
    }
    row = dict[psi]

    # assumes a range is [1, 2, 5, 10, 20, 50, 100]
    dict = {
        1 : 0,
        2 : 1,
        5 : 2,
        10 : 3,
        20 : 4,
        50 : 5,
        100 : 6
    }
    col = dict[a]

    return row, col

#---------------------------------------------------------------------------------------------------

def stressOrganization(simulationSets) :

    newSets = []

    for s in simulationSets :

        # grab the parameter values and subplot indicies
        params, plotParams, d = s
        psi, a, b, l0, numRods_init, angle, seed_val = params
        row, col = subplot_index(psi, a)

        qVecs = list(d["q"])

        stressXs, stressYs = [], []
        stressMags = []
        stressMeans = []
        stressParallels = []
        stressPerps = []
        for i in range(0, len(d["t"]), 1) :

            # isolate theta values and compute deltaTheta values
            thetas = qVecs[i][2:]
            deltaThetas = [thetas[i] - thetas[i-1] for i in range(1, len(thetas))]

            # isloate the stress data sets
            stressX, stressY = list(d["Fx"][i]), list(d["Fy"][i])

            # compute the stress parallel to the current node
            stressParallel = np.array( [ stressX[j] * np.cos( deltaThetas[j]/2 ) + stressY[j] * np.sin(deltaThetas[j]/2) for j in range(len(stressX)) ] )

            stressPerp = np.array( [ stressX[j] * np.sin( deltaThetas[j]/2 ) - stressY[j] * np.cos(deltaThetas[j]/2) for j in range(len(stressX)) ] )

            # add in stress at tips (fpara, fperp = 0)
            stressParallel = np.insert(stressParallel, [0, len(stressParallel)], [0,0])
            stressPerp = np.insert(stressPerp, [0, len(stressPerp)], [0,0])

            stressParallels.append(stressParallel)
            stressPerps.append(stressPerp)

            # add in stress at tips (fx, fy = 0)
            stressX = np.insert(stressX, [0, len(stressX)], [0,0])
            stressY = np.insert(stressY, [0, len(stressY)], [0,0])

            stressXs.append(stressX)
            stressYs.append(stressY)

            # compute the magnitudes of the stresses
            stress = [ np.sqrt(stressX[j]**2 + stressY[j]**2) for j in range(len(stressX)) ]

            stressMags.append(stress)
            stressMeans.append( np.mean(stress) )

        d['Fx'] = stressXs
        d['Fy'] = stressYs
        d["Fmag"] = stressMags
        d["Favg"] = stressMeans
        d["Fparallel"] = stressParallels
        d["Fperp"] = stressPerps

        newSets.append( (params, plotParams, d) )

    return newSets

#---------------------------------------------------------------------------------------------------

def config_curvature_analysis(simulationSets) :

    print("\tCONFIGURATION PLOTS...")

    exp_rel = np.zeros((7,7))

    for s in simulationSets :
        #s = simulationSets[0]

        params, plotParams, d = s

        t = list(d["t"])

        psi, a, b, l0, numRods_init, angle, seed_val = params

        minX, maxX, minY, maxY = plotParams
        minAx = round( min( [minX, minY] ) - 5 )
        maxAx = round( max( [maxX, maxY] ) + 5 )

        nodeX, nodeY = d["xNode"], d["yNode"]
        qVecs = list(d["q"])

        comp_curv = ComputeCurvature()
        curvatures = []
        curvatureProfiles = []
        radii = []

        #expDist = []

        #plt.figure(1)
        minAx = round( min( [minX, minY] ) - 5 )
        maxAx = round( max( [maxX, maxY] ) + 5 )
        #axesRange = [round(minAx-5), round(maxAx+5), round(minAx-5), round(maxAx+5)]

        plt.rcParams['savefig.bbox'] = 'standard'
        plt.rcParams['lines.linewidth'] = 3
        plt.rcParams['lines.markersize'] = 8

        fig = plt.figure(1)
        l1, = plt.plot([], [], '-bo')       # filament 1 nodes / cells
        l2, = plt.plot([], [], '-r')       # filament 2 nodes / cells
        #l3, = plt.plot([], [], 'go')        # filament 1 binding sites
        #l4, = plt.plot([], [], 'go')        # filament 2 binding sites

        plt.axis('scaled')

        plt.xlabel(r"$x$ coordinate ($\mathrm{\mu}$m)", fontsize=16)
        plt.ylabel(r"$y$ coordinate ($\mathrm{\mu}$m)", fontsize=16)

        plt.xlim(minAx, maxAx)
        plt.ylim(minAx, maxAx)

        plt.xticks( plt.yticks()[0][1:-1] )

        framesPerSec = 10
        #framesPerSec = round(len(data)/data[-1][0]*10)
        #print("fps =", framesPerSec)

        # sets up video output stuff
        FFMpegWriter = manimation.writers['ffmpeg']
        metadata = dict(title='Movie Test', artist='Matplotlib',
                        comment='Movie support!')
        writer = FFMpegWriter(fps=framesPerSec, metadata=metadata)

        # remember to only use on .csv files in the data.dir directory
        # Splicing [8:-4] removes the "data.dir" at the front and ".csv" at the end
        #outfile = "plots.dir" + str(sys.argv[1])[8:-4] + ".mp4"
        #outfile = "plots.dir" + filename[8:-4] + ".mp4"
        #outfile = f"{output}/configurations/configuration_psi{round(psi,0)}_a{round(a,0)}_N{numRods_init}.mp4"
        outfile = f"{output}/psi{round(psi)}_a{round(a)}.dir/configuration_psi{round(psi,0)}_a{round(a,0)}_l0{round(l0)}_N{numRods_init}.mp4"
        #print(outfile)
        count = 0

        with writer.saving(fig, outfile, 100):
        # third arugment is dpi --> controls resolution and size of the plot
            """for d in data :

                #if count % 20 == 0 :  # indent the 3 lines below and uncomment this line
                plt.title("Filament Configuration at t = " + str(round(d[0],2)) )
                l1.set_data(d[1], d[2])
                #l2.set_data(d[3], d[4])

                writer.grab_frame()

                count+=1
                #plt.savefig('plots.dir/snapshots.dir/rigid-rod'+ str(round(d[0],0))+'.svg', format='svg')"""

            for i in range(0, len(t), 1) :
                print(i)
                plt.title(rf"Chain Configuration at t = {str(round(t[i]))} min ($\Psi$ = {round(psi)}, $a$ = {round(a)})", fontsize=16)
                l1.set_data(nodeX[i], nodeY[i])

                # compute the horizontal expansion distance
                #expDist.append( nodeX[i][-1] - nodeX[i][0] )

                # compute the curvature using the method of Menger curvature
                thetas = qVecs[i][2:]
                deltaThetas = [thetas[k+1]-thetas[k] for k in range(len(thetas)-1)]

                curvature = [0]
                for j in range( len( deltaThetas ) ) :

                    numer = - 2 * np.sin( deltaThetas[j] )
                    pt1 = np.array( (nodeX[i][j], nodeY[i][j]) )
                    pt2 = np.array( [nodeX[i][j+2], nodeY[i][j+2]] )
                    denom = np.linalg.norm(pt1-pt2)
                    kappa =  numer / denom
                    curvature.append(kappa)

                curvature.append(0)
                curvatures.append(curvature)

                #kappa_profile = sum( [ kappa**2 for kappa in curvature ] ) / len(curvature)
                kappa_profile = sum( [ dTheta**2 for dTheta in deltaThetas ] ) / len(deltaThetas)
                curvatureProfiles.append( kappa_profile )

                writer.grab_frame()

                # use the next line to save indivdual frames if desired
                #if count == 0 or int(round(t[i],0)) == 90 :
                #    plt.savefig('plots.dir/configuration' + str(round(t[i],0))+'.pdf', format='pdf')

                count+=1

        plt.gcf().clear()
        plt.style.use('./mystyle.mplstyle')

        #plt.rcParams['axes.grid'] = False

        # import the predicted critical kinking length
        kp = []
        with open("fitParams.csv") as f :
            csv_reader = csv.reader(f, delimiter=',')
            for row in csv_reader : kp.append(row)
        kp = kp[1:]     # remove the header row
        temp = list(filter(lambda x : ( round(float(x[0])) == round(a) ), kp))
        m, z = float(temp[0][2]), float(temp[0][1])
        nCrit_k = np.sqrt( 8 * (m * psi + z) )

        # import the predicted critical buckling length
        bp = []
        with open("bucklingParameters.csv") as f :
            csv_reader = csv.reader(f, delimiter=',')
            for row in csv_reader : bp.append(row)
        temp = list(filter(lambda x : ( x[0] == str(int(psi)) and x[1] == str(int(a)) ), bp))
        nCrit_b = float(temp[0][3])

        fig, ax = plt.subplots()

        plt.title(rf"Curvature profile and chain length ($\Psi$ = {round(psi)}, $a$ = {round(a)})")
        plt.xlabel(r"Time (mins)")
        #plt.ylabel(r"Curvature profile = -$\frac{1}{2} \int_{0}^{1} \kappa^2 dq$")

        ax.plot(t, curvatureProfiles, '-b', label='Curvature profile')
        #ax.plot(t, np.gradient( np.array(curvatureProfiles) ), '-b', label='Curvature profile rate')

        r = 0.02
        tdiv = np.log(2) / r
        chain_length = lambda t : numRods_init * l0 * np.exp(r * t)

        LCrit_k = nCrit_k * l0
        LCrit_b = lambda t : nCrit_b * l0 * np.exp( 0.25 * r * (t % tdiv) )

        ax.vlines([tdiv, 2*tdiv], ymin=min(curvatureProfiles), ymax=max(curvatureProfiles), colors='k', linestyles='dashed')

        ax2 = ax.twinx()
        ax2.plot(t, chain_length(np.array(t)), '-g', label="Chain length")
        #ax2.hlines(y=nCrit_b, xmin=min(t), xmax=max(t), colors='r', label=r"$N_{crit}$ buckling")
        ax2.plot(t, LCrit_b(np.array(t)), '-r', label=r"$N_{crit}$ buckling")
        ax2.hlines(y=LCrit_k, xmin=min(t), xmax=max(t), colors='r', linestyles='dashed', label=r"$N_{crit}$ kinking")

        plt.legend(loc='best')

        #plt.savefig(f'{output}/curvatureProfiles/profileVsTime_psi{str(round(psi))}_a{str(round(a))}.pdf', format='pdf')
        plt.savefig(f'{output}/psi{round(psi)}_a{round(a)}.dir/profileVsTime_psi{str(round(psi))}_a{str(round(a))}.pdf', format='pdf')


        plt.gcf().clear()

        # expansion plotting

        t_crit = np.log(nCrit_k / numRods_init) / r
        D_crit = numRods_init * l0 * np.exp( r * t_crit )
        Ddot = 0.5 * nCrit_k * l0 * r / np.log(2)

        t = np.array(t)
        expDist_theory = np.piecewise(t, [t <= t_crit, t > t_crit], [lambda t: numRods_init * l0 * np.exp( r * t ), lambda t : Ddot * (t-t_crit) + (nCrit_k * l0)])
        #expDist_theory = Ddot * t + (numRods_init * l0)

        row, col = subplot_index(psi, a)
        exp_rel[row, col] = expDist[-1] / expDist_theory[-1]

        #chain_expansion = lambda t : np.piecewise(t, [t <= t_crit, t > t_crit], [numRods_init * l0 * np.exp( r * t ), D_crit + Ddot * t])
        """def chain_expansion(t) :
            if t < t_crit : return numRods_init * l0 * np.exp( r * t )
            else : return D_crit + Ddot * t"""

        plt.title(rf"Expansion distance ($\Psi$ = {round(psi)}, $a$ = {round(a)})")
        plt.xlabel(f"Time (min)")
        plt.ylabel(f"Horizontal expansion distance")
        plt.plot(t, expDist, label="Simulation")
        plt.plot(t, chain_length(np.array(t)), label="Exponential growth")
        plt.plot(t, expDist_theory, label="Expansion with breaks")
        plt.legend(loc='best')

        plt.savefig(f'{output}/psi{round(psi)}_a{round(a)}.dir/expDist_psi{str(round(psi))}_a{str(round(a))}.pdf', format='pdf')

        plt.gcf().clear()


        """def align_zeros(axes):
            #https://stackoverflow.com/questions/55646777/matplotlib-aligning-two-y-axis-around-zero

            ylims_current = {}   #  Current ylims
            ylims_mod     = {}   #  Modified ylims
            deltas        = {}   #  ymax - ymin for ylims_current
            ratios        = {}   #  ratio of the zero point within deltas

            for ax in axes:
                ylims_current[ax] = list(ax.get_ylim())
                                # Need to convert a tuple to a list to manipulate elements.
                deltas[ax]        = ylims_current[ax][1] - ylims_current[ax][0]
                ratios[ax]        = -ylims_current[ax][0]/deltas[ax]

            for ax in axes:      # Loop through all axes to ensure each ax fits in others.
                ylims_mod[ax]     = [np.nan,np.nan]   # Construct a blank list
                ylims_mod[ax][1]  = max(deltas[ax] * (1-np.array(list(ratios.values()))))
                                # Choose the max value among (delta for ax)*(1-ratios),
                                # and apply it to ymax for ax
                ylims_mod[ax][0]  = min(-deltas[ax] * np.array(list(ratios.values())))
                                # Do the same for ymin
                ax.set_ylim(tuple(ylims_mod[ax]))"""

        #plt.rcParams['axes.grid'] = False

        num_modes = 7

        fit_vals = np.zeros((num_modes, len(curvatures)))
        fit_errs = np.zeros((num_modes, len(curvatures)))
        fit_r2s = np.zeros((num_modes, len(curvatures)))

        for i in range( len(curvatures) ) :

            # perfrom curvature best fit
            zeta = np.linspace(-1, 1, num=len(curvatures[i]), endpoint=True)

            """def Fkappa(var, i) :
                #var = 2 * var - 1
                if i==0 : return 0
                elif (i%2) != 0 :     # odd mode
                    return np.cos( (i-1) * np.pi * var / 2 ) + np.cos( (i+1) * np.pi * var / 2 )
                else :              # even mode
                    return np.sin( (i-1) * np.pi * var / 2 ) + np.sin( (i+1) * np.pi * var / 2 )
                    #return 0"""

            """def Fkappa(var, i)  :
                n = 2 * i + 1
                return np.cos( (n-1) * np.pi * var / 2 ) + np.cos( (n+1) * np.pi * var / 2 )

            def func(var, *c) :
                return c[i] * Fkappa(var, i) for i in range(len(c)) """

            for j in range(num_modes) :

                n = 2*j + 1
                func = lambda zeta, c : c * ( np.cos( (n-1) * np.pi * zeta / 2 ) + np.cos( (n+1) * np.pi * zeta / 2 ) )

                p0 = [0]
                popt, pcov = optimize.curve_fit(f=func, xdata=zeta, ydata=curvatures[i], p0=p0)
                perr = np.sqrt(np.diag(pcov))

                r2 = sum( ( func(zeta, popt) - np.array(curvatures[i]) )**2 )

                fit_vals[j,i] = popt
                fit_errs[j,i] = perr
                fit_r2s[j,i] = r2

            #p0 = tuple( [0] * num_params )
            #popt, pcov = optimize.curve_fit(f=func, xdata=zeta, ydata=curvatures[i], p0=p0)

            """fit_set = np.zeros(num_params_total)
            for j in range( len(popt) ) : fit_set[j] = popt[j]
            fit_sets.append(fit_set)

            popt_norm = np.abs(popt) / max( np.abs(popt) )
            fit_set_norm = np.zeros(num_params_total)
            for j in range( len(popt_norm) ) : fit_set[j] = popt_norm[j]
            fit_sets_norm.append(fit_set_norm)

            fit_data = func(zeta, *tuple(popt)) """

            if i%5 == 0 :

                mode_best = np.argmin(fit_r2s[:,i])

                n = 2 * mode_best + 1
                func = lambda zeta, c : c * ( np.cos( (n-1) * np.pi * zeta / 2 ) + np.cos( (n+1) * np.pi * zeta / 2 ) )

                pbest = fit_vals[mode_best, i]
                zeta_data = np.linspace(-1, 1, num=1000, endpoint=True)
                fit_data = func(zeta_data, pbest)

                fig, ax = plt.subplots()
                plt.title(rf"Curvature Profile at t = {round(t[i],0)} mins ($\Psi$ = {round(psi)}, $a$ = {round(a)})")

                ax.plot(zeta, curvatures[i], '-ro', label='Simulation')
                ax.plot(zeta_data, fit_data, '-b', label=rf'Best Fit ($n$ = {2*mode_best+1})')
                ax.hlines(y=0, xmin=min(zeta), xmax=max(zeta), colors='k')


                plt.legend(loc='best')

                plt.xlabel(r"Location $\zeta$")
                ax.set_ylabel(r"Curvature $\kappa$ ($\mu$m$^{-1}$)")

                #plt.savefig(f'{output}/curvatureProfiles/psi{str(round(psi))}_a{str(round(a))}.dir/time{str(round(t[i],0))}.png', format='png')

                plt.savefig(f'{output}/psi{round(psi)}_a{round(a)}.dir/curvatureSnapshots.dir/time{str(round(t[i],0))}.png', format='png')

                plt.gcf().clear()
                plt.close()

        best_modes = [ 2*np.argmin(fit_r2s[:,i])+1 for i in range(len(curvatures)) ]
        plt.plot(t, best_modes, linestyle='solid')
        plt.title(rf"Best Fit Mode ($\Psi$ = {round(psi)}, $a$ = {round(a)})")
        plt.xlabel(r"Time (min)")
        plt.ylabel(r"Mode $n$")
        plt.savefig(f'{output}/psi{round(psi)}_a{round(a)}.dir/bestMode_psi{round(psi)}_a{round(a)}.png', format='png')
        plt.gcf().clear()
        plt.close()

        from itertools import cycle
        styles = ["solid", "dashed", "dashdot", "dotted"]
        style_cycler = cycle(styles)
        current_style = next(style_cycler)

        """fit_sets = np.array(fit_sets)
        for i in range( num_params ) :
            coeff_str = '{' + str(2*i+1) + '}'
            plt.plot(t, fit_sets[:,i], label=rf'$c_{coeff_str}$', linestyle=current_style)
            if (i+1) % 6 == 0 : current_style = next(style_cycler)
        plt.vlines(np.log(nCrit_b/numRods_init)/r, ymin=0, ymax=1, colors='k', linestyles='dashed')
        plt.title(rf"Fit parameter emergence ($\Psi$ = {round(psi)}, $a$ = {round(a)})")
        plt.xlabel(r"Time (min)")
        plt.ylabel(r"Normalized fit coefficient $c_n$")
        plt.legend(loc='best')
        plt.savefig(f'{output}/fit_params_psi{round(psi)}_a{round(a)}.png', format='png')"""

        for i in range( num_modes ) :
            coeff_str = '{' + str(2*i+1) + '}'
            plt.plot(t, fit_vals[i], label=rf'$c_{coeff_str}$', linestyle=current_style)
            if (i+1) % 6 == 0 : current_style = next(style_cycler)
        #plt.vlines(np.log(nCrit_b/numRods_init)/r, ymin=0, ymax=1, colors='k', linestyles='dashed')
        plt.title(rf"Fit parameter emergence ($\Psi$ = {round(psi)}, $a$ = {round(a)})")
        plt.xlabel(r"Time (min)")
        plt.ylabel(r"Fit coefficient $c_n$")
        plt.legend(loc='best')
        plt.savefig(f'{output}/fit_params_psi{round(psi)}_a{round(a)}.png', format='png')

        plt.gcf().clear()
        plt.close()

        for i in range( num_modes ) :
            coeff_str = '{' + str(2*i+1) + '}'
            plt.plot(t, fit_r2s[i], label=rf'$c_{coeff_str}$', linestyle=current_style)
            if (i+1) % 6 == 0 : current_style = next(style_cycler)
        #plt.vlines(np.log(nCrit_b/numRods_init)/r, ymin=0, ymax=1, colors='k', linestyles='dashed')
        plt.title(rf"Fit residiuals squared ($\Psi$ = {round(psi)}, $a$ = {round(a)})")
        plt.xlabel(r"Time (min)")
        plt.ylabel(r"Residual sqaured for $c_n$")
        plt.legend(loc='best')
        plt.savefig(f'{output}/psi{round(psi)}_a{round(a)}.dir/r2s_psi{round(psi)}_a{round(a)}.png', format='png')

        plt.gcf().clear()
        plt.close()

    print("\t\t Done!")


#---------------------------------------------------------------------------------------------------


def configurationPlotGrid(simulationSets) :

    plt.rcParams['savefig.bbox'] = 'standard'
    plt.rcParams['lines.linewidth'] = 3
    plt.rcParams['lines.markersize'] = 8

    print("\tCONFIGURATION PLOT GRID...")

    comp_curv = ComputeCurvature()

    # initialize the subplot
    fig, axs = plt.subplots(7,7, sharex=False, sharey=False, figsize=(24,18))

    count = 0   # counter to track how many frames are complete

    t = list(simulationSets[0][2]["t"])
    for i in range(0, len(t), 1) :
        print(i)
        fig.suptitle(rf"Chain Configurations at t = {str(round(t[i]))} min", fontsize=48)

        cols = ['{}'.format(col) for col in [1, 2, 5, 10, 20, 50, 100]]
        rows = ['{}'.format(row) for row in [100, 50, 20, 10, 5, 2, 1]]

        for s in simulationSets :

            params, plotParams, d = s

            psi, a, b, numRods_init, angle = params

            minX, maxX, minY, maxY = plotParams
            minAx = round( min( [minX, minY] ) - 5 )
            maxAx = round( max( [maxX, maxY] ) + 5 )

            nodeX, nodeY = d["xNode"], d["yNode"]

            row, col = subplot_index(psi, a)
            ax = axs[row, col]
            ax.clear()
            ax.axis('scaled')
            #ax.set_xlabel(r"$x$ ($\mathrm{\mu}$m)", fontsize=20)
            #ax.set_ylabel(r"$y$ ($\mathrm{\mu}$m)", fontsize=20)

            ax.set_xlim(minAx, maxAx)
            ax.set_ylim(minAx, maxAx)

            ax.set_xticks( ax.get_yticks()[1:-1] )
            #ax.tick_params(axis='both', which='major', labelsize=20)

            # compute curvature and circle data
            numMiddle = 7   # number of nodes in the "middle"; always use even numCells so this should be odd
            numNodes = len(nodeX[i])
            index = numMiddle + ( (numNodes - numMiddle) // 2 )
            x = np.array( nodeX[i][-index:index] )
            y = np.array( nodeY[i][-index:index] )
            radius, xc, yc = comp_curv.circleFit(x, y)

            theta_circ = np.linspace(0, 2*np.pi, 150)
            x_circ = xc + radius * np.cos(theta_circ)
            y_circ = yc + radius * np.sin(theta_circ)

            ax.plot(nodeX[i], nodeY[i], '-bo')      # plot the configuration
            #ax.plot(x_circ, y_circ, '-r')           # plot the fitted circle

        #plt.text(x=-3, y=5, s=r"$a$", fontsize=48)
        #plt.text(x=0, y=0, s=r"$a$", fontsize=48)

        pad = 5 # in points

        for ax, col in zip(axs[-1], cols):
            ax.annotate(rf"$a$ = {col}", xy=(0.5, -0.5), xytext=(0, pad),
                        xycoords='axes fraction', textcoords='offset points',
                        size=32, ha='center', va='baseline')

        for ax, row in zip(axs[:,0], rows):
            ax.annotate(rf"$\Psi$ = {row}", xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                        xycoords=ax.yaxis.label, textcoords='offset points',
                        size=32, ha='right', va='center')

        """axs[0,0].annotate(r"$a$", xy=(0.5, -0.5), xytext=(0, pad),
                    xycoords='axes fraction', textcoords='offset points',
                    size=48, ha='center', va='baseline')
        axs[0,0].annotate(r"$\Psi$", xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size=48, ha='right', va='center')"""

        plt.savefig(f'{output}/configurationGrid/time{str(round(t[i],0))}.png', format='png')

        count+=1


    plt.gcf().clear()
    plt.style.use('./mystyle.mplstyle')

    print("\t\t Done!")


#---------------------------------------------------------------------------------------------------

def stressAnalysis(simulationSets) :

    crit_check = np.zeros((7,7))
    crit_time_buckled = np.empty((7,7))
    crit_time_buckled[:] = np.NaN
    crit_time_straight = np.empty((7,7))
    crit_time_straight[:] = np.NaN

    r = 0.02 # 1/min
    mu0 = 5/278

    for s in simulationSets :
        #s = simulationSets[0]

        params, plotParams, d = s
        t = np.array(list(d["t"]))
        psi, a, b, l0, numRods_init, angle, seed_val = params

        qVecs = list(d["q"])

        # get and non-dimensionalize the stress components
        stressX, stressY = np.array(d["Fx"]), np.array(d["Fy"])
        stressX_nd = stressX / (mu0 * l0**2 * r)
        stressY_nd = stressY / (mu0 * l0**2 * r)

        # get and non-dimensionalize the stress magnitudes
        stressMag = np.array(d["Fmag"])     # (stressX**2 + stressY**2)**(1/2)
        stressMag_nd = (stressX_nd**2 + stressY_nd**2)**(1/2)

        # get and non-dimesnionalize the parallel and perpendicular stresses
        stressParallel = np.array(d["Fparallel"])
        stressPerp = np.array(d["Fperp"])

        stressParallel_nd = stressParallel / (mu0 * l0**2 * r)
        stressPerp_nd = stressPerp / (mu0 * l0**2 * r)

        # compute the stress in the center most linkage
        center_stress_nd = [ stressMag_nd[i][ (len(stressMag_nd[i])-1)//2 ] for i in range(len(stressMag_nd)) ]

        # compute the largest stress in the chain at each point in the simulation
        max_stress_nd = [ max(stressMag_nd[i]) for i in range(len(stressMag_nd)) ]

        # compute the stress in the center linkage of a corresponding straight chain
        stress_straight_nd = lambda t : numRods_init**2 * np.exp(2 * r * t) / 8

        # import the predicted critical kinking length
        kp = []
        with open("fitParams.csv") as f :
            csv_reader = csv.reader(f, delimiter=',')
            for row in csv_reader : kp.append(row)
        kp = kp[1:]     # remove the header row
        temp = list(filter(lambda x : ( round(float(x[0])) == round(a) ), kp))
        m, z = float(temp[0][2]), float(temp[0][1])
        crit_stress_nd = m * psi + z

        # plot the magnitude of the stress in the center linkage
        plt.title(rf"Stress in the center linkage ($\Psi$ = {round(psi)}, $a$ = {round(a)})")
        plt.xlabel("Time (min)")
        plt.ylabel("Stress (non-dimensional)")
        plt.plot(t, center_stress_nd, '-', label="Simulation")
        #plt.plot(t, max_stress_nd, '-', label="Simulation max")
        plt.plot(t, stress_straight_nd(t), '-', label="Straight chain")
        plt.hlines(crit_stress_nd, xmin=t[0], xmax=t[-1], colors='k', linestyles='dashed', label="Critical breaking stress")
        plt.legend(loc='best')

        plt.savefig(f'{output}/psi{round(psi)}_a{round(a)}.dir/center_stress.pdf', format='pdf')

        plt.gcf().clear()
        plt.close()

        exit()



        for i in range(0, len(t), 5) :

            row, col = subplot_index(psi, a)

            if crit_check[row, col] == 0 :
                try :
                    if (max_stress_nd[i] > crit_stress_nd) and (max_stress_nd[i+1] > crit_stress_nd) :
                        crit_check[row, col] = 1
                        crit_time_buckled[row, col] = t[i]

                except IndexError :
                    if max_stress_nd[i] > crit_stress_nd : crit_check[row, col] = 1

            if np.isnan(crit_time_straight[row, col]) :
                if stress_straight_nd(t[i]) > crit_stress_nd : crit_time_straight[row,col] = t[i]

            # compute the delta thetas
            thetas = qVecs[i][2:]
            deltaThetas = [thetas[k+1]-thetas[k] for k in range(len(thetas)-1)]

            """# plot of the profile of the stress magnitude at each point in time
            plt.title(rf"$t$ = {round(t[i])}, $\Psi$ = {round(psi)}, $a$ = {round(a)}")
            plt.xlabel(r"$\Delta\theta$")
            plt.ylabel("Stress magnitude (non-dimensional)")
            plt.plot(deltaThetas, stressMag_nd[i][1:-1], '-')

            plt.savefig(f'{output}/psi{round(psi)}_a{round(a)}.dir/stressVsDeltaTheta.dir/time{round(t[i])}.pdf', format='pdf')

            plt.gcf().clear()
            plt.close()"""


            # plot of the profile of the stress magnitude at each point in time
            plt.title(rf"$t$ = {round(t[i])} min, $\Psi$ = {round(psi)}, $a$ = {round(a)}, " + r"$F^*_{crit}$ = " + f"{round(crit_stress_nd)}")
            plt.xlabel(r"Parameterized position ($\zeta$)")
            plt.ylabel("Stress magnitude (non-dimensional)")
            zeta = np.linspace(-1, 1, num=len(stressMag_nd[i]), endpoint=True)
            plt.plot(zeta, stressMag_nd[i], 'o-')
            #plt.hlines(crit_stress_nd, xmin=zeta[0], xmax=zeta[-1], colors='k', linestyles='dashed', label="Critical stress")
            #plt.legend(loc='best')

            plt.savefig(f'{output}/psi{round(psi)}_a{round(a)}.dir/stressProfiles.dir/time{round(t[i])}.pdf', format='pdf')

            plt.gcf().clear()
            plt.close()

            # plot of the profile of the x/y components of the stress
            fig, ax = plt.subplots()
            ax2 = ax.twinx()

            ax.set_title(rf"$t$ = {round(t[i])} min, $\Psi$ = {round(psi)}, $a$ = {round(a)}")
            ax.set_xlabel(r"Parameterized position ($\zeta$)")
            ax.set_ylabel(r"$F_x$ (non-dimensional)", color='b')
            ax2.set_ylabel(r"$F_y$ (non-dimensional)", color='r')

            zeta = np.linspace(-1, 1, num=len(stressX_nd[i]), endpoint=True)
            ax.plot(zeta, stressX_nd[i], 'bo-')
            ax2.plot(zeta, stressY_nd[i], 'ro-')

            plt.savefig(f'{output}/psi{round(psi)}_a{round(a)}.dir/stress_x-y.dir/time{round(t[i])}.pdf', format='pdf')

            plt.gcf().clear()
            plt.close()

            # plot of the profile of the parallell/perp components of the stress
            fig, ax = plt.subplots()
            ax2 = ax.twinx()

            ax.set_title(rf"$t$ = {round(t[i])} min, $\Psi$ = {round(psi)}, $a$ = {round(a)}")
            ax.set_xlabel(r"Parameterized position ($\zeta$)")
            ax.set_ylabel(r"$F_{\parallel}$ (non-dimensional)", color='b')
            ax2.set_ylabel(r"$F_{\perp}$ (non-dimensional)", color='r')

            zeta = np.linspace(-1, 1, num=len(stressParallel_nd[i]), endpoint=True)
            ax.plot(zeta, stressParallel_nd[i], 'bo-')
            ax2.plot(zeta, stressPerp_nd[i], 'ro-')


            plt.savefig(f'{output}/psi{round(psi)}_a{round(a)}.dir/stress_para-perp.dir/time{round(t[i])}.pdf', format='pdf')

            plt.gcf().clear()
            plt.close()


    with open(f'{output}/crit_stress_check.csv', 'w', newline='') as file :
        mywriter = csv.writer(file, delimiter=',')
        mywriter.writerows(crit_check)

    with open(f'{output}/crit_time_buckled.csv', 'w', newline='') as file :
        mywriter = csv.writer(file, delimiter=',')
        mywriter.writerows(crit_time_buckled)

    with open(f'{output}/crit_time_straight.csv', 'w', newline='') as file :
        mywriter = csv.writer(file, delimiter=',')
        mywriter.writerows(crit_time_straight)

    crit_time_diff = crit_time_straight - crit_time_buckled

    with open(f'{output}/crit_time_diff.csv', 'w', newline='') as file :
        mywriter = csv.writer(file, delimiter=',')
        mywriter.writerows(crit_time_diff)

#---------------------------------------------------------------------------------------------------

def expansionAnalysis(simulationSets) :

    exp_rel = np.zeros((7,7))

    for s in simulationSets :

        params, plotParams, d = s

        t = list(d["t"])

        psi, a, b, l0, numRods_init, angle, seed_val = params

        nodeX, nodeY = d["xNode"], d["yNode"]
        qVecs = list(d["q"])

        # compute the horizontal expansion distances
        expDist = [nodeX[i][-1] - nodeX[i][0] for i in range(0, len(t), 1)]

        # import the predicted critical kinking length
        kp = []
        with open("fitParams.csv") as f :
            csv_reader = csv.reader(f, delimiter=',')
            for row in csv_reader : kp.append(row)
        kp = kp[1:]     # remove the header row
        temp = list(filter(lambda x : ( round(float(x[0])) == round(a) ), kp))
        m, z = float(temp[0][2]), float(temp[0][1])
        nCrit_k = np.sqrt( 8 * (m * psi + z) )


        r = 0.02
        tdiv = np.log(2) / r
        chain_length = lambda t : numRods_init * l0 * np.exp(r * t)

        LCrit_k = nCrit_k * l0
        LCrit_b = lambda t : nCrit_b * l0 * np.exp( 0.25 * r * (t % tdiv) )

        # expansion plotting

        t_crit = np.log(nCrit_k / numRods_init) / r
        D_crit = numRods_init * l0 * np.exp( r * t_crit )
        Ddot = 0.5 * nCrit_k * l0 * r / np.log(2)

        t = np.array(t)
        expDist_theory = np.piecewise(t, [t <= t_crit, t > t_crit], [lambda t: numRods_init * l0 * np.exp( r * t ), lambda t : Ddot * (t-t_crit) + (nCrit_k * l0)])
        #expDist_theory = Ddot * t + (numRods_init * l0)

        row, col = subplot_index(psi, a)
        exp_rel[row, col] = expDist[-1] / expDist_theory[-1]


        plt.title(rf"Expansion distance ($\Psi$ = {round(psi)}, $a$ = {round(a)})")
        plt.xlabel(f"Time (min)")
        plt.ylabel(f"Horizontal expansion distance")
        plt.plot(t, expDist, label="Simulation")
        plt.plot(t, chain_length(np.array(t)), label="Exponential growth")
        plt.plot(t, expDist_theory, label="Expansion with breaks")
        plt.legend(loc='best')

        plt.savefig(f'{output}/psi{round(psi)}_a{round(a)}.dir/expDist_psi{str(round(psi))}_a{str(round(a))}.pdf', format='pdf')

        plt.gcf().clear()

    with open(f'{output}/exp_efficiency.csv', 'w', newline='') as file :
        mywriter = csv.writer(file, delimiter=',')
        mywriter.writerows(exp_rel)

#---------------------------------------------------------------------------------------------------

####################################################################################################
# Data Organization
####################################################################################################

# formats figures with larger fonts
#plt.rcParams.update({'figure.autolayout': True})
plt.style.use('./mystyle.mplstyle')
#plt.rcParams['text.usetex'] = True
#plt.rcParams['axes.grid'] = False

comp_curv = ComputeCurvature()

print("LOADING DATA...")
count = 1

output = sys.argv[1]

simulationSets = []
for file in sys.argv[2:] :

    try :
        filename = str(file)
    except IndexError :
        print("If loading package, continue, otherwise\nERROR: No input file given -> try again")
        exit()

    #sol = pickle.load(open("data.dir/exponentialGrowth_epsilonLJ0.0_rm1.5_Lc5.0_endTime180.0.pkl", "rb"))
    sol = pickle.load(open(filename, "rb"))
    l0, growthRate, kb, mu, nu, epsilon, numRods_init, angle, rtol, atol, seed_val = sol[0]
    psi = int(kb / (mu * l0**2 * growthRate))
    a = int(nu/mu)
    b = epsilon/mu
    sol = sol[1:]

    #print( sum( [ sum( [ len(sol[i].y[j]) for j in range( len(sol[i].y) ) ] ) for i in range( len(sol) ) ] ) )
    #print( len(sol[0].y[0]) )
    #print( len(sol[0].t) )
    # use these three lines to coarsen data if desired
    n = 100     # take every nth output
    for s in sol :
        s.t = s.t[0::n]
        s.y = np.array( [ s.y[i][0::n] for i in range(len(s.y)) ] )
    #print( sum( [ sum( [ len(sol[i].y[j]) for j in range( len(sol[i].y) ) ] ) for i in range( len(sol) ) ] ) )
    #print( len(sol[0].y[0]) )
    #print( len(sol[0].t) )



    print(f"{count} ORGANIZING DATA ... Psi = {psi} \t a = {a}")
    count += 1

    t = np.concatenate( [s.t for s in sol], axis=None )

    minX, maxX, minY, maxY = 1E100, 0, 1E100, 0
    qs, qdot, lambdaX, lambdaY, nodeX, nodeY = [], [], [], [], [], []
    lastAddTime = [sol[0].t[0]]
    ltVec, lpVec = [], []

    qVec = []
    for i in range( len(sol[-1].y) ) : qVec.append( [None] * len(t) )

    k = 0   # time index

    for s in sol :

        #print("starting a new sol")

        q = [ s.y[:,i] for i in range(len(s.y[0])) ]
        numRods = len(q[0]) - 2

        lt = [ [ length(time, l0, growthRate, lastAddTime[-1]) ] * numRods for time in s.t ]
        ltVec += lt
        lp = [ [ growthRateFunc(time, l0, growthRate, lastAddTime[-1]) ] * numRods for time in s.t ]
        lpVec += lp

        # interating over time
        for i in range(len(q)) :

            numRods = len(q[i])-2
            lt = length(s.t[i], l0, growthRate, lastAddTime[-1])
            lp = growthRateFunc(s.t[i], l0, growthRate, lastAddTime[-1])

            # organize q
            for j in range( len(s.y) ) :
                qVec[j][k] = s.y[j][i]
            k += 1

            # compute qdots and Lagrange multipliers
            sols = growthDynamics(s.t[i], q[i], lt, kb, mu, nu, epsilon, lp)

            # organize q arrays
            qs.append(q[i])

            # organize qdots
            qdots = sols[: numRods+2]
            qdot.append(qdots)

            # organize Lagrange multipliers
            lambdaXs = sols[numRods+2 : 2*numRods+1]
            lambdaX.append(lambdaXs)
            lambdaYs = sols[2*numRods+1 :]
            lambdaY.append(lambdaYs)

            # calculate node positions
            thetas = q[i][2:]
            xNodes = [ q[i][0] - lt/2 * np.cos( thetas[0] ) ]
            yNodes = [ q[i][1] - lt/2 * np.sin( thetas[0] ) ]
            for i in range( len(q[i]) - 2 ) :
                xNodes.append( xNodes[-1] + lt * np.cos(thetas[i]) )
                yNodes.append( yNodes[-1] + lt * np.sin(thetas[i]) )
                if min(xNodes) < minX : minX = min(xNodes)
                if max(xNodes) > maxX : maxX = max(xNodes)
                if min(yNodes) < minY : minY = min(yNodes)
                if max(yNodes) > maxY : maxY = max(yNodes)
            nodeX.append(xNodes)
            nodeY.append(yNodes)

        lastAddTime.append(s.t[-1])

    d = {
        "t" : t,
        "psi" : [psi] * len(t),
        "a" : [a] * len(t),
        "q" : qs,
        "qdot" : qdot,
        "Fx" : lambdaX,
        "Fy" : lambdaY,
        "xNode" : nodeX,
        "yNode" : nodeY,
        "lt" : ltVec,
        "ldot" : lpVec
    }

    curvatures, deltaThetas, deltaThetaDots = [], [], []

    for i in range( len(d["t"]) ) :

        numRods = len(d["q"][i])-2
        numNodes = numRods - 1

        # compute curvature
        numMiddle = 7   # number of nodes in the "middle"; always use even numCells so this should be odd
        index = numMiddle + ( (numNodes - numMiddle) // 2 )
        x = np.array( d["xNode"][i][-index:index] )
        y = np.array( d["yNode"][i][-index:index] )
        curvature = comp_curv.fit(x, y)
        curvatures.append(curvature)

        # compute deltaTheta
        thetas = d["q"][i][2:]
        deltaTheta = thetas[numRods//2] - thetas[numRods//2-1]
        deltaThetas.append(deltaTheta * 180 / np.pi)

        # compute deltaThetaDot
        thetaDots = d["qdot"][i][2:]
        deltaThetaDot = thetaDots[numRods//2] - thetaDots[numRods//2-1]
        deltaThetaDots.append(deltaThetaDot * 180 / np.pi)

    # compute the difference of the curvature form the initial value at each times
    curvatureDiffs = list( 100 * (np.array(curvatures) - np.array( [curvatures[0]] * len(curvatures) ) ) / curvatures[0] )

    # compute the rate of change of the curvatures as a function of time
    curvatureRates = [None] + [(curvatures[i+1] - curvatures[i]) / (d["t"][i+1] - d["t"][i]) for i in range( len(d["t"])-1 )]

    d["curvature"] = curvatures
    d["curvatureDiff"] = curvatureDiffs
    d["curvatureRate"] = curvatureRates
    d["deltaTheta"] = deltaThetas
    d["deltaThetaDot"] = deltaThetaDots

    #configurationPlot(t, nodeX, nodeY, minX, maxX, minY, maxY, psi, a, b, numRods_init, angle)

    params = ( psi, a, b, l0, numRods_init, angle, seed_val )
    plotParams = ( minX, maxX, minY, maxY )
    simulationSets.append( (params, plotParams, pd.DataFrame(d)) )

# Organize the stress data -- compute mean stress, magnitudes, and parallel stresses
# Add these data to the existing data frames
simulationSets = stressOrganization(simulationSets)

#config_curvature_analysis(simulationSets)
#stressAnalysis(simulationSets)
expansionAnalysis(simulationSets)

#configurationPlotGrid(simulationSets)
#curvatureAnalysis(simulationSets)
#curvatureProfile(simulationSets)
#stressAnalysis(simulationSets)
#stress_df = breakingStressAnalysis(simulationSets)
#bending_dfs, breakAngles = breakingAnalysis(simulationSets)

#f = open("stressVsBending.pkl", 'wb')
#pickle.dump((stress_df, bending_dfs, breakAngles), f)
#f.close()

#stressVsBending(stress_df, bending_dfs, breakAngles)
