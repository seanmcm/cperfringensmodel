#!/usr/local/env python

import numpy as np
import scipy as sp
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy.stats import gamma
from random import randrange
import statistics as stat
import math
import time
import csv
import os
import pickle
import sys

import matplotlib
#matplotlib.use("Agg")      # COMMENT THIS FOR PLOTS FOR plt.show(), UNCOMMENT FOR ANIMATIONS
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'figure.autolayout': True})


for psiVal in range(1,2) :

    # import data
    chainData = pickle.load(open(f"plots.dir/chainData.dir/chainData_psi{psiVal}.pkl", "rb"))
    times = chainData[0][0]

    # organize chain length data
    chainLens = np.array([data[1] for data in chainData])
    chainLens = [ sum(chainLens[:,i], []) for i in range(len(times)) ]
    chainLenAvg = [np.mean(lens) for lens in chainLens]

    if psiVal == 1 :

        #plt.title(rf"Chain Length Distribution at $t = {round(times[i],2)}$ min")
        plt.xlabel(rf"Time (min)")
        plt.ylabel(rf"Average Chain Length ($\mu$m)")
        plt.plot(times, chainLenAvg, lw=3)
        #plt.show()
        plt.savefig(f"plots.dir/chainLenAvgVsTime.pdf")
        plt.clf()

        for i in range(0, len(chainLens), 100) :
            plt.title(rf"Chain Length Distribution at $t = {round(times[i],2)}$ min")
            plt.xlabel(rf"Chain Length ($\mu$m)")
            plt.hist(chainLens[i])
            #plt.show()
            plt.savefig(f"plots.dir/chainLenDistribution.dir/distribution_time{round(times[i],2)}.pdf")
            plt.clf()




    # Chain tip configuration plotting
    chainTips = [data[2] for data in chainData]
    if psiVal == 1 :

        print(f"Time = {times[250]}")
        print('\t', chainTips[-1][250])
        tips75 = chainTips[-1][250]

        x = tips75[0]
        y = [1,1]
        plt.plot(x,y,'-bo')

        print(f"Time = {times[345]}")
        print('\t', chainTips[-1][345])
        tips103 = chainTips[-1][345]

        x = tips103[0]
        y = [10,10]
        plt.plot(x,y,'-ro')

        x = tips103[1]
        y = [11,11]
        plt.plot(x,y,'-ro')

        print(f"Time = {times[450]}")
        print('\t', chainTips[-1][450])
        tips135 = chainTips[-1][450]

        x = tips135[0]
        y = [20,20]
        plt.plot(x,y,'-go')

        x = tips135[1]
        y = [21,21]
        plt.plot(x,y,'-go')

        x = tips135[2]
        y = [22,22]
        plt.plot(x,y,'-go')

        x = tips135[3]
        y = [23,23]
        plt.plot(x,y,'-go')

        #plt.show()
        plt.savefig(f"plots.dir/tipPositionExample.pdf")
