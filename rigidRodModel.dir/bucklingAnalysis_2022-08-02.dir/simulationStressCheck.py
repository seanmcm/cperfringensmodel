#import pdb
import numpy as np
import scipy as sp
from scipy import signal
from scipy import integrate
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy import optimize
from scipy.interpolate import interp1d
from scipy import interpolate
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
from FilamentV5 import *
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
#import matplotlib.animation as manimation
import sys
import pandas as pd
import seaborn as sns

plt.style.use('./mystyle.mplstyle')

# import the predicted critical kinking length
data = []
with open("data_test.csv") as f :
    csv_reader = csv.reader(f, delimiter=',')
    for row in csv_reader :
        row = [ float(x) for x in row ]     # convert the values form strings to floats
        row = np.array(row)                 # convert the list to a numpy array
        data.append( row )
data = np.array(data)

# upack the parameters
psi, a, l0, r, kb, mu, nu, epsilon, numRods0, angle, rel_tol, abs_tol, seed_val = tuple(data[0])
states = data[1:]

# determine the number of cells in the system based on the length of the state array
numRods = lambda state : int( (len(state) - 3) // 4 )

# sort the state array values
t = np.array([state[0] for state in states])
x0 = np.array([state[1] for state in states])
y0 = np.array([state[2] for state in states])
thetas = np.array([np.array(state[3:numRods(state)+3]) for state in states])
x0dot = np.array([np.array(state[numRods(state)+3]) for state in states])
y0dot = np.array([np.array(state[numRods(state)+4]) for state in states])
thetadots = np.array([np.array(state[numRods(state)+5:2*numRods(state)+5]) for state in states])
Fxs = np.array([np.array(state[2*numRods(state)+5:3*numRods(state)+4]) for state in states])
Fys = np.array([np.array(state[3*numRods(state)+4:]) for state in states])

# insert zeros into the force arrays
Fxs = np.array([np.insert(Fx, [0, len(Fx)], [0,0]) for Fx in Fxs])
Fys = np.array([np.insert(Fy, [0, len(Fy)], [0,0]) for Fy in Fys])

Fmags = [ np.sqrt(Fxs[i]**2 + Fys[i]**2) for i in range(len(Fxs)) ]

stress_straight = lambda t : numRods0**2 * np.exp( 2*r*t ) / 8
center_stress = np.array( [Fmag[len(Fmag)//2] for Fmag in Fmags] ) / (mu * l0**2 * r)

with open('output.dir/out.txt', 'w') as f:
    for i in range(len(t)) :
        print(f't = {t[i]} \t F = {center_stress[i]}', file=f)

plt.plot(t, center_stress)
plt.plot(t, stress_straight(t) )
plt.savefig("stress_check.dir/center_stress.pdf")
