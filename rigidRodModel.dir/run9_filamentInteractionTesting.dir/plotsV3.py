import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys
import csv
import pickle
import math

"""
NOTES:
- remember to only use on .pkl files in the data.dir directory
"""

def configurationPlot(filename) :

    # import simulation results
    rawData = pickle.load(open(filename, "rb"))
    #rawData = pickle.load(open( str(sys.argv[1]) , "rb"))

    data = []
    minX, maxX, minY, maxY = 10000000, 0, 10000000, 0
    print(len(rawData[0]))
    #for row in rows :
    for i in range( len(rawData[0]) ) :

        t = rawData[0][i]
        x1 = rawData[1][i]
        y1 = rawData[2][i]
        x2 = rawData[3][i]
        y2 = rawData[4][i]

        if min(x1 + x2) < minX : minX = min(x1 + x2)
        if max(x1 + x2) > maxX : maxX = max(x1 + x2)
        if min(y1 + y2) < minY : minY = min(y1 + y2)
        if max(y1 + y2) > maxY : maxY = max(y1 + y2)

        #print("sample", [t,x,y])
        data.append([t, x1, y1, x2, y2])

    #plt.figure(1)
    minAx = round( min( [minX, minY] ) - 5 )
    maxAx = round( max( [maxX, maxY] ) + 5 )
    #axesRange = [round(minAx-5), round(maxAx+5), round(minAx-5), round(maxAx+5)]

    fig = plt.figure(1)
    l1, = plt.plot([], [], '-bo')
    l2, = plt.plot([], [], '-ro')

    plt.xlim(minAx, maxAx)
    plt.ylim(minAx, maxAx)

    framesPerSec = 15
    #framesPerSec = round(len(data)/data[-1][0]*10)
    print("fps =", framesPerSec)

    # sets up video output stuff
    FFMpegWriter = manimation.writers['ffmpeg']
    metadata = dict(title='Movie Test', artist='Matplotlib',
                    comment='Movie support!')
    writer = FFMpegWriter(fps=framesPerSec, metadata=metadata)

    # remember to only use on .csv files in the data.dir directory
    # Splicing [8:-4] removes the "data.dir" at the front and ".csv" at the end
    #outfile = "plots.dir" + str(sys.argv[1])[8:-4] + ".mp4"
    outfile = "plots.dir" + filename[8:-4] + ".mp4"
    print(outfile)
    count = 0

    with writer.saving(fig, outfile, 100):
        for d in data :

            #if count % 20 == 0 :  # indent the 3 lines below and uncomment this line
            plt.title("Filament Configuration at t = " + str(round(d[0],2)) )
            l1.set_data(d[1], d[2])
            l2.set_data(d[3], d[4])
            writer.grab_frame()

            count+=1

    plt.gcf().clear()

####################################################################################################

def constraintForcePlot(filename) :

    # import simulation results
    rawData = pickle.load(open(filename, "rb"))
    #rawData = pickle.load(open( str(sys.argv[1]) , "rb"))

    data = []
    minX, maxX, minY, maxY = 10000000, 0, 10000000, 0
    print(len(rawData[0]))
    #for row in rows :
    for i in range( len(rawData[0]) ) :

        t = rawData[0][i]
        lambdaX = []
        lambdaY = []

        if i == 0 :
            lambdaX.append( [0]*len(rawData[3][1]) )
            lambdaY.append( [0]*len(rawData[3][1]) )

        else:

            for j in range( len(rawData[3][i]) ) :

                lambdaX.append( rawData[3][i][j][0] )
                lambdaY.append( rawData[3][i][j][1] )

                if rawData[3][i][j][0] < minX : minX = rawData[3][i][j][0]
                if rawData[3][i][j][0] > maxX : maxX = rawData[3][i][j][0]
                if rawData[3][i][j][1] < minY : minY = rawData[3][i][j][1]
                if rawData[3][i][j][1] > maxY : maxY = rawData[3][i][j][1]

        data.append([t,lambdaX,lambdaY])

    #plt.figure(1)
    minAx = round( min( [minX, minY] ) - 5 )
    maxAx = round( max( [maxX, maxY] ) + 5 )
    #axesRange = [round(minAx-5), round(maxAx+5), round(minAx-5), round(maxAx+5)]

    fig2 = plt.figure(2)
    l, = plt.plot([], [], 'o')

    maxNodes = max(list(map( lambda x : len(x), rawData[3][1:])))
    plt.xlim(-2, maxNodes + 2 )
    plt.ylim(round(minX-5), round(maxX+5))

    framesPerSec = 15
    #framesPerSec = round(len(data)/data[-1][0]*10)
    print("fps =", framesPerSec)

    # sets up video output stuff
    FFMpegWriter = manimation.writers['ffmpeg']
    metadata = dict(title='Movie Test', artist='Matplotlib',
                    comment='Movie support!')
    writer = FFMpegWriter(fps=framesPerSec, metadata=metadata)

    # remember to only use on .csv files in the data.dir directory
    # Splicing [8:-4] removes the "data.dir" at the front and ".csv" at the end
    #outfile = "plots.dir" + str(sys.argv[1])[8:-4] + ".mp4"
    xOutfile = "plots.dir" + filename[8:-4] + "_xForces.mp4"
    print(xOutfile)
    count = 0

    with writer.saving(fig2, xOutfile, 100):
        for d in data :

            #if count % 20 == 0 :  # indent the 3 lines below and uncomment this line
            plt.title("x constraint forces at t = " + str(round(d[0],2)) )

            l.set_data(range(len(d[1])), d[1])
            writer.grab_frame()

            count+=1


    fig3 = plt.figure(3)
    l, = plt.plot([], [], 'o')

    plt.xlim(-2, maxNodes + 2 )
    plt.ylim(minY, maxY)

    # sets up video output stuff
    FFMpegWriter = manimation.writers['ffmpeg']
    metadata = dict(title='Movie Test', artist='Matplotlib',
                    comment='Movie support!')
    writer = FFMpegWriter(fps=framesPerSec, metadata=metadata)

    # remember to only use on .csv files in the data.dir directory
    # Splicing [8:-4] removes the "data.dir" at the front and ".csv" at the end
    #outfile = "plots.dir" + str(sys.argv[1])[8:-4] + ".mp4"
    yOutfile = "plots.dir" + filename[8:-4] + "_yForces.mp4"
    print(yOutfile)
    count = 0

    with writer.saving(fig3, yOutfile, 100):
        for d in data :

            #if count % 20 == 0 :  # indent the 3 lines below and uncomment this line
            plt.title("y constraint forces at t = " + str(round(d[0],2)) )
            l.set_data(range(len(d[2])), d[2])
            writer.grab_frame()

            count+=1

    plt.gcf().clear()

####################################################################################################

try :
    filename = str(sys.argv[1])
    #configurationPlot(filename)
    constraintForcePlot(filename)
except IndexError :
    print("ERROR: no file given -> try again")
