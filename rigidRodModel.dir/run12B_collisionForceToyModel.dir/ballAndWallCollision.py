#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 05/09/2019

import pdb
import numpy as np
from scipy.integrate import ode
from scipy.integrate import odeint
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
import random
from random import randrange
import math
import time
import csv
import os
import pickle
#import plotsV4
#import plotsV4_withSites
from FilamentV9 import *
from matplotlib.animation import FuncAnimation


def odefunc(q) :

    x0 = q[0]
    y0 = q[1]
    theta = q[2]

    s0 = 1.0
    sc = 0.6
    ri = 0.99
    rd = 4.0
    rf = 400.0
    rt = 2 * np.atanh(ri) / (s0 - sc)
    rc = (s0 + sc) / 2

    xWall = 10

    l = 5

    xNodePos = [x0 - 0.5 * l * np.cos(theta), x0 + 0.5 * l * np.cos(theta) ]
    yNodePos = [y0 - 0.5 * l * np.sin(theta), y0 + 0.5 * l * np.sin(theta) ]


    dRdx0dot = 0.5 * rf * (0.5 - 0.5 * np.tanh(rd * (x0dot + thetadot * (-x0 + xpt)))) * (1 - np.tanh(rt * (-rc - xpt + xwall)))


    """# end points of the cell in self
    p1 = Point(xNodesPos[0], yNodesPos[0])
    p2 = Point(xNodesPos[0], yNodesPos[0])
    segSelf = Segment(p1, p2)

    p3 = Point(xWall, 0)
    p4 = Point(xWall, xWall)
    segOther = Line(p3, p4)      # line along the wall

    # NOTE:
    # x1, y1 corresponds to cell in self
    # x2, y2 corresponds to cell in other

    xc1, yc1, theta1, l1 = self.com[j], self.com[ j + len(self.com)//2 ], self.q[j+2], self.lt[j]
    xc2, yc2, theta2, l2 = xcom[i], ycom[i], other.q[i+2], other.lt[i]
    #print("com check1:", xc2, yc2)

    if j == 0 :
        # 1st cell, first point is end
        x1, y1 = p1.x, p1.y
        p = segOther.projection(p1)
    else :
        # Last cell, second point is end
        x1, y1 = p2.x, p2.y
        p = segOther.projection(p2)

    # check that p lies on the segment of the cell in other
    if len(segOther.intersection(p)) == 0 :
        # p not on segOther
        if p.distance(p3) < p.distance(p4) :
            x2, y2 = p3.x, p3.y     # p closer to p3
        else :
            x2, y2 = p4.x, p4.y     # p closer to p4
    else :
        # p on segOther
        x2, y2 = p.x, p.y

    #print("com check2:", xc2, yc2)

    x1, y1, x2, y2 = float(x1), float(y1), float(x2), float(y2)

    n1x = (x1 - xc1) / ( 0.5 * l1 * np.cos(theta1) )
    if np.isnan(n1x) : n1x = 0.0

    n1y = (y1 - yc1) / ( 0.5 * l1 * np.sin(theta1) )
    if np.isnan(n1y) : n1y = 0.0

    n2x = (x2 - xc2) / ( 0.5 * l2 * np.cos(theta2) )
    if np.isnan(n2x) : n2x = 0.0

    n2y = (y2 - yc2) / ( 0.5 * l2 * np.sin(theta2) )
    if np.isnan(n2y) : n2y = 0.0



    r12 = np.sqrt( (x2-x1)**2 + (y2-y1)**2 )"""


def f(q, t, m, l, rf, rd, rc, rt, xWall) :

    x0, y0, theta, v0x, v0y, omega = q
    print([ x0 - 0.5 * l * np.cos(theta), x0 + 0.5 * l * np.cos(theta) ])
    x = max( [ x0 - 0.5 * l * np.cos(theta), x0 + 0.5 * l * np.cos(theta) ] )

    print(t, "(x0, x):", x0, x)
    v0xdot = 0.25 * rf * (1 - np.tanh(rd * (v0x + omega * (-x0 + x)))) * (1 - np.tanh(rt * (-rc - x + xWall))) / m

    omegadot = 3 / (m * l) * rf * (x - x0) * (1 - np.tanh(rd * (v0x + omega * (-x0 + x)))) * (1 - np.tanh(rt * (-rc - x + xWall))) / m

    dqdt = [ v0x, v0y, omega, v0xdot, 0.0, omegadot ]

    return dqdt


def fallingBall(q, t, m, l, rf, rd, rc, rt, xWall, grav) :

    x0, v0x = q

    #v0xdot = m * grav - ( 0.25 * rf * (1 - np.tanh(rd * (-v0x)) ) * (1 - np.tanh(rt * (-rc - x0 + xWall))) / m )
    v0xdot = -grav + ( 0.25 * rf * (1 - np.tanh(rd * v0x) ) * (1 - np.tanh(rt * (-rc + x0 - xWall))) / m )

    dqdt = [ v0x, v0xdot ]

    return dqdt

def fallingBallElastic(q, t, m, l, rf, rd, rc, rt, yWall, alpha, grav) :

    y0, v0y = q

    #v0ydot = grav - ( ( 1 / m * ( alpha + rf * (1 - np.tanh(rt * (-rc - y0 + xWall))) / 4 ) ) * v0y )
    v0ydot = -grav - alpha/m * v0y + rf/m * (1 - np.tanh(rt * (y0 - yWall - rc))) / 4

    #dqdt = m * grav / ( alpha + rf * (1 - np.tanh(rt * (-rc - x0 + xWall))) / 4 )

    dqdt = [ v0y, v0ydot ]

    return dqdt


def fallingBallPlastic(q, t, m, l, rf, rd, rc, rt, yWall, alpha, grav) :

    print(q)
    y0, v0y = q

    """if v0y > 0 :
        print("on!")
        v0ydot = grav - ( ( 1 / m * ( alpha + rf * (1 - np.tanh(rt * (-rc - y0 + xWall))) / 2 ) ) * v0y )
    else :
        print("off!")
        v0ydot = grav - ((alpha / m ) * v0y)"""

    if v0y < 0 :
        v0ydot = -grav - (alpha * v0y / m) + ( rf/(2*m) * (1 - np.tanh(rt * (y0 - yWall - rc))) )
        #v0ydot = -grav - (alpha * v0y / m) + rf/m * ( 1 - np.tanh(rt * (y0 - xWall - rc)) / 2 )
    else :
        v0ydot = -grav - (alpha * v0y / m)

    dqdt = [ v0y, v0ydot ]

    return dqdt



def main() :

    s0 = 0.01 #1.0
    sc = 0.006 #0.6
    ri = 0.99
    rd = 4.0
    rf = 400.0
    rt = 2 * np.arctanh(ri) / (s0 - sc)
    rc = (s0 + sc) / 2

    xWall = 0.0

    l = 5
    m = 0.1
    grav = 9.8

    alpha = 0.1
    beta = 0.1
    gamma = 0.1

    t = np.linspace(0, 10, 101)

    # FALLING BALL TOY MODEL
    q0 = [10, -5]
    #q0 = [0, 5]
    #sol = odeint( fallingBall, q0, t, args=(m, l, rf, rd, rc, rt, xWall, grav) )
    #sol = odeint( fallingBallElastic, q0, t, args=(m, l, rf, rd, rc, rt, xWall, alpha, grav) )
    sol = odeint( fallingBallPlastic, q0, t, args=(m, l, rf, rd, rc, rt, xWall, alpha, grav) )

    import matplotlib.pyplot as plt
    plt.plot(t, sol[:, 0], 'b', label='$y(t)$')
    plt.plot(t, sol[:, 1], 'g', label='$v_y(t)$')
    plt.legend(loc='best')
    plt.xlabel('t')
    plt.grid()
    plt.show()


main()
