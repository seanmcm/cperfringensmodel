#!/usr/local/env python

import numpy as np
import scipy as sp
from scipy.integrate import ode
from scipy.integrate import solve_ivp
from scipy.optimize import fsolve
from scipy import stats
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
from random import randrange
import statistics as stat
import math
import time
import csv
import os
import pickle
import matplotlib
#matplotlib.use("Agg")      # COMMENT THIS FOR PLOTS FOR plt.show(), UNCOMMENT FOR ANIMATIONS
import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import sys

####################################################################################################

class filament(object) :

    #-----------------------------------------------------------------------------------------------

    def __init__(self, t, numRods, l0, com, growthRate, mu, kb, Fcrit, kbDist) :
        """ creates a filament object
                numRods : number of rods in the filament
                l0 : initial length of the cells
                com : center of mass of the filaments
                growthRate : initial growth rate of the
                Lb : length at which a filament breaks
                mu : drag per unit lenght parallel to the axis of the rod
                kb : angular spring constant
                Fcrit : breaking force as a function of psi"""

        self.numRods = numRods
        self.l0 = l0
        self.l0s = [l0] * numRods
        self.growthRate = growthRate
        self.T = l0/growthRate * np.log(2)  # cell cycle time
        l = l0 * np.exp( (growthRate/l0) * (t % self.T) )   # cell length
        self.L = numRods * l    # filament length
        self.com = com
        #self.Lb = Lb
        self.mu = mu
        self.kb = kb
        self.Fcrit = Fcrit

        self.kbDist = kbDist

        kbs =  [ self.kbDist(self.kb) for i in range(self.numRods-1) ]
        self.fc = [self.Fcrit(kbs[i]/(self.mu * self.l0**2 * self.growthRate)) * (self.mu * self.l0 * self.growthRate) for i in range(len(kbs)) ]
        self.broken = [False] * (self.numRods-1)

        self.lastDivTime = t
        self.lastDivTimes = [t] * numRods
        self.nextDivTimes = list(np.random.normal(t+self.T, 0.1*self.T, numRods))

        self.updateStress(t)
        self.updateTips()

    #-----------------------------------------------------------------------------------------------

    def updateLength(self, t) :
        """ updates the length of the filament
                t : the current time """
        #l = self.l0 * np.exp( (self.growthRate/self.l0) * (t % self.T) )

        r = self.growthRate / self.l0
        self.L = sum( [ self.l0s[i] * np.exp( r * (t-self.lastDivTimes[i]) ) for i in range(self.numRods)] )

        #l = self.l0 * np.exp( (self.growthRate/self.l0) * (t - self.lastDivTime) )

        #self.L = self.numRods * l

        #if self.L > self.Lb : self.broken = True

    #-----------------------------------------------------------------------------------------------

    def cellDivision(self, t, index = None) :
        """ handles cell division """

        if index == None :

            self.numRods = int(self.numRods * 2)
            self.broken = [False] * (self.numRods-1)

            oldFc = self.fc
            newFc = []
            for i in range( len(oldFc) ) :
                newFc.append( self.Fcrit( kbDist(self.kb) / (self.mu * self.l0**2 * self.growthRate) ) * (self.mu * self.l0 * self.growthRate) )
                newFc.append(oldFc[i])
            newFc.append( self.Fcrit( kbDist(self.kb) / (self.mu * self.l0**2 * self.growthRate) ) * (self.mu * self.l0 * self.growthRate) )

            self.fc = newFc

        else :

            self.numRods = int(self.numRods) + 1
            self.broken = [False] * (self.numRods-1)

            self.fc.insert( index, self.Fcrit( kbDist(self.kb) / (self.mu * self.l0**2 * self.growthRate) ) * (self.mu * self.l0 * self.growthRate) )

            r = self.growthRate/self.l0

            newLength = ( self.l0s[index] * np.exp( r * (t - self.lastDivTimes[index])) ) / 2
            self.l0s[index] = newLength
            self.l0s.insert( index, newLength )

            self.lastDivTimes[index] = t
            self.lastDivTimes.insert( index, t )

            self.nextDivTimes[index] = np.random.normal(t+self.T, 0.1*self.T)
            self.nextDivTimes.insert( index, np.random.normal(t+self.T, 0.1*self.T) )


    #-----------------------------------------------------------------------------------------------

    def updateTips(self) :
        """ calculate the location of the end tips of the filaments """
        self.tip_locations = [ self.com - (self.L / 2), self.com + (self.L / 2) ]

    #-----------------------------------------------------------------------------------------------

    def updateStress(self, t) :

        stress = []
        for i in range(1, self.numRods ) :
            #q = i / self.numRods
            q = sum(self.l0s[:i]) / sum(self.l0s)
            #s = 0.5 * self.mu * q * (1-q) * self.L * self.numRods * self.growthRate
            #s = 0.5 * self.mu * q * (1-q) * self.L**2 * self.growthRate / self.l0

            r = self.growthRate / self.l0
            L = sum([ self.l0s[j] * np.exp(r*(t-self.lastDivTimes[j])) for j in range(self.numRods) ])
            Lprime = sum([ self.l0s[j] * r * np.exp(r*(t-self.lastDivTimes[j])) for j in range(self.numRods) ])

            s = 0.5 * self.mu * q * (1-q) * L * Lprime

            #s = 0.5 * self.mu * q * (1-q) * ( self.numRods * self.l0 * np.exp( (self.growthRate/self.l0) * (t % self.T) ) )**2 * self.growthRate / self.l0

            #s = 0.5 * q * (1-q) * self.numRods**2
            stress.append(s)
        self.stress = stress

    #-----------------------------------------------------------------------------------------------

    def update(self, t, dt, eventBased) :
        """ updates the attributes of the filament """

        self.updateLength(t)
        self.updateTips()   # updateTips must be called AFTER updateLength
        self.updateStress(t)

        #a = self.F0 / np.sqrt(2)    # Maxwell distribution parameter in terms of the critical stress
        # check if a break as occured

        if not eventBased :

            if self.numRods > 1 :
                #print("Diffs Before:", np.array(self.stress) - np.array( self.fc ) )
                for i in range(self.numRods-1) :

                    q = i / self.numRods    # fractional location of the ith joint
                    s = self.stress[i]      # stress in the ith joint

                    #prob_break = 2/np.pi * s**2 * np.exp( -s**2 / (2 * a**2) ) / a**3   # Maxwell-Boltzmann Distribution
                    #prob_break = 1
                    #prob_break = (np.exp(s/self.F0) - 1) * dt
                    #if random.uniform(0,1) < prob_break : self.broken[i] = True

                    if self.stress[i] > self.fc[i]  :
                        #print(i, self.stress[i] - self.fc[i])
                        self.broken[i] = True

                    #if s > self.fc[i] : self.broken[i] = True

        #print("Flags:", self.broken)
        if len(self.broken) != self.numRods-1 : input("HEY LOOK HERE")


####################################################################################################

def main(eventBased, endTime, numRods, l0, growthRate, mu, kb, Fcrit, kbDist) :

    #growthRate = 0.1    # intitial  growth rate
    #numRods = 4         # number of initial cells
    #l0 = 5              # initial cell length

    #Lb = 2000           # filament breaking length
    #mu = 1e-9           # drag coefficient per unit length for parallel drag

    # critical stress at which breaking probability peaks
    #F0 = 0.5 * mu * 0.25 * 32**2 * l0 * growthRate
    #F0 = 0.75e-7
    # ~6e-6 for mu = 1e-7, growthRate = 0.1

    times = np.linspace(0, endTime, 1000)
    time_index = 0
    displacementVec = []
    chainLens = []
    chainTips = []

    timesDiv = []
    displacementVecDiv = []

    timesBreak = []
    displacementVecBreak = []

    t = 0
    T = l0/growthRate * np.log(2)       # cell cycle time
    dt = T / 5000     # timestep
    #endTime = 350
    nextDivTime = t + T
    nextEventTime = nextDivTime
    lastDivTime = 0.0

    numBreaks = 0
    breakLens = []


    filaments = [ filament(t, numRods, l0, 0.0, growthRate, mu, kb, Fcrit, kbDist) ]     # initiial filament
    totalLength = [[t, numRods * l0]]    # array of data points [t, Lt] where Lt is the length from the two farthest tips
    totalLengthRate = []
    while t < endTime :

        print(f'Begin Time = {t}')
        #print(f'Number of cells: {[f.numRods for f in filaments]}')
        #print(f'Filament Lengths: {[f.L for f in filaments]}')

        if eventBased :

            divEventTimes = []
            for f in filaments : divEventTimes += f.nextDivTimes
            print(f'Division Times: {divEventTimes}')
            nextDivTime = min(divEventTimes)

            index = divEventTimes.index(nextDivTime)
            for i in range( len(filaments) ) :
                if index >= filaments[i].numRods :
                    index -= filaments[i].numRods
                else:
                    f_div_index = i
                    div_index = index
                    break


            breakEventTimes = []
            for f in filaments :
                for i in range(len(f.stress)) :
                    #q = (i+1) / f.numRods
                    q = sum(f.l0s[:i+1]) / sum(f.l0s)

                    #print(f'break calc:, index: {i}, q: {q}, numRods: {f.numRods}, l0s: {len(f.l0s)}, fc: {len(f.fc)}')

                    if len(f.fc) != 0 :

                        r = f.growthRate / f.l0

                        breakTimeFunc = lambda t : f.fc[0] - 0.5 * f.mu * q * (1-q) * sum( [ f.l0s[i] * np.exp(r*(t-f.lastDivTimes[i])) for i in range(f.numRods) ] ) * sum( [ f.l0s[i] * r * np.exp(r*(t-f.lastDivTimes[i])) for i in range(f.numRods) ] )

                        nextBreakTime = fsolve(breakTimeFunc, t + (f.growthRate/f.l0))[0]

                        breakEventTimes.append(nextBreakTime)


                        #nextBreakTime = (f.l0s[i] / (2 * f.growthRate)) * np.log( 2 * f.fc[i] / ( f.growthRate * f.mu * q * (1-q) * f.numRods**2 * f.l0 ) )
                        #if nextBreakTime < 0 : nextBreakTime = t
                        #breakEventTimes.append(nextBreakTime + f.lastDivTimes[i])

            print(f'Break Times = {breakEventTimes}')
            if len(breakEventTimes) == 0 : nextBreakTime = 1e100
            else :
                nextBreakTime = min(breakEventTimes)
                index = breakEventTimes.index(nextBreakTime)
                for i in range(len(filaments)) :
                    if index >= len(filaments[i].stress) :
                        index -= len(filaments[i].stress)
                    else :
                        f_break_index = i
                        break_index = index
                        break

            #print(f'Next Break: Time: {nextBreakTime}, Filament: {f_break_index}, Node: {break_index}')
            #print(f'Next Div, Time: {nextDivTime}, Filament: {f_div_index}, Cell: {div_index}\n')

            while ( times[time_index] < min([nextBreakTime, nextDivTime]) ) :
                for f in filaments: f.update(times[time_index], dt, eventBased)
                tip_max = max( [ max(f.tip_locations) for f in filaments ] )
                tip_min = min( [ min(f.tip_locations) for f in filaments ] )
                displacementVec.append( tip_max - tip_min )
                chainLens.append([f.L for f in filaments])
                chainTips.append([f.tip_locations for f in filaments])
                print(f"\t\tIntermediate Time = {times[time_index]}, \tDisplacement = {tip_max - tip_min}")
                time_index += 1
                if time_index > len(times)-1 : break

            if nextBreakTime < nextDivTime :

                numBreaks += 1
                t = nextBreakTime

                print(f'BREAK, Time: {t}, Filament: {f_break_index}, Node: {break_index}')

                filaments[f_break_index].broken[break_index] = True

                newFilaments = []
                for f in filaments :

                    f.update(t, dt, eventBased)

                    # if the filament has reached the breaking length
                    if True in f.broken :
                        breakLens.append(f.L)

                        if sum(f.broken) > 1 :
                            #print("Fcrits", f.fc)
                            #print("stress", f.stress)
                            #print("F-Fc:", np.array(f.stress) - np.array(f.fc) )
                            #print("Flags:", f.broken)
                            input("UH OH -- More than one breaking event occured")

                        """print(f'\tCorrect Index? : {filaments.index(f) == f_break_index}')
                        print(f'\tBroken Links: {f.broken}')
                        print(f'\tForce Before: {f.stress}')
                        print(f'\tCrit Stress : {f.fc}')
                        print(f'\tLength Before : {f.L}')"""

                        #print(f'\tBefore Break:, Fc: {f.fc}, l0s: {f.l0s}, lastDivTimes: {f.lastDivTimes}, nextDivTimes: {f.nextDivTimes}' )

                        oldFc = f.fc
                        oldl0s = f.l0s
                        oldLastDivTimes = f.lastDivTimes
                        oldNextDivTimes = f.nextDivTimes

                        new_com_1 = f.com - f.L/2 * (1 - ( (break_index+1) / f.numRods ) )
                        f1 = filament(t, break_index+1, l0, new_com_1, growthRate, mu, kb, Fcrit, kbDist)
                        f1.fc = oldFc[:break_index]
                        f1.l0s = oldl0s[:break_index+1]
                        f1.lastDivTimes = oldLastDivTimes[:break_index+1]
                        f1.nextDivTimes = oldNextDivTimes[:break_index+1]
                        newFilaments.append(f1)

                        #print(f'\tAfter Break:, Fc: {f1.fc}, l0s: {f1.l0s}, lastDivTimes: {f1.lastDivTimes}, nextDivTimes: {f1.nextDivTimes}' )

                        new_com_2 = f.com - f.L/2 * (1 - ( (f.numRods + break_index +1 ) / f.numRods) )
                        f2 = filament(t, f.numRods-break_index-1, l0, new_com_2, growthRate, mu, kb, Fcrit, kbDist)
                        f2.fc = oldFc[break_index+1:]
                        f2.l0s = oldl0s[break_index+1:]
                        f2.lastDivTimes = oldLastDivTimes[break_index+1:]
                        f2.nextDivTimes = oldNextDivTimes[break_index+1:]
                        newFilaments.append(f2)


                        #print(f'\tAfter Break:, Fc: {f2.fc}, l0s: {f2.l0s}, lastDivTimes: {f2.lastDivTimes}, nextDivTimes: {f2.nextDivTimes}' )
                        print(f'\tLengths After: {f1.L}, {f2.L}')


                    else : newFilaments.append(f)

                filaments = newFilaments

                timesBreak.append(t)
                tip_max = max( [ max(f.tip_locations) for f in filaments ] )
                tip_min = min( [ min(f.tip_locations) for f in filaments ] )
                displacementVecBreak.append( tip_max - tip_min )

            else :
                print(f'DIV, num_filaments = {len(filaments)}, f_div_index = {f_div_index}, num_cells = {filaments[f_div_index].numRods}, div_index = {div_index}')

                t = nextDivTime
                for f in filaments :
                    f.update(t, dt, eventBased)
                    #f.cellDivision()   # check if cells divide
                    #f.lastDivTime = t
                    #f.update(t, dt, eventBased)

                print(f'\n\tCells before: {filaments[f_div_index].numRods}, Length Before: {filaments[f_div_index].L}')
                print(f'\tLastDivTimes before: {filaments[f_div_index].lastDivTimes}')
                print(f'\tNextDivTimes before: {filaments[f_div_index].nextDivTimes}')

                filaments[f_div_index].cellDivision(t, div_index)
                filaments[f_div_index].lastDivTime = t

                print(f'\n\tCells after: {filaments[f_div_index].numRods}, Length after: {filaments[f_div_index].L}')
                print(f'\tLastDivTimes after: {filaments[f_div_index].lastDivTimes}')
                print(f'\tNextDivTimes after: {filaments[f_div_index].nextDivTimes}')

                for f in filaments : f.update(t, dt, eventBased)

                print(f'\n\tCells after: {filaments[f_div_index].numRods}, Length after: {filaments[f_div_index].L}')
                print(f'\tLastDivTimes after: {filaments[f_div_index].lastDivTimes}')
                print(f'\tNextDivTimes after: {filaments[f_div_index].nextDivTimes}')

                lastDivTime = t
                nextDivTime = t + T

                timesDiv.append(t)
                tip_max = max( [ max(f.tip_locations) for f in filaments ] )
                tip_min = min( [ min(f.tip_locations) for f in filaments ] )
                displacementVecDiv.append( tip_max - tip_min )

            tip_max = max( [ max(f.tip_locations) for f in filaments ] )
            tip_min = min( [ min(f.tip_locations) for f in filaments ] )
            totalLength.append( [t, tip_max - tip_min] )
            Lt_rate =  (totalLength[-1][1] - totalLength[-2][1]) / (totalLength[-1][0] - totalLength[-2][0])
            totalLengthRate.append( [t, Lt_rate] )

            print(f'\nEnd Time = {t}')
            print(f'\nFilament Properties: \n')
            for f in filaments:
                print(f'\tCells  : {f.numRods}')
                print(f'\tLength : {f.L}')
                print(f'\tStress : {f.stress}')
                print(f'\tFcrits : {f.fc}\n')

            #input("step complete------------------------------------\n")

        else:

            newFilaments =[]
            for f in filaments :

                f.update(t, dt, eventBased)
                # if the filament has reached the breaking length
                if True in f.broken :

                    #input("BREAK!")

                    if sum(f.broken) > 1 :
                        print("Fcrits", f.fc)
                        print("stress", f.stress)
                        #input("UH OH -- More than one breaking event occured")
                    ic = (f.numRods-2)/2
                    indices = [i for i, x in enumerate(f.broken) if x == True]
                    i_break = indices[[np.abs(i - ic) for i in indices].index( min([np.abs(i - ic) for i in indices]) )]

                    oldFc = f.fc

                    new_com_1 = f.com - f.L/2 * (1 - ( (i_break+1) / f.numRods ) )
                    f1 = filament(t, i_break+1, l0, new_com_1, growthRate, mu, kb, Fcrit, kbDist)
                    f1.fc = oldFc[:i_break]
                    newFilaments.append(f1)

                    new_com_2 = f.com - f.L/2 * (1 - ( (f.numRods + i_break +1 ) / f.numRods) )
                    f2 = filament(t, f.numRods-i_break-1, l0, new_com_2, growthRate, mu, kb, Fcrit, kbDist)
                    f2.fc = oldFc[i_break+1:]
                    newFilaments.append(f2)

                    """
                    for i in range(f.numRods-1) :
                        lastBreakIndex = 0
                        if f.broken[i] :        # check if the ith joint is broken
                            newfilaments.append( filament(t, lastBreakIndex+i+1), f.com )
                            lastBreakIndex = i

                    newFilaments.append( filament(t, f.numRods/2, l0, f.com - (f.L/4), growthRate, Lb) )
                    newFilaments.append( filament(t, f.numRods/2, l0, f.com + (f.L/4), growthRate, Lb) )"""

                else : newFilaments.append(f)

            filaments = newFilaments

            if (t+dt)%T < t%T  :
                for f in filaments :
                    f.cellDivision()   # check if cells divide

            t += dt
            print("Time =", t)

            tip_max = max( [ max(f.tip_locations) for f in filaments ] )
            tip_min = min( [ min(f.tip_locations) for f in filaments ] )
            totalLength.append( [t, tip_max - tip_min] )
            Lt_rate =  (totalLength[-1][1] - totalLength[-2][1]) / (totalLength[-1][0] - totalLength[-2][0])
            totalLengthRate.append( [t, Lt_rate] )

    print("Average Rate =", totalLength[-1][1]/endTime, "um/min")
    print("Number of breaks:", numBreaks)
    return totalLength[-1][1]/endTime, numBreaks, breakLens, times, displacementVec, timesDiv, displacementVecDiv, timesBreak, displacementVecBreak, chainLens, chainTips

    """totalLength = np.array(totalLength)
    plt.figure(1)
    plt.title(r"Tip-to-Tip Expansion Displacement" )
    plt.xlabel(r"Time(min)")
    plt.ylabel(r"Displacement ($\mu$m)")
    plt.plot(totalLength[:,0], totalLength[:,1])

    totalLengthRate = np.array(totalLengthRate)
    plt.figure(2)
    plt.title(r"Tip-to-Tip Expansion Rate" )
    plt.xlabel(r"Time(min)")
    plt.ylabel(r"Rate of Expansion ($\mu$m/min)")
    plt.plot(totalLengthRate[:,0], totalLengthRate[:,1])

    plt.show() """

plt.rcParams.update({'font.size': 16})
plt.rcParams.update({'figure.autolayout': True})

# SIMULATION PARAMETER
np.random.seed(32)
eventBased = True
mc_steps = 10
endTime = 300

# PHYSICAL PARAMETERS
numRods = 1         # number of initial cells
l0 = 5              # initial cell length
growthRate = 0.1    # intitial  growth rate
mu = 1e-5           # drag coefficient per unit length for parallel drag

Fcrit = lambda psi : 3.466036922444842 * psi + 0.43012681408031994
#kbDist = lambda kb : abs(np.random.normal(kb, 0.1 * kb))
kbDist = lambda kb : sp.stats.gamma.rvs(100, scale=kb/100)

"""#kbVals = [1e-8, 2e-8, 3e-8, 4e-8, 5e-8, 6e-8, 7e-8, 8e-8, 9e-8, 1e-7]
#psiVals = [kbVal / (mu * l0**2 * growthRate) for kbVal in kbVals]

psiVals = [4, 8, 12, 16, 20, 24, 28, 32, 36, 40]
kbVals = [psiVal * (mu * l0**2 * growthRate) for psiVal in psiVals]

# SIMULATION CALLS
expRateVals = []
for kbVal in kbVals :
    expRate = main(eventBased, endTime, numRods, l0, growthRate, mu, kbVal, Fcrit, kbDist)
    expRateVals.append( expRate )

plt.title(r"Expansion Rate vs. $\Psi$" )
plt.xlabel(r"$\Psi (\frac{k_b}{\mu l_0^3 r})$")
plt.ylabel(r"Average Expansion Rate ($\mu$m/min)")
plt.plot(psiVals, expRateVals)
plt.savefig("plots.dir/averageExpansionRate.pdf")


#kbVals = [1e-7, 2e-7, 3e-7, 4e-7, 5e-7, 6e-7, 7e-7, 8e-7, 9e-7, 1e-6]
#psiVals = [kbVal / (mu * l0**2 * growthRate) for kbVal in kbVals]

psiVals = [40, 80, 120, 160, 200, 240, 280, 320, 360, 400]
kbVals = [psiVal * (mu * l0**2 * growthRate) for psiVal in psiVals]

# SIMULATION CALLS
expRateVals = []
for kbVal in kbVals :
    expRate = main(eventBased, endTime, numRods, l0, growthRate, mu, kbVal, Fcrit, kbDist)
    expRateVals.append( expRate )

plt.title(r"Expansion Rate vs. $\Psi$" )
plt.xlabel(r"$\Psi (\frac{k_b}{\mu l_0^3 r})$")
plt.ylabel(r"Average Expansion Rate ($\mu$m/min)")
plt.plot(psiVals, expRateVals)
plt.savefig("plots.dir/averageExpansionRate2.pdf")"""



psiVals = [1,2,3,4,5,6,7,8,9,10]
#psiVals = [1e-1, 1, 1e1, 1e2, 1e3, 1e4, 1e5]
kbVals = [psiVal * (mu * l0**2 * growthRate) for psiVal in psiVals]
muVals = [1e5 / (psiVal * l0**2 * growthRate) for psiVal in psiVals]

# SIMULATION CALLS
expRateMeans, expRateStDevs = [], []
avgNumBreaks, stDevNumBreaks = [], []
meanBreakLens = []
#for kbVal in kbVals :
for psiVal in psiVals :

    print(f"Running Psi = {psiVal} ", end="", flush=True)

    kbVal = psiVal * (mu * l0**2 * growthRate)

    chainData = []

    vals = []
    breakCount = []
    breakLenVals = []
    for i in range(mc_steps) :
        print(".", end="", flush=True)
        sys.stdout = open(os.devnull, "w")
        sys.stderr = open(os.devnull, "w")
        expRate, numBreaks, breakLens, times, displacementVec, timesDiv, displacementVecDiv, timesBreak, displacementVecBreak, chainLens, chainTips = main(eventBased, endTime, numRods, l0, growthRate, mu, kbVal, Fcrit, kbDist)
        chainData.append([times, chainLens, chainTips])
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__
        vals.append( expRate )
        breakCount.append(numBreaks)
        breakLenVals.append(stat.mean(breakLens))

        plt.clf()
        plt.title(rf"Expansion Displacement ($\Psi$ = {psiVal})" )
        plt.xlabel(r"Time (min)")
        plt.ylabel(r"Total Expansion Distance ($\mu$m)")
        plt.plot(times, displacementVec, label="Displacement", lw=3)
        timeRange = int(500 + 25*(psiVal-1))
        plt.plot(np.array(times[:timeRange]), numRods * l0 * np.exp(growthRate * np.array(times[:timeRange]) / l0), label="Exp Growth", ls=':', lw=3)
        plt.plot(timesBreak[:-1], displacementVecBreak[:-1], 'o', label="Breaks", alpha=0.75, marker='|', ms=20)
        plt.plot(timesDiv, displacementVecDiv, 'o', label="Divisions", ms=5)
        plt.legend(loc='upper left')
        plt.savefig(f"plots.dir/trajectories.dir/displacementVsTime_psi{psiVal}.pdf")

        expRateVec = [ (displacementVec[i+1]-displacementVec[i])/(times[i+1]-times[i]) for i in range(len(displacementVec)-1)]

        plt.clf()
        plt.title(rf"Expansion Rate ($\Psi$ = {psiVal})" )
        plt.xlabel(r"Time (min)")
        plt.ylabel(r"Total Expansion Rate ($\mu$m/min)")
        plt.plot(times[1:], expRateVec, lw=3)
        plt.savefig(f"plots.dir/trajectories.dir/expansionRateVsTime_psi{psiVal}.pdf")

    pickle.dump(chainData, open(f"plots.dir/chainData.dir/chainData_psi{psiVal}.pkl", "wb"))

    print(f" Done")
    expRateMeans.append(stat.mean(vals))
    expRateStDevs.append(stat.stdev(vals))
    avgNumBreaks.append(stat.mean(breakCount))
    stDevNumBreaks.append(stat.stdev(breakCount))
    meanBreakLens.append(stat.mean(breakLenVals))

plt.clf()
plt.title(r"Expansion Rate Varies with $\Psi$" )
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Average Expansion Rate ($\mu$m/min)")
#plt.plot(psiVals, expRateMeans)
plt.errorbar(psiVals, expRateMeans, yerr=None, fmt='-o', ecolor='r')
#plt.xscale('log')
#plt.yscale('log')
#plt.axvline(x=4e5, color='r')
#plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
plt.savefig("plots.dir/averageExpansionRate.pdf")

plt.clf()
plt.title(r"Number of Breaks vs. $\Psi$" )
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Number of Breaks")
#plt.plot(psiVals, expRateMeans)
plt.errorbar(psiVals, avgNumBreaks, yerr=None, fmt='-o', ecolor='r')
#plt.xscale('log')
#plt.yscale('log')
#plt.axvline(x=4e5, color='r')
#plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
plt.savefig("plots.dir/numBreaksVsPsi.pdf")

plt.clf()
plt.title(r"Average Break Length Varies with $\Psi$" )
#plt.xlabel(r"Parallel Drag Coefficient $\mu$ (kg / $\mu$m min)")
plt.xlabel(r"$\Psi \left(\frac{k_b}{\mu l_0^3 r}\right)$")
plt.ylabel(r"Breaking Length ($\mu$m)")
plt.plot(psiVals, meanBreakLens, '-o')
#plt.errorbar(psiVals, meanBreakLens, yerr=[], fmt='-o', ecolor='r')
#plt.axvline(x=4e5, color='r')
#plt.ticklabel_format(axis='x',style='sci', scilimits=(5,5))
plt.savefig("plots.dir/breakLenVsPsi.pdf")
