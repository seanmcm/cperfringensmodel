#!/usr/local/env python

# S.G McMahon
# numpy random tests
# 08/28/2018

import numpy.random as rand

def f1() :
    print("f1", rand.normal(100, 10))
    return

def f2() :
    print("f2", rand.normal(100, 10))
    return

def f3() :
    print("f3", rand.normal(100, 10))
    return


rand.seed(7)
print("main", rand.normal(100, 10))
f1()
f2()
print("main", rand.normal(100, 10))
