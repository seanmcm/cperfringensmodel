#!/usr/local/env python

# S.G McMahon
# C. Perfringens rigid rod model simulation
# 05/06/2019

import numpy as np
from scipy.integrate import ode
from sympy.physics.vector import *
from mpmath import *
import sympy as sym
from sympy.matrices import Matrix
from sympy.interactive.printing import init_printing
init_printing(use_unicode=False, wrap_line=False)
import random
from random import randrange
import math
import time
import csv
import os
import pickle

def growthDynamics(numRods) :
    """ updates the qdots for the filament based only on growth dynamics
            l (array) : lengths of the cells in the Filaments
            lp (array) : growth rates for each cell
            kb (float) : angular spring constant
            mu (float) : parallel drag coefficient
            nu (float) : perpendiular drag coefficient
            epsilion (float) : rotational drag coefficient """

    kb = sym.symbols('kb')
    mu = sym.symbols('mu')
    nu = sym.symbols('nu')
    epsilon = sym.symbols('epsilon')

    #print("Filament=", self, l[0])
    #l = self.lt
    #lp = self.lp

    l = [ sym.symbols('l' + str(i)) for i in range(numRods) ]
    lp = [ sym.symbols('lp' + str(i)) for i in range(numRods) ]

    #x0 = self.q[0]
    #y0 = self.q[1]
    #thetas = self.q[2:]
    x0 = sym.symbols('x0')
    y0 = sym.symbols('y0')
    thetas = [ sym.symbols('theta' + str(i)) for i in range(numRods) ]

    x0dot = sym.symbols('x0dot')
    y0dot = sym.symbols('y0dot')
    thetadots = [ sym.symbols('thetadot' + str(i)) for i in range(numRods) ]
    lambdaX = [sym.symbols('lambdaX' + str(i)) for i in range(numRods-1)]
    lambdaY = [sym.symbols('lambdaY' + str(i)) for i in range(numRods-1)]
    qdots = [x0dot, y0dot] + thetadots + lambdaX + lambdaY

    #numRods = len(thetas)
    numNodes = numRods + 1

    totalLength = sum(l)

    #--------------------------------------------------------

    xEqns, yEqns, thetaEqns = [], [], []

    # x_i Euler-Lagrange equations --------------------------------------------------------

    # initialize x_0 E-L equation and x E_L equation array
    #xEqn0 = np.zeros(3*numRods)
    xEqn0 = [0] * (3*numRods)
    xEqn0[0] = ( mu * sym.cos(thetas[0])**2 + nu * sym.sin(thetas[0])**2 ) * l[0]
    xEqn0[1] = ( mu - nu ) * l[0] * sym.cos(thetas[0]) * sym.sin(thetas[0])
    xEqn0[numRods + 2] = -1
    xEqns.append(list(xEqn0))

    # initialize y_0 E-L equation and y E_L equation array
    #yEqn0 = np.zeros(3*numRods)
    yEqn0 = [0] * (3*numRods)
    yEqn0[0] = ( mu - nu ) * l[0] * sym.cos(thetas[0]) * sym.sin(thetas[0])
    yEqn0[1] = ( mu * sym.sin(thetas[0])**2 + nu * sym.cos(thetas[0])**2 ) * l[0]
    yEqn0[2*numRods + 1] = -1
    yEqns.append(list(yEqn0))

    # initialize theta_0 E-L equation and theta E_L equation array
    thetaEqn0 = [0] * (3*numRods)
    #thetaEqn0 = np.zeros(3*numRods)
    thetaEqn0[2] = epsilon * l[0]**3
    thetaEqn0[numRods + 2] = 0.5 * l[0] * sym.sin(thetas[0])
    thetaEqn0[2*numRods + 1] = -0.5 * l[0] * sym.cos(thetas[0])
    thetaEqns.append(list(thetaEqn0))

    #generate the 2 through N-1 x, y, and theta E-L equations
    for i in range(1, numRods) :

        # x_i E_L equations -----------------------------------------------------------

        #xEqni = np.zeros(3*numRods)
        xEqni = [0] * (3*numRods)

        xEqni[0] = ( mu * sym.cos(thetas[i])**2 + nu * sym.sin(thetas[i])**2 ) * l[i]

        xEqni[1] = ( mu - nu ) * l[i] * sym.cos(thetas[i]) * sym.sin(thetas[i])

        xEqni[2] = 0.5 * ( (mu - nu) * l[0] * l[i] * sym.cos(thetas[0]) * sym.sin(thetas[i]) * sym.cos(thetas[i]) - (mu * sym.cos(thetas[i])**2 + nu * sym.sin(thetas[i])**2 ) * l[0] * l[i] * sym.sin(thetas[0]) )

        for j in range(1, i) :
            xEqni[j+2] = (mu - nu) * l[j] * l[i] * sym.cos(thetas[j]) * sym.sin(thetas[i]) * sym.cos(thetas[i]) - (mu * sym.cos(thetas[i])**2 + nu * sym.sin(thetas[i])**2) * l[j] * l[i] * sym.sin(thetas[j])

        xEqni[i+2] = -0.5 * nu * l[i]**2 * ( sym.sin(thetas[i]) * sym.cos(thetas[i])**2 + sym.sin(thetas[i])**3 )

        xEqni[i + numRods + 1] = 1

        if i != (numRods-1) :
            xEqni[i + numRods + 2] = -1

        xEqns.append(list(xEqni))


        # y_i E_L equations -----------------------------------------------------------

        #yEqni = np.zeros(3*numRods)
        yEqni = [0] * (3*numRods)

        yEqni[0] = ( mu - nu ) * l[i] * sym.cos(thetas[i]) * sym.sin(thetas[i])

        yEqni[1] = ( mu * sym.sin(thetas[i])**2 + nu * sym.cos(thetas[i])**2 ) * l[i]

        yEqni[2] = 0.5 * ( (mu * sym.sin(thetas[i])**2 + nu * sym.cos(thetas[i])**2) * l[0] * l[i] * sym.cos(thetas[0]) - (mu - nu) * l[0] * l[i] * sym.sin(thetas[0]) * sym.sin(thetas[i]) * sym.cos(thetas[i]) )

        for j in range(1, i) :
            yEqni[j+2] = ( mu * sym.sin(thetas[i])**2 + nu * sym.cos(thetas[i])**2 ) * l[j] * l[i] * sym.cos(thetas[j]) - (mu - nu) * l[j] * l[i] * sym.sin(thetas[j]) * sym.sin(thetas[i]) * sym.cos(thetas[i])

        yEqni[i+2] = 0.5 * nu * l[i]**2 * (sym.cos(thetas[i]) * sym.sin(thetas[i])**2 + sym.cos(thetas[i])**3)

        yEqni[i + (2*numRods)] = 1

        if i != (numRods-1) :
            yEqni[i + (2*numRods) + 1] = -1

        yEqns.append(list(yEqni))

        # theta_i E_L equations -----------------------------------------------------------

        #thetaEqni = np.zeros(3*numRods)
        thetaEqni = [0] * (3*numRods)

        thetaEqni[i+2] = epsilon * l[i]**3

        thetaEqni[i + numRods + 1] = 0.5 * l[i] * sym.sin(thetas[i])

        thetaEqni[i + (2*numRods)] = -0.5 * l[i] * sym.cos(thetas[i])

        if i != (numRods-1) :
            thetaEqni[i + numRods + 2] = 0.5 * l[i] * sym.sin(thetas[i])
            thetaEqni[i + (2*numRods) +1] = -0.5 * l[i] * sym.cos(thetas[i])

        thetaEqns.append(list(thetaEqni))

    # combine all E-L equation coeffcients into a single array
    eqns = xEqns + yEqns + thetaEqns
    #self.eqns = np.array(eqns)
    # Note: np.linalg.solve takes an np.array but xEqns, yEqns, and thetaEqns
    #   are standard arrays
    #print(eqns)
    #input("stopza")

    xVals, yVals, thetaVals = [0], [0], [-kb * (thetas[0]-thetas[1])]

    for i in range(1, numRods) :

        xVal = -(mu * sym.cos(thetas[i])**2 + nu * sym.sin(thetas[i])**2) * l[i] *(0.5 * lp[0] * sym.cos(thetas[0]) + 0.5 * lp[i] * sym.cos(thetas[i])) - (mu - nu) * l[i] * sym.sin(thetas[i]) * sym.cos(thetas[i]) * (0.5 * lp[0] * sym.sin(thetas[0]) + 0.5 * lp[i] * sym.sin(thetas[i]))
        for j in range(1,i) :
            xVal -= (mu * sym.cos(thetas[i])**2 + nu * sym.sin(thetas[i])**2) * l[i] * lp[j] * sym.cos(thetas[j]) + (mu - nu) * l[i] * lp[j] * sym.sin(thetas[j]) * sym.sin(thetas[i]) * sym.cos(thetas[i])
        xVals.append(xVal)

        yVal = -(mu * sym.sin(thetas[i])**2 + nu * sym.cos(thetas[i])**2) * l[i] *(0.5 * lp[0] * sym.sin(thetas[0]) + 0.5 * lp[i] * sym.sin(thetas[i])) - (mu - nu) * l[i] * sym.sin(thetas[i]) * sym.cos(thetas[i]) * (0.5 * lp[0] * sym.cos(thetas[0]) + 0.5 * lp[i] * sym.cos(thetas[i]))
        for j in range(1,i) :
            yVal -= (mu * sym.sin(thetas[i])**2 + nu * sym.cos(thetas[i])**2) * l[i] * lp[j] * sym.sin(thetas[j]) + (mu - nu) * l[i] * lp[j] * sym.cos(thetas[j]) * sym.sin(thetas[i]) * sym.cos(thetas[i])
        yVals.append(yVal)

        if i == (numRods-1) :
            thetaVal = -kb * (thetas[numRods-1]-thetas[numRods-2])
        else :
            thetaVal = -kb * (2*thetas[i] - thetas[i+1] - thetas[i-1])
        thetaVals.append(thetaVal)

    vals = xVals + yVals + thetaVals

    for i in range(len(eqns)) :
        eqns[i].append(vals[i])


    # system of ode's organized in augmented matrix form: A*x = b
    # A = Matrix(eqns), b = vals, x = q

    sol = sym.linsolve( Matrix(eqns), qdots )
    print(sol)




#---------------------------------------------------------------------------


def lateralDynamics( self, other, width, kon, koff0, ks, Lmax, Lc, Fc, dt ) :
    """ Out dated version see below for new version of this function which
        uses Lagrangian mechanics instead

        simulates the lateral interaction dynamics on the cell
            other (Filament) : the adjacent filament
            lt (array) : array of cell lengths
            width (float) : width of a single cell
            kon (float) : rate at which bonds form
            koff0 (float) : characteristic rate at which bonds break
            ks (float) : spring constant for force between bonded site
            Lmax (float) : maximum distance at which a bond can form
            Lc (float) : cutoff distance at which a bond immediately breaks
            Fc (float) : characterstic force between bonded site
            dt (float) : time step -- current time minus the previous time """

    #print("check num bonds", len(self.bonds))

    # iterate over all bonds and break old ones if necessary
    if ( len(self.bonds) != 0 ) and ( len(other.bonds) != 0 ) :
        for i in range( len(self.bonds)-1, -1, -1 ) :
            for j in range( len(other.bonds)-1, -1, -1 ) :
                if self.bonds[i] == other.bonds[j] :

                    self.bonds[i].calculateForce(ks, width)
                    koff = koff0 * sym.exp( self.bonds[i].force / Fc )

                    # check sites are within cutoff distance
                    Lc_val = np.sqrt( (self.bonds[i].site1.bx - self.bonds[i].site2.bx)**2 + (self.bonds[i].site1.by - self.bonds[i].site2.by)**2 ) > Lc
                    # determine if a break occurs
                    koffCheck = random.random()
                    koff_val = koffCheck <= koff * dt

                    #print("ratecheck", koffCheck, sym.exp( self.bonds[i].force / Fc ) )
                    if Lc_val or koff_val:
                        # remove bond reference from the two sites
                        print("DESTROYING BOND!")
                        self.bonds[i].site1.bond = None
                        self.bonds[i].site2.bond = None
                        # remove the bond objects from the list of bonds
                        del self.bonds[i]
                        del other.bonds[j]
                        break

    # iterate over all binding sites and form new bonds if necessary
    for i in range( len(self.bindingSites) ) :
        for j in range( len(self.bindingSites[i]) ) :

            for m in range( len(other.bindingSites) ) :
                for n in range( len(other.bindingSites[m]) ) :
                    # check if sites are within maximum distance at which a bond can form
                    if self.bindingSites[i][j].calculateDistance( other.bindingSites[m][n] ) <= Lmax :
                        if random.random() <= kon * dt :
                            newBond = Bond( self.bindingSites[i][j], other.bindingSites[m][n] )
                            newBond.calculateForce(ks, width)
                            self.bonds.append( newBond )
                            other.bonds.append( newBond )
                            #print("creating new bond")


    # update qdots based on torques
    for i in range( len(self.bonds) ) :
        for j in range( len(other.bonds) ) :
            if self.bonds[i] == self.bonds[j] :


                # update each Euler-Lagrange equation based on the lateral dynamics
                # note: these terms depend only on the generalized coordinates and not
                # their derivatives so this only changes the "value" side of the equation

                if self.bonds[i].site1.filament == self :
                    siteSelf = self.bonds[i].site1
                    siteOther = self.bonds[i].site2
                else :
                    siteSelf = self.bonds[i].site2
                    siteOther = self.bonds[i].site1
                xDiff = siteOther.bx - siteSelf.bx
                yDiff = siteOther.by - siteSelf.by
                sqrtDist = np.sqrt( xDiff**2 + yDiff**2 )


                # update the "values" on the self filament

                index = siteSelf.cellIndex
                numRods = len(self.q)-2
                xcom = self.com[:numRods]
                ycom = self.com[numRods:]

                # x-coodinate adjustment
                self.vals[index] += ( -ks * xDiff * ( sqrtDist - width ) ) / sqrtDist

                # y-coodinate adjustment
                self.vals[numRods+index] += ( -ks * yDiff * ( sqrtDist - width ) ) / sqrtDist

                # theta-coodinate adjustment
                numer = ks * (sqrtDist - width) * ( yDiff * xcom[index] + siteOther.bx * (siteSelf.by - xcom[index]) + siteSelf.bx * ( ycom[index] - siteOther.by ) )
                #print("numerator is zero", numer == 0)
                self.vals[2*numRods+index] += numer / sqrtDist


                # update the "values" on the other filament

                index = siteOther.cellIndex
                numRods = len(other.q)-2
                xcom = other.com[:numRods]
                ycom = other.com[numRods:]

                # x-coodinate adjustment
                other.vals[index] += ( ks * xDiff * ( sqrtDist - width ) ) / sqrtDist

                # y-coodinate adjustment
                other.vals[numRods+index] += ( ks * yDiff * ( sqrtDist - width ) ) / sqrtDist

                # theta-coodinate adjustment
                numer = -ks * (sqrtDist - width) * ( yDiff * xcom[index] + siteOther.bx * (siteSelf.by - xcom[index]) + siteSelf.bx * ( ycom[index] - siteOther.by ) )
                #numer = -numer
                #numer = ks * (sqrtDist - width) * ( -yDiff * xcom[index] + siteSelf.bx * (siteOther.by - xcom[index]) + siteOther.bx * ( ycom[index] - siteSelf.by ) )
                other.vals[2*numRods+index] += numer / sqrtDist


#---------------------------------------------------------------------------


def lateralDynamicsLJ( self, other, epsilon, rm, Lc , dt ) :
    """ simulates the lateral interaction dynamics between adjcent cells
        uses Lennard Jones potential for binding site interactions

        V_LJ = epsilon [ (rm/r)^12  - 2 (rm/r)^6 ]

        Parameters
            other (Filament) : the adjacent filament
            epsilon (float) : tunes the depth of the LJ potential
            rm (float) : distance at which LJ potential is minimized
            Lc (float) : cutoff distance at which LJ potential drops to zero
            dt (float) : time step -- current time minus the previous time """

    #print("check num bonds", len(self.bonds))

    # iterate over all bonds and break old ones if necessary
    """if ( len(self.bonds) != 0 ) and ( len(other.bonds) != 0 ) :
        for i in range( len(self.bonds)-1, -1, -1 ) :
            for j in range( len(other.bonds)-1, -1, -1 ) :
                if self.bonds[i] == other.bonds[j] :

                    self.bonds[i].calculateForce(ks, width)
                    koff = koff0 * sym.exp( self.bonds[i].force / Fc )

                    # check sites are within cutoff distance
                    Lc_val = np.sqrt( (self.bonds[i].site1.bx - self.bonds[i].site2.bx)**2 + (self.bonds[i].site1.by - self.bonds[i].site2.by)**2 ) > Lc
                    # determine if a break occurs
                    koffCheck = random.random()
                    koff_val = koffCheck <= koff * dt

                    #print("ratecheck", koffCheck, sym.exp( self.bonds[i].force / Fc ) )
                    if Lc_val or koff_val:
                        # remove bond reference from the two sites
                        print("DESTROYING BOND!")
                        self.bonds[i].site1.bond = None
                        self.bonds[i].site2.bond = None
                        # remove the bond objects from the list of bonds
                        del self.bonds[i]
                        del other.bonds[j]
                        break

    # iterate over all binding sites and form new bonds if necessary
    for i in range( len(self.bindingSites) ) :
        for j in range( len(self.bindingSites[i]) ) :

            for m in range( len(other.bindingSites) ) :
                for n in range( len(other.bindingSites[m]) ) :
                    # check if sites are within maximum distance at which a bond can form
                    if self.bindingSites[i][j].calculateDistance( other.bindingSites[m][n] ) <= Lmax :
                        if random.random() <= kon * dt :
                            newBond = Bond( self.bindingSites[i][j], other.bindingSites[m][n] )
                            newBond.calculateForce(ks, width)
                            self.bonds.append( newBond )
                            other.bonds.append( newBond )
                            #print("creating new bond")


    # update qdots based on torques
    for i in range( len(self.bonds) ) :
        for j in range( len(other.bonds) ) :
            if self.bonds[i] == self.bonds[j] :"""


                # update each Euler-Lagrange equation based on the lateral dynamics
                # note: these terms depend only on the generalized coordinates and not
                # their derivatives so this only changes the "value" side of the equation

    for i in range( len(self.bindingSites) ) :
        for j in range( len(other.bindingSites) ) :

                for siteSelf in self.bindingSites[i] :
                    for siteOther in other.bindingSites[j] :


                        """if self.bonds[i].site1.filament == self :
                            siteSelf = self.bonds[i].site1
                            siteOther = self.bonds[i].site2
                        else :
                            siteSelf = self.bonds[i].site2
                            siteOther = self.bonds[i].site1"""
                        #xDiff = siteOther.bx - siteSelf.bx
                        #yDiff = siteOther.by - siteSelf.by
                        xDiff = siteSelf.bx - siteOther.bx
                        yDiff = siteSelf.by - siteOther.by
                        r = np.sqrt( xDiff**2 + yDiff**2 )
                        rDiff = (rm**6 / r**8) - (rm**12 / r**14)

                        # check that binding site are with in cutoff distance
                        if r < Lc :
                            #print("LJ\t r =", r, "\t Lc =", Lc, "\t rDiff =", rDiff )
                            # update the "values" on the self filament

                            index = siteSelf.cellIndex
                            numRods = len(self.q)-2
                            xcom = self.com[:numRods]
                            ycom = self.com[numRods:]
                            theta = self.q[index+2]

                            # x-coodinate adjustment
                            #print("val before", self.vals[index])
                            self.vals[index] += 12 * epsilon * xDiff * rDiff

                            # y-coodinate adjustment
                            self.vals[numRods+index] += 12 * epsilon * yDiff * rDiff

                            # theta-coodinate adjustment
                            self.vals[2*numRods+index] += 12 * epsilon * siteSelf.z * self.lt[index] * rDiff * ( yDiff * sym.cos(theta) - xDiff * sym.sin(theta) )


                            # update the "values" on the other filament

                            index = siteOther.cellIndex
                            numRods = len(other.q)-2
                            xcom = other.com[:numRods]
                            ycom = other.com[numRods:]
                            theta = other.q[index+2]

                            # x-coodinate adjustment
                            other.vals[index] -= 12 * epsilon * xDiff * rDiff

                            # y-coodinate adjustment
                            other.vals[numRods+index] -= 12 * epsilon * yDiff * rDiff

                            # theta-coodinate adjustment
                            other.vals[2*numRods+index] += 12 * epsilon * siteOther.z * other.lt[index] * rDiff * ( xDiff * sym.sin(theta) - yDiff * sym.cos(theta) )


#---------------------------------------------------------------------------

growthDynamics(3)
